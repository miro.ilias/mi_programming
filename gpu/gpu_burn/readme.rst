gpu stress test
===============

in Makefile:
#NVCC=nvcc
NVCC=nvcc --compiler-bindir /usr/bin/gcc-4.8


nvcc --version

mkdir gputest
cd gputest

wget http://wili.cc/blog/entries/gpu-burn/gpu_burn-0.7.tar.gz
tar xvf gpu_burn-0.7.tar.gz
make
./gpu_burn
