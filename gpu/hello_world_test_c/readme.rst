Fix on my x86_64 home Desktop Ubuntu 14.04 LTS
==============================================

Problems
--------

https://stackoverflow.com/questions/17251316/nvcc-unable-to-compile

into file /usr/include/c++/4.8.2/cstdlib

177 #undef _GLIBCXX_USE_INT128 # inserted
178 
179 #if !defined(__STRICT_ANSI__) && defined(_GLIBCXX_USE_INT128)
180   inline __int128
181   abs(__int128 __x) { return __x >= 0 ? __x : -__x; }
182 #endif
183 

ilias@miro_ilias_desktop:~/Dokumenty/Work/programming/miro_ilias_programming/gpu/hello_world_test_c/.nvcc hello.cu 
In file included from /usr/local/cuda/bin/..//include/cuda_runtime.h:59:0,
                 from <command-line>:0:
/usr/local/cuda/bin/..//include/host_config.h:82:2: error: #error -- unsupported GNU version! gcc 4.9 and up are not supported!
 #error -- unsupported GNU version! gcc 4.9 and up are not supported!

Setting
-------
sudo ln -s /usr/bin/gcc-4.8 /usr/local/cuda/bin/gcc # bude v PATH ako prvy


Working
-------

nvcc --compiler-bindir /usr/bin/gcc-4.7 hello.cu 
nvcc --compiler-bindir /usr/bin/gcc-4.8 hello.cu 

ilias@miro_ilias_desktop:~/Dokumenty/Work/programming/programming_collection/gpu/hello_world_test/.nvcc hello.cu 
ilias@miro_ilias_desktop:~/Dokumenty/Work/programming/programming_collection/gpu/hello_world_test/.ls
a.out*	hello.cu  readme.txt
ilias@miro_ilias_desktop:~/Dokumenty/Work/programming/programming_collection/gpu/hello_world_test/.a.out 
Hello, world!



