#include <stdio.h>
#include <stdlib.h>

#define N 16
#define THREADS_PER_BLOCK 4

__global__ void add_gpu( int* a ){
  int index = threadIdx.x + blockIdx.x * blockDim.x;
  a[index] = index;
  printf("Block %d - Thread %d - index %d - a=%d \n", blockIdx.x, threadIdx.x, index, a[index]);
}

int main( void ) {

  int *a, *dev_a, size;

  size = N * sizeof( int );
  cudaMalloc( (void**)&dev_a, size );
  a = (int*)malloc( size );

//   printf("Input -  ");
//   for (int i = 0; i < N; i++) {
//       a[i] = N-i-1;
//       printf("%d ",a[i]);
//   }
//   printf("\n");

  cudaMemcpy( dev_a, a, size, cudaMemcpyHostToDevice );

  add_gpu<<< N/THREADS_PER_BLOCK, THREADS_PER_BLOCK >>>( dev_a );

  cudaMemcpy( a, dev_a, size, cudaMemcpyDeviceToHost );

//   printf("Output - ");
//   for (int i = 0; i < N; i++)
//       printf("%d ",a[i]);
//   printf("\n");

  return 0;
}
