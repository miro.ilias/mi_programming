#include <stdio.h>
#include <stdlib.h>

__global__ void hello( ){
  int index = threadIdx.x+blockDim.x*blockIdx.x;
  printf("Block %d - Thread %d - index %d \n", blockDim.x*blockIdx.x, threadIdx.x, index);
}

int main( void ) {
  hello<<< 2, 4 >>>(  );
  cudaDeviceSynchronize();
  return 0;
}
