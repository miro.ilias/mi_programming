module load cuda
nvcc -lcublas test_cublas.c -o test_cublas_nvcc
gcc -lcublas -lcudart test_cublas.c -o test_cublas_gcc

module load intel
icc -lcublas -lcudart test_cublas.c -o test_cublas_icc
