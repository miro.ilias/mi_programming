#include <stdio.h>         /* Includes, system */
#include <stdlib.h>
#include <cuda_runtime.h>  /* Includes, cuda */
#include <cublas_v2.h>
#define N  (32)            /* Vector size */

/* Host implementation of a simple version of saxpi */
void saxpy(int n, float alpha, const float *x, float *y) {
    int i; 
    for (i = 0; i < n; ++i)
    y[i] = alpha*x[i] + y[i]; }

int main(int argc, char **argv)
{ float *h_X, *h_Y, *h_Y_ref; 
  float *d_X = 0; float *d_Y = 0; const float alpha = 1.0f; int i;

  /* Initialize CUBLAS */
  cublasHandle_t handle; cublasCreate(&handle);

  /* Allocate host memory for the matrices */
  h_X = (float *)malloc(N * sizeof(h_X[0])); 
  h_Y = (float *)malloc(N * sizeof(h_Y[0]));
  h_Y_ref = (float *)malloc(N * sizeof(h_Y_ref[0]));

  /* Fill the matrices with test data */
  for (i = 0; i < N; i++) {
    h_X[i] = rand() / (float)RAND_MAX; 
    h_Y[i] = rand() / (float)RAND_MAX; h_Y_ref[i] = h_Y[i];}

  /* Allocate device memory for the matrices */
  cudaMalloc((void **)&d_X, N * sizeof(d_X[0])); 
  cudaMalloc((void **)&d_Y, N * sizeof(d_Y[0]));

  /* Initialize the device matrices with the host matrices */
  cublasSetVector(N, sizeof(h_X[0]), h_X, 1, d_X, 1); 
  cublasSetVector(N, sizeof(h_Y[0]), h_Y, 1, d_Y, 1);

  /* Performs operation using plain C code */
  saxpy(N, alpha, h_X, h_Y_ref); 

  /* Performs operation using cublas */
  cublasSaxpy(handle, N, &alpha, d_X, 1, d_Y, 1); 

  /* Read the result back */
  cublasGetVector(N, sizeof(h_Y[0]), d_Y, 1, h_Y, 1); 

  /* Check result against reference */
  for (i = 0; i < N; ++i)
    printf("CPU res = %f \t GPU res = %f \t diff = %f \n", h_Y_ref[i],  
           h_Y[i], h_Y_ref[i] - h_Y[i]);

  /* Memory clean up */
  free(h_X); free(h_Y); free(h_Y_ref); cudaFree(d_X); cudaFree(d_Y);

  /* Shutdown */
  cublasDestroy(handle);
}

