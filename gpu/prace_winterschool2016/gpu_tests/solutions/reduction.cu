#include <stdio.h>
#include <stdlib.h>

#define N 8

__global__ void add_gpu( int* a ){
  __shared__ int sdata[N];
  int index = threadIdx.x + blockIdx.x * blockDim.x;
  int tid = threadIdx.x;
  printf("Block %d - Thread %d - index %d - a[%d]=%d \n", blockIdx.x, threadIdx.x, index, tid, a[index]);

  sdata[tid] = a[tid];
  __syncthreads();

  printf("Block %d - Thread %d - index %d - step %d - sdata[%d]=%d \n", blockIdx.x, threadIdx.x, index, blockDim.x, tid, sdata[tid]);
  for (unsigned int s=blockDim.x/2; s>0; s>>=1) {
      if (tid < s) {
         sdata[tid] += sdata[tid + s];
         printf("Block %d - Thread %d - index %d - step %d - sdata[%d]=%d \n", blockIdx.x, threadIdx.x, index, s, tid, sdata[tid]);
      }
      __syncthreads();
  }
  if (tid == 0) a[0] = sdata[0];
}

int main( void ) {

  int *a, *dev_a, size;

  size = N * sizeof( int );
  cudaMalloc( (void**)&dev_a, size );
  a = (int*)malloc( size );

  printf("Input -  ");
  for (int i = 0; i < N; i++) {
      a[i] = N-i;
      printf("%d ",a[i]);
  }
  printf("\n");

  cudaMemcpy( dev_a, a, size, cudaMemcpyHostToDevice );

  add_gpu<<< 1, N >>>( dev_a );

  cudaMemcpy( a, dev_a, size, cudaMemcpyDeviceToHost );

  printf("Output - %d \n",a[0]);

  return 0;
}