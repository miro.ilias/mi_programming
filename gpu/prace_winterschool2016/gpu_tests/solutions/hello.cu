#include <stdio.h>
#include <stdlib.h>

__global__ void hello( ){
  int index = threadIdx.x + blockIdx.x * blockDim.x;
  printf("Block %d - Thread %d - index %d \n", blockIdx.x, threadIdx.x, index);
}

int main( void ) {
  hello<<< 2, 4 >>>(  );
  cudaDeviceSynchronize();
  return 0;
}