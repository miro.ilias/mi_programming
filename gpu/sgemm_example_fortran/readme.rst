http://www.nvidia.com/content/pdf/isc-2011/fatica.pdf

===========================
Calling CUBLAS from FORTRAN
===========================

Two interfaces:

Thunking
========
- Allows interfacing to existing applications without any changes
- During each call, the wrappers allocate GPU memory, copy source data from CPU
memory space to GPU
memory space, call CUBLAS, and finally copy back the results to CPU memory
space and deallocate the
GPGPU memory
- Intended for light testing due to call overhead

To use the host BLAS routine:
gfortran -O3 sgemm_thunking.F90 -L/usr/lib64 -lblas
To use the CUBLAS routine 
(fortran_thunking.c is included in the toolkit /usr/local/cuda/src):
nvcc -O3 -DCUBLAS_GFORTRAN -c /usr/local/cuda/src/fortran_thunking.c
gfortran -O3 -DCUBLAS sgemm_thunking.F90 fortran_thunking.o -L/usr/local/cuda/lib64 -lcudart -lcublas 

Non-Thunking (default)
======================
- Intended for production code
- Substitute device pointers for vector and matrix arguments in all BLAS
functions
- Existing applications need to be modified slightly to allocate and deallocate
data structures in GPGPU
- memory space (using CUBLAS_ALLOC and CUBLAS_FREE) and to copy data between GPU
and CPU memory spaces (using CUBLAS_SET_VECTOR, CUBLAS_GET_VECTOR,
CUBLAS_SET_MATRIX, and CUBLAS_GET_MATRIX)

To use the CUBLAS routine (fortran.c is included in the toolkit
/usr/local/cuda/src):
nvcc -O3 -DCUBLAS_GFORTRAN -c /usr/local/cuda/src/fortran.c
gfortran -O3 sgemm_non-thunking.F90 fortran.o -L/usr/local/cuda/lib64 -lcudart -lcublas 

