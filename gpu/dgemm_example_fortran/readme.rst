===========================
Calling CUBLAS from FORTRAN
===========================

To use the CUBLAS routine 

nvcc -O3 -DCUBLAS_GFORTRAN -c /usr/local/cuda/src/fortran.c
gfortran -O3 dgemm_cpu_gpu.F90 fortran.o -L/usr/local/cuda/lib64 -lcudart -lcublas 

