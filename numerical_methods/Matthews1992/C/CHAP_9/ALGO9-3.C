/*
---------------------------------------------------------------------------
 Algo9-3.c   C program for implementing Algorithm 9.3
 Algorithm translated to  C  by: Dr. Norman Fahrer
 IBM and Macintosh verification by: Daniel Mathews
 
 NUMERICAL METHODS: C Programs, (c) John H. Mathews 1994
 To accompany the text:
 NUMERICAL METHODS for Mathematics, Science and Engineering, 2nd Ed, 1992
 Prentice Hall, Englewood Cliffs, New Jersey, 07632, U.S.A.
 Prentice Hall, Inc.; USA, Canada, Mexico ISBN 0-13-624990-6
 Prentice Hall, International Editions:   ISBN 0-13-625047-5
 This free software is compliments of the author.
 E-mail address:       in%"mathews@fullerton.edu"
 
 Algorithm 9.3 (Taylor's Method of Order 4).
 Section   9.4, Taylor Series Method, Page 448
---------------------------------------------------------------------------
*/
/*
---------------------------------------------------------------------------
 
 Algorithm 9.3 (Taylor's Method of Order 4).

 To approximate the solution of the initial value problem  y' = f(t,y)
 with  y(a) = y_0  over [a,b] by evaluating  y'', y''', and y''''
 and using the Taylor polynomial at each step.

 User has to supply  functions named : f1function, f2function, 
 f3function  and  f4function. See book chapter 9.4 .
 An example is included in this program.

---------------------------------------------------------------------------
*/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#define MAX 500


/*  define prototype for USER-SUPPLIED function f(x)  */

    double f1function(double t, double y);
    double f2function(double t, double y);
    double f31function(double t, double y);
    double f4function(double t, double y);


/*  EXAMPLE for "f1function"   */

    double f1function(double t, double y)

    {
        return ( (t - y) / 2.0 );
    }

/*  EXAMPLE for "f2function"   */

    double f2function(double t, double y)

    {
        return ( (2 - t + y) / 4.0 );
    }

/*  EXAMPLE for "f3function"   */

    double f3function(double t, double y)

    {
        return ( (t - 2 - y) / 8.0 );
    }

/*  EXAMPLE for "f4function"   */

    double f4function(double t, double y)

    {
        return ( (2 - t + y) / 16.0 );
    }


/* -------------------------------------------------------- */

/*  Main program for algorithm 9.3  */

    void main(void)

{
    int K;                     /* Loop counter                   */
    int M;                     /* INPUT: Number of steps         */
    double A, B, Y[MAX];       /* Endpoints and inital value     */
    double H;                  /* Compute the step size          */
    double T[MAX]; 
    double D1, D2, D3, D4;     /* Derivatives                    */
    double t, y;


    printf("----------- Taylor's Method -------------------\n");
    printf("--------- Example 9.8 on page 445 -------------\n");
    printf("-----------------------------------------------\n");
    printf("Please enter endpoints of the interval [A,B]:\n");
    printf("For used Example type  : 0, 3\n");
    scanf("%lf, %lf", &A, &B);
    printf("You entered [%lf, %lf]\n", A, B);
    printf("-----------------------------------------\n");
    printf("Please enter number of steps: (Not more than 500 !)\n");
    scanf("%d",&M);
    if(M > MAX)
    {
    printf(" Not prepared for more than %d steps. Terminating. Sorry\n",MAX);
    exit(0);
    }

    printf("-----------------------------------------\n");
    printf("Please enter initial value Y[0] :\n");
    printf("For used Example type : 1\n");
    scanf("%lf", &Y[0]);
    printf("You entered Y[0] = %lf\n", Y[0]);

    /* Compute the step size */

    H    = (B - A) / M;      

    /* Initialize the variable */

    T[0] = A;


    for(K = 0; K <= M-1; K++)
    {
      t  = T[K];
      y  = Y[K];
      D1 = f1function(t,y);  /* Slope function    f(t,y(t)) */
      D2 = f2function(t,y);  /* Derivative of     f(t,y(t)) */
      D3 = f3function(t,y);  /* 2nd derivative of f(t,y(t)) */
      D4 = f4function(t,y);  /* 3rd derivative of f(t,y(t)) */
      Y[K+1] = y + H * ( D1 + H * ( D2 / 2.0 + H * ( D3 / 6.0 +
               H * D4 / 24.0 ) ) );
      T[K+1] = A + H * (K+1);
    }

    /* Output */

    for ( K = 0; K <= M; K++)
    {
       printf("K = %d, T[K] = %lf, Y[K] = %lf\n", K, T[K], Y[K]);
    } 

}   /* End of main program */

