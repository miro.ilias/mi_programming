/*
---------------------------------------------------------------------------
 Algo2-7.c  C program for implementing Algorithm 2.7
 Algorithm translated to  C  by: Dr. Norman Fahrer
 IBM and Macintosh verification by: Daniel Mathews
 
 NUMERICAL METHODS: C Programs, (c) John H. Mathews 1995
 To accompany the text:
 NUMERICAL METHODS for Mathematics, Science and Engineering, 2nd Ed, 1992
 Prentice Hall, Englewood Cliffs, New Jersey, 07632, U.S.A.
 Prentice Hall, Inc.; USA, Canada, Mexico ISBN 0-13-624990-6
 Prentice Hall, International Editions:   ISBN 0-13-625047-5
 This free software is compliments of the author.
 E-mail address:       in%"mathews@fullerton.edu"
 
 Algorithm 2.7 (Steffensen's Acceleration).
 Section   2.5, Aitken's Process & Steffensen's & Muller's Methods, Page 96
---------------------------------------------------------------------------
*/
/*
---------------------------------------------------------------------------

 Algorithm 2.7 (Steffensen's Acceleration). To quickly find
 a solution of the fixed-point equation  x = g(x)  given an
 initial approximation  p_0, where it is assumed that both
 g(x)  and  g'(x)  are continuous and  |g'(p)| < 1  and that
 ordinary fixed-point iteration converges slowly (linearly) to  p.

---------------------------------------------------------------------------
*/

/* User has to supply a function named :  gfunction

   An example is included in this program     */

#include<stdio.h>
#include<stdlib.h>
#include<math.h>


/*  define prototype for USER-SUPPLIED function g(x)  */

    double gfunction(double x);


/*  EXAMPLE for "gfunction" : from Sec 2.5, p. 99  */

    double gfunction(double x)

    {
         return ( pow( (6 + x),0.5 ) );
    }


/* -------------------------------------------------------- */

/*  Main program for algorithm 2.7  */

    void main()

{
    double Delta   = 1E-6;       /* Tolerance  */
    double Small   = 1E-6;       /* Tolerance  */

    int Max = 99;   /* Maximum number of iterations  */
    int Cond = 0;   /* Condition fo loop termination */
    int K;          /* Counter for loop              */

    double P0;        /* INPUT : Initial approximation */
    double P1;        /* First new iterate  */
    double P2;        /* Second new iterate */
    double P3;
    double D1;        /* Difference         */
    double D2;        /* Difference         */
    double DP;        /* Difference         */
    double RelErr;    /* Relative Error     */


    printf("----------------------------------------------\n");
    printf("Please enter initial approximation !\n");
    scanf("%lf",&P0);
    printf("----------------------------------------------\n");
    printf("The initial approximation is : %lf\n",P0);

    for ( K = 1; K <= Max ; K++) {

        if(Cond) break;

        P1 = gfunction(P0);         /* First new iterate     */
        P2 = gfunction(P1);         /* Second new iterate    */
        D1 = pow( (P1 - P0),2);     /* Form the differences  */
        D2 = P2 - 2 * P1 + P0;

         if( D2 == 0) {             /* Check division by zero */
             Cond = 1;
             DP   = P2 -P1;
             P3 = P2;
         }

         else {
             DP = D1 / D2;
             P3 = P0 - DP;           /* Aitken's improvement   */
         }


        RelErr = 2 * fabs(DP) / ( fabs(P3) + Small );  /* Relative error */

        if( (RelErr < Delta) )         { /* Check for   */

            if( Cond != 1) Cond = 2;     /* convergence */

        }

        P0 = P3;

    }   /* End of for-loop */


    printf("----------------------------------------------\n");
    printf("The number of iterations is: %d\n",K-1);
    printf("The computed fixed point of g(x) is: %lf\n",P0);
    printf("Consecutive iterates are closer than %lf\n",DP);
    printf("----------------------------------------------\n");

if(Cond == 0) printf("The maximum number of iterations was exceeded !\n");

if(Cond == 1) printf("Division by zero was encountered !\n");

if(Cond == 2) printf("The root was found with the desired tolerance !\n");

    printf("----------------------------------------------\n");

}   /* End of main program */

