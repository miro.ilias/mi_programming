/*
---------------------------------------------------------------------------
 Algo2-4.c   C program for implementing Algorithm 2.4
 Algorithm translated to  C  by: Dr. Norman Fahrer
 IBM and Macintosh verification by: Daniel Mathews
 
 NUMERICAL METHODS: C Programs, (c) John H. Mathews 1995
 To accompany the text:
 NUMERICAL METHODS for Mathematics, Science and Engineering, 2nd Ed, 1992
 Prentice Hall, Englewood Cliffs, New Jersey, 07632, U.S.A.
 Prentice Hall, Inc.; USA, Canada, Mexico ISBN 0-13-624990-6
 Prentice Hall, International Editions:   ISBN 0-13-625047-5
 This free software is compliments of the author.
 E-mail address:       in%"mathews@fullerton.edu"
 
 Algorithm 2.4 (Approximate Location of Roots).
 Section   2.3, Initial Approximations & Convergence Criteria, Page 70
---------------------------------------------------------------------------
*/
/*
---------------------------------------------------------------------------

 Algorithm 2.4 (Approximate Location of Roots). To roughly
 estimate locations of the roots of the equation  f(x) = 0
 over the interval [a,b], by using the equally spaced sample
 points  (x_k, f(x_k))  and the following criteria:

 (i)  y_(k-1) * y_k < 0  , or

 (ii) |y_k| < epsilon  and  (y_k - y(k-1))*(y_(k+1) - y_k) ) < 0.

 That is, either  f(x_(k-1))  and f(x_k)  have opposite signs, or
 |f(x_k)|  is small and the slope of the curve  y = f(x)  changes
 sign near ( x_k,f(x_k) ).

---------------------------------------------------------------------------
*/

/* User has to supply a function named : ffunction
   An example is included in this program */

#include<stdio.h>
#include<stdlib.h>
#include<math.h>

/* Define needed for THINK C 5.0 compatibility. 
     actions of int variable of the same name in main() */

#define Max 1000

/*  define prototype for USER-SUPPLIED function f(x)  */

    double ffunction(double x);


/*  EXAMPLE for "ffunction"   */

    double ffunction(double x)

    {
        return ( pow(x,3) - pow(x,2) - x + 1 );
    }

/* -------------------------------------------------------- */

/*  Main program for algorithm 2.4  */

    void main()

{
    double Epsilon = 1E-2;     /* Tolerance of |f(x_k)|          */
    int N;                     /* INPUT : number of subintervals */
    float A,B;                /* INPUT : endpoints of interval  */
    float H;                  /* Subinterval width              */
    int M;                     /* Counter for the number of roots*/
    int K;                     /* Counter for loop               */
/*  int Max = 1000;               Maximal field length           
                                    Replaced by #define above    */  
    static double X[Max],Y[Max];      /* Abscissa and Ordinate          */
    static double R[Max];             /* Approximate root               */

    double S;

    fflush(stdin);
    printf("----------------------------------------------\n");
    printf("Please enter the endpoints of the interval [A,B]\n");
    printf("EXAMPLE: A = -1.2 and B = 1.2, so type: -1.2 1.2\n");
    scanf("%lf %lf",&A,&B);
    printf("The endpoints of the interval are:  %lf %lf\n",A,B);
    printf("----------------------------------------------\n");
    printf("Please enter the number of SUBINTERVALS.\n");
    scanf("%d",&N);

    if( N >= Max) {
      printf("%d is too big ! Adapt variable 'Max' in program !\n",N);
      exit(0);
    }

    printf("You say : %d subintervals.\n",N);

    H = (B - A)/N;              /*  Subinterval width             */

    for (K = 0; K <= N; K++) {
        X[K] = A + H*K;         /*  Compute the abscissas         */
        Y[K] = ffunction(X[K]); /*  Compute the ordinates         */
    }

    M = 0;                        /*  Initialize counter for roots  */
    Y[N + 1]  =  Y[N];            /*  This permits one loop         */

    
    for (K = 1; K <= N; K++) {
        if ( (Y[K-1] * Y[K]) <= 0 ) {      /* Check for a change in */
            M++;                           /* sign or finds a root  */
            R[M] = (X[K-1] + X[K]) / 2;    /* Approximate root found*/
        }

    S = ( Y[K] - Y[K-1] ) * ( Y[K+1] - Y[K] );

        if ( ( fabs(X[K]) < Epsilon ) && ( S < 0 ) ) {  /* Small function */
            M++;                          /* value and slope changes sign */
            R[M] = X[K];                  /* Tentative root found         */
        }

    }   /* End of  for-loop  */


    printf("The approximate location of the roots are:\n");
    printf("----------------------------------------------\n");

    for(K = 1; K <= M; K++) { printf("No.: %d  %lf\n",K, R[K]); }

    printf("----------------------------------------------\n");

}   /* End of main program */

