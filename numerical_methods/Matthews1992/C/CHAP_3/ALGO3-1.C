/*
---------------------------------------------------------------------------
 Algo3-1.c   C program for implementing Algorithm 3.1
 Algorithm translated to  C  by: Dr. Norman Fahrer
 IBM and Macintosh verification by: Daniel Mathews
 
 NUMERICAL METHODS: C Programs, (c) John H. Mathews 1995
 To accompany the text:
 NUMERICAL METHODS for Mathematics, Science and Engineering, 2nd Ed, 1992
 Prentice Hall, Englewood Cliffs, New Jersey, 07632, U.S.A.
 Prentice Hall, Inc.; USA, Canada, Mexico ISBN 0-13-624990-6
 Prentice Hall, International Editions:   ISBN 0-13-625047-5
 This free software is compliments of the author.
 E-mail address:       in%"mathews@fullerton.edu"
 
 Algorithm 3.1 (Back Substitution).
 Section   3.3, Upper-Triangular Linear Systems, Page 145
---------------------------------------------------------------------------
*/
/*
---------------------------------------------------------------------------
 
 Algorithm 3.1 (Back Substitution) to solve the upper-triangular system,

 a_(1,1)*x_1 + a_(1,2)*x_2 +...+ a_(1,N-1)*x_(N-1) + a_(1,N)*x_N = b_1

               a_(2,2)*x_2 +...+ a_(2,N-1)*x_(N-1) + a_(2,N)*x_N = b_2

                                                                 .
                                                                 .
                                                                 .

                            a_(N-1,N-1)*x_(N-1)  + a_(N-1,N)*x_N = b_(N-1)

                                                     a_(N,N)*x_N = b_N

 Proceed with the method only if all the diagonal elements are nonzero.
 First compute  x_N = b_N / A_(N,N)  and then use the rule

                         N
                b_r  -  SUM  a_(r,j) * x_j
                       j=r+1
   x_r     =   --------------------------     for  r = N-1, N-2, ...., 1.
                       a_(r,r)

 Remark : Additional steps have been placed in the algorithm which
          perform the side calculations of finding the determinant of A.

---------------------------------------------------------------------------
*/


/* main for algo 3.1 : for examples see page 146 */


#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX  20

void main(void)
{

int N, R, J;                         /* Number of equations and counter */
double A[MAX][MAX], B[MAX], X[MAX];  /* Matrix and vectors              */
double DET;                          /* Determinant                     */
double SUM;                          /* variable for summation          */      
          


    printf("-------Try example on page 146 No. 1-------------------\n");

    do  /* force proper input */
    {
        printf("Please enter number of equations [Not more than 20]\n");
        scanf("%d", &N);
    } while( N > MAX);

    printf("You say there are %d equations.\n", N);
    printf("-----------------------------------------------------\n");
    printf("From  AX = B enter components of vector B one by one:\n");

    for (R = 1; R <= N; R++)
    {
        printf("Enter %d st/nd/rd component of vector B\n", R);
        scanf("%lf", &B[R-1]);
    }
    printf("-----------------------------------------------------\n");
    printf("From  AX = B enter elements of A row by row:\n");
    printf("You will be asked for the UPPER triangular elements only.\n");
    printf("-----------------------------------------------------\n");

    for (R = 1; R <= N; R++)
    {
        for (J = 1; J <= N; J++)
        {
            if(R > J) A[R-1][J-1] = 0.0;
            else
            {
             printf(" For row %d enter element %d please :\n", R, J);
             scanf("%lf", &A[R-1][J-1]);
            }
        }
    }

    DET  = A[N-1][N-1];                   /* Initialize the variable     */     
    
    X[N-1] = B[N-1]/A[N-1][N-1];         /* Start the back substitution */

    for ( R = N -1; R >= 1; R--)
    {
        DET = DET * A[R-1][R-1];         /* Multiply the diagonal elements */
        SUM = 0;                         /* Solve for X_r in row r         */

        for(J = R + 1; J <= N; J++)
        {
            SUM += A[R-1][J-1] * X[J-1];
        }

        X[R-1] = ( B[R-1] - SUM ) / A[R-1][R-1];  /* End back substitution */
    }

    /* output */
    printf("\n");
    printf("-----------------------------------------------------\n");
    printf("The value of Det(A) is : %lf\n", DET);
    printf("The solution to the upper-triangular system is:\n");
    for ( R = 1; R <= N; R++) printf("X %d = %lf\n", R, X[R-1]);

}

