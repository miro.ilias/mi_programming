      PROGRAM FINITED
C     ----------------------------------------------------------------
C     Alg10'1.for   FORTRAN program for implementing Algorithm 10.1
C     
C     NUMERICAL METHODS: FORTRAN Programs, (c) John H. Mathews 1995
C     To accompany the text:
C     NUMERICAL METHODS for Math., Science & Engineering, 2nd Ed, 1992
C     Prentice Hall, Englewood Cliffs, New Jersey, 07632, U.S.A.
C     Prentice Hall, Inc.; USA, Canada, Mexico ISBN 0-13-624990-6
C     Prentice Hall, International Editions:   ISBN 0-13-625047-5
C     This free software is compliments of the author.
C     E-mail address:       in%"mathews@fullerton.edu"
C     
C     Algorithm 10.1 (Finite-Difference Soln. for the Wave Equation).
C     Section   10.1, Hyperbolic Equations, Page 507
C     ----------------------------------------------------------------
      PARAMETER(Pi = 3.14159265)
      PARAMETER(MaxN = 11,MaxM = 11)
      CHARACTER Ans*1,Ach*1,Mess*80
      REAL V
      DIMENSION V(1:MaxN,1:MaxM)
      REAL U
      DIMENSION U(1:MaxN,1:MaxM)
      INTEGER FunType, Inum, M, Meth, N
      REAL A, B, C, Rnum, Y0
      EXTERNAL F,G,Fi,Gi
      Meth = 1
      FunType = 1
      A = 0
      B = 1
      Y0 = 0
      M = 1
      CALL MESSAGE(Meth)
      CALL INPUT(FunType)
      CALL EPOINTS(A,B,C,N,M,FunType)
      CALL FDIFF(F,G,Fi,Gi,A,B,C,N,M,U)
      CALL RESULT(FunType,U,N,M)
      END

      REAL FUNCTION F(X)
      REAL X
        F = SIN(3.14159265*X) + SIN(2*3.14159265*X)
      RETURN
      END

      REAL FUNCTION G(X)
      REAL X
        G = 0
      RETURN
      END

      SUBROUTINE PRTFUN(FunType)
      INTEGER FunType
      IF (FunType.EQ.1) THEN
        WRITE(9,*)' '
        WRITE(9,*)'      The boundary functions are:'
        WRITE(9,*)' '
        WRITE(9,*)'       u(x,0)  =  f(x) =  SIN(Pi*X) + SIN(2*Pi*X)'
        WRITE(9,*)' '
        WRITE(9,*)'      u (x,0)  =  g(x) =  0'
        WRITE(9,*)'       t'
      ENDIF
      RETURN
      END

      REAL FUNCTION Fi(I,H)
      REAL H
      INTEGER I
      EXTERNAL F
       Fi = F(H*(I-1))
      RETURN
      END

      REAL FUNCTION Gi(I,H)
      REAL H
      INTEGER I
      EXTERNAL G
        Gi = G(H*(I-1))
      RETURN
      END

      SUBROUTINE FDIFF(F,G,Fi,Gi,A,B,C,N,M,U)
      PARAMETER(MaxN = 11,MaxM = 11)
      INTEGER N,M
      REAL A,B,C
      REAL U
      DIMENSION U(1:MaxN,1:MaxM)
      INTEGER I,J
      REAL H,K,R,R2,R22,S1,S2
      EXTERNAL F,G,Fi,Gi
      H = A/(N-1)
      K = B/(M-1)
      R = C*K/H
      R2 = R*R
      R22 = R*R/2
      S1 = 1 - R*R
      S2 = 2 - 2*R*R
      DO J=1,M
        U(1,J) = 0
        U(N,J) = 0
      ENDDO
      DO I=2,(N-1)
        U(I,1) = Fi(I,H)
        U(I,2) = S1*Fi(I,H) + K*Gi(I,H) + R22*(Fi(I+1,H) + Fi(I-1,H))
      ENDDO
      DO J=3,M
        DO I=2,(N-1)
          U(I,J) = S2*U(I,J-1) + R2*(U(I-1,J-1) + U(I+1,J-1)) - U(I,J-2)
        ENDDO
      ENDDO
      RETURN
      END

      SUBROUTINE MESSAGE(Meth)
      INTEGER K,Meth
      WRITE(9,*)'         SOLUTION OF HYPERBOLIC EQUATIONS'
      WRITE(9,*)' '
      Meth = 1
      RETURN
      END

      SUBROUTINE INPUT(FunType)
      INTEGER K,FunType
      CHARACTER Ans*1
      WRITE(9,*)' '
      WRITE(9,*)' '
      WRITE(9,*)'The finite difference method is used'
      WRITE(9,*)'to solve the wave equation'
      WRITE(9,*)' '
      WRITE(9,*)'                          2          '
      WRITE(9,*)'           u  (x,t)   =  c  u  (x,t)'
      WRITE(9,*)'            tt               xx'
      WRITE(9,*)' '
      WRITE(9,*)'with   u(0,t) = 0  and  u(a,t) = 0   for 0 <= t <= B.'
      WRITE(9,*)' '
      WRITE(9,*)'and'
      WRITE(9,*)' '
      WRITE(9,*)'u(x,0) = f(x) and u (x,0) = g(x)  for  0 < x < A.'
      WRITE(9,*)'                   t'
      WRITE(9,*)' '
      WRITE(9,*)' '
      WRITE(9,*)'A numerical approximation is'
      WRITE(9,*)'computed over the rectangle'
      WRITE(9,*)' '
      WRITE(9,*)'                   0 <= x <= A.'
      WRITE(9,*)'                   0 <= t <= B.'
      WRITE(9,*)' '
      WRITE(9,*)'You must supply the endpoints for the intervals.'
      WRITE(9,*)' '
      WRITE(9,*)' '
      !WRITE(9,*)'Press the <ENTER> key. '
      !READ(9,*) Ans
      WRITE(9,*)' '
      FunType = 1
      RETURN
      END

      SUBROUTINE EPOINTS(A, B, C, N, M,FunType)
      INTEGER I,N,M,FunType
      REAL A, B, C, Valu
      CHARACTER Resp*1
      CHARACTER Mess*80
      WRITE(9,*)' '
      WRITE(9,*)'                  '
      CALL PRTFUN(FunType)
      WRITE(9,*)' '
      WRITE(9,*)' '
      Mess = 'For the interval [0,A], ENTER the endpoint   A = '
      WRITE(9,*) Mess
      WRITE(*,*) Mess
      !READ(9,*) A
      READ(*,*) A
      WRITE(9,*)' '
      Mess = 'For the interval [0,B], ENTER the endpoint   B = '
      WRITE(9,*) Mess
      WRITE(*,*) Mess
      READ(*,*) B
      !READ(9,*) B
      WRITE(9,*)' '
      Mess = '                        ENTER the constant   C = '
      WRITE(9,*) Mess
      WRITE(*,*) Mess
      !READ(9,*) C
      READ(*,*) C
      WRITE(9,*)' '
      Mess = '                 ENTER the number of steps   N = '
      WRITE(9,*) Mess
      WRITE(*,*) Mess
      !READ(9,*) N
      READ(*,*) N
      IF (N.LT.2) THEN
        N = 2
      ENDIF
      IF (N.GT.25) THEN
        N = 25
      ENDIF
      WRITE(9,*)' '
      Mess = '                 ENTER the number of steps   M = '
      WRITE(9,*) Mess
      WRITE(*,*) Mess
      !READ(9,*) M
      READ(*,*) M
      IF (M.LT.2) THEN
        M = 2
      ENDIF
      IF (M.GT.100) THEN
        M = 25
      ENDIF
      RETURN
      END

      SUBROUTINE RESULT(FunType,U,N,M)
      PARAMETER(MaxN = 11,MaxM = 11)
      REAL U
      DIMENSION U(1:MaxN,1:MaxM)
      INTEGER FunType,I,J,N,M
      WRITE(9,*)' '
      WRITE(9,*)' '
      WRITE(9,*)'       '
      CALL PRTFUN(FunType)
      WRITE(9,*)' '
      WRITE(9,*)' '
      WRITE(9,*)'           u(x ,t )   .....    u(x   ,t )'
      WRITE(9,*)'              2  j                N-1  j'
      WRITE(9,*)' -------------------------------------------------'
      WRITE(9,*)' '
      DO J=1,M
        WRITE(9,999) (U(I,J), I=2,(N-1))
      ENDDO
      WRITE(9,*)' '
      WRITE(9,*)'                   Press the <ENTER> key.  '
      !READ(9,*) Ans
      WRITE(9,*)' '
!999   FORMAT(1X,9F8.4)
999   FORMAT(1X,9E9.3)
      RETURN
      END
