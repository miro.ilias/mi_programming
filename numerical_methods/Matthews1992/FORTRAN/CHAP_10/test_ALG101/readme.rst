ALG101 test 
===========

module load intel/v2021.1

milias@hydra.jinr.ru:~/work/projects/mi_programming/numerical_methods/Matthews1992/FORTRAN/CHAP_10/test_ALG101/.module list
Currently Loaded Modulefiles:
  1) GVR/v1.0-1      2) intel/v2021.1

as FORTRAN77 file

or use pure gfortran

gfortran alg101.f

reads A,B,C, M, N
1
1
1
5
5

produces fort.9 file



