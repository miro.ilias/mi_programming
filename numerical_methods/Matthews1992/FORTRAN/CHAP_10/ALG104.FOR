      PROGRAM LAPLACE
C     ----------------------------------------------------------------
C     Alg10'4.for   FORTRAN program for implementing Algorithm 10.4
C     
C     NUMERICAL METHODS: FORTRAN Programs, (c) John H. Mathews 1995
C     To accompany the text:
C     NUMERICAL METHODS for Math., Science & Engineering, 2nd Ed, 1992
C     Prentice Hall, Englewood Cliffs, New Jersey, 07632, U.S.A.
C     Prentice Hall, Inc.; USA, Canada, Mexico ISBN 0-13-624990-6
C     Prentice Hall, International Editions:   ISBN 0-13-625047-5
C     This free software is compliments of the author.
C     E-mail address:       in%"mathews@fullerton.edu"
C     
C     Algorithm 10.4 (Dirichlet Method for Laplace's Equation).
C     Section, 10.3 Elliptic Equations, Page 531
C     ----------------------------------------------------------------
      PARAMETER(Pi = 3.1415926535)
      PARAMETER(MaxN = 11,MaxM = 11)
      CHARACTER Ans*1,Ach*1,Mess*80
      REAL U
      DIMENSION U(1:MaxN,1:MaxM)
      INTEGER FunType, M, Meth, N
      REAL A, B, C, C1, C2, H, Rnum, Y0
      EXTERNAL F1,F2,F3,F4,F1i,F2i,F3i,F4i
      Meth = 1
      FunType = 1
      A = 0
      B = 1
      Y0 = 0
      M = 1
      CALL MESSAGE(Meth)
      CALL INPUT(FunType, Meth)
      CALL EPOINTS(A,B,H,N,M)
      CALL Dirich(F1,F2,F3,F4,A,B,H,N,M,U)
      CALL RESULTS(FunType,U,N,M)
      END

      REAL FUNCTION F1(X)
      REAL X
        F1 = 20
      RETURN
      END

      REAL FUNCTION F2(X)
      REAL X
        F2 = 180
      RETURN
      END

      REAL FUNCTION F3(X)
      REAL X
        F3 = 80
      RETURN
      END

      REAL FUNCTION F4(X)
      REAL X
        F4 = 0
      RETURN
      END

      SUBROUTINE PRTFUN(FunType)
      INTEGER FunType
      WRITE(9,*)' '
      WRITE(9,*)'      The boundary functions are:'
      WRITE(9,*)' '
      WRITE(9,*)'      u(x,0) = f1(x) = 20'
      WRITE(9,*)' '
      WRITE(9,*)'      u(x,b) = f2(x) = 180'
      WRITE(9,*)' '
      WRITE(9,*)'      u(0,y) = f3(x) = 80'
      WRITE(9,*)' '
      WRITE(9,*)'      u(a,y) = f4(x) = 0'
      RETURN
      END

      REAL FUNCTION F1i(I,H)
      INTEGER I
      REAL H
        F1i = F1(H*(I-1))
      RETURN
      END

      REAL FUNCTION F2i(I,H)
      INTEGER I
      REAL H
        F2i = F2(H*(I-1))
      RETURN
      END

      REAL FUNCTION F3i(I,H)
      INTEGER I
      REAL H
        F3i = F3(H*(I-1))
      RETURN
      END

      REAL FUNCTION F4i(I,H)
      INTEGER I
      REAL H
        F4i = F4(H*(I-1))
      RETURN
      END

      SUBROUTINE Dirich(F1,F2,F3,F4,A,B,H,N,M,U)
      PARAMETER(Pi = 3.1415926535)
      PARAMETER(MaxN = 11,MaxM = 11)
      REAL U
      DIMENSION U(1:MaxN,1:MaxM)
      INTEGER Count,I,J,N,M
      REAL A,B,H,Ave,K,R,S,W,Relax,Tol
      EXTERNAL F1,F2,F3,F4,F1i,F2i,F3i,F4i
      Ave = (A*(f1(0)+f2(0)) + B*(f3(0)+f4(0)))/(2*A+2*B)
      DO I=2,(N-1)
        DO J=2,(M-1)
          U(I,J) = Ave
        ENDDO
      ENDDO
      DO J=1,M
        U(1,J) = F3i(J)
        U(N,J) = F4i(J)
      ENDDO
      DO I=1,N
        U(I,1) = F1i(I)
        U(I,M) = F2i(I)
      ENDDO
      U(1,1) = (U(1,2) + U(2,1))/2
      U(1,M) = (U(1,M-1) + U(2,M))/2
      U(N,1) = (U(N-1,1) + U(N,2))/2
      U(N,M) = (U(N-1,M) + U(N,M-1))/2
      W = 4/(2+SQRT(4-(COS(Pi/(N-1))+COS(Pi/(M-1)))**2))
      Tol = 1
      Count = 0
      WHILE ((Tol.GT.0.001).AND.(Count.LE.25))
        Tol = 0
        DO J=2,(M-1)
          DO I=2,(N-1)
            Relax = W*(U(I,J+1) + U(I,J-1) + U(I+1,J) + 
     &              U(I-1,J) - 4.0*U(I,J))/4.0
            U(I,J) = U(I,J) + Relax
            IF (Tol.LE.ABS(Relax)) THEN
              Tol = ABS(Relax)
            ENDIF
          ENDDO
        ENDDO
        Count = Count+1
      REPEAT
      RETURN
      END

      SUBROUTINE MESSAGE(Meth)
      INTEGER K,Meth
      WRITE(9,*)'        SOLUTION OF ELLIPTIC EQUATIONS'
      WRITE(9,*)' '
      Meth = 1
      RETURN
      END

      SUBROUTINE INPUT(FunType,Meth)
      INTEGER K,FunType,Meth
      CHARACTER Ans*1
      WRITE(9,*)' '
      WRITE(9,*)' '
      WRITE(9,*)'Solution of Laplace`s equation'
      WRITE(9,*)' '
      WRITE(9,*)'           u  (x,y)  +   u  (x,y)  =  0'
      WRITE(9,*)'            xx            yy'
      WRITE(9,*)' '
      WRITE(9,*)'with the boundary values:'
      WRITE(9,*)' '
      WRITE(9,*)'u(x,0)=f1(x), u(x,b)=f2(x) for 0 <= x <= A,'
      WRITE(9,*)' '
      WRITE(9,*)'u(0,y)=f3(x), u(a,y)=f4(x) for 0 <= x <= B,'
      WRITE(9,*)' '
      WRITE(9,*)'A numerical approximation is'
      WRITE(9,*)'computed over the rectangle'
      WRITE(9,*)' '
      WRITE(9,*)'                   0 <= x <= A.'
      WRITE(9,*)'                   0 <= t <= B.'
      WRITE(9,*)' '
      WRITE(9,*)'You supply the endpoints for the intervals.'
      WRITE(9,*)' '
      WRITE(9,*)' '
      WRITE(9,*)'Press the <ENTER> key. '
      READ(9,*) Ans
      WRITE(9,*)' '
      FunType = 1
      Meth = 1
      RETURN
      END
      
      SUBROUTINE EPOINTS(A,B,H,N,M)
      INTEGER I,N,M
      REAL A, B, H,Valu
      CHARACTER Resp*1,Mess*80
      WRITE(9,*)' '
      CALL PRTFUN(FunType)
      WRITE(9,*)' '
      Mess = 'For the interval[0,A], ENTER  the endpoint A ='
      WRITE(9,*) Mess
      READ(9,*) A
      WRITE(9,*)' '
      Mess = 'For the interval[0,B], ENTER  the endpoint B ='
      WRITE(9,*) Mess
      READ(9,*) B
      WRITE(9,*)' '
      Mess = '                       ENTER the step size H ='
      WRITE(9,*) Mess
      READ(9,*) H
      H = ABS(H)
      N = ANINT(A/H)+1
      M = ANINT(B/H)+1
      RETURN
      END

      SUBROUTINE RESULTS(FunType,U,N,M)
      PARAMETER(MaxN = 11,MaxM = 11)
      REAL U
      DIMENSION U(1:MaxN,1:MaxM)
      INTEGER FunType,I,J,N,M
      WRITE(9,*)' '
      WRITE(9,*)' '
      WRITE(9,*)'       '
      CALL PRTFUN(FunType)
      WRITE(9,*)' '
      WRITE(9,*)' '
      WRITE(9,*)'           u(x ,y )   .....    u(x ,y )'
      WRITE(9,*)'              1  j                N  j'
      WRITE(9,*)' ---------------------------------------------'
      WRITE(9,*)' '
      DO J=M,1,-1
        WRITE(9,999) (U(I,J), I=1,N)
      ENDDO
      WRITE(9,*)' '
      WRITE(9,*)'           Press the <ENTER> key.  '
      READ(9,*) Ans
      WRITE(9,*)' '
999   FORMAT(1X,9F8.3)
      RETURN
      END
