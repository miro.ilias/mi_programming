      PROGRAM TSERIES
C     ----------------------------------------------------------------
C     Alg4'1.for   FORTRAN program for implementing Algorithm 4.1
C     
C     NUMERICAL METHODS: FORTRAN Programs, (c) John H. Mathews 1995
C     To accompany the text:
C     NUMERICAL METHODS for Math., Science & Engineering, 2nd Ed, 1992
C     Prentice Hall, Englewood Cliffs, New Jersey, 07632, U.S.A.
C     Prentice Hall, Inc.; USA, Canada, Mexico ISBN 0-13-624990-6
C     Prentice Hall, International Editions:   ISBN 0-13-625047-5
C     This free software is compliments of the author.
C     E-mail address:       in%"mathews@fullerton.edu"
C     
C     Algorithm 4.1 (Evaluation of a Taylor Series).
C     Section   4.1, Taylor Series and Calculation of Fun., Page 203
C     ----------------------------------------------------------------
      PARAMETER (MAX=501,Tol=0.00000001)
      INTEGER K,METH,N
      REAL Close,Sum,X,X0
      REAL D(0:MAX)
      CHARACTER ANS*1
      CALL INX0XN(X0,N)
      CALL GETDERIV(N,METH,D)
10    CALL INPUTX(X)
      CALL TAYLOR(X0,D,X,Tol,Sum,Close,K,N)
      CALL RESULTSS(X0,X,Tol,Sum,Close,K,N)
      WRITE(9,*)' '
      WRITE(9,*)'WANT TO TRY ANOTHER X <Y/N> ? '
      READ(9,'(A)') ANS
      IF(ANS .EQ. 'Y' .OR. ANS .EQ. 'y') GOTO 10
      STOP
      END

      SUBROUTINE TAYLOR(X0,D,X,Tol,Sum,Close,K,N)
      PARAMETER (MAX=501)
      INTEGER K,N
      REAL Close,Prod,Sum,Term,Tol,X,X0
      REAL D(0:MAX)
      Close=1
      K=0
      Sum=D(0)
      Prod=1
      IF (X.EQ.X0) Close=0
      WHILE ((Close.GE.Tol).AND.(K.LT.N))
        K=K+1
        Prod=Prod*(X-X0)/K
        WHILE ((D(K).EQ.0).AND.(K.LT.N))
          K=K+1
          Prod=Prod*(X-X0)/K
        REPEAT
        Term=D(K)*Prod
        IF (Term.NE.0) Close=ABS(Term)
        Sum=Sum+Term
      REPEAT
      RETURN
      END

      SUBROUTINE XAYLOR(X0,D,X,Tol,Sum,Close,K,N)
      PARAMETER (MAX=501)
      INTEGER K,N
      REAL Close,Prod,Sum,Term,Tol,X,X0
      REAL D(0:MAX)
      Close=1
      K=0
      Sum=D(0)
      Prod=1
      IF (X.EQ.X0) Close=0
10    IF ((Close.GE.Tol).AND.(K.LT.N)) THEN
        K=K+1
        Prod=Prod*(X-X0)/K
20      IF ((D(K).EQ.0).AND.(K.LT.N)) THEN
          K=K+1
          Prod=Prod*(X-X0)/K
          GOTO 20
        ENDIF
        Term=D(K)*Prod
        IF (Term.NE.0) Close=ABS(Term)
        Sum=Sum+Term
        GOTO 10
      ENDIF
      RETURN
      END

      SUBROUTINE GETDERIV(N,METH,D)
      PARAMETER (MAX=501)
      INTEGER I,K,METH,N
      REAL D(0:MAX)
      DO 5 I=1,14
        WRITE(9,*)' '
5     CONTINUE
      WRITE(9,*)'CHOOSE ONE OF THE FOLLOWING:'
      WRITE(9,*)' '
      WRITE(9,*)'<1> F(X) = EXP(X) EXPANDED ABOUT X0 = 0'
      WRITE(9,*)' '
      WRITE(9,*)'<2> F(X) = COS(X) EXPANDED ABOUT X0 = 0'
      WRITE(9,*)' '
      WRITE(9,*)'<3> F(X) = SIN(X) EXPANDED ABOUT X0 = 0'
      WRITE(9,*)' '
      WRITE(9,*)'<4> ENTER THE DERIVATIVES OF '
      WRITE(9,*)' '
      WRITE(9,*)'    F(X)  EXPANDED ABOUT  X0 = ',X0
      WRITE(9,*)' '
      WRITE(9,*)'SELECT <1-4> ?  '
      READ(9,*) METH
      WRITE(9,*)' '
      GOTO (10,20,30,40), METH
10    CONTINUE
C     { EXP(X) }
      X0=0
        DO 15 K=0,N
          D(K)=1
15      CONTINUE
      GOTO 50
20    CONTINUE
C     { COS(X) }
      X0=0
        DO 25 K=0,INT(N/4)
          D(4*K)=1
          D(4*K+1)=0
          D(4*K+2)=-1
          D(4*K+3)=0
25      CONTINUE
      GOTO 50
30    CONTINUE
C     { SIN(X) }
      X0=0
        DO 35 K=0,INT(N/4)
          D(4*K)=0
          D(4*K+1)=1
          D(4*K+2)=0
          D(4*K+3)=-1
35      CONTINUE
      GOTO 50
40    CONTINUE
        WRITE(9,*)'ENTER THE ',N+1,' DERIVATIVES OF F(X)'
        WRITE(9,*)'EVALUATED AT THE CENTER VALUE  X0 = ',X0
        WRITE(9,*)' '
        WRITE(9,*)'D(0) , D(1) , D(2) ,..., D(N)'
        WRITE(9,*)' '
        DO 45 K=0,N
          WRITE(9,*)'D(',K,') = '
          READ(9,*) D(K)
45      CONTINUE
50    CONTINUE
      RETURN
      END

      SUBROUTINE INX0XN(X0,N)
      PARAMETER (MAX=501)
      INTEGER I,N
      REAL X0
      DO 10 I=1,14
        WRITE(9,*)' '
10    CONTINUE
      WRITE(9,*)'COMPUTER EVALUATION OF THE TAYLOR`S SERIES'
      WRITE(9,*)' '
      WRITE(9,*)'FOR THE FUNCTION  F(X)  CENTERED AT  X  '
      WRITE(9,*)'                                      0 '
      WRITE(9,*)' '
      WRITE(9,*)'THE  N-THE PARTIAL SUM IS COMPUTED: '
      WRITE(9,*)' '
      WRITE(9,*)'                           2             K
     +N '
      WRITE(9,*)'P(X) = A +A (X-X )+A (X-X ) +...+A (X-X ) +...+A (X-X )
     +  '
      WRITE(9,*)'        0  1    0   2    0        K    0        N    0
     +  '
      WRITE(9,*)' '
      WRITE(9,*)'                            (K)                 '
      WRITE(9,*)'THE K-COEFFICIENT IS  A  = F  (X )/K! = D(K)/K! '
      WRITE(9,*)'                       K        0               '
      WRITE(9,*)' '
      WRITE(9,*)'ENTER THE CENTER  X  OF THE SERIES'
      WRITE(9,*)'                   0              '
      WRITE(9,*)' '
      WRITE(9,*)'ENTER THE CENTER X0 = '
      READ(9,*) X0
      WRITE(9,*)' '
      WRITE(9,*)'ENTER THE DEGREE  N = '
      READ(9,*) N
      IF (N.GT.MAX) THEN
        N=MAX
      ENDIF
      WRITE(9,*)' '
      RETURN
      END

      SUBROUTINE INPUTX(X)
      REAL X
      WRITE(9,*)' '
      WRITE(9,*)'ENTER THE VALUE OF THE INDEPENDENT VARIABLE'
      WRITE(9,*)' '
      WRITE(9,*)'X = '
      READ(9,*) X
      RETURN
      END

      SUBROUTINE RESULTSS(X0,X,Tol,Sum,Close,K,N)
      INTEGER I,K,N
      REAL Close,Sum,Tol,X,X0
      DO 5 I=1,12
        WRITE(9,*)' '
5     CONTINUE
      WRITE(9,*)'YOU CHOSE TO APPROXIMATE '
      GOTO (10,20,30,40), METH
10      WRITE(9,*)' EXP(',X,'  ).'
      GOTO 50
20      WRITE(9,*)' COS(',X,'  ).'
      GOTO 50
30      WRITE(9,*)' SIN(',X,'  ).'
      GOTO 50
40      WRITE(9,*)'YOUR OWN TAYLOR SERIES.'
        WRITE(9,*)'EXPANDED ABOUT THE VALUE X0 = ',X0
50    CONTINUE
      WRITE(9,*)' '
      IF ((Close.LE.Tol).AND.(K.LE.N)) THEN
        WRITE(9,*)'THE VALUE OF THE TAYLOR POLYNOMIAL IS:'
        WRITE(9,*)' '
        WRITE(9,*)'       P   (',X,'  )  =',Sum
        WRITE(9,*)'       ', K
        WRITE(9,*)' '
        WRITE(9,*)'CONSECUTIVE PARTIAL SUMS ARE CLOSER THAN',Close
        WRITE(9,*)' '
      ELSE
        WRITE(9,*)'THE CURRENT PARTIAL SUM IS:'
        WRITE(9,*)' '
        WRITE(9,*)'       P   (',X,'  )  =',Sum
        WRITE(9,*)'       ',K
        WRITE(9,*)' '
        WRITE(9,*)'CONSECUTIVE PARTIAL SUMS DIFFER BY ',Close
        WRITE(9,*)' '
        WRITE(9,*)'CONVERGENCE HAS NOT BEEN ACHIEVED.'
        WRITE(9,*)' '
      ENDIF
      RETURN
      END
