      PROGRAM CHEBYS
C     ----------------------------------------------------------------
C     Alg4'6.for   FORTRAN program for implementing Algorithm 4.6
C     
C     NUMERICAL METHODS: FORTRAN Programs, (c) John H. Mathews 1995
C     To accompany the text:
C     NUMERICAL METHODS for Math., Science & Engineering, 2nd Ed, 1992
C     Prentice Hall, Englewood Cliffs, New Jersey, 07632, U.S.A.
C     Prentice Hall, Inc.; USA, Canada, Mexico ISBN 0-13-624990-6
C     Prentice Hall, International Editions:   ISBN 0-13-625047-5
C     This free software is compliments of the author.
C     E-mail address:       in%"mathews@fullerton.edu"
C     
C     Algorithm 4.6 (Chebyshev Approximation).
C     Section   4.5, Chebyshev Polynomials, Page 246
C     ----------------------------------------------------------------
      PARAMETER(MaxN=15)
      INTEGER J,K,N
      REAL C,D,Sum,X,Y,Z
      CHARACTER ANS*1
      DIMENSION T(0:MaxN,0:MaxN)
      DIMENSION C(0:MaxN),P(0:MaxN),X(0:MaxN),Y(0:MaxN)
      EXTERNAL CH,F
10    CONTINUE
      CALL MESSAGE(N)
      CALL CHEBY(C,X,Y,N)
      CALL CREATEP(T,N)
      CALL MAKECHE(T,C,P,N)
      CALL PRTFUN
      CALL PRINTCHE(C,N)
      CALL PRINTPOL(P,N)
      WRITE(9,*)' '
      WRITE(9,*)'     Press the <ENTER> key. '
      READ(9,'(A)') ANS
20    CONTINUE
      CALL RESULT(X,C,N)
      WRITE(9,*)' '
      WRITE(9,*)'WANT TO EVALUATE P(T) AGAIN ? '
      READ(9,'(A)') ANS
      IF(ANS .EQ. 'Y' .OR. ANS .EQ. 'y') GOTO 20
      WRITE(9,*)' '
      WRITE(9,*)'WANT TO EVALUATE ANOTHER POLYNOMIAL ? '
      READ(9,'(A)') ANS
      IF(ANS .EQ. 'Y' .OR. ANS .EQ. 'y') GOTO 10
      STOP
      END

      REAL FUNCTION F(X)
      F=EXP(X)
      RETURN
      END

      SUBROUTINE PRINTFUN
      WRITE(9,*) 'EXP(X)'
      RETURN
      END

      SUBROUTINE MAKECHE(T,C,P,N)
      PARAMETER(MaxN=15)
      INTEGER J,K,N
      REAL C,D,P,Sum,T
      DIMENSION T(0:MaxN,0:MaxN)
      DIMENSION C(0:MaxN),P(0:MaxN)
      DO J=0,N
        Sum=0.0
        DO K=0,N
          Sum=Sum+C(K)*T(K,J)
        ENDDO
        P(J)=Sum
      ENDDO
      RETURN
      END

      SUBROUTINE CHEBY(C,X,Y,N)
      PARAMETER(MaxN=15)
      INTEGER J,K,N
      REAL C,D,Sum,X,Y,Z
      DIMENSION C(0:MaxN),X(0:MaxN),Y(0:MaxN)
      EXTERNAL F
      D=3.1415926535/(2.0*N+2.0)
      DO K=0,N
        X(K)=COS((2.0*K+1.0)*D)
        Y(K)=F(X(K))
        C(K)=0.0
      ENDDO
      DO K=0,N
        Z=(2.0*K+1.0)*D
        DO J=0,N
          C(J)=C(J)+Y(K)*COS(J*Z)
        ENDDO
      ENDDO
      C(0)=C(0)/(N+1.0)
      DO J=1,N
        C(J)=2.0*C(J)/(N+1.0)
      ENDDO
      RETURN
      END

      FUNCTION CH(X,C,N)
      PARAMETER(MaxN=15)
      INTEGER J,N
      REAL C,Sum,T,X
      DIMENSION C(0:MaxN),T(0:MaxN)
        T(0)=1.0
        T(1)=X
        IF (N.GT.1) THEN
          DO J=1,N-1
            T(J+1)=2.0*X*T(J)-T(J-1)
          ENDDO
        ENDIF
        Sum=0.0
        DO J=0,N
          Sum=Sum+C(J)*T(J)
        ENDDO
        CH=Sum
        RETURN
      END

      SUBROUTINE CREATEP(T,N)
      PARAMETER(MaxN=15)
      INTEGER J,K,N
      REAL T
      DIMENSION T(0:MaxN,0:MaxN)
      DO K=0,N
        DO J=0,N
          T(K,J)=0.0
        ENDDO
      ENDDO
      T(0,0)=1.0
      T(1,0)=0.0
      T(1,1)=1.0
      DO K=2,N
        DO J=1,K
          T(K,J)=2.0*T(K-1,J-1)-T(K-2,J)
        ENDDO
        T(K,0)=-T(K-2,0)
      ENDDO
      RETURN
      END

      SUBROUTINE XMAKECHE(T,C,P,N)
      PARAMETER(MaxN=15)
      INTEGER J,K,N
      REAL C,D,P,Sum,T
      DIMENSION T(0:MaxN,0:MaxN)
      DIMENSION C(0:MaxN),P(0:MaxN)
      DO 20 J=0,N
        Sum=0.0
        DO 10 K=0,N
          Sum=Sum+C(K)*T(K,J)
10      CONTINUE
        P(J)=Sum
20    CONTINUE
      RETURN
      END

      SUBROUTINE XCHEBY(C,X,Y,N)
      PARAMETER(MaxN=15)
      INTEGER J,K,N
      REAL C,D,Sum,X,Y,Z
      DIMENSION C(0:MaxN),X(0:MaxN),Y(0:MaxN)
      EXTERNAL F
      D=3.1415926535/(2.0*N+2.0)
      DO 10 K=0,N
        X(K)=COS((2.0*K+1.0)*D)
        Y(K)=F(X(K))
        C(K)=0.0
10    CONTINUE
      DO 30 K=0,N
      Z=(2.0*K+1.0)*D
      DO 20 J=0,N
        C(J)=C(J)+Y(K)*COS(J*Z)
20    CONTINUE
30    CONTINUE
      C(0)=C(0)/(N+1.0)
      DO 40 J=1,N
        C(J)=2.0*C(J)/(N+1.0)
40    CONTINUE
      RETURN
      END

      FUNCTION XCH(X,C,N)
      PARAMETER(MaxN=15)
      INTEGER J,N
      REAL C,Sum,T,X
      DIMENSION C(0:MaxN),T(0:MaxN)
      T(0)=1.0
      T(1)=X
      IF (N.GT.1) THEN
        DO 10 J=1,N-1
          T(J+1)=2.0*X*T(J)-T(J-1)
10      CONTINUE
      ENDIF
      Sum=0.0
      DO 20 J=0,N
        Sum=Sum+C(J)*T(J)
20    CONTINUE
      CH=Sum
      RETURN
      END

      SUBROUTINE XCREATEP(T,N)
      PARAMETER(MaxN=15)
      INTEGER J,K,N
      REAL T
      DIMENSION T(0:MaxN,0:MaxN)
      DO 20 K=0,N
        DO 10 J=0,N
          T(K,J)=0.0
10      CONTINUE
20    CONTINUE
      T(0,0)=1.0
      T(1,0)=0.0
      T(1,1)=1.0
      DO 40 K=2,N
        DO 30 J=1,K
          T(K,J)=2.0*T(K-1,J-1)-T(K-2,J)
30      CONTINUE
        T(K,0)=-T(K-2,0)
40    CONTINUE
      RETURN
      END

      SUBROUTINE MESSAGE(N)
      PARAMETER(MaxN=15)
      INTEGER N
      WRITE(9,*)' '
      WRITE(9,*)'         Construction of the Chebyshev polynomial for
     +the function F(X).'
      WRITE(9,*)' '
      WRITE(9,*)'     P(X) = C T (x) + C T (x) + C T (x) +...+ C T (x)'
      WRITE(9,*)'             0 0       1 1       2 2           N N   '
      WRITE(9,*)' '
      WRITE(9,*)'     over the interval [-1,1]. The coefficients {C } '
      WRITE(9,*)'                                                  j  '
      WRITE(9,*)'     are computed using the formulas:'
      WRITE(9,*)' '
      WRITE(9,*)'              N                   N'
      WRITE(9,*)'           1                   2                  2k+1
     +'
      WRITE(9,*)'     C  = ---Sum f(x ) , C  = ---Sum f(x )COS(J*Pi----)
     + for j=1,2,...,N'
      WRITE(9,*)'      0   N+1       k     j   N+1       k         2N+1'
      WRITE(9,*)'             k=0                 k=0                  '
      WRITE(9,*)' '
      WRITE(9,*)'     where x  = COS(Pi*(2k+1)/(2N+2))'
      WRITE(9,*)'            k'
      WRITE(9,*)' '
      WRITE(9,*)' '
      WRITE(9,*)' '
      WRITE(9,*)'     ENTER the maximum number of terms   N = '
      N=MaxN
      READ(9,*) N
      IF (N.LT.1) THEN
        N=1
      ENDIF
      IF (N.GT.MaxN) THEN
        N=MaxN
      ENDIF
      WRITE(9,*)' '
      RETURN
      END

      SUBROUTINE  PRINTPOL(A,N)
      PARAMETER(MaxN=15)
      INTEGER I,N
      REAL A
      DIMENSION A(0:MaxN)
      WRITE(9,*)' '
      WRITE(9,*)'The Chebyshev polynomial of degree  N = ',N,' is:'
      IF (N.EQ.1) THEN
        WRITE(9,*)' '
        WRITE(9,*)'P(X)  =  A X  +  A'
        WRITE(9,*)'          1       0'
        WRITE(9,*)' '
      ELSEIF (N.EQ.2) THEN
        WRITE(9,*)'            2'
        WRITE(9,*)'P(X)  =  A X   +  A X  +  A'
        WRITE(9,*)'          2        1       0'
        WRITE(9,*)' '
      ELSEIF (N.EQ.3) THEN
        WRITE(9,*)'            3        2'
        WRITE(9,*)'P(X)  =  A X   +  A X   +  A X  +  A '
        WRITE(9,*)'          3        2        1       0'
        WRITE(9,*)' '
      ELSE
        WRITE(9,*)'  ',N,'            2'
        WRITE(9,*)'P(X)  =  A X   +...+  A X  +   A X  +  A '
        WRITE(9,*) N,'            2        1       0'
        WRITE(9,*)' '
      ENDIF
      DO 10 K=N,0,-1
        WRITE(9,*)'A(',K,')  =',A(K)
10    CONTINUE
      RETURN
      END

      SUBROUTINE  PRINTCHE(C,N)
      PARAMETER(MaxN=15)
      INTEGER I,K,N
      REAL C
      DIMENSION C(0:MaxN)
      WRITE(9,*)' '
      WRITE(9,*)'The Chebyshev series with N = ',N,' terms is'
      IF (N.EQ.1) THEN
        WRITE(9,*)' '
        WRITE(9,*)'T(X) = C T (X) + C T (X)'
        WRITE(9,*)'        0 0       1 1'
        WRITE(9,*)' '
      ELSEIF (N.EQ.2) THEN
        WRITE(9,*)' '
        WRITE(9,*)'T(X) = C T (X) + C T (X) + C T (X)'
        WRITE(9,*)'        0 0       1 1       2 2'
        WRITE(9,*)' '
      ELSEIF (N.EQ.3) THEN
        WRITE(9,*)' '
        WRITE(9,*)'T(X) = C T (X) + C T (X) + C T (X) + C T (X)'
        WRITE(9,*)'        0 0       1 1       2 2       3 3'
        WRITE(9,*)' '
      ELSE
        WRITE(9,*)' '
        WRITE(9,*)'T(X) = C T (X) + C T (X) + ... + C T (X)'
        WRITE(9,*)'        0 0       1 1             N N'
        WRITE(9,*)' '
      ENDIF
      DO 10 K=0,N
        WRITE(9,*)'C(',K,')  =',C(K)
10    CONTINUE
      RETURN
      END

      SUBROUTINE PRTFUN
      WRITE(9,*)
      WRITE(9,*)
      WRITE(9,*)'You chose to construct the Chebyshev polynomial for '
      WRITE(9,*)
      WRITE(9,*)'    F(X) = EXP(X)   over the interval [-1,1].'
      WRITE(9,*)
      RETURN
      END

      SUBROUTINE RESULT(X,C,N)
      INTEGER N
      REAL C,X,Z
      DIMENSION C(0:MaxN),X(0:MaxN)
      WRITE(9,*)
      WRITE(9,*)
      WRITE(9,*)'The results for the approximation of F(x)
     + over [-1,1] are:'
      WRITE(9,*)
      WRITE(9,*)'   x             F(x)          P(x)
     +           F(x) - P(x).'
      WRITE(9,*)
      DO 10 K=0,20
        Z=-1 + K*0.1
        WRITE(9,*) Z,'      ',F(Z),'      ',CH(Z,C,N),
     +'      ',F(Z)-CH(Z,C,N)
10    CONTINUE
      RETURN
      END
