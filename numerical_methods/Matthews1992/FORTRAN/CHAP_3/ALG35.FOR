      PROGRAM GAUSSIT
C     ----------------------------------------------------------------
C     Alg3'5.for   FORTRAN program for implementing Algorithm 3.5
C     
C     NUMERICAL METHODS: FORTRAN Programs, (c) John H. Mathews 1995
C     To accompany the text:
C     NUMERICAL METHODS for Math., Science & Engineering, 2nd Ed, 1992
C     Prentice Hall, Englewood Cliffs, New Jersey, 07632, U.S.A.
C     Prentice Hall, Inc.; USA, Canada, Mexico ISBN 0-13-624990-6
C     Prentice Hall, International Editions:   ISBN 0-13-625047-5
C     This free software is compliments of the author.
C     E-mail address:       in%"mathews@fullerton.edu"
C     
C     Algorithm 3.5 (Gauss-Seidel Iteration).
C     Section   3.7, Iterative Methods for Linear Systems, Page 187
C     ----------------------------------------------------------------
      PARAMETER(MaxR=25,Max=99,Tol=1E-7)
      INTEGER Cond,Count,InRC,N,Row
      REAL A,A1,B,DET,Sep,X
      CHARACTER Ans*1,Ach*1,Bch*1,Xch*1
      DIMENSION A(1:MaxR,1:MaxR),A1(1:MaxR,1:MaxR)
      DIMENSION B(1:MaxR),Row(1:MaxR),X(1:MaxR)
10    CALL INPUTS(A,A1,B,N,InRC)
      WRITE(9,*)'ENTER the starting vector.'
      Xch='X'
      CALL VECIN(Xch,X,N)
      CALL ITERATE(A,B,X,N,Max,Tol,Sep,Cond,Count)
      CALL RESULTS(A,B,X,N,Tol,Sep,Cond,Count)
      WRITE(9,*)' '
      WRITE(9,*)'Want to solve another linear system ? <Y/N> '
      READ(9,'(A)') Ans
      IF (Ans.EQ.'Y' .OR. Ans.EQ.'y') GOTO 10
      END

      SUBROUTINE ITERATE(A,B,X,N,Max,Tol,Sep,Cond,Count)
      PARAMETER(MaxR=25)
      INTEGER C,Cond,Count,J,K,N,Max,R
      REAL A,A1,B,DET,X,Xold,Row,Sep,Sum,T,Tol
      CHARACTER Ans*1,Ach*1,Bch*1,Xch*1
      DIMENSION A(1:MaxR,1:MaxR)
      DIMENSION B(1:MaxR),X(1:MaxR),Xold(1:MaxR)
      Sep=1
      K=0
      Cond=1
      DO J=1,N
        Xold(J)=X(J)
      ENDDO
      DO R=1,N
        Row=0
        DO C=1,N
          Row=Row+ABS(A(R,C))
        ENDDO
        IF (Row.GE.2*ABS(A(R,R))) Cond=0
      ENDDO
      IF (Cond.EQ.0) RETURN
      WRITE(9,1000) K, ( Xold(J), J=1,N )
      WHILE ((K.LT.Max).AND.(Sep.GT.Tol))
        DO R=1,N
          Sum=B(R)
          DO C=1,N
            IF (C.NE.R) Sum=Sum-A(R,C)*X(C)
          ENDDO
          X(R)=Sum/A(R,R)
        ENDDO
        Sep=0
        DO J=1,N
          Sep=Sep+ABS(X(J)-Xold(J))
          Xold(J)=X(J)
        ENDDO
        K=K+1
        WRITE(9,1000) K, ( Xold(J), J=1,N )
      REPEAT
      Count=K
1000  FORMAT(I2,4X,5F15.7)
      PAUSE
      RETURN
      END

      SUBROUTINE XTERATE(A,B,X,N,Max,Tol,Sep,Cond,Count)
      PARAMETER(MaxR=25)
      INTEGER C,Cond,Count,J,K,N,Max,R
      REAL A,A1,B,DET,X,Xold,Row,Sep,Sum,T,Tol
      CHARACTER Ans*1,Ach*1,Bch*1,Xch*1
      DIMENSION A(1:MaxR,1:MaxR)
      DIMENSION B(1:MaxR),X(1:MaxR),Xold(1:MaxR)
      Sep=1
      K=0
      Cond=1
      DO 10 J=1,N
        Xold(J)=X(J)
10    CONTINUE
      DO 30 R=1,N
        Row=0
        DO 20 C=1,N
          Row=Row+ABS(A(R,C))
20      CONTINUE
        IF (Row.GE.2*ABS(A(R,R))) Cond=0
30    CONTINUE
      IF (Cond.EQ.0) RETURN
      DO 40 J=1,10
      WRITE(9,*) ' '      
40    CONTINUE
      WRITE(9,1000) K, ( Xold(J), J=1,N )
50    IF ((K.LT.Max).AND.(Sep.GT.Tol)) THEN
        DO 70 R=1,N
          Sum=B(R)
          DO 60 C=1,N
            IF (C.NE.R) Sum=Sum-A(R,C)*X(C)
60        CONTINUE
          X(R)=Sum/A(R,R)
70      CONTINUE
        Sep=0
        DO 80 J=1,N
          Sep=Sep+ABS(X(J)-Xold(J))
          Xold(J)=X(J)
80      CONTINUE
        K=K+1
        WRITE(9,1000) K, ( Xold(J), J=1,N )
        GOTO 50
      ENDIF
      Count=K
1000  FORMAT(I2,4X,5F15.7)
      PAUSE
      RETURN
      END

      SUBROUTINE INPUTS(A,A1,B,N,InRC)
      PARAMETER(MaxR=25)
      INTEGER Col,I,InRC,N,Row
      REAL A,B
      CHARACTER Ans*1,Ach*1,Bch*1
      DIMENSION A(1:MaxR,1:MaxR),A1(1:MaxR,1:MaxR),B(1:MaxR)
      DO 10 I=1,12
        WRITE(9,*)' '
10    CONTINUE
      WRITE(9,*)' '
      WRITE(9,*)'       Gauss-Seidel iteration is is performed to find t
     +he solution to the linear'
      WRITE(9,*)' '
      WRITE(9,*)'     system  A*X = B.   The matrix must be diagonally d
     +ominant.   Starting with'
      WRITE(9,*)' '
      WRITE(9,*)'                                             (k)  (k)
     +      (k)    '
      WRITE(9,*)'     X  = (0,0,...,0),  a sequence  { X  = (x   ,x   ,.
     +..,x   ) }  is generated'
      WRITE(9,*)'      0                                k     1    2
     +        N      '
      WRITE(9,*)' '
      WRITE(9,*)'     which converges to the solution.'
      WRITE(9,*)' '
      WRITE(9,*)'                           (k+1)'
      WRITE(9,*)'     the j-th coordinate  x       of  X     is computed
     + using the formula:'
      WRITE(9,*)'                           j           k+1'
      WRITE(9,*)' '
      WRITE(9,*)'      (k+1)                (k+1)              (k+1)'
      WRITE(9,*)'     x       =  [b  - a   x      -...- a     x     '
      WRITE(9,*)'      j           j    j,1 1            j,j-1 j-1  '
      WRITE(9,*)' '
      WRITE(9,*)'                             (k)            (k)      '
      WRITE(9,*)'                    - a     x    -...- a   x   ]/a
     +       for  j=1,2,...,N. '
      WRITE(9,*)'                       j,j+1 j+1        j,N N     j,j'
      WRITE(9,*)' '
      WRITE(9,*)' '
      WRITE(9,*)'     give the dimension  N = '
      READ(9,*) N
      InRC=0
      WRITE(9,*)'Want to input columns ? <Y/N> '
      READ(9,'(A)') Ans
      IF (Ans.EQ.'Y' .OR. Ans.EQ.'y') THEN
        InRC=1
      ENDIF
      WRITE(9,*)' '
      WRITE(9,*)'ENTER the coefficient matrix  A:'
      Ach='A'
      CALL MATIN(Ach,A,N,InRC)
      WRITE(9,*)'The matrix  A  is:'
      CALL MATPRINT(A,N)
      Bch='B'
      CALL VECIN(Bch,B,N)
      RETURN
      END

      SUBROUTINE MATIN(Ach,A,N,InRC)
      PARAMETER(MaxR=25)
      INTEGER Col,InRC,N,Row
      REAL A
      CHARACTER Ach*1
      DIMENSION A(1:MaxR,1:MaxR)
      DO 20 Row=1,N
      DO 10 Col=1,N
        A(Row,Col)=0
10    CONTINUE
20    CONTINUE
      WRITE(9,*)'Give the elements of the matrix ',Ach
      WRITE(9,*)' '
      IF (InRC.EQ.0) THEN
        DO 40 Row=1,N
          WRITE(9,*)'ENTER the elements of row ',Row
          WRITE(9,*)' '
        DO 30 Col=1,N
          WRITE(9,*) Ach,'(',Row,',',Col,') = '
          READ(9,*) A(Row,Col)
30      CONTINUE
40      CONTINUE
      ELSE
        DO 60 Col=1,N
          WRITE(9,*)'ENTER the elements of column ',Col
          WRITE(9,*)' '
        DO 50 Row=1,N
          WRITE(9,*) Ach,'(',Row,',',Col,') = '
          READ(9,*) A(Row,Col)
50      CONTINUE
60      CONTINUE
      ENDIF
      RETURN
      END

      SUBROUTINE VECIN(Bch,B,N)
      PARAMETER(MaxR=25)
      INTEGER N,Row
      REAL B
      CHARACTER Bch*1
      DIMENSION B(1:MaxR)
      WRITE(9,*)'ENTER the elements of the column vector ',Bch
      WRITE(9,*)' '
      DO 10 Row=1,N
        WRITE(9,*) Bch,'(',Row,') = '
        READ(9,*) B(Row)
10    CONTINUE
      RETURN
      END

      SUBROUTINE MATPRINT(A,N)
      PARAMETER(MaxR=25)
      INTEGER Col,N,Row
      REAL A
      DIMENSION A(1:MaxR,1:MaxR)
      DO 10 Row=1,N
        WRITE(9,999) ( A(Row,Col), Col=1,N )
10    CONTINUE
999   FORMAT(1X,5F15.7,4X)
      RETURN
      END

      SUBROUTINE VECPRINT(Bch,Xch,B,X,N)
      PARAMETER(MaxR=25)
      INTEGER N,Row
      REAL B,X
      CHARACTER Bch*1,Xch*1
      DIMENSION B(1:MaxR),X(1:MaxR)
      WRITE(9,*) Bch,' COEFFICIENT VECTOR                ',Xch,
     +' SOLUTION VECTOR'
      WRITE(9,*)
      DO 10 Row=1,N
        WRITE(9,*) Bch,'(',Row,') =',B(Row),'    ',Xch,'(',Row,') ='
     +,X(Row)
10    CONTINUE
      RETURN
      END

      SUBROUTINE RESULTS(A,B,X,N,Tol,Sep,Cond,Count)
      PARAMETER(MaxR=25)
      INTEGER C,Cond,Count,K,N,R
      REAL A,B,X,Tol,Sep
      CHARACTER Bch*1,Xch*1
      DIMENSION A(1:MaxR,1:MaxR),B(1:MaxR),X(1:MaxR)
      DO 10 I=1,15
        WRITE(9,*)' '
10    CONTINUE
      WRITE(9,*)' '
      WRITE(9,*)'The matrix A(I,J) is:'
      IF (N.LT.6) THEN
        WRITE(9,*)' '
      ENDIF
      CALL MATPRINT(A,N)
      WRITE(9,*)' '
      WRITE(9,*)'The vector B is:'
      IF (N.LT.6) THEN
        WRITE(9,*)' '
      ENDIF
      DO 20 K=1,N
        WRITE(9,*)'      B(',K,') = ',B(K)
20    CONTINUE
      WRITE(9,*)' '
      IF (Cond.EQ.1) THEN
        IF (Sep.LT.Tol) THEN
          WRITE(9,*)'Gauss-Seidel iteration solved  A*X = B.'
          WRITE(9,*)'It took',Count,' iterations to find the solution:'
        ELSE
          WRITE(9,*)'Gauss-Seidel iteration did not converge.'
          WRITE(9,*)'The status after ',Count,' iterations is'
        ENDIF
        IF (N.LT.6) THEN
          WRITE(9,*)' '
        ENDIF
        WRITE(9,*)' '
        DO 30 K=1,N
            WRITE(9,*)'      X(',K,') = ',X(K)
30      CONTINUE
        WRITE(9,*)' '
        WRITE(9,*)'           +-',Sep,'  is the estimated accuracy.'
      ELSE
        WRITE(9,*)' '
        WRITE(9,*)'The matrix is not diagonally dominant.'
        WRITE(9,*)' '
        WRITE(9,*)'Gauss-Seidel iteration cannot be used.'
      ENDIF
      RETURN
      END
