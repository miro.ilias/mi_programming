      PROGRAM BISECTION
C     ----------------------------------------------------------------
C     Alg2'2.for   FORTRAN program for implementing Algorithm 2.2
C     
C     NUMERICAL METHODS: FORTRAN Programs, (c) John H. Mathews 1995
C     To accompany the text:
C     NUMERICAL METHODS for Math., Science & Engineering, 2nd Ed, 1992
C     Prentice Hall, Englewood Cliffs, New Jersey, 07632, U.S.A.
C     Prentice Hall, Inc.; USA, Canada, Mexico ISBN 0-13-624990-6
C     Prentice Hall, International Editions:   ISBN 0-13-625047-5
C     This free software is compliments of the author.
C     E-mail address:       in%"mathews@fullerton.edu"
C     
C     Algorithm 2.2 (Bisection Method).
C     Section   2.2, Bracketing Methods for Locating a Root, Page  61
C     ----------------------------------------------------------------
      PARAMETER(Delta=5E-7)
      INTEGER Cond,KL,KR
      REAL A,A1,B,B1,C,D
      CHARACTER ANS*1
      EXTERNAL F
10    CALL GETPTS(A,B)
      A1=A
      B1=B
      CALL BISEC(F,A1,B1,Delta,C,D,Cond,KL,KR)
      CALL RESULT(F,A,B,Delta,C,D,Cond,KL,KR)
      WRITE(9,*)' '
      WRITE(9,*)'WANT TO TRY ANOTHER INTERVAL? <Y/N> '
      READ(9,'(A)') ANS
      IF(ANS .EQ. 'Y' .OR. ANS .EQ. 'y') GOTO 10
      STOP
      END

      REAL FUNCTION F(X)
        F = SIN(X)/COS(X)
      RETURN
      END

      SUBROUTINE PRINTFN
        WRITE(9,*)'F(X) = SIN(X)/COS(X)'
      RETURN
      END

      SUBROUTINE BISEC(F,A,B,Delta,C,D,Cond,KL,KR)
      PARAMETER(Big=1E5)
      INTEGER Cond,K,KL,KR,Max
      REAL A,B,C,D,Delta,YA,YB,YC
      EXTERNAL F
      K=0
      KL=0
      KR=0
      YA=F(A)
      YB=F(B)
      D=B-A
      Cond=0
      Max=1+INT((ALOG(D)-ALOG(Delta))/ALOG(2.0))
      IF (YA*YB.GE.0) THEN
        Cond=1 
        RETURN 
      ENDIF
      WHILE (Cond.EQ.0).AND.(K.LT.Max)
        C=(A+B)/2.0
        YC=F(C)
          IF (YC.EQ.0) THEN
            A=C
            B=C
            Cond=2
          ELSEIF ((YB*YC).GT.0) THEN
            B=C
            YB=YC
            KR=KR+1
          ELSE
            A=C
            YA=YC
            KL=KL+1
          ENDIF
        K=K+1
        WRITE(9,1000) K,A,C,B
      REPEAT
      D=B-A
      IF (D.LT.Delta) THEN
        IF (Cond.NE.2) Cond=3
        IF ((ABS(YA).GT.Big).AND.(ABS(YB).GT.Big)) Cond=4
      ENDIF
      PAUSE
      RETURN
1000  FORMAT(I2,4X,F15.7,4X,F15.7,4X,F15.7)
      END

      SUBROUTINE XISEC(F,A,B,Delta,C,D,Cond,KL,KR)
      PARAMETER(Big=1E5)
      INTEGER Cond,K,KL,KR,Max
      REAL A,B,C,D,Delta,YA,YB,YC
      EXTERNAL F
      K=0
      KL=0
      KR=0
      YA=F(A)
      YB=F(B)
      D=B-A
      Cond=0
      Max=1+INT((ALOG(D)-ALOG(Delta))/ALOG(2.0))
      IF (YA*YB.GE.0) THEN
        Cond=1 
        RETURN 
      ENDIF
10    IF ((Cond.EQ.0).AND.(K.LT.Max)) THEN
        C=(A+B)/2.0
        YC=F(C)
          IF (YC.EQ.0) THEN
            A=C
            B=C
            Cond=2
          ELSEIF ((YB*YC).GT.0) THEN
            B=C
            YB=YC
            KR=KR+1
          ELSE
            A=C
            YA=YC
            KL=KL+1
          ENDIF
        K=K+1
        WRITE(9,1000) K,A,C,B
        GOTO 10
      ENDIF
      D=B-A
      IF (D.LT.Delta) THEN
        IF (Cond.NE.2) Cond=3
        IF ((ABS(YA).GT.Big).AND.(ABS(YB).GT.Big)) Cond=4
      ENDIF
      PAUSE
      RETURN
1000  FORMAT(I2,4X,F15.7,4X,F15.7,4X,F15.7)
      END

      SUBROUTINE GETPTS(A,B)
      INTEGER I
      REAL A,B
      DO 10 I=1,17
        WRITE(9,*)' '
10    CONTINUE
      WRITE(9,*)'BISECTION METHOD IS USED TO FIND A ZERO THE FUNCTION'
      WRITE(9,*)' '
      CALL PRINTFN
      WRITE(9,*)' '
      WRITE(9,*)'THAT LIES IN THE INTERVAL  [A,B]'
      WRITE(9,*)' '
      WRITE(9,*)'ENTER LEFT  ENDPOINT A = '
      READ(9,*) A
      WRITE(9,*)'ENTER RIGHT ENDPOINT B = '
      READ(9,*) B
      WRITE(9,*)' '
      RETURN
      END

      SUBROUTINE RESULT(F,A,B,Delta,C,D,Cond,KL,KR)
      INTEGER Cond,I,J,K,KL,KR
      REAL A,B,Delta,C,D
      EXTERNAL F
      DO 10 I=1,16
        WRITE(9,*)' '
10    CONTINUE
      WRITE(9,*)'THE BISECTION METHOD WAS USED TO FIND A ZERO OF THE FUN
     +CTION'
      WRITE(9,*)' '
      CALL PRINTFN
      WRITE(9,*)' '
      WRITE(9,*)'IN THE INTERVAL  [',A,'  ,',B,'  ]'
      WRITE(9,*)' '
      K=KL+KR
      IF (Cond.EQ.1) THEN
        WRITE(9,*)'THE VALUES F(A) AND F(B)'
        WRITE(9,*)'DO NOT DIFFER IN SIGN.'
        WRITE(9,*)' '
        WRITE(9,*)'F(',A,'  ) =',F(A)
        WRITE(9,*)'F(',B,'  ) =',F(B)
        WRITE(9,*)' '
        WRITE(9,*)'THE BISECTION METHOD CANNOT BE USED.'
      ELSEIF ((Cond.EQ.2).OR.(Cond.EQ.3)) THEN
        WRITE(9,*)'THE BISECTION METHOD TOOK ',K,' ITERATIONS.'
        WRITE(9,*)' '
        WRITE(9,*)'THERE WERE',KR,' SQUEEZES FROM THE RIGHT'
        WRITE(9,*)' '
        WRITE(9,*)'       AND',KL,' SQUEEZES FROM THE LEFT.'
        WRITE(9,*)' '
        WRITE(9,*)'  THE ROOT IS  C =',C
        WRITE(9,*)' '
        WRITE(9,*)'THE ACCURACY IS +-',D
        WRITE(9,*)' '
        WRITE(9,*)'FUNCTION VALUE  F(',C,'  ) =',F(C)
        WRITE(9,*)' '
        IF (Cond.EQ.2) THEN
          WRITE(9,*)'THE FUNCTION VALUE IS EXACTLY ZERO!'
        ENDIF
      ELSEIF ((Cond.EQ.0).OR.(Cond.EQ.4)) THEN
        WRITE(9,*)'THE CURRENT ITERATE IS C(',K,') =',C
        WRITE(9,*)' '
        WRITE(9,*)'THE CURRENT INTERVAL WIDTH IS ',D
        WRITE(9,*)' '
        WRITE(9,*)'THE CURRENT FUNCTION VALUE  F(',C,'  ) =',F(C)
        WRITE(9,*)' '
        IF (Cond.EQ.0) THEN
          WRITE(9,*)'THE MAXIMUM NUMBER OF ITERATIONS WAS EXCEEDED.'
        ENDIF
        IF (Cond.EQ.4) THEN
          WRITE(9,*)'SURPRISE, A POLE WAS FOUND INSTEAD OF A ZERO!'
        ENDIF
      ENDIF
      WRITE(9,*)' '
      RETURN
      END
