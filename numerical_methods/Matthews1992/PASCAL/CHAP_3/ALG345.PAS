program LINEARSYSTEM;
{--------------------------------------------------------------------}
{  Alg3'45.pas   Pascal program for implementing Algorithm 3.4-5     }
{                                                                    }
{  NUMERICAL METHODS: Pascal Programs, (c) John H. Mathews 1995      }
{  To accompany the text:                                            }
{  NUMERICAL METHODS for Math., Science & Engineering, 2nd Ed, 1992  }
{  Prentice Hall, Englewood Cliffs, New Jersey, 07632, U.S.A.        }
{  Prentice Hall, Inc.; USA, Canada, Mexico ISBN 0-13-624990-6       }
{  Prentice Hall, International Editions:   ISBN 0-13-625047-5       }
{  This free software is compliments of the author.                  }
{  E-mail address:       in%"mathews@fullerton.edu"                  }
{                                                                    }
{  Algorithm 3.4 (Jacobi Iteration).                                 }
{  Section   3.7, Iterative Methods for Linear Systems, Page 186     }
{                                                                    }
{  Algorithm 3.5  (Gauss-Seidel Iteration).                          }
{  Section   3.7,  Iterative Methods for Linear Systems, Page 187    }
{--------------------------------------------------------------------}

  uses
    crt;

  const
    MaxN = 12;
    Max = 151;
    Tol = 1E-6;

  type
    SubS = 1..MaxN;
    MATRIX = array[SubS, SubS] of real;
    VECTOR = array[SubS] of real;
    POINTER = array[SubS] of integer;
    MATTYPE = (ColDom, DiagDom, LowerT, NotDom, Singular, Square, UpperT);
    LETTER = string[1];
    LETTERS = string[200];
    Status = (Done, Working);
    DoSome = (Go, New, Stop);
    Option = (Auto, Observe);

  var
    Count, InRC, Inum, Meth, N, Sub: integer;
    Hor, Ver: integer;
    B, X, Xstart: VECTOR;
    A, A1, C, D, E, L, M1, MI, U: MATRIX;
    Cnorm, Det, Rnorm, Rnum, Sep, W: real;
    Row: POINTER;
    Ans: CHAR;
    Ach, Bch, Xch: LETTERS;
    Mess: LETTERS;
    Stat: Status;
    Mtype: MATTYPE;
    DoMo: DoSome;
    Optn: Option;

  procedure Voutput (VE: VECTOR; Count: integer);
    var
      J: integer;
  begin
    for J := 1 to N - 1 do
      begin
        if ABS(VE[J]) < 1E8 then
          WRITE(VE[J] : 17 : 8, '   ')
        else
          WRITE(VE[J] : 17 : 8, '   ');
      end;
    if ABS(VE[N]) < 1E8 then
      WRITE(VE[N] : 17 : 8)
    else
      WRITE(VE[N] : 17 : 8);
    WRITELN;
    if (Count mod 20 = 0) and (Count > 0) and (N < 5) then
      begin
        WRITE('                  Press the  <ENTER>  key.  ');
        READLN(Ans);
        WRITELN;
      end;
    if (Count mod 10 = 0) and (Count > 0) and (N > 4) then
      begin
        WRITE('                  Press the  <ENTER>  key.  ');
        READLN(Ans);
        WRITELN;
      end;
  end;                                       {End of procedure Voutput}

  procedure ITERATEJI (A: MATRIX; B: VECTOR; var X: VECTOR;
                       N, Max: integer; Tol: real; var Sep: real;
                       var Mtype: MATTYPE; var Count: integer);
    const
      Big = 1E10;
    var
      Xold: VECTOR;
      C, Cond, J, K, R: integer;
      Row, Sum: real;
    label
      990;

    function NORM (V: VECTOR): real;
      var
        I: integer;
        Sum: real;
    begin
      Sum := 0;
      for I := 1 to N do
        Sum := Sum + V[I] * V[I];
      NORM := SQRT(Sum);
    end;

  begin                                              {Jacobi iteration}
    Sep := 1;
    K := 0;
    Count := K;
    Cond := 1;
    for J := 1 to N do                      {Initialize Starting Value}
      begin
        Xold[J] := X[J];
      end;
    if Optn = Observe then
      Voutput(X, Count);
    while (K < Max) and (Sep > Tol) and (NORM(X) < Big) do
      begin
        for R := 1 to N do
          begin
            Sum := B[R];
            for C := 1 to N do
              begin
                if C <> R then
                  Sum := Sum - A[R, C] * Xold[C];
              end;
            X[R] := Sum / A[R, R];
          end;
        if Optn = Observe then
          Voutput(X, Count);
        Sep := 0;
        for J := 1 to N do
          Sep := Sep + ABS(X[J] - Xold[J]);
        for J := 1 to N do
          Xold[J] := X[J];
        K := K + 1;
        Count := K;
      end;
    if Optn = Observe then
      begin
        WRITELN;
        WRITE('Press the  <ENTER> key.  ');
        READLN(Ans);
        WRITELN;
      end;
    Count := K;
990:
  end;                              {End of Jacobi iteration Procedure}

  procedure ITERATEGS (A: MATRIX; B: VECTOR; var X: VECTOR;
                       N, Max: integer; Tol: real; var Sep: real;
                       var Mtype: MATTYPE; var Count: integer);
    const
      Big = 1E15;
    var
      Xold: VECTOR;
      C, Cond, J, K, R: integer;
      Row, Sum: real;
    label
      990;

    function NORM (V: VECTOR): real;
      var
        I: integer;
        Sum: real;
    begin
      Sum := 0;
      for I := 1 to N do
        Sum := Sum + V[I] * V[I];
      NORM := SQRT(Sum);
    end;

  begin                                        {Gauss-Seidel iteration}
    Sep := 1;
    K := 0;
    Count := K;
    Cond := 1;
    for J := 1 to N do                      {Initialize Starting Value}
      begin
        Xold[J] := X[J];
      end;
    if Optn = Observe then
      Voutput(X, Count);
    while (K < Max) and (Sep > Tol) and (NORM(X) < Big) do
      begin
        for R := 1 to N do
          begin
            Sum := B[R];
            for C := 1 to N do
              begin
                if C <> R then
                  Sum := Sum - A[R, C] * X[C];
              end;
            X[R] := Sum / A[R, R];
          end;
        if Optn = Observe then
          Voutput(X, Count);
        Sep := 0;
        for J := 1 to N do
          Sep := Sep + ABS(X[J] - Xold[J]);
        for J := 1 to N do
          Xold[J] := X[J];
        K := K + 1;
        Count := K;
      end;
    if Optn = Observe then
      begin
        WRITELN;
        WRITE('Press the  <ENTER> key.  ');
        READLN(Ans);
        WRITELN;
      end;
    Count := K;
990:
  end;                        {End of Gauss-Seidel iteration Procedure}

  procedure INPUTMATRIX (var Ach: LETTERS; var A, A1: MATRIX; N, InRC: integer);
    var
      Count, C, CL, CU, K, R, RL, RU: integer;
      Z: VECTOR;
  begin
    for R := 1 to N do
      begin
        for C := 1 to N do
          begin
            A[R, C] := 0;
            A1[R, C] := A[R, C];
          end;
      end;
    WRITELN;
    WRITELN;
    WRITELN('     Input the elements of the ', N : 1, ' by ', N : 1, ' coefficient matrix  ', Ach);
    RL := 1;
    RU := N;
    CL := 1;
    CU := N;
    if (InRC = 1) then
      begin
        for R := 1 to N do
          begin
            WRITELN;
            WRITELN('ENTER all the coefficients of row ', R, ' on one row');
            WRITELN;
            for K := 1 to N do
              Z[K] := 0;
            case N of
              1: 
                READLN(Z[1]);
              2: 
                READLN(Z[1], Z[2]);
              3: 
                READLN(Z[1], Z[2], Z[3]);
              4: 
                READLN(Z[1], Z[2], Z[3], Z[4]);
              5: 
                READLN(Z[1], Z[2], Z[3], Z[4], Z[5]);
              6: 
                READLN(Z[1], Z[2], Z[3], Z[4], Z[5], Z[6]);
              7: 
                READLN(Z[1], Z[2], Z[3], Z[4], Z[5], Z[6], Z[7]);
              8: 
                READLN(Z[1], Z[2], Z[3], Z[4], Z[5], Z[6], Z[7], Z[8]);
              9: 
                READLN(Z[1], Z[2], Z[3], Z[4], Z[5], Z[6], Z[7], Z[8], Z[9]);
              10: 
                READLN(Z[1], Z[2], Z[3], Z[4], Z[5], Z[6], Z[7], Z[8], Z[9], Z[10]);
            end;
            for C := 1 to N do
              begin
                A[R, C] := Z[C];
                A1[R, C] := A[R, C];
              end;
          end;
      end
    else if (InRC = 2) then
      begin
        for R := 1 to N do
          begin
            WRITELN;
            WRITELN('     ENTER the coefficients of row ', R);
            WRITELN;
            if Mtype = LowerT then
              CU := R;
            if Mtype = UpperT then
              CL := R;
            for C := CL to CU do
              begin
                WRITE('     A(', R : 2, ',', C : 2, ') = ');
                READLN(A[R, C]);
                A1[R, C] := A[R, C];
              end;
          end;
      end
    else
      begin
        for C := 1 to N do
          begin
            WRITELN;
            WRITELN('     ENTER the coefficients of column ', C);
            WRITELN;
            if Mtype = LowerT then
              RL := C;
            if Mtype = UpperT then
              RU := C;
            for R := RL to RU do
              begin
                WRITE('     A(', R : 2, ',', C : 2, ') = ');
                READLN(A[R, C]);
                A1[R, C] := A[R, C];
              end;
          end;
      end;
    Mtype := Square;
  end;                                   {End of procedure INPUTMATRIX}

  procedure REFRESH (var A: MATRIX; A1: MATRIX; var X: VECTOR; Xstart: VECTOR; N: integer);
    var
      C, R: integer;
  begin
    for R := 1 to N do
      begin
        for C := 1 to N do
          begin
            A[R, C] := A1[R, C];
          end;
      end;
    for R := 1 to N do
      X[R] := Xstart[R];
  end;

  procedure Aoutput (Ach: LETTERS; A: MATRIX; N: integer);
    var
      Digits, Mdigits, C, R: integer;
      Log10: real;
  begin
    Log10 := LN(10);
    WRITELN;
    WRITELN('The matrix  ', Ach, '  is:');
    for R := 1 to N do
      begin
        WRITELN;
        for C := 1 to N - 1 do
          begin
            Digits := 7;
            if A[R, C] <> 0 then
              Mdigits := 1 + TRUNC(LN(ABS(A[R, C])) / Log10);
            if A[R, C] < 0 then
              Mdigits := Mdigits + 1;
            if Mdigits < 7 then
              Mdigits := 7;
            Digits := 14 - Mdigits;
            WRITE(A[R, C] : 15 : Digits, ' ');
          end;
        Digits := 7;
        if A[R, N] <> 0 then
          Mdigits := 1 + TRUNC(LN(ABS(A[R, N])) / Log10);
        if A[R, N] < 0 then
          Mdigits := Mdigits + 1;
        if Mdigits < 7 then
          Mdigits := 7;
        Digits := 14 - Mdigits;
        WRITE(A[R, N] : 15 : Digits);
      end;
    WRITELN;
  end;                                       {End of procedure Aoutput}

  procedure Boutput (Bch: LETTER; B: VECTOR; N: integer);
    var
      J: integer;
  begin
    WRITELN;
    WRITELN('     The Vector  ', Bch, '  is:');
    WRITELN;
    for J := 1 to N do
      begin
        WRITELN('     ', Bch, '(', J : 2, ') =', B[J] : 15 : 7);
      end;
    WRITELN;
  end;                                      {End of procedure Boutput}

  procedure CHANGEMATRIX (Ach: LETTERS; var A, A1: MATRIX; N: integer);
    type
      STATUS = (Bad1, Enter, Diverge, Done);
      LETTER = string[1];
    var
      Cond, Count, C, I, J, K, R: integer;
      Row, Valu: real;
      Resp: CHAR;
      Stat: STATUS;
  begin
    Stat := Enter;
    while (Stat = Enter) or (Stat = Bad1) do
      begin
        CLRSCR;
        Aoutput(Ach, A1, N);
        if (Stat = Bad1) then
          begin
            WRITELN('The  matrix  is  NOT  diagonally  dominant !');
            WRITELN;
            WRITELN('Perhaps you SHOULD change some diagonal element(s).');
            WRITELN;
            WRITE('Do you want to change  any  matrix element ?  <Y/N>  ');
            READLN(Ans);
            WRITELN;
            if (Ans <> 'Y') and (Ans <> 'y') then
              Stat := Diverge;
            WRITELN;
          end;
        Resp := 'N';
        if (Stat <> Bad1) and (Stat <> Diverge) then
          begin
            WRITELN;
            WRITE('Do you want to make a change in the matrix ? <Y/N> ');
            READLN(Resp);
            WRITELN;
          end;
        if (Resp = 'Y') or (Resp = 'y') or (Stat = Bad1) then
          begin
            WRITELN('     To change a coefficient select');
            case N of
              2: 
                begin
                  WRITELN('        the row    R = 1,2');
                  WRITELN('        and column C = 1,2');
                end;
              3: 
                begin
                  WRITELN('        the row    R = 1,2,3');
                  WRITELN('        and column C = 1,2,3');
                end;
              else
                begin
                  WRITELN('        the row    R = 1,2,...,', N : 2);
                  WRITELN('        and column C = 1,2,...,', N : 2);
                end;
            end;
            Mess := '     ENTER the row R = ';
            R := 0;
            WRITELN;
            WRITE(Mess);
            READLN(R);
            Mess := '     ENTER column  C = ';
            C := 0;
            WRITELN;
            WRITE(Mess);
            READLN(C);
            if (1 <= R) and (R <= N) and (1 <= C) and (C <= N) then
              begin
                WRITELN('     The current value is A(', R : 2, ',', C : 2, ') =', A[R, C] : 15 : 7);
                WRITELN;
                WRITE('     ENTER the NEW value  A(', R : 2, ',', C : 2, ') = ');
                READLN(A[R, C]);
                A1[R, C] := A[R, C];
                WRITELN;
              end;
          end
        else if Stat <> Diverge then
          Stat := Done;
        Cond := 1;
        for I := 1 to N do
          begin
            Row := 0;
            for J := 1 to N do
              Row := Row + ABS(A[I, J]);
            if Row >= 2 * ABS(A[I, I]) then
              begin
                Cond := 0;
                R := I;
                C := I;
              end;
          end;
        if (Cond = 1) and (Stat = Bad1) then
          Stat := Enter;
        if Cond = 0 then
          if (Stat <> Diverge) and (Meth = 1) then
            Stat := Bad1;
      end;
  end;

  procedure CHANGEVECTOR (Bch: LETTER; var B: VECTOR; N: integer);
    type
      STATUS = (Enter, Done);
      LETTER = string[1];
    var
      Count, C, I, K, R: integer;
      Valu: real;
      Resp: CHAR;
      Stat: STATUS;
  begin
    Stat := Enter;
    while (Stat = Enter) do
      begin
        CLRSCR;
        WRITELN;
        Boutput(Bch, B, N);
        WRITE('Want  to  make  a change  in  the vector ?  <Y/N>  ');
        READLN(Resp);
        WRITELN;
        if (Resp = 'Y') or (Resp = 'y') then
          begin
            WRITE('     To change a coefficient select the row ');
            case N of
              2: 
                WRITELN('R = 1,2');
              3: 
                WRITELN('R = 1,2,3');
              else
                WRITELN('R = 1,2,...,', N : 2);
            end;
            WRITELN;
            Mess := '                              ENTER the row R = ';
            WRITE(Mess);
            READLN(R);
            if (1 <= R) and (R <= N) then
              begin
                WRITELN('     The current value is B(', R : 2, ') =', B[R] : 17 : 8);
                WRITELN;
                WRITE('     ENTER the NEW value  B(', R : 2, ') = ');
                READLN(B[R]);
                WRITELN;
              end;
          end
        else
          Stat := Done;
      end;
  end;

  procedure INPUTS (var A, A1: MATRIX; var N, InRC: integer);
    var
      C, R: integer;
  begin
    CLRSCR;
    WRITELN('     Solution of the linear system of equations  A X = B.');
    WRITELN;
    WRITELN('          A  is a matrix of dimension  N by N.');
    WRITELN;
    WRITELN('          B  is an  N  dimensional vector.');
    WRITELN;
    WRITELN('         {N  must be an integer between 1 and 10}');
    WRITELN;
    Mess := '   ENTER  N = ';
    N := 1;
    WRITE(Mess);
    READLN(N);
    if (N < 1) then
      N := 1;
    if (N > 10) then
      N := 10;
    CLRSCR;
    Ach := 'A';
    INPUTMATRIX(Ach, A, A1, N, InRC);
  end;                                   {End of procedure INPUTS}

  procedure INPUTVECTOR (Bch: LETTER; var B, B1: VECTOR; N: integer);
    var
      R: integer;
  begin
    for R := 1 to N do
      B[R] := B1[R];
    WRITELN;
    WRITELN('Enter the Column Vector ', Bch, ' .');
    WRITELN;
    for R := 1 to N do                               {Input Vector B}
      begin
        WRITE('     ', Bch, '(', R : 2, ') = ');
        READLN(B[R]);
        B1[R] := B[R];
      end;
  end;                                   {End of procedure INPUTVECTOR}

  procedure DOMORE (var Stat: Status);
    var
      Resp: CHAR;
  begin
    WRITE('Press the <ENTER> key.');
    READLN(Ans);
    READLN(Resp);
    WRITELN;
    if (Resp <> 'y') and (Resp <> 'Y') then
      Stat := Done;
  end;                                        {End of procedure DOMORE}

  procedure RESULTS (A: MATRIX; B: VECTOR; X: VECTOR; N: integer; Tol, Sep: real; Mtype: MATTYPE; Count: integer);
    var
      C, K, R: integer;
  begin
    CLRSCR;
    WRITELN;
    WRITE('The matrix  A(I,J)  is:');
    if (N < 5) or (N > 5) then
      WRITELN;
    for R := 1 to N do
      begin
        WRITELN;
        for C := 1 to N - 1 do
          WRITE(A[R, C] : 15 : 7, ' ');
        WRITE(A[R, N] : 15 : 7);
      end;
    WRITELN;
    WRITELN;
    if (N > 5) then
      begin
        WRITE('                  Press the  <ENTER>  key.  ');
        READLN(Ans);
        WRITELN;
        WRITELN;
      end;
    WRITELN('The constant vector B is:             Initial guess vector X was:');
    if (N < 5) or (N > 5) then
      WRITELN;
    for K := 1 to N do
      begin
        WRITELN('B(', K : 2, ') = ', B[K] : 15 : 7, '                X(', K : 2, ') = ', Xstart[K] : 15 : 7);
      end;
    WRITELN;
    if (N > 5) then
      begin
        WRITE('                  Press the  <ENTER>  key.  ');
        READLN(Ans);
        WRITELN;
        WRITELN;
      end;
    Mtype := DiagDom;    {Remark: Need to add the check to see if this is true.}
    if (Mtype = DiagDom) or (Mtype = ColDom) then
      begin
        if Sep < Tol then
          begin
            case Meth of
              1: 
                WRITELN('Jacobi iteration solved  A X = B.');
              2: 
                WRITELN('Gauss-Seidel iteration solved  A X = B.');
            end;
            WRITE('It took', Count : 3, ' iterations to find the solution:');
          end
        else
          begin
            case Meth of
              1: 
                WRITELN('Jacobi iteration did NOT converge.');
              2: 
                WRITELN('Gauss-Seidel iteration did NOT converge.');
            end;
            WRITELN('The status after ', Count : 3, ' iterations is:');
            WRITELN;
          end;
        if (N < 5) or (N > 5) then
          WRITELN;
        WRITELN;
        for K := 1 to N do
          begin
            WRITELN('      X(', K : 2, ') = ', X[K] : 15 : 7);
          end;
        if (N < 5) or (N > 5) then
          WRITELN;
        WRITELN('            +-', Sep : 15 : 7, '  is the estimated accuracy.');
      end
    else
      begin
        WRITELN('The iteration matrix might have a large norm !');
        case Meth of
          1: 
            WRITELN('Jacobi iteration may not be successful.');
          2: 
            WRITELN('Gauss-Seidel iteration may not be successful.');
        end;
        WRITELN('The status after ', Count : 3, ' iterations is:');
        WRITELN;
        for K := 1 to N do
          begin
            WRITELN('      X(', K : 2, ') = ', X[K] : 15 : 7);
          end;
        WRITELN;
        WRITELN('            +-', Sep : 15 : 7, '  is the estimated accuracy.');
      end;
  end;

  procedure INPUT (var N: integer);
    var
      C, J, R: integer;
  begin
    CLRSCR;
    case Meth of
      1: 
        begin
          WRITELN;
          WRITELN('         You chose the Jacobi iteration method to solve A X = B.  Starting with');
          WRITELN('                                                   (k)  (k)      (k)    ');
          WRITELN('     X  = (x , x  ,..., x ),  a sequence  { X  = (x   ,x   ,...,x   ) } ');
          WRITELN('      0     1   2        N                   k     1    2        N      ');
          WRITELN('     is generated which converges to the solution.');
          WRITELN;
          WRITELN('                           (k+1)');
          WRITELN('     The j-th coordinate  x       of  X     is computed using the formula:');
          WRITELN('                           j           k+1');
          WRITELN;
          WRITELN('      (k+1)                (k)               (k)');
          WRITELN('     x       =  [b  - a   x     -...- a     x     ');
          WRITELN('      j           j    j,1 1           j,j-1 j-1  ');
          WRITELN;
          WRITELN('                             (k)            (k)      ');
          WRITELN('                    - a     x    -...- a   x   ]/a      for  j=1,2,...,N. ');
          WRITELN('                       j,j+1 j+1        j,N N     j,j');
        end;
      2: 
        begin
          WRITELN;
          WRITELN('         You chose the Gauss-Seidel method to solve  A X = B.  Starting with');
          WRITELN('                                             (k)  (k)      (k)    ');
          WRITELN('     X  = (0,0,...,0),  a sequence  { X  = (x   ,x   ,...,x   ) }  is generated');
          WRITELN('      0                                k     1    2        N      ');
          WRITELN('     which converges to the solution.');
          WRITELN;
          WRITELN('                           (k+1)');
          WRITELN('     The j-th coordinate  x       of  X     is computed using the formula:');
          WRITELN('                           j           k+1');
          WRITELN;
          WRITELN('      (k+1)                (k+1)              (k+1)');
          WRITELN('     x       =  [b  - a   x      -...- a     x     ');
          WRITELN('      j           j    j,1 1            j,j-1 j-1  ');
          WRITELN;
          WRITELN('                             (k)            (k)      ');
          WRITELN('                    - a     x    -...- a   x   ]/a      for  j=1,2,...,N. ');
          WRITELN('                       j,j+1 j+1        j,N N     j,j');
        end;
    end;
    WRITELN;
    WRITELN;
    WRITELN('     Now you must state the size of the system to be solved.');
    WRITELN;
    WRITELN;
    Mess := '     ENTER the number of equations  N = ';
    N := 1;
    WRITE(Mess);
    READLN(N);
    if N < 1 then
      N := 1;
    if N > MaxN then
      N := MaxN;
    for J := 1 to N do
      B[J] := 0;
    for J := 1 to N do
      X[J] := 0;
    for J := 1 to N do
      Xstart[J] := 0;
  end;                                            {End Input Procedure}

  procedure STRATEGY (var Meth: integer);
    var
      I: integer;
  begin
    CLRSCR;
    WRITELN;
    WRITELN('                   ITERATIVE METHODS FOR LINEAR SYSTEMS');
    WRITELN;
    WRITELN;
    WRITELN('         Iterative methods are used for the solution of the linear system');
    WRITELN;
    WRITELN('                   A X = B.');
    WRITELN;
    WRITELN('     If you choose either Jacobi or Gauss-Seidel iteration, then the matrix');
    WRITELN;
    WRITELN('     should be diagonally dominant in order to guarantee converges.');
    WRITELN;
    WRITELN;
    WRITELN('                   You have a choice of three methods:');
    WRITELN;
    WRITELN('            < 1 >  Jacobi iteration');
    WRITELN;
    WRITELN('            < 2 >  Gauss-Seidel iteration');
    WRITELN;
    WRITELN;
    Mess := '                   SELECT the method  < 1 or 3 >  ?  ';
    Meth := 2;
    WRITE(Mess);
    READLN(Meth);
    if Meth < 1 then
      Meth := 1;
    if Meth > 2 then
      Meth := 2;
    CLRSCR;
    if DoMo = Go then
      begin
        WRITELN;
        WRITELN;
        WRITELN('          What type of matrix will you enter ?');
        WRITELN;
        WRITELN;
        WRITELN('          < 1 > An N by N square matrix.');
        WRITELN;
        WRITELN;
        WRITELN('          < 2 > An N by N lower-triangular matrix.');
        WRITELN;
        WRITELN;
        WRITELN('          < 3 > An N by N upper-triangular matrix.');
        WRITELN;
        WRITELN;
        Mess := '          SELECT the type of matrix  < 1 - 3 > ?  ';
        I := 1;
        WRITE(Mess);
        READLN(I);
        if (I < 1) or (3 < I) then
          I := 1;
        if I = 1 then
          Mtype := Square;
        if I = 2 then
          Mtype := LowerT;
        if I = 3 then
          Mtype := UpperT;
        CLRSCR;
      end;
  end;                                  {End of procedure STRATEGY}

  procedure MESSAGE (var InRC, Meth: integer);
  begin
    STRATEGY(Meth);
    CLRSCR;
    WRITELN;
    WRITELN;
    WRITELN('        Choose how you want to input the elements of the matrix.');
    WRITELN;
    WRITELN('    <1> Enter the elements of each row on one line separated by spaces, i.e.');
    WRITELN;
    WRITELN('        A(J,1)  A(J,2)  ...  A(J,N)           for J=1,2,...,N');
    WRITELN;
    WRITELN('    <2> Enter each element of a row on a separate line, i.e.');
    WRITELN;
    WRITELN('        A(J,1)');
    WRITELN('        A(J,2)');
    WRITELN('           .');
    WRITELN('           :');
    WRITELN('        A(J,N)     for J=1,2,...,N');
    WRITELN;
    WRITELN('    <3> Enter each element of a column on a separate line, i.e.');
    WRITELN;
    WRITELN('        A(1,K)');
    WRITELN('        A(2,K)');
    WRITELN('           .');
    WRITELN('           :');
    WRITELN('        A(N,K)     for K=1,2,...,N');
    WRITELN;
    WRITELN;
    Mess := '        SELECT < 1 - 3 >  ?   ';
    InRC := 2;
    WRITE(Mess);
    READLN(InRC);
    if InRC < 1 then
      InRC := 1;
    if InRC > 3 then
      InRC := 3;
  end;                                  {End of procedure MESSAGE}

  procedure MULTIPLY (A, B: MATRIX; N: integer; var C: MATRIX);
    var
      I, J, K: integer;
      Sum: real;
  begin
    for I := 1 to N do
      begin
        for J := 1 to N do
          begin
            Sum := 0;
            for K := 1 to N do
              Sum := Sum + A[I, K] * B[K, J];
            C[I, J] := Sum;
          end;
      end;
  end;

begin                                            {Begin Main Program}
  Ach := 'A';
  Bch := 'B';
  Xch := 'X';
  W := 1;
  MESSAGE(InRC, Meth);
  DoMo := Go;
  while (DoMo = Go) or (DoMo = New) do
    begin
      if DoMo = Go then
        begin
          N := MaxN;
          INPUT(N);
          CLRSCR;
          INPUTMATRIX(Ach, A, A1, N, InRC);
        end
      else
        begin
          WRITE('Want to change the  method  of iteration ?  <Y/N>  ');
          READLN(Ans);
          WRITELN;
          if (Ans = 'Y') or (Ans = 'y') then
            STRATEGY(Meth);
          WRITELN;
          WRITE('Do you  want  a  completely  new  matrix ?  <Y/N>  ');
          READLN(Ans);
          WRITELN;
          if (Ans = 'Y') or (Ans = 'y') then
            INPUTS(A, A1, N, InRC)
          else
            REFRESH(A, A1, X, Xstart, N);
        end;
      CHANGEMATRIX(Ach, A, A1, N);
      Stat := Working;
      while Stat = Working do
        begin
          CLRSCR;
          if DoMo = Go then
            begin
              CLRSCR;
              WRITELN;
              WRITELN;
              Aoutput(Ach, A1, N);
              WRITELN;
              WRITELN;
              WRITE('We are solving A X = B, you must enter the vector B.');
              INPUTVECTOR('B', B, B, N);
              CHANGEVECTOR('B', B, N);
              CLRSCR;
              WRITE('Now you must choose the initial vector X for the iteration.');
              X := Xstart;
              INPUTVECTOR('X', X, Xstart, N);
              CHANGEVECTOR('X', X, N);
              Xstart := X;
            end
          else
            begin
              WRITELN;
              WRITE('Do you  want  a  completely  new  vector ?  <Y/N>  ');
              READLN(Ans);
              WRITELN;
              if (Ans = 'Y') or (Ans = 'y') then
                begin
                  CLRSCR;
                  WRITELN;
                  WRITELN;
                  Aoutput(Ach, A1, N);
                  WRITELN;
                  WRITE('We are solving A X = B, you must enter the vector B.');
                  INPUTVECTOR('B', B, B, N);
                  CHANGEVECTOR('B', B, N);
                  CLRSCR;
                  WRITE('Now you must choose the initial vector X for the iteration.');
                  X := Xstart;
                  INPUTVECTOR('X', X, Xstart, N);
                  CHANGEVECTOR('X', X, N);
                  Xstart := X;
                end;
            end;
          WRITELN;
          WRITE('Want  to see  all of  the approximations ?  <Y/N>  ');
          READ(Ans);
          WRITELN;
          Optn := Auto;
          if (Ans = 'Y') or (Ans = 'y') then
            begin
              CLRSCR;
              WRITELN('Iteration produces the following approximations.');
              WRITELN;
              Optn := Observe;
            end;
          case Meth of
            1: 
              ITERATEJI(A, B, X, N, Max, Tol, Sep, Mtype, Count);
            2: 
              ITERATEGS(A, B, X, N, Max, Tol, Sep, Mtype, Count);
          end;
          RESULTS(A, B, X, N, Tol, Sep, Mtype, Count);
          WRITELN;
          if Stat <> Done then
            DOMORE(Stat);
        end;
      WRITELN;
      WRITE('Do  you  want  to  solve  a  new  system ?  <Y/N>  ');
      READLN(Ans);
      WRITELN;
      if (Ans = 'Y') or (Ans = 'y') then
        DoMo := New
      else
        DoMo := Stop;
    end;
end.                                            {End of Main Program}

