program POLYNOMIALCALCULUS;
{--------------------------------------------------------------------}
{  Alg4'2.pas   Pascal program for implementing Algorithm 4.2        }
{                                                                    }
{  NUMERICAL METHODS: Pascal Programs, (c) John H. Mathews 1995      }
{  To accompany the text:                                            }
{  NUMERICAL METHODS for Math., Science & Engineering, 2nd Ed, 1992  }
{  Prentice Hall, Englewood Cliffs, New Jersey, 07632, U.S.A.        }
{  Prentice Hall, Inc.; USA, Canada, Mexico ISBN 0-13-624990-6       }
{  Prentice Hall, International Editions:   ISBN 0-13-625047-5       }
{  This free software is compliments of the author.                  }
{  E-mail address:       in%"mathews@fullerton.edu"                  }
{                                                                    }
{  Algorithm 4.2 (Polynomial Calculus).                              }
{  Section   4.2, Introduction to Interpolation, Page 212            }
{--------------------------------------------------------------------}

  uses
    crt;

  const
    MaxN = 50;

  type
    VECTOR = array[0..MaxN] of real;
    LETTERS = string[200];
    STATUS = (Computing, Done, More, Working);
    PRTC = (NoCoeff, YesCoeff);

  var
    Inum, Meth, N, Sub: integer;
    Rnum, X: real;
    XA, XB, IA, JB: real;
    Ans, Resp: CHAR;
    Mess: LETTERS;
    A, B, D, I, J: VECTOR;
    Stat, State: STATUS;
    PC: PRTC;

  function P (X: real): real;
    var
      K: integer;
      Poly: real;
  begin
    Poly := A[N];
    B[N] := A[N];
    for K := N - 1 downto 0 do
      begin
        Poly := A[K] + Poly * X;
        B[K] := A[K] + B[K + 1] * X;
      end;
    P := Poly;
  end;

  function DP (X: real): real;
    var
      K: integer;
      Deriv: real;
  begin
    Deriv := N * A[N];
    D[N - 1] := N * A[N];
    for K := N - 1 downto 1 do
      begin
        Deriv := K * A[K] + Deriv * X;
        D[K - 1] := K * A[K] + D[K] * X;
      end;
    DP := Deriv;
  end;

  function IP (X: real): real;
    var
      K: integer;
      Integ: real;
  begin
    Integ := A[N] / (N + 1);
    I[N + 1] := A[N] / (N + 1);
    for K := N downto 1 do
      begin
        Integ := A[K - 1] / K + Integ * X;
        I[K] := A[K - 1] / K + I[K + 1] * X;
      end;
    Integ := Integ * X;
    I[0] := I[1] * X;
    IP := Integ;
  end;

  function JP (X: real): real;
    var
      K: integer;
      Integ: real;
  begin
    Integ := A[N] / (N + 1);
    J[N + 1] := A[N] / (N + 1);
    for K := N downto 1 do
      begin
        Integ := A[K - 1] / K + Integ * X;
        J[K] := A[K - 1] / K + J[K + 1] * X;
      end;
    Integ := Integ * X;
    J[0] := J[1] * X;
    JP := Integ;
  end;

  procedure PRINTPOLY (A: VECTOR; N: integer);
    var
      K, U, V: integer;
  begin
    case N of
      1: 
        begin
          WRITELN;
          WRITELN('P(x)  =  a x  +  a');
          WRITELN('          1       0');
          WRITELN;
        end;
      2: 
        begin
          WRITELN('            2');
          WRITELN('P(x)  =  a x   +  a x  +  a');
          WRITELN('          2        1       0');
          WRITELN;
        end;
      3: 
        begin
          WRITELN('            3        2');
          WRITELN('P(x)  =  a x   +  a x   +  a x  +  a ');
          WRITELN('          3        2        1       0');
          WRITELN;
        end;
      4, 5, 6, 7, 8, 9: 
        begin
          WRITELN('            ', N : 1, '        ', N - 1 : 1, '            2          ');
          WRITELN('P(x)  =  a x   +  a x   +...+  a x  +   a x  +  a ');
          WRITELN('          ', N, '        ', N - 1, '            2        1       0');
          WRITELN;
        end;
      10: 
        begin
          WRITELN('             ', N : 2, '       ', N - 1 : 1, '            2          ');
          WRITELN('P(x)  =  a  x   +  a x   +...+  a x  +   a x  +  a ');
          WRITELN('          ', N, '        ', N - 1, '            2        1       0');
          WRITELN;
        end;
      else
        begin
          WRITELN('             ', N : 2, '        ', N - 1 : 2, '           2          ');
          WRITELN('P(x)  =  a  x   +  a  x   +...+  a x  +   a x  +  a ');
          WRITELN('          ', N : 2, '        ', N - 1, '            2        1       0');
          WRITELN;
        end;
    end;
    if (PC = YesCoeff) then
      begin
        for K := 0 to TRUNC(N / 2) do                {Print the coefficients}
          begin
            U := 2 * K;                                {of P(X) in two columns}
            V := 2 * K + 1;
            if U <= N then
              begin
                if N < 10 then
                  WRITE('A(', N - U : 1, ')  =', A[N - U] : 15 : 7, '         ')
                else
                  WRITE('A(', N - U : 2, ')  =', A[N - U] : 15 : 7, '         ');
                if V <= N then
                  begin
                    if N < 10 then
                      WRITELN('A(', N - V : 1, ')  =', A[N - V] : 15 : 7)
                    else
                      WRITELN('A(', N - V : 2, ')  =', A[N - V] : 15 : 7);
                  end
                else
                  WRITELN;
              end;
          end;
      end;
  end;

  procedure GETPOLYNOMIAL (var A: VECTOR; var N: integer);
    var
      I, K: integer;
  begin
    CLRSCR;
    for K := 1 to 4 do
      WRITELN;
    WRITELN('                     POLYNOMIAL CALCULUS');
    WRITELN;
    WRITELN;
    WRITELN('          Polynomial calculus for evaluating, differentiating');
    WRITELN;
    WRITELN('     and integrating the polynomial  P(x)  of degree  N,  where ');
    WRITELN;
    WRITELN;
    WRITELN('                      N         N-1                    ');
    WRITELN('          P(x)  =  a x  +  a   x    + ... +  a x  +  a ');
    WRITELN('                    N       N-1               1       0');
    WRITELN;
    WRITELN;
    Mess := '     ENTER the  degree  N = ';
    if State <> More then
      N := 1;
    WRITE(Mess);
    READLN(N);
    if N < 1 then
      N := 1;
    if N > MaxN then
      N := MaxN;
    CLRSCR;
    WRITELN('                      N         N-1                    ');
    WRITELN('          P(x)  =  a x  +  a   x    + ... +  a x  +  a ');
    WRITELN('                    N       N-1               1       0');
    WRITELN;
    case N of
      0: 
        begin
          WRITELN('Now give the coefficient  a ');
          WRITELN('                           0');
        end;
      1: 
        begin
          WRITELN('Now give the coefficients  a  and  a ');
          WRITELN('                            1       0');
        end;
      2: 
        begin
          WRITELN('Now give the coefficients  a , a , a ');
          WRITELN('                            2   1   0');
        end;
      3: 
        begin
          WRITELN('Now give the coefficients  a , a , a , a ');
          WRITELN('                            3   2   1   0');
        end;
      4, 5, 6, 7, 8: 
        begin
          WRITELN('Now give the ', N + 1 : 1, ' coefficients  a  , a  , ... , a , a ');
          WRITELN('                              ', N : 1, '    ', N - 1 : 1, '          1   0');
        end;
      9: 
        begin
          WRITELN('Now give the ', N + 1 : 2, ' coefficients  a  , a  , ... , a , a ');
          WRITELN('                               ', N : 1, '    ', N - 1 : 1, '          1   0');
        end;
      10: 
        begin
          WRITELN('Now give the ', N + 1 : 2, ' coefficients  a   , a  , ... , a , a ');
          WRITELN('                               ', N : 2, '    ', N - 1 : 1, '          1   0');
        end;
      else
        begin
          WRITELN('Now give the ', N + 1 : 2, ' coefficients  a   , a   , ... , a , a ');
          WRITELN('                               ', N : 2, '    ', N - 1 : 2, '          1   0');
        end;
    end;
    WRITELN;
    Mess := 'Enter  a';
    if State <> More then
      A[N] := 1;
    WRITE(Mess, N : 1, ' = ');
    READLN(A[N]);
    for K := N - 1 downto 0 do
      begin
        WRITELN;
        Mess := '       a';
        if State <> More then
          A[K] := 0;
        WRITE(Mess, K : 1, ' = ');
        READLN(A[K]);
      end;
    WRITELN;
  end;

  procedure CHOICE (var Meth: integer);
  begin
    CLRSCR;
    WRITELN;
    WRITELN('The polynomial of degree ', N, ' is:');
    WRITELN;
    PC := YesCoeff;
    PRINTPOLY(A, N);
    WRITELN;
    WRITELN;
    WRITELN('Choose one of the following:');
    WRITELN;
    WRITELN('1 - Evaluate   P(x).');
    WRITELN;
    WRITELN('2 - Evaluate  P`(x).');
    WRITELN;
    WRITELN('3 - Integrate  P(x)  over  [A,B].');
    WRITELN;
    Mess := 'SELECT <1-3>  ';
    Meth := 1;
    WRITE(Mess);
    READLN(Meth);
    if Meth < 1 then
      Meth := 1;
    if Meth > 3 then
      Meth := 3;
    WRITELN;
  end;

  procedure EVALUATE (A: VECTOR; N, Meth: integer);
  begin
    CLRSCR;
    WRITE('You chose to ');
    case Meth of
      1: 
        WRITE('evaluate ');
      2: 
        WRITE('differentiate ');
      3: 
        WRITE('integrate  ');
    end;
    WRITELN('the polynomial:');
    WRITELN;
    PC := YesCoeff;
    PRINTPOLY(A, N);
    WRITELN;
    case Meth of
      1: 
        begin
          WRITELN('Evaluate   P(x)');
          WRITELN;
          Mess := '   ENTER     x = ';
          X := 0;
          WRITE(Mess);
          READLN(X);
          CLRSCR;
          WRITELN;
          PC := YesCoeff;
          PRINTPOLY(A, N);
          WRITELN;
          WRITELN('P(', X : 15 : 7, '  )  = ', P(X) : 15 : 7);
        end;
      2: 
        begin
          WRITELN('Evaluate  P`(x)');
          WRITELN;
          Mess := '   ENTER     x = ';
          X := 0;
          WRITE(Mess);
          READLN(X);
          CLRSCR;
          WRITELN;
          PC := YesCoeff;
          PRINTPOLY(A, N);
          WRITELN;
          WRITELN('P`(', X : 15 : 7, '  )  = ', DP(X) : 15 : 7);
        end;
      3: 
        begin
          WRITELN('Integrate  P(x)  over   [A,B]');
          WRITELN;
          Mess := 'ENTER the left endpoint  A = ';
          XA := 0;
          WRITE(Mess);
          READLN(XA);
          WRITELN;
          Mess := 'ENTER the right endpoint B = ';
          XB := 1;
          WRITE(Mess);
          READLN(XB);
          CLRSCR;
          WRITELN;
          PC := YesCoeff;
          PRINTPOLY(A, N);
          WRITELN;
          WRITELN(XB : 15 : 7);
          WRITELN('       /');
          IA := IP(XA);
          JB := JP(XB);
          if 0 <= IA then
            WRITELN('       |P(x)dx  =', JB : 15 : 7, '  -', IA : 15 : 7, '  =', JB - IA : 15 : 7);
          if IA < 0 then
            WRITELN('       |P(x)dx  =', JB : 15 : 7, '  -', ABS(IA) : 15 : 7, '  =', JB - IA : 15 : 7);
          WRITELN('       /');
          WRITELN(XA : 15 : 7);
        end
    end;
    WRITELN;
  end;

  procedure APPROXIMATIONS;
    var
      K: integer;
  begin
    case Meth of
      1: 
        begin
          CLRSCR;
          WRITELN;
          WRITELN;
          WRITELN('                      N         N-1                    ');
          WRITELN('          P(x)  =  a x  +  a   x    + ... +  a x  +  a ');
          WRITELN('                    N       N-1               1       0');
          WRITELN;
          WRITELN;
          WRITELN('          The algorithm to evaluate  P(x)  is as follows: ');
          WRITELN;
          WRITELN;
          WRITELN('          First, set  b   =  a .   Next, generate the sequence ');
          WRITELN('                       N      N');
          WRITELN;
          WRITELN('          b   , b   , ... , b , b  using the recursive formula:');
          WRITELN('           N-1   N-2         1   0 ');
          WRITELN;
          WRITELN('                b   =   a   + b   x    for  k = N-1,N-2,...,1,0.');
          WRITELN('                 k       k     k+1  ');
          WRITELN;
          WRITELN('          Then, the value of the polynomial is   P(x)  =  b  ');
          WRITELN('                                                           0 ');
          WRITELN;
          WRITELN;
          WRITE('                Press the <ENTER> key.  ');
          READLN(Ans);
          WRITELN;
          CLRSCR;
          PC := NoCoeff;
          PRINTPOLY(A, N);
          B[N + 1] := 0;
          WRITELN('  P(', X : 15 : 7, '  )  = ', P(X) : 15 : 7);
          WRITELN;
          WRITELN(' k             b          =        a          +        b   x ');
          WRITELN('                k                   k                   k+1  ');
          WRITELN('------------------------------------------------------------------');
          if N < 7 then
            WRITELN;
          WRITELN(N : 2, '     ', B[N] : 15 : 7);
          for K := N - 1 downto 0 do
            begin
              if N < 7 then
                WRITELN;
              WRITELN(K : 2, '     ', B[K] : 15 : 7, '     ', A[K] : 15 : 7, '     ', B[K + 1] * X : 15 : 7);
            end;
        end;
      2: 
        begin
          CLRSCR;
          WRITELN;
          WRITELN;
          WRITELN('                      N         N-1                    ');
          WRITELN('          P(x)  =  a x  +  a   x    + ... +  a x  +  a ');
          WRITELN('                    N       N-1               1       0');
          WRITELN;
          WRITELN;
          WRITELN('          The algorithm to evaluate  P`(x)  is as follows: ');
          WRITELN;
          WRITELN;
          WRITELN('          First, set  d     =  N a .   Next, generate the sequence ');
          WRITELN('                       N-1        N');
          WRITELN;
          WRITELN('          d   , d   , ... , d , d  using the resursive formula:');
          WRITELN('           N-2   N-3         1   0 ');
          WRITELN;
          WRITELN('                d   =   (k+1) a    +  d   x    for  k = N-2,N-3,...,0.');
          WRITELN('                 k             k+1     k+1  ');
          WRITELN;
          WRITELN('          Then, the value of the derivative is   P`(x)  =  d  ');
          WRITELN('                                                            0 ');
          WRITELN;
          WRITELN;
          WRITE('                Press the <ENTER> key.  ');
          READLN(Ans);
          WRITELN;
          CLRSCR;
          PC := NoCoeff;
          PRINTPOLY(A, N);
          D[N + 1] := 0;
          D[N] := 0;
          WRITELN(' P`(', X : 15 : 7, '  )  = ', DP(X) : 15 : 7);
          WRITELN;
          WRITELN(' k             d          =        (k+1) a        +    d   x ');
          WRITELN('                k                         k+1           k+1  ');
          WRITELN('------------------------------------------------------------------');
          WRITELN(N - 1 : 2, '     ', D[N - 1] : 15 : 7);
          for K := N - 2 downto 0 do
            begin
              if N < 7 then
                WRITELN;
              WRITELN(K : 2, '     ', D[K] : 15 : 7, '     ', (K + 1) * A[K + 1] : 15 : 7, '     ', D[K + 1] * X : 15 : 7);
            end;

        end;
      3: 
        begin
          CLRSCR;
          WRITELN('                                               x ');
          WRITELN('                                               / ');
          WRITELN('          The algorithm to evaluate   I(x)  =  | P(t) dt   is: ');
          WRITELN('                                               / ');
          WRITELN('                                               0 ');
          WRITELN;
          WRITELN;
          WRITELN('          First, set  i     =  a /(N+1).   Next, generate the sequence ');
          WRITELN('                       N+1      N');
          WRITELN;
          WRITELN('          i , i   , ... , i , i   using the resursive formula:');
          WRITELN('           N   N-1         2   1 ');
          WRITELN;
          WRITELN('                i    =   a   /k  + i   x    for  k = N,N-1,...,1.');
          WRITELN('                 k        k-1       k+1  ');
          WRITELN;
          WRITELN('          and   i    =   0  +  i x ');
          WRITELN('                 0              1  ');
          WRITELN;
          WRITELN('          Then, the value of the integral is   I(x)  =  i  ');
          WRITELN('                                                         0 ');
          WRITELN;
          WRITELN;
          WRITE('                Press the <ENTER> key.  ');
          READLN(Ans);
          WRITELN;
          CLRSCR;
          PC := NoCoeff;
          PRINTPOLY(A, N);
          X := XA;
          WRITELN('At the left endpoint  I(', X : 15 : 7, '  )  = ', IA : 15 : 7);
          WRITELN;
          WRITELN(' k             i          =        a   /k       +      i   x ');
          WRITELN('                k                   k-1                 k+1  ');
          WRITELN('------------------------------------------------------------------');
          if N < 7 then
            WRITELN;
          WRITELN(N + 1 : 2, '     ', I[N + 1] : 15 : 7);
          for K := N downto 1 do
            begin
              if N < 7 then
                WRITELN;
              WRITELN(K : 2, '     ', I[K] : 15 : 7, '     ', A[K - 1] / K : 15 : 7, '     ', I[K + 1] * X : 15 : 7);
            end;
          if N < 7 then
            WRITELN;
          WRITELN(0 : 2, '     ', I[0] : 15 : 7, '                         ', I[1] * X : 15 : 7);
          if N < 7 then
            WRITELN;
          WRITE('               Press the <ENTER> key.  ');
          READLN(Ans);
          WRITELN;
          CLRSCR;
          PC := NoCoeff;
          PRINTPOLY(A, N);
          X := XB;
          WRITELN('At the right endpoint  I(', X : 15 : 7, '  )  = ', JB : 15 : 7);
          WRITELN;
          WRITELN(' k             i                   a   /k              i   x ');
          WRITELN('                k                   k-1                 k+1  ');
          WRITELN('------------------------------------------------------------------');
          if N < 7 then
            WRITELN;
          WRITELN(N + 1 : 2, '     ', J[N + 1] : 15 : 7);
          for K := N downto 1 do
            begin
              if N < 7 then
                WRITELN;
              WRITELN(K : 2, '     ', J[K] : 15 : 7, '     ', A[K - 1] / K : 15 : 7, '     ', J[K + 1] * X : 15 : 7);
            end;
          if N < 7 then
            WRITELN;
          WRITELN(0 : 2, '     ', J[0] : 15 : 7, '                         ', J[1] * X : 15 : 7);
          if N < 7 then
            WRITELN;
          WRITE('               Press the <ENTER> key.  ');
          READLN(Ans);
          WRITELN;
          CLRSCR;
          PRINTPOLY(A, N);
          WRITELN;
          WRITELN(XB : 15 : 7);
          WRITELN('       /');
          if 0 <= IA then
            WRITELN('       |P(x)dx  =', JB : 15 : 7, '  -', IA : 15 : 7, '  =', JB - IA : 15 : 7);
          if IA < 0 then
            WRITELN('       |P(x)dx  =', JB : 15 : 7, '  -', ABS(IA) : 15 : 7, '  =', JB - IA : 15 : 7);
          WRITELN('       /');
          WRITELN(XA : 15 : 7);
          WRITELN;
          WRITELN(' k          i    At         i    At   ');
          WRITELN('             k   x = A       k   x = B');
          WRITELN('-----------------------------------------');
          for K := N + 1 downto 0 do
            WRITELN(K : 2, '     ', J[K] : 15 : 7, '   ', I[K] : 15 : 7);
        end;
    end;
  end;

begin                                            {Begin Main Program}
  Stat := Working;
  while (Stat = Working) do
    begin
      GETPOLYNOMIAL(A, N);
      State := Computing;
      while (State = Computing) or (State = More) do
        begin
          if State = Computing then
            CHOICE(Meth)
          else
            begin
              WRITELN;
              WRITE('Want to change the calculus operation ? <Y/N>  ');
              READLN(Resp);
              WRITELN;
              if (Resp = 'Y') or (Resp = 'y') then
                CHOICE(Meth);
            end;
          EVALUATE(A, N, Meth);
          WRITELN;
          WRITE('Want  to see  all  the approximations ? <Y/N>  ');
          READLN(Resp);
          WRITELN;
          if (Resp = 'Y') or (Resp = 'y') then
            APPROXIMATIONS;
          WRITELN;
          WRITE('Do you want to do another calculation ? <Y/N>  ');
          READLN(Resp);
          WRITELN;
          if (Resp <> 'Y') and (Resp <> 'y') then
            State := Done;
          if (Resp = 'Y') or (Resp = 'y') then
            State := More;
        end;
      WRITELN;
      WRITE('Want  to try  a  different polynomial ? <Y/N>  ');
      READLN(Resp);
      WRITELN;
      if (Resp <> 'Y') and (Resp <> 'y') then
        Stat := Done;
      if (Resp = 'Y') or (Resp = 'y') then
        State := More;
    end;
end.                                               {End Main Program}

