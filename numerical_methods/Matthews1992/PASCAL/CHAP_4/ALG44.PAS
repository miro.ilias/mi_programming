program NESTEDMULTIPLICATION;
{--------------------------------------------------------------------}
{  Alg4'4.pas   Pascal program for implementing Algorithm 4.4        }
{                                                                    }
{  NUMERICAL METHODS: Pascal Programs, (c) John H. Mathews 1995      }
{  To accompany the text:                                            }
{  NUMERICAL METHODS for Math., Science & Engineering, 2nd Ed, 1992  }
{  Prentice Hall, Englewood Cliffs, New Jersey, 07632, U.S.A.        }
{  Prentice Hall, Inc.; USA, Canada, Mexico ISBN 0-13-624990-6       }
{  Prentice Hall, International Editions:   ISBN 0-13-625047-5       }
{  This free software is compliments of the author.                  }
{  E-mail address:       in%"mathews@fullerton.edu"                  }
{                                                                    }
{  Algorithm 4.4 (Nested Multiplication with Multiple Centers).      }
{  Section   4.4, Newton Polynomials, Page 233                       }
{--------------------------------------------------------------------}

  uses
    crt;

  const
    MaxN = 99;

  type
    VECTOR = array[0..MaxN] of real;
    LETTERS = string[200];
    STATUS = (Computing, Done, Working);

  var
    Inum, N, Sub: integer;
    Rnum, T: real;
    A, S, X: VECTOR;
    Ans, Resp: CHAR;
    Mess: LETTERS;
    Stat, State: STATUS;

  function P (A, X: VECTOR; N: integer; T: real): real;
    var
      K: integer;
      Sum: real;
  begin
    Sum := A[N];
    S[N] := A[N];
    for K := N - 1 downto 0 do
      begin
        Sum := Sum * (T - X[K]) + A[K];
        S[K] := Sum;
      end;
    P := Sum;
  end;

  procedure PRINTPOLY (A, X: VECTOR; N: integer);
    var
      K: integer;
  begin
    CLRSCR;
    WRITELN;
    case N of
      0: 
        begin
          WRITELN('P(x) = a ');
          WRITELN('        0');
        end;
      1: 
        begin
          WRITELN('P(x) = a  + a [x - x ]');
          WRITELN('        0    1      0 ');
        end;
      2: 
        begin
          WRITELN('P(x) = a  + a [x - x ] + a [x - x ][x - x ]');
          WRITELN('        0    1      0     2      0       1 ');
        end;
      3: 
        begin
          WRITELN('P(x) = a  + a [x - x ] + a [x - x ][x - x ] + a [x - x ][x - x ][x - x ]');
          WRITELN('        0    1      0     2      0       1     3      0       1       2 ');
        end;
      4, 5, 6, 7, 8, 9: 
        begin
          WRITELN('P(x) = a  + a [x - x ] + a [x - x ][x - x ] + a [x - x ][x - x ][x - x ]');
          WRITELN('        0    1      0     2      0       1     3      0       1       2 ');
          WRITELN;
          WRITELN('      +...+ a [x - x ][x - x ]...[x - x ]');
          WRITELN('             ', N : 1, '      0       1          ', N - 1 : 1);
        end;
      10: 
        begin
          WRITELN('P(x) = a  + a [x - x ] + a [x - x ][x - x ] + a [x - x ][x - x ][x - x ]');
          WRITELN('        0    1      0     2      0       1     3      0       1       2 ');
          WRITELN;
          WRITELN('      +...+ a  [x - x ][x - x ]...[x - x ]');
          WRITELN('             ', N : 2, '      0       1          ', N - 1 : 1);
        end;
      else
        begin
          WRITELN('P(x) = a  + a [x - x ] + a [x - x ][x - x ] + a [x - x ][x - x ][x - x ]');
          WRITELN('        0    1      0     2      0       1     3      0       1       2 ');
          WRITELN;
          WRITELN('      +...+ a  [x - x ][x - x ]...[x - x  ]');
          WRITELN('             ', N : 2, '      0       1          ', N - 1 : 2);
        end;
    end;
    WRITELN;
    if N > 0 then
      WRITELN('The coefficients:              The centers:')
    else
      WRITELN('The coefficient:');
    WRITELN;
    if N < 10 then
      begin
        for K := 0 to N - 1 do
          WRITELN('a(', K : 1, ')  =', A[K] : 12 : 6, '           x(', K : 1, ')  =', X[K] : 12 : 6);
        WRITELN('a(', N : 1, ')  =', A[N] : 12 : 6);
      end
    else
      begin
        for K := 0 to N - 1 do
          WRITELN('a(', K : 2, ')  =', A[K] : 12 : 6, '           x(', K : 2, ')  =', X[K] : 12 : 6);
        WRITELN('a(', N : 2, ')  =', A[N] : 12 : 6);
      end;
  end;

  procedure GETDATA (var A, X: VECTOR; var N: integer);
    var
      K: integer;
  begin
    CLRSCR;
    WRITELN;
    WRITELN('                         NESTED MULTIPLICATION');
    WRITELN;
    WRITELN;
    WRITELN('     Nested multiplication is used to evaluate the Newton polynomial:');
    WRITELN;
    WRITELN;
    WRITELN('     P(x) = a  + a [x-x ] + a [x-x ][x-x ] + a [x-x ][x-x ][x-x ]');
    WRITELN('             0    1    0     2    0     1     3    0     1     2 ');
    WRITELN;
    WRITELN('           +...+ a [x-x ][x-x ]...[x-x   ]');
    WRITELN('                  N    0     1        N-1 ');
    WRITELN;
    WRITELN;
    WRITELN('     The  centers are   x  , x  ,..., x      and the');
    WRITELN('                         0    1        N-1 ');
    WRITELN;
    WRITELN('     Coefficients are   a  , a  ,..., a    , a  ');
    WRITELN('                         0    1        N-1    N ');
    WRITELN;
    WRITELN;
    Mess := '     ENTER the  degree  N = ';
    N := 0;
    WRITE(Mess);
    READLN(N);
    if (N < 0) then
      N := 0;
    if (N > 99) then
      N := 99;
    CLRSCR;
    WRITELN;
    WRITELN('     P(x) = a  + a [x-x ] + a [x-x ][x-x ] + a [x-x ][x-x ][x-x ]');
    WRITELN('             0    1    0     2    0     1     3    0     1     2 ');
    WRITELN;
    WRITELN('           +...+ a [x-x ][x-x ]...[x-x   ]');
    WRITELN('                  N    0     1        N-1 ');
    WRITELN;
    if N > 1 then
      WRITE('Now ENTER the ', N, ' centers:');
    if N = 1 then
      WRITE('Now ENTER the center:');
    WRITELN;
    for K := 0 to N - 1 do
      begin
        WRITELN;
        Mess := '     x';
        X[K] := 0;
        WRITE(Mess, K : 1, ' = ');
        READLN(X[K]);
      end;
    WRITELN;
    if N > 0 then
      WRITE('Now ENTER the ', N + 1, ' coefficients:')
    else
      WRITE('Now ENTER the coefficient:');
    WRITELN;
    for K := 0 to N do
      begin
        WRITELN;
        Mess := '     a';
        A[K] := 0;
        WRITE(Mess, K : 1, ' = ');
        READLN(A[K]);
      end;
    WRITELN;
  end;

  procedure EVALUATE (A, X: VECTOR; N: integer; var T: real);
    var
      I: integer;
  begin
    WRITELN;
    WRITELN('Now evaluate  P(x)');
    Mess := 'ENTER a value   x = ';
    T := 0;
    WRITE(Mess);
    READLN(T);
    WRITELN;
    WRITELN;
    WRITELN('The value of the Newton polynomial is:');
    WRITELN;
    WRITELN('P(', T : 15 : 7, '  )  = ', P(A, X, N, T) : 15 : 7);
  end;

  procedure APPROXIMATIONS;
    var
      K: integer;
  begin
    CLRSCR;
    WRITELN('          P(x) = a  + a [x-x ] + a [x-x ][x-x ] + a [x-x ][x-x ][x-x ]');
    WRITELN('                  0    1    0     2    0     1     3    0     1     2 ');
    WRITELN;
    WRITELN('                +...+ a [x-x ][x-x ]...[x-x   ]');
    WRITELN('                       N    0     1        N-1 ');
    WRITELN;
    WRITELN;
    WRITELN('          The nested multiplication algorithm is used to evaluate  P(x): ');
    WRITELN;
    WRITELN;
    WRITELN('          First, set   S   =  a .   Next, generate the sequence ');
    WRITELN('                        N      N');
    WRITELN;
    WRITELN('          S   , S   , ... , S , S   using the recursive formula:');
    WRITELN('           N-1   N-2         1   0 ');
    WRITELN;
    WRITELN('          S    =   S   (x - x )  +  a    for  k = N-1,N-2,...,1,0.');
    WRITELN('           k        k+1      k       k ');
    WRITELN;
    WRITELN('          Then, the value of the polynomial is   P(x)  =  S  ');
    WRITELN('                                                           0 ');
    WRITELN;
    WRITELN;
    WRITE('                    Press the <ENTER> key.  ');
    READLN(Ans);
    WRITELN;
    CLRSCR;
    S[N + 1] := 0;
    WRITELN('  P(', T : 15 : 7, '  )  = ', P(A, X, N, T) : 15 : 7);
    WRITELN;
    WRITELN(' k             S          =     S   (x - x )    +      a   ');
    WRITELN('                k                k+1      k             k  ');
    WRITELN('------------------------------------------------------------------');
    if N < 7 then
      WRITELN;
    WRITELN(N : 2, '     ', S[N] : 15 : 7);
    for K := N - 1 downto 0 do
      begin
        if N < 7 then
          WRITELN;
        WRITELN(K : 2, '     ', S[K] : 15 : 7, '     ', S[K + 1] * (T - X[K]) : 15 : 7, '     ', A[K] : 15 : 7);
      end;
  end;

begin                                            {Begin Main Program}
  Stat := Working;
  while (Stat = Working) do
    begin
      GETDATA(A, X, N);
      State := Computing;
      while (State = Computing) do
        begin
          PRINTPOLY(A, X, N);
          EVALUATE(A, X, N, T);
          WRITELN;
          WRITE('Want  to see  all of the  approximations ?  <Y/N>  ');
          READLN(Resp);
          WRITELN;
          if (Resp = 'Y') or (Resp = 'y') then
            APPROXIMATIONS;
          WRITELN;
          WRITE('Want  to evaluate  the polynomial  again ?  <Y/N>  ');
          READLN(Resp);
          WRITELN;
          State := Done;
          if (Resp = 'Y') or (Resp = 'y') then
            begin
              CLRSCR;
              State := Computing;
            end
        end;
      WRITELN;
      WRITE('Want to construct a different polynomial ?  <Y/N>  ');
      READLN(Resp);
      WRITELN;
      if (Resp <> 'Y') and (Resp <> 'y') then
        Stat := Done
    end;
end.                                               {End Main Program}

