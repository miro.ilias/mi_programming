program GRADIENTSEARCH;
{--------------------------------------------------------------------}
{  Alg8'4.pas   Pascal program for implementing Algorithm 8.4        }
{                                                                    }
{  NUMERICAL METHODS: Pascal Programs, (c) John H. Mathews 1995      }
{  To accompany the text:                                            }
{  NUMERICAL METHODS for Math., Science & Engineering, 2nd Ed, 1992  }
{  Prentice Hall, Englewood Cliffs, New Jersey, 07632, U.S.A.        }
{  Prentice Hall, Inc.; USA, Canada, Mexico ISBN 0-13-624990-6       }
{  Prentice Hall, International Editions:   ISBN 0-13-625047-5       }
{  This free software is compliments of the author.                  }
{  E-mail address:       in%"mathews@fullerton.edu"                  }
{                                                                    }
{  Algorithm 8.4 (Steepest Descent  or Gradient Method).             }
{  Section   8.1, Minimization of a Function, Page 418               }
{--------------------------------------------------------------------}

  uses
    crt;

  const
    MaxN = 6;
    Jmax = 20;
    Max = 200;
    Delta = 1E-7;
    Epsilon = 1E-9;
    FunMax = 9;
    MaxV = 200;

  type
    VECTOR = array[1..MaxN] of real;
    BIGMAT = array[0..MaxV, 1..3] of real;
    BIGVEC = array[0..MaxV] of real;
    LETTERS = string[200];
    Status = (Done, Working);
    DoSome = (Go, Stop);

  var
    Cond, Count, FunType, InRC, Inum, N, Sub: integer;
    S, P0, P1, P2, Pmin: VECTOR;
    VA: BIGMAT;
    VY: BIGVEC;
    Error, H, Rnum, Y0, Ymin: real;
    Ans: CHAR;
    Stat: Status;
    DoMo: DoSome;
    Mess: LETTERS;

  function F (P: VECTOR; N: integer): real;
    var
      X, Y, Z, U, V, W: real;
  begin
    X := P[1];
    Y := P[2];
    Z := P[3];
    U := P[4];
    V := P[5];
    W := P[6];
    case FunType of
      1: 
        F := X * X - 4 * X + Y * Y - Y - X * Y;
      2: 
        F := X * X * X + Y * Y * Y - 3 * X - 3 * Y + 5;
      3: 
        F := X * X + Y * Y + X - 2 * Y - X * Y + 1;
      4: 
        F := X * X * Y + X * Y * Y - 3 * X * Y;
      5: 
        F := X * X * X * X - 8 * X * Y + 2 * Y * Y;
      6: 
        F := (X - Y) / (2 + X * X + Y * Y);
      7: 
        F := X * X * X * X + Y * Y * Y * Y - (X + Y) * (X + Y);
      8: 
        F := (X - Y) * (X - Y) * (X - Y) * (X - Y) + (X + Y - 2) * (X + Y - 2);
      9: 
        F := 100 * (Y - X * X) * (Y - X * X) + (1 - X) * (1 - X);
    end;
  end;

  procedure PRINTFUNCTION (FunType: integer; var N: integer);
    var
      PFUN: string[50];
  begin
    case FunType of
      1: 
        begin
          N := 2;
          PFUN := 'X^2 - 4*X + Y^2 - Y - X*Y';
        end;
      2: 
        begin
          N := 2;
          PFUN := 'X^3 + Y^3 - 3*X - 3*Y + 5';
        end;
      3: 
        begin
          N := 2;
          PFUN := 'X^2 + Y^2 + X - 2*Y - X*Y + 1';
        end;
      4: 
        begin
          N := 2;
          PFUN := 'Y*X^2 + X*Y^2 - 3*X*Y';
        end;
      5: 
        begin
          N := 2;
          PFUN := 'X^4 - 8*X*Y + 2*Y^2';
        end;
      6: 
        begin
          N := 2;
          PFUN := '(X - Y)/(2 + X^2 + Y^2)';
        end;
      7: 
        begin
          N := 2;
          PFUN := 'X^4 + Y^4 - (X+Y)^2';
        end;
      8: 
        begin
          N := 2;
          PFUN := '(X-Y)^4 + (X+Y-2)^2';
        end;
      9: 
        begin
          N := 2;
          PFUN := '100*(Y-X*X)^2 + (1-X)^2';
        end;
    end;
    WRITELN(COPY('F(X,Y,Z,U,V,W', 1, 2 * N + 1), ') = ', PFUN);
  end;

  function NORM (V: VECTOR; N: integer): real;
    var
      K: integer;
      Sum: real;
  begin
    Sum := 0;
    for K := 1 to N do
      Sum := Sum + SQR(V[K]);
    NORM := SQRT(Sum);
  end;

  procedure GRADVECTOR (FunType: integer; P: VECTOR; var S: VECTOR; N: integer);
    var
      K: integer;
      Length, X, Y, Z, U, V, W: real;
      G: VECTOR;
  begin
    X := P[1];
    Y := P[2];
    Z := P[3];
    U := P[4];
    V := P[5];
    W := P[6];
    case FunType of
      1: 
        begin
          N := 2;
          G[1] := 2 * X - 4 - Y;                                  {dF1/dx}
          G[2] := 2 * Y - 1 - X;                                  {dF1/dy}
        end;
      2: 
        begin
          N := 2;
          G[1] := 3 * X * X - 3;                                    {dF1/dx}
          G[2] := 3 * Y * Y - 3;                                    {dF1/dy}
        end;
      3: 
        begin
          N := 2;
          G[1] := 2 * X + 1 - Y;                                  {dF1/dx}
          G[2] := 2 * Y - 2 - X;                                  {dF1/dy}
        end;
      4: 
        begin
          N := 2;
          G[1] := 2 * X * Y + Y * Y - 3 * Y;                            {dF1/dx}
          G[2] := 2 * X * Y + X * X - 3 * X;                            {dF1/dy}
        end;
      5: 
        begin
          N := 2;
          G[1] := 4 * X * X * X - 8 * Y;                                {dF1/dx}
          G[2] := -8 * X + 4 * Y;                                  {dF1/dy}
        end;
      6: 
        begin
          N := 2;
          G[1] := (2 - X * X + 2 * X * Y + Y * Y) / (2 + X * X + Y * Y) / (2 + X * X + Y * Y);    {dF1/dx}
          G[2] := (Y * Y - 2 * X * Y - X * X - 2) / (2 + X * X + Y * Y) / (2 + X * X + Y * Y);    {dF1/dy}
        end;
      7: 
        begin
          N := 2;
          G[1] := 4 * X * X * X - 2 * (X + Y);                            {dF1/dx}
          G[2] := 4 * Y * Y * Y - 2 * (X + Y);                            {dF1/dy}
        end;
      8: 
        begin
          N := 2;
          G[1] := 4 * (X - Y) * (X - Y) * (X - Y) + 2 * (X + Y - 2);              {dF1/dx}
          G[2] := 4 * (Y - X) * (Y - X) * (Y - X) + 2 * (X + Y - 2);              {dF1/dy}
        end;
      9: 
        begin
          N := 2;
          G[1] := 400 * (X * X - Y) * X - 2 * (1 - X);                      {dF1/dx}
          G[2] := 200 * (Y - X * X);                                  {dF1/dy}
        end;
    end;
    Length := NORM(G, N);
    if Length = 0 then
      Length := 1;
    for K := 1 to N do                       {Return negative gradient}
      begin
        G[K] := -G[K];
        S[K] := G[K] / Length;                  {Unit direction of minimum}
      end;
  end;

  procedure QMIN ( {FUNCTION F(P: VECTOR): real;}
                    P0: VECTOR; Y0: real; N, Jmax: integer;
                    Delta, Epsilon: real; var P1, P2, Pmin, S: VECTOR;
                    var H, Ymin, Error: real; var Cond, Count: integer);
    label
      999;
    const
      Big = 1E8;
    var
      J, K: integer;
      D, E0, E1, E2, H0, H1, H2, Hmin, Length, Y1, Y2: real;
  begin
    for K := 1 to N do
      begin
        P1[K] := P0[K] + H * S[K];
        P2[K] := P0[K] + 2 * H * S[K];
      end;
    Y1 := F(P1, N);
    Y2 := F(P2, N);
    Cond := 0;
    J := 0;
    while (J < Jmax) and (Cond = 0) do
      begin
        Length := NORM(P0, N);
        if Y0 <= Y1 then
          begin
            P2 := P1;
            Y2 := Y1;
            H := H / 2;
            for K := 1 to N do
              P1[K] := P0[K] + H * S[K];
            Y1 := F(P1, N);
          end
        else
          begin
            if Y2 < Y1 then
              begin
                P1 := P2;
                Y1 := Y2;
                H := 2 * H;
                for K := 1 to N do
                  P2[K] := P0[K] + 2 * H * S[K];
                Y2 := F(P2, N);
              end
            else
              Cond := -1;
          end;
        J := J + 1;
        if H < Delta then
          Cond := 1;
        if (ABS(H) > Big) or (Length > Big) then
          Cond := 5;
      end;
 { Count:=Count+J; }
    if Cond = 5 then
      begin
        Pmin := P1;
        Ymin := Y1;
        goto 999;
      end;
    D := 4 * Y1 - 2 * Y0 - 2 * Y2;
    if D < 0 then
      Hmin := H * (4 * Y1 - 3 * Y0 - Y2) / D
    else
      begin
        Cond := 4;
        Hmin := H / 3;
      end;
    for K := 1 to N do
      Pmin[K] := P0[K] + Hmin * S[K];
    Ymin := F(Pmin, N);
    H0 := ABS(Hmin);
    H1 := ABS(Hmin - H);
    H2 := ABS(Hmin - 2 * H);
    if H0 < H then
      H := H0;
    if H1 < H then
      H := H1;
    if H2 < H then
      H := H2;
    if H = 0 then
      H := Hmin;
    if H < Delta then
      Cond := 1;
    E0 := ABS(Y0 - Ymin);
    E1 := ABS(Y1 - Ymin);
    E2 := ABS(Y2 - Ymin);
    if (E0 <> 0) and (E0 < Error) then
      Error := E0;
    if (E1 <> 0) and (E1 < Error) then
      Error := E1;
    if (E2 <> 0) and (E2 < Error) then
      Error := E2;
    if (E0 = 0) and (E1 = 0) and (E2 = 0) then
      Error := 0;
    if Error < Epsilon then
      Cond := 2;
    if (Cond = 2) and (H < Delta) then
      Cond := 3;
999:
  end;

  procedure GRADSEARCH ({FUNCTION F(P:VECTOR): real;}
                         var P0, P1, P2, Pmin, S: VECTOR;
                         N, Jmax, Max: integer; Delta, Epsilon: real;
                         var H, Ymin, Error: real;
                         var Cond, Count: integer);
    var
      K: integer;
      Length: real;
  begin                           {The main part of Procedure Gradient}
    H := 1;
    Length := NORM(P0, N);
    if Length > 1E4 then
      H := Length / 1E4;
    Error := 1;
    Count := 0;
    VA[0, 1] := P0[1];
    VA[0, 2] := P0[2];
    VA[0, 3] := P0[3];
    VY[0] := F(P0, N);
    while ((Count < Max) and (Cond <> 5) and ((H > Delta) or (Error > Epsilon))) do
      begin
        GRADVECTOR(FunType, P0, S, N);
        QMIN(P0, Y0, N, Jmax, Delta, Epsilon, P1, P2, Pmin, S, H, Ymin, Error, Cond, Count);
        P0 := Pmin;
        Y0 := Ymin;
        Count := Count + 1;
        VA[Count, 1] := P0[1];
        VA[Count, 2] := P0[2];
        VA[Count, 3] := P0[3];
        VY[Count] := F(P0, N);
      end;
    WRITELN;
  end;                                      {End of Procedure Gradient}

  procedure GETFUN (var FunType, InRC, N: integer);
    var
      K: integer;
      Resp: CHAR;
  begin
    CLRSCR;
    WRITELN;
    WRITELN('    Choose your function to be minimized:');
    for K := 1 to FunMax do
      begin
        WRITELN;
        WRITE('    <', K : 2, ' >  ');
        PRINTFUNCTION(K, N);
      end;
    WRITELN;
    WRITE('           SELECT < 1 - ', FunMax : 1, ' > ?  ');
    FunType := 1;
    READLN(FunType);
    if FunType < 1 then
      FunType := 1;
    if FunType > FunMax then
      FunType := FunMax;
  end;

  procedure GETPOINT (var P0: VECTOR; var Y0: real; var N: integer);
    var
      J, K: integer;
      Z: VECTOR;
      RESP: string[40];
  begin
    CLRSCR;
    WRITELN;
    WRITELN('         You chose the function:');
    WRITELN;
    WRITE('     ');
    PRINTFUNCTION(FunType, N);
    WRITELN;
    case N of
      2: 
        begin
          WRITELN('     Give the starting point P  = (p ,p )');
          WRITELN('                              0     1  2 ');
        end;
      3: 
        begin
          WRITELN('     Give the starting point P  = (p ,p ,p )');
          WRITELN('                              0     1  2  3 ');
        end;
      else
        begin
          WRITELN('     Give the starting point P  = (p ,p ,...,p ) ');
          WRITELN('                              0     1  2      ', N);
        end;
    end;
    if (InRC = 1) then
      begin
        WRITELN;
        WRITELN('     ENTER the ', N : 1, ' coefficients of the point P  on one line. ');
        WRITELN('                                            0');
        WRITELN;
        for K := 1 to N do
          P0[K] := 0;
        WRITE('     ');
        case N of
          1: 
            READLN(P0[1]);
          2: 
            READLN(P0[1], P0[2]);
          3: 
            READLN(P0[1], P0[2], P0[3]);
          4: 
            READLN(P0[1], P0[2], P0[3], P0[4]);
          5: 
            READLN(P0[1], P0[2], P0[3], P0[4], P0[5]);
          6: 
            READLN(P0[1], P0[2], P0[3], P0[4], P0[5], P0[6]);
        end;
      end
    else
      begin
        WRITELN;
        case N of
          2: 
            begin
              WRITELN('     ENTER the coefficients of the point P  = (p ,p )');
              WRITELN('                                          0     1  2 ');
            end;
          3: 
            begin
              WRITELN('     ENTER the coefficients of the point P  = (p ,p ,p )');
              WRITELN('                                          0     1  2  3 ');
            end;
          else
            begin
              WRITELN('     ENTER the coefficients of the point P  = (p ,p ,...,p )   ');
              WRITELN('                                          0     1  2      ', N : 1);
            end;
        end;
        for K := 1 to N do
          begin
            WRITELN;
            Mess := '     p';
            P0[K] := 0;
            WRITE(Mess, K : 1, ' = ');
            READLN(P0[K]);
            WRITELN;
          end;
      end;
    Y0 := F(P0, N);
    WRITELN;
  end;

  procedure OUTPUT (P0, Pmin: VECTOR; N: integer; H, Ymin, Error, Epsilon: real; Cond, Count: integer);
    var
      K: integer;
  begin
    CLRSCR;
    WRITELN;
    WRITELN('   The gradient search method minimized the function of ', N : 1, ' variables:');
    WRITELN;
    PRINTFUNCTION(FunType, N);
    WRITELN;
    WRITELN('It took ', Count : 2, ' iterations to find an approximation for the local minimum.');
    WRITELN;
    for K := 1 to N do
      WRITELN('P(', K : 1, ')  = ', Pmin[K] : 15 : 7);
    WRITELN;
    WRITELN('  DP  = ', H : 15 : 7, '  is an estimate for the accuracy.');
    WRITELN;
    WRITELN('The minimum value of the function is:');
    WRITELN;
    WRITELN('F(P)  = ', Ymin : 15 : 7);
    WRITELN;
    WRITELN('  DF  = ', Error : 15 : 7, '  is an estimate for the accuracy.');
    WRITELN;
    case Cond of
      0: 
        begin
          WRITELN('Convergence is doubtful because the');
          WRITELN('maximum number of iterations was exceeded.');
        end;
      1: 
        begin
          WRITELN('Convergence has been achieved because ');
          WRITELN('consecutive abscissas are closer than ', H : 15 : 7);
        end;
      2: 
        begin
          WRITELN('Convergence has been achieved because ');
          WRITELN('consecutive ordinates are closer than ', Error : 15 : 7);
        end;
      3: 
        begin
          WRITELN('Convergence has been achieved because');
          WRITELN('the consecutive abscissas are closer than ', H : 15 : 7);
          WRITELN('and consecutive ordinates are closer than ', Error : 15 : 7);
        end;
      4: 
        begin
          WRITELN('Convergence is doubtful because division by zero was encountered.');
          if Error < Epsilon / 100 then
            WRITELN('However, the consecutive ordinates are close.');
        end;
      5: 
        begin
          WRITELN('Convergence is doubtful,because H is too large, H = ', H : 15 : 7);
          WRITELN('It is possible that there is no local minimum.');
        end;
    end;
  end;

  procedure MESSAGE;
  begin
    CLRSCR;
    WRITELN;
    WRITELN;
    WRITELN('                       GRADIENT SEARCH FOR A MINIMUM');
    WRITELN;
    WRITELN;
    WRITELN('          The gradient method or "method of steepest descent method"');
    WRITELN;
    WRITELN('     is used to find a local minimum of the function:');
    WRITELN;
    WRITELN;
    WRITELN('              f(p ,p ,...,p )');
    WRITELN('                 1  2      N');
    WRITELN;
    WRITELN('     where V = (p ,p ,...,p )  is a vector of dimension (and N<=6).');
    WRITELN('                 1  2      N');
    WRITELN;
    WRITELN;
    WRITELN('          To start the method one point must be given. Then a sequence');
    WRITELN;
    WRITELN('     of points is generated which converges to the local minimum.');
    WRITELN;
    WRITELN;
    WRITELN;
    WRITE('                       Press the <ENTER> key.  ');
    READLN(Ans);
    WRITELN;
    CLRSCR;
    WRITELN('             Suppose that P  has been obtained. ');
    WRITELN('                           k');
    WRITELN;
    WRITELN('     Step 1. Evaluate the gradient vector  G = grad f(P ).');
    WRITELN('                                                       k ');
    WRITELN;
    WRITELN('     Step 2. Compute the search direction  S = -G/||G||.');
    WRITELN;
    WRITELN;
    WRITELN('     Step 3. Perform a single parameter minimization of');
    WRITELN;
    WRITELN('             z(h) = f(P + hS)  and get  h   .');
    WRITELN('                       k                 min');
    WRITELN;
    WRITELN('     Step 4. Construct the next point P    = P  + h   S');
    WRITELN('                                       k+1    k    min');
    WRITELN;
    WRITELN('     Step 5. Perform the termination test.');
    WRITELN;
    WRITELN;
    WRITELN('             Repeat the process if necessary.');
    WRITELN;
    WRITELN;
    WRITE('             Press the <ENTER> key.  ');
    READLN(Ans);
    WRITELN;
    CLRSCR;
    WRITELN('        Choose how you want to input the coordinates of the vector.');
    WRITELN;
    WRITELN;
    WRITELN;
    WRITELN('    <1> Enter the coefficients on one line separated by spaces, i.e.');
    WRITELN;
    WRITELN('        p   p   ...  p  ');
    WRITELN('         1   2        N ');
    WRITELN;
    WRITELN('    <2> Enter each coefficient on a separate line, i.e.');
    WRITELN;
    WRITELN('        p  ');
    WRITELN('         1 ');
    WRITELN('        p  ');
    WRITELN('         2 ');
    WRITELN('        .  ');
    WRITELN('        :  ');
    WRITELN('        p      for j=1,2,...,N');
    WRITELN('         N ');
    WRITELN;
    WRITELN;
    Mess := '        SELECT <1 - 2> ?  ';
    InRC := 2;
    WRITE(Mess);
    READLN(InRC);
    if (InRC <> 1) and (InRC <> 2) then
      InRC := 2;
    CLRSCR;
    WRITELN;
    WRITELN('     For convenience, the function f(P) is written using the');
    WRITELN;
    WRITELN('     variables x = p , y = p , z = p , u = p , v = p , w = p .');
    WRITELN('                    1       2       3       4       5       6 ');
    WRITELN;
    WRITELN;
    WRITELN('     That is, f(x,y)');
    WRITELN;
    WRITELN('          or  f(x,y,z)');
    WRITELN;
    WRITELN('          or  f(x,y,z,u)');
    WRITELN('                   .');
    WRITELN('                   :');
    WRITELN('          or  f(x,y,z,u,v,w)');
    WRITELN;
    WRITELN;
    WRITELN;
    WRITE('     Press the <ENTER> key.  ');
    READLN(Ans);
    WRITELN;
  end;

  procedure PRINTAPPROXS;
    var
      J: integer;
  begin
    CLRSCR;
    WRITELN;
    WRITELN('For each k, the coordinates of the current approximation are:');
    WRITELN;
    if N = 2 then
      begin
        WRITELN('            (k)  (k)               (k)  (k)                      ');
        WRITELN('  k       (v   ,v   )            (v   ,v   )             f(V ) ');
        WRITELN('            1,1  1,2               2,1  2,2                 k    ');
        WRITELN('-------------------------------------------------------------------------');
      end;
    if N = 3 then
      begin
        WRITELN('            (k)  (k)  (k)          (k)  (k)  (k)          (k)  (k)  (k) ');
        WRITELN('  k       (v   ,v   ,v   )       (v   ,v   ,v   )       (v   ,v   ,v   )');
        WRITELN('            1,1  1,2  1,3          2,1  2,2  2,3          3,1  3,2  3,3 ');
        WRITELN('------------------------------------------------------------------------');
      end;
    for J := 0 to Count do
      begin
        WRITELN;
        if N = 2 then
          WRITELN(' ', J : 2, '     ', VA[J, 1] : 15 : 7, '     ', VA[J, 2] : 15 : 7, '     ', VY[J] : 15 : 7);
        if N = 3 then
          WRITELN(' ', J : 2, '     ', VA[J, 1] : 15 : 7, '     ', VA[J, 2] : 15 : 7, '     ', VA[J, 3] : 15 : 7);
        if (J mod 11 = 8) and (J <> Count) then
          begin
            WRITELN;
            WRITE('                  Press the <ENTER> key.  ');
            READLN(Ans);
            WRITELN;
          end;
      end;
    WRITELN;
    WRITE('                  Press the <ENTER> key.  ');
    READLN(Ans);
    WRITELN;
    WRITELN;
  end;

begin                                            {Begin Main Program}
  MESSAGE;
  DoMo := Go;
  while DoMo = Go do
    begin
      GETFUN(FunType, InRC, N);
      Stat := Working;
      while Stat = Working do
        begin
          GETPOINT(P0, Y0, N);
          GRADSEARCH(P0, P1, P2, Pmin, S, N, Jmax, Max, Delta, Epsilon, H, Ymin, Error, Cond, Count);
          OUTPUT(P0, Pmin, N, H, Ymin, Error, Epsilon, Cond, Count);
          WRITELN;
          WRITE('Do you want to see all the approximations ?  <Y/N>  ');
          READLN(Ans);
          WRITELN;
          if (Ans = 'Y') or (Ans = 'y') then
            PRINTAPPROXS
          else
            WRITELN;
          WRITE('Do you want  to try a new  starting point ?  <Y/N>  ');
          READLN(Ans);
          WRITELN;
          if (Ans <> 'Y') and (Ans <> 'y') then
            Stat := Done;
        end;
      WRITELN;
      WRITE('Want  to  minimize  a different  function ?  <Y/N>  ');
      READLN(Ans);
      WRITELN;
      if (Ans <> 'Y') and (Ans <> 'y') then
        DoMo := Stop;
    end;
end.                                            {End of Main Program}

