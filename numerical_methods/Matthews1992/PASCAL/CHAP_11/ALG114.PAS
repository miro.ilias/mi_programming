program HOUSEHOLDER_METHOD;
{--------------------------------------------------------------------}
{  Alg11'4.pas   Pascal program for implementing Algorithm 11.4      }
{                                                                    }
{  NUMERICAL METHODS: Pascal Programs, (c) John H. Mathews 1995      }
{  To accompany the text:                                            }
{  NUMERICAL METHODS for Math., Science & Engineering, 2nd Ed, 1992  }
{  Prentice Hall, Englewood Cliffs, New Jersey, 07632, U.S.A.        }
{  Prentice Hall, Inc.; USA, Canada, Mexico ISBN 0-13-624990-6       }
{  Prentice Hall, International Editions:   ISBN 0-13-625047-5       }
{  This free software is compliments of the author.                  }
{  E-mail address:       in%"mathews@fullerton.edu"                  }
{                                                                    }
{  Algorithm 11.4 (Reduction to Tridiagonal Form).                   }
{  Section   11.4, Eigenvalues of Symmetric Matrices, Page 581       }
{--------------------------------------------------------------------}

  uses
    crt;

  const
    MaxR = 10;
    MaxS = 50;
  type
    SubS = 1..MaxR;
    VECTOR = array[SubS] of real;
    MATRIX = array[SubS, SubS] of real;
    LETTER = string[4];
    LETTERS = string[200];
    Status = (Done, Working);
    DoSome = (Go, New, Stop);
    MatType = (LowerT, Square, UpperT);
    Process = (Auto, Observe);

  var
    CountR, CountS, I, InRC, Inum, K, N, Sub: integer;
    C, MaxA, Rnum, R, S, T: real;
    W, V, Q: VECTOR;
    A, A1: MATRIX;
    Ach, Ans: LETTER;
    Mess: LETTERS;
    Stat: Status;
    DoMo: DoSome;
    Mtype: MatType;
    Proc: Process;

  procedure Aoutput (A: MATRIX; K, N: integer);
    var
      Digits, Mdigits, C, R: integer;
      Log10: real;
  begin
    Log10 := LN(10);
    if K = 0 then
      WRITELN('The Matrix  A   is:')
    else
      WRITELN('The Matrix  A   is:');
    WRITELN('             ', K);
    for R := 1 to N do
      begin
        WRITELN;
        for C := 1 to N - 1 do
          begin
            Digits := 7;
            if A[R, C] <> 0 then
              Mdigits := 1 + TRUNC(LN(ABS(A[R, C])) / Log10);
            if A[R, C] < 0 then
              Mdigits := Mdigits + 1;
            if Mdigits < 7 then
              Mdigits := 7;
            Digits := 14 - Mdigits;
            WRITE(A[R, C] : 15 : Digits, ' ');
          end;
        Digits := 7;
        if A[R, N] <> 0 then
          Mdigits := 1 + TRUNC(LN(ABS(A[R, N])) / Log10);
        if A[R, N] < 0 then
          Mdigits := Mdigits + 1;
        if Mdigits < 7 then
          Mdigits := 7;
        Digits := 14 - Mdigits;
        WRITE(A[R, N] : 15 : Digits);
        if N > 5 then
          WRITELN;
      end;
    WRITELN;
  end;                                       {End of procedure Aoutput}

  procedure StepOutput (W, V, Q: VECTOR;      {Print intermediate step}
                  S, R, C: real; N: integer);
    var
      J: integer;
  begin
    WRITELN;
    WRITELN('      Constant S              Constant R              Constant C');
    WRITELN(S : 15 : 7, '         ', R : 15 : 7, '         ', C : 15 : 7);
    WRITELN;
    WRITELN('      Vector W                Vector V                Vector Q');
    for J := 1 to N do
      WRITELN(W[J] : 15 : 7, '         ', V[J] : 15 : 7, '         ', Q[J] : 15 : 7);
  end;                                     {End of procedure Aoutput}

  procedure HOUSEHOLDER (var A: MATRIX; var W, V, Q: VECTOR; var S, R, C: real; N: integer);
    var
      K: integer;

    procedure Form_W (var W: VECTOR;         {Construct the vector W}
                    A: MATRIX; var S, R: real; K, N: integer);
      var
        J: integer;
        Sum: real;
    begin
      Sum := 0;
      for J := K + 1 to N do
        Sum := Sum + SQR(A[J, K]);
      S := SQRT(Sum);
      if A[K + 1, K] < 0 then
        S := -S;
      R := SQRT(2 * (A[K + 1, K] + S) * S);
      for J := 1 to K do                {First k elements are 0}
        W[J] := 0;
      W[K + 1] := (A[K + 1, K] + S) / R;
      for J := K + 2 to N do
        W[J] := A[J, K] / R;
    end;                                  {End of procedure Form_W}

    procedure Form_V (var V: VECTOR;         {Construct the vector V}
                    A: MATRIX; W: VECTOR; K, N: integer);
      var
        I, J: integer;
        Sum: real;
    begin
      for I := 1 to K do               {First k elements are 0}
        V[I] := 0;
      for I := K + 1 to N do
        begin
          Sum := 0;
          for J := K + 1 to N do
            Sum := Sum + A[I, J] * W[J];
          V[I] := Sum;
        end;
    end;                                  {End of procedure Form_V}

    procedure Form_Q (var Q: VECTOR;         {Construct the vector Q}
                    V, W: VECTOR; var C: real; K, N: integer);
      var
        J: integer;
    begin
      C := 0;
      for J := K + 1 to N do
        C := C + W[J] * V[J];
      for J := 1 to K do               {First k elements are 0}
        Q[J] := 0;
      for J := K + 1 to N do
        Q[J] := V[J] - C * W[J];
    end;                                  {End of procedure Form_Q}

    procedure Form_A (var A: MATRIX;         {Construct the matrix A}
                    Q, W: VECTOR; S: real; K, N: integer);
      var
        I, J: integer;
    begin
      for J := K + 2 to N do
        begin
          A[J, K] := 0;
          A[K, J] := 0;
        end;
      A[K + 1, K] := -S;
      A[K, K + 1] := -S;
      for J := K to N do
        A[J, J] := A[J, J] - 4 * Q[J] * W[J];
      for I := K + 1 to N do
        begin
          for J := I + 1 to N do
            begin
              A[I, J] := A[I, J] - 2 * W[I] * Q[J] - 2 * Q[I] * W[J];
              A[J, I] := A[I, J];
            end;
        end;
    end;                                  {End of procedure Form_A}

  begin                              {Start of PROCEDURE HOUSEHOLDER}
    for K := 1 to N - 2 do
      begin
        Form_W(W, A, S, R, K, N);
        Form_V(V, A, W, K, N);
        Form_Q(Q, V, W, C, K, N);
        Form_A(A, Q, W, S, K, N);
        if Proc = Observe then
          begin
            CLRSCR;
            StepOutput(W, V, Q, S, R, C, N);
            Aoutput(A, K, N);
            WRITELN;
            WRITELN;
            WRITE('      Press the <ENTER> key.  ');
            READLN(ANS);
          end;
      end;
  end;                                 {End of PROCEDURE HOUSEHOLDER}

  procedure INPUTMATRIX (var Ach: LETTER; var A, A1: MATRIX; N, InRC: integer);
    var
      Count, C, CL, CU, K, R, RL, RU: integer;
      Z: VECTOR;
  begin
    for R := 1 to N do
      begin
        for C := 1 to N do
          begin
            A[R, C] := 0;
            A1[R, C] := A[R, C];
          end;
      end;
    WRITELN('     Input the elements of the ', N : 1, ' by ', N : 1, ' coefficient matrix  ', Ach);
    RL := 1;
    RU := N;
    CL := 1;
    CU := N;
    if (Mtype = LowerT) and (InRC = 1) then
      begin
        for R := 1 to N do
          begin
            WRITELN;
            case R of
              1:
                WRITELN('ENTER   A[1,1]');
              2:
                WRITELN('ENTER   A[2,1]   A[2,2]   on one row');
              3:
                WRITELN('ENTER   A[3,1]   A[3,2]   A[3,3]   on one row');
              else
                WRITELN('ENTER   A[', R : 1, ',1]   A[', R : 1, ',2]  ...  A[', R : 1, ',', R : 1, ']   on one row');
            end;
            WRITELN;
            for K := 1 to R do
              Z[K] := 0;
            case R of
              1: 
                READLN(Z[1]);
              2: 
                READLN(Z[1], Z[2]);
              3: 
                READLN(Z[1], Z[2], Z[3]);
              4: 
                READLN(Z[1], Z[2], Z[3], Z[4]);
              5: 
                READLN(Z[1], Z[2], Z[3], Z[4], Z[5]);
              6: 
                READLN(Z[1], Z[2], Z[3], Z[4], Z[5], Z[6]);
              7: 
                READLN(Z[1], Z[2], Z[3], Z[4], Z[5], Z[6], Z[7]);
              8: 
                READLN(Z[1], Z[2], Z[3], Z[4], Z[5], Z[6], Z[7], Z[8]);
              9: 
                READLN(Z[1], Z[2], Z[3], Z[4], Z[5], Z[6], Z[7], Z[8], Z[9]);
              10: 
                READLN(Z[1], Z[2], Z[3], Z[4], Z[5], Z[6], Z[7], Z[8], Z[9], Z[10]);
            end;
            for C := 1 to R do
              begin
                A[R, C] := Z[C];
                A1[R, C] := A[R, C];
              end;
          end;
      end;
    if (Mtype = UpperT) and (InRC = 1) then
      begin
        for R := 1 to N do
          begin
            WRITELN;
            case R of
              1: 
                WRITELN('ENTER   A[1,1]   A[1,2]  ...  A[1,', N : 1, ']   on one row');
              2: 
                WRITELN('ENTER   A[2,2]   A[2,3]  ...  A[2,', N : 1, ']   on one row');
              else
                WRITELN('ENTER   A[', R : 1, ',', R : 1, ']   A[',R:1,',',R+1:1,']  ...  A[',R:1,',',N:1, ']   on one row');
            end;
            WRITELN;
            for K := 1 to N do
              Z[K] := 0;
            case N - R + 1 of
              1: 
                READLN(Z[1]);
              2: 
                READLN(Z[1], Z[2]);
              3: 
                READLN(Z[1], Z[2], Z[3]);
              4: 
                READLN(Z[1], Z[2], Z[3], Z[4]);
              5: 
                READLN(Z[1], Z[2], Z[3], Z[4], Z[5]);
              6: 
                READLN(Z[1], Z[2], Z[3], Z[4], Z[5], Z[6]);
              7: 
                READLN(Z[1], Z[2], Z[3], Z[4], Z[5], Z[6], Z[7]);
              8: 
                READLN(Z[1], Z[2], Z[3], Z[4], Z[5], Z[6], Z[7], Z[8]);
              9: 
                READLN(Z[1], Z[2], Z[3], Z[4], Z[5], Z[6], Z[7], Z[8], Z[9]);
              10: 
                READLN(Z[1], Z[2], Z[3], Z[4], Z[5], Z[6], Z[7], Z[8], Z[9], Z[10]);
            end;
            for C := R to N do
              begin
                A[R, C] := Z[C - R + 1];
                A1[R, C] := A[R, C];
              end;
          end;
      end;
    if (InRC = 1) and (Mtype <> LowerT) and (Mtype <> UpperT) then
      begin
        for R := 1 to N do
          begin
            WRITELN;
            WRITELN('ENTER all the coefficients of row ', R, ' on one row');
            WRITELN;
            for K := 1 to N do
              Z[K] := 0;
            case N of
              1: 
                READLN(Z[1]);
              2: 
                READLN(Z[1], Z[2]);
              3: 
                READLN(Z[1], Z[2], Z[3]);
              4: 
                READLN(Z[1], Z[2], Z[3], Z[4]);
              5: 
                READLN(Z[1], Z[2], Z[3], Z[4], Z[5]);
              6: 
                READLN(Z[1], Z[2], Z[3], Z[4], Z[5], Z[6]);
              7: 
                READLN(Z[1], Z[2], Z[3], Z[4], Z[5], Z[6], Z[7]);
              8: 
                READLN(Z[1], Z[2], Z[3], Z[4], Z[5], Z[6], Z[7], Z[8]);
              9: 
                READLN(Z[1], Z[2], Z[3], Z[4], Z[5], Z[6], Z[7], Z[8], Z[9]);
              10: 
                READLN(Z[1], Z[2], Z[3], Z[4], Z[5], Z[6], Z[7], Z[8], Z[9], Z[10]);
            end;
            for C := 1 to N do
              begin
                A[R, C] := Z[C];
                A1[R, C] := A[R, C];
              end;
          end;
      end;
    if (InRC = 2) then
      begin
        for R := 1 to N do
          begin
            WRITELN;
            WRITELN('     ENTER the coefficients of row ', R);
            WRITELN;
            if Mtype = LowerT then
              CU := R;
            if Mtype = UpperT then
              CL := R;
            for C := CL to CU do
              begin
                WRITE('     A(', R, ',', C, ') = ');
                READLN(A[R, C]);
                A1[R, C] := A[R, C];
              end;
          end;
      end;
    if (InRC = 3) then
      begin
        for C := 1 to N do
          begin
            WRITELN;
            WRITELN('     ENTER the coefficients of column ', C);
            WRITELN;
            if Mtype = LowerT then
              RL := C;
            if Mtype = UpperT then
              RU := C;
            for R := RL to RU do
              begin
                WRITE('     A(', R, ',', C, ') = ');
                READLN(A[R, C]);
                A1[R, C] := A[R, C];
              end;
          end;
      end;
    if Mtype = LowerT then
      begin
        for R := 1 to N do
          for C := 1 to R do
            begin
              A[C, R] := A[R, C];
              A1[C, R] := A[C, R];
            end;
      end;
    if Mtype = UpperT then
      begin
        for R := 1 to N do
          for C := R to N do
            begin
              A[C, R] := A[R, C];
              A1[C, R] := A[C, R];
            end;
      end;
    Mtype := Square;
  end;                                   {End of procedure INPUTMATRIX}

  procedure REFRESH (var A: MATRIX; A1: MATRIX; N: integer);
    var
      C, R: integer;
  begin
    for R := 1 to N do
      begin
        for C := 1 to N do
          begin
            A[R, C] := A1[R, C];
          end;
      end;
  end;

  procedure PrintResults (A, A1: MATRIX; N: integer);
    var
      C, R: integer;
      A0: real;
  begin
    CLRSCR;
    WRITELN;
    WRITELN;
    WRITELN('The matrix  A  is:');
    for R := 1 to N do
      begin
        WRITELN;
        for C := 1 to N - 1 do
          WRITE(A1[R, C] : 15 : 8, ' ');
        WRITE(A1[R, N] : 15 : 8);
        if N > 5 then
          WRITELN;
      end;
    WRITELN;
    WRITELN;
    WRITELN;
    WRITELN('The similar tridiagonal matrix is:');
    for R := 1 to N do
      begin
        WRITELN;
        for C := 1 to N - 1 do
          WRITE(A[R, C] : 15 : 8, ' ');
        WRITE(A[R, N] : 15 : 8);
        if N > 5 then
          WRITELN;
      end;
    WRITELN;
  end;                                {End of procedure PrintResults}

  procedure CHANGEMATRIX (Ach: LETTER; var A, A1: MATRIX; N: integer);
    type
      STATUS = (Bad, Enter, Done);
      LETTER = string[1];
    var
      Count, C, I, J, K, R: integer;
      Valu: real;
      Resp: LETTER;
      Stat: STATUS;
  begin
    Stat := Enter;
    while (Stat = Enter) or (Stat = Bad) do
      begin
        CLRSCR;
        Aoutput(A1, 0, N);
        WRITELN;
        Stat := Enter;
        for I := 1 to N do
          for J := I + 1 to N do
            begin
              if A[I, J] <> A[J, I] then
                begin
                  R := I;
                  C := J;
                  Stat := Bad;
                end;
            end;
        if (Stat = Bad) then
          begin
            WRITELN('The matrix is NOT symmetric, you must change an element.');
          end;
        if (Stat <> Bad) then
          begin
            WRITE('Do you want to make a change in the matrix ? <Y/N> ');
            READLN(Resp);
          end;
        if (Resp = 'Y') or (Resp = 'y') or (Stat = Bad) then
          begin
            WRITELN;
            WRITELN('     To change a coefficient select');
            case N of
              2: 
                begin
                  WRITELN('        the row    R = 1,2');
                  WRITELN('        and column C = 1,2');
                end;
              3: 
                begin
                  WRITELN('        the row    R = 1,2,3');
                  WRITELN('        and column C = 1,2,3');
                end;
              else
                begin
                  WRITELN('        the row    R = 1,2,...,', N : 2);
                  WRITELN('        and column C = 1,2,...,', N : 2);
                end;
            end;
            WRITELN;
            WRITE('     ENTER the row R = ');
            READLN(R);
            WRITE('     ENTER column  C = ');
            READLN(C);
            if (1 <= R) and (R <= N) and (1 <= C) and (C <= N) then
              begin
                WRITELN;
                WRITELN('     The current value is   A(', R, ',', C, ')  =', A[R, C]);
                if A[R, C] <> A[C, R] then
                  begin
                    WRITELN('     Which is NOT equal to  A(', C, ',', R, ')  =', A[C, R]);
                    WRITELN('     The computer will set  A(', R, ',', C, ')  =  A(', C, ',', R, ')');
                    WRITELN;
                  end;
                WRITE('     ENTER the  NEW  value  A(', R, ',', C, ') = ');
                READLN(A[R, C]);
                A1[R, C] := A[R, C];
                A[C, R] := A[R, C];
                A1[C, R] := A[C, R];
              end;
          end
        else
          Stat := Done;
      end;
  end;

  procedure MESSAGE (var InRC: integer; var Mtype: MatType);
    var
      I: integer;
      Ans: LETTER;
  begin
    CLRSCR;
    WRITELN('                      HOUSEHOLDER`S  METHOD');
    WRITELN;
    WRITELN;
    WRITELN('     Assume that A is an N by N real symmetric matrix.  Then A is similar to');
    WRITELN;
    WRITELN;
    WRITELN('a tridiagonal matrix. Starting with  A  = A, Householder`s method constructs');
    WRITELN('                                      0 ');
    WRITELN;
    WRITELN('a sequence of orthogonal symmetric matrices {P } such that A  = P  A    P  ,');
    WRITELN('                                              k             k    k  k-1  k  ');
    WRITELN;
    WRITELN('for  k = 1,2,...,N-2, then  A    is the desired tridiagonal matrix.');
    WRITELN('                             N-2 ');
    WRITELN;
    WRITELN;
    WRITE('                      Press the  <ENTER>  key.  ');
    READLN(Ans);
    CLRSCR;
    for I := 1 to 5 do
      WRITELN;
    WRITELN('     Now you must choose how the symmetric matrix A will be input.');
    WRITELN;
    WRITELN('You can enter all the elements or only the lower or upper portion.?');
    WRITELN;
    WRITELN('If you enter a portion of the matrix then the other elements will');
    WRITELN;
    WRITELN('be computed by symmetry.');
    WRITELN;
    WRITELN;
    WRITELN('     < 1 > Enter the complete  N by N  symmetric matrix.');
    WRITELN;
    WRITELN;
    WRITELN('     < 2 > Enter the lower-triangular portion of the matrix.');
    WRITELN;
    WRITELN;
    WRITELN('     < 3 > Enter the upper-triangular portion of the matrix.');
    WRITELN;
    WRITELN;
    WRITE('           SELECT your choice for input  < 1 - 3 > ? ');
    I := 1;
    READLN(I);
    if (I < 1) or (3 < I) then
      I := 1;
    if I = 1 then
      Mtype := Square;
    if I = 2 then
      Mtype := LowerT;
    if I = 3 then
      Mtype := UpperT;
    CLRSCR;
    WRITELN;
    WRITELN('        Choose how you want to input the elements of the matrix.');
    WRITELN;
    WRITELN('    <1> Enter the elements of each row on one line separated by spaces, i.e.');
    WRITELN;
    WRITELN('        A(J,1)  A(J,2)  ...  A(J,N)           for J=1,2,...,N');
    WRITELN;
    WRITELN('    <2> Enter each element of a row on a separate line, i.e.');
    WRITELN;
    WRITELN('        A(J,1)');
    WRITELN('        A(J,2)');
    WRITELN('           .');
    WRITELN('           :');
    WRITELN('        A(J,N)     for J=1,2,...,N');
    WRITELN;
    WRITELN('    <3> Enter each element of a column on a separate line, i.e.');
    WRITELN;
    WRITELN('        A(1,K)');
    WRITELN('        A(2,K)');
    WRITELN('           .');
    WRITELN('           :');
    WRITELN('        A(N,K)     for K=1,2,...,N');
    WRITELN;
    WRITE('        SELECT <1 - 3> ? ');
    InRC := 3;
    READLN(InRC);
    if (InRC <> 1) and (InRC <> 2) and (InRC <> 3) then
      InRC := 2;
  end;                                  {End of procedure MESSAGE}

  procedure INPUTS (var A, A1: MATRIX; var N, InRC: integer);
    var
      C, I, R: integer;
  begin
    CLRSCR;
    WRITELN('    We will now proceed with Householder`s method of iteration to find the');
    WRITELN;
    WRITELN('tridiagonal matrix  A    that is similar to  A.');
    WRITELN('                     N-2 ');
    WRITELN;
    WRITELN('           A  must be a symmetric matrix of dimension  N by N.');
    WRITELN;
    WRITELN('          {N  must be an integer between 1 and 10}');
    WRITELN;
    WRITE('    ENTER  N  = ');
    N := 2;
    READLN(N);
    if (N < 1) then
      N := 1;
    if (N > 10) then
      N := 10;
    CLRSCR;
    Ach := 'A';
    INPUTMATRIX(Ach, A, A1, N, InRC);
  end;                                   {End of procedure INPUTS}

  procedure PROCESSES;
    var
      I: integer;
  begin
    CLRSCR;
    for I := 1 to 5 do
      WRITELN;
    WRITELN('          To what extent do you want to control the program?');
    WRITELN;
    WRITELN;
    WRITELN('          < 1 > The computer does it all automatically.');
    WRITELN;
    WRITELN;
    WRITELN('          < 2 > As the computer works, we observe each step.');
    WRITELN;
    WRITELN;
    WRITE('          SELECT your choice for input  < 1 - 2 > ? ');
    I := 1;
    READLN(I);
    if (I < 1) or (2 < I) then
      I := 2;
    if I = 1 then
      Proc := Auto;
    if I = 2 then
      Proc := Observe;
  end;

begin                                            {Begin Main Program}
  MESSAGE(InRC, Mtype);
  DoMo := Go;
  while (DoMo = Go) or (DoMo = New) do
    begin
      if DoMo = Go then
        INPUTS(A, A1, N, InRC)
      else
        begin
          WRITELN;
          WRITE('Want a completely new matrix ? <Y/N> ');
          READLN(Ans);
          if (Ans = 'Y') or (Ans = 'y') then
            INPUTS(A, A1, N, InRC)
          else
            REFRESH(A, A1, N);
          WRITELN;
        end;
      CHANGEMATRIX(Ach, A, A1, N);
      PROCESSES;
      HOUSEHOLDER(A, W, V, Q, S, R, C, N);
      CLRSCR;
      PrintResults(A, A1, N);
      WRITELN;
      WRITE('Want  to tyr  a new matrix ? <Y/N> ');
      READLN(Ans);
      if (Ans = 'Y') or (Ans = 'y') then
        begin
          DoMo := New;
          WRITELN;
        end
      else
        DoMo := Stop;
    end;
end.                                            {End of Main Program}

