/*
---------------------------------------------------------------------------
 Algo2-10.c   C program for implementing Algorithm 2.10
 Algorithm translated to  C  by: Dr. Norman Fahrer
 IBM and Macintosh verification by: Daniel Mathews
 
 NUMERICAL METHODS: C Programs, (c) John H. Mathews 1995
 To accompany the text:
 NUMERICAL METHODS for Mathematics, Science and Engineering, 2nd Ed, 1992
 Prentice Hall, Englewood Cliffs, New Jersey, 07632, U.S.A.
 Prentice Hall, Inc.; USA, Canada, Mexico ISBN 0-13-624990-6
 Prentice Hall, International Editions:   ISBN 0-13-625047-5
 This free software is compliments of the author.
 E-mail address:       in%"mathews@fullerton.edu"
 
 Algorithm 2.10 (Newton-Raphson Method in 2-Dimensions).
 Section   2.7,  Newton's Method for Systems, Page 116
---------------------------------------------------------------------------
*/
/*
---------------------------------------------------------------------------

 Algorithm 2.10 (Newton-Raphson Method in Two Dimensions).
 To solve

              0 = f_1 (x,y)
              0 = f_2 (x,y)

 given one initial approximation  (p_0,q_0)  and using
 Newton-Raphson iteration.

---------------------------------------------------------------------------
*/

/* User has to supply SIX functions named :
   f1function, f2function and the derivatives
   d11function, d12function, d21function, d22function.

   An example is included in this program : page 114/115
   example 2.20    */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define Max     99          //  Maximum Number of iterations


/*  define prototype for USER-SUPPLIED functions  */

    double f1function(double x, double y);
    double f2function(double x, double y);
    double d11function(double x, double y);
    double d12function(double x, double y);
    double d21function(double x, double y);
    double d22function(double x, double y);

/*  EXAMPLE for "f1function" :  */
    double f1function(double x, double y)

    {
         return ( x * x - 2.0 * x - y + 0.5 );
    }
/*  EXAMPLE for "f2function" :  */
    double f2function(double x, double y)

    {
         return ( x * x + 4.0 * y * y - 4.0 );
    }
/*  EXAMPLE for "d11function" : df1/dx = D(1,1)  */
    double d11function(double x, double y)

    {
         return ( 2.0 * x - 2.0 );
    }

/*  EXAMPLE for "d21function" : df2/dx = D(2,1) */
    double d21function(double x, double y)

    {
         return ( 2.0 * x );
    }
/*  EXAMPLE for "d12function" : df1/dy = D(1,2) */
    double d12function(double x, double y)

    {
         return ( -1.0 );
    }

/*  EXAMPLE for "d22function" : df2/dy = D(2,2) */
    double d22function(double x, double y)

    {
         return ( 8.0 * y );
    }

/* -------------------------------------------------------- */

/*  Main program for algorithm 2.10  */

    int  main(void)

{
    int Cond = 0;                 /* Initialize                   */
    double Delta   = 1E-5;        /* Tolerance                    */
    double Epsilon = 1E-5;        /* Tolerance                    */
    double Small   = 1E-5;        /* Tolerance                    */
    double Inf = 1E+100;          /* Maximum for Sep              */
    int K = 0;                    /* Loop-counter                 */
    double P0, Q0;                /* INPUT: Initial values        */
    double DP, Det, DQ, P1, U0, U1, V0, V1, Q1 ;
    double D11, D12, D21, D22;    /* partial derivatives          */
    double FnZero;
    double RelErr;                /* Relative error */

    double P[Max], Q[Max], R[Max];

    printf("-----------------------------------------------------\n");
    printf("----Newton-Raphson Method in Two Dimensions----------\n");
    printf("----Example 2.20 from book on page 114---------------\n");
    printf("-----------------------------------------------------\n");
    printf("Please enter initial approximations close to solution!\n");
    printf("For this EXAMPLE type : 2.0 0.25 \n");
    scanf("%lf %lf",&P0, &Q0);
    printf("-----------------------------------------------------\n");
    printf("The initial approximation are : %lf %lf\n",P0, Q0);

    U0 = f1function(P0,Q0);
    V0 = f2function(P0,Q0);

    for (K = 1; K <= Max; K++) {

        if(Cond != 0) break;

        D11 = d11function(P0,Q0);   /* Compute partial derivatives */
        D12 = d12function(P0,Q0);
        D21 = d21function(P0,Q0);
        D22 = d22function(P0,Q0);

        Det = D11 * D22 - D12 * D21; /* Compute the determinant  */

        if (Det == 0) {              /* Check division by zero   */
            DP = 0;
            DQ = 0;
            Cond = 1;
        }
        else {                       /* Solve the linear system  */

            DP = (U0 * D22 - V0 * D12) /Det;
            DQ = (V0 * D11 - U0 * D21) /Det;
        }

        P1 = P0 - DP;             /* New iterates and function values */
        Q1 = Q0 - DQ;
        U1 = f1function(P1,Q1);
        V1 = f2function(P1,Q1);

        if( (U1 > Inf) || (V1 > Inf) ) {
            printf("Function values become unreasonably high !\n");
            exit(0);
        }

        RelErr = ( fabs(DP) + fabs(DQ) ) / ( fabs(P1) + fabs(Q1) + Small );
        FnZero = fabs(U1) + fabs(V1);

        if ( (RelErr < Delta) && (FnZero < Epsilon) ) { /* Check for   */
            if (Cond != 1) Cond = 2;                    /* convergence */
        }

        P0 = P1;     /* Update values  */
        Q0 = Q1;
        U0 = U1;
        V0 = V1;
    }                /* End of for-loop */


    printf("------------------------------------------------------\n");
    printf("The current %d - th iterate is: %lf %lf\n",K-1,P1,Q1);
    printf("The function values are : %lf %lf\n", U1, V1);
    printf("------------------------------------------------------\n");

if(Cond == 0) printf("The max. number of iterations was exceeded!\n");
if(Cond == 1) printf("Division by zero was encountered !\n");
if(Cond == 2) printf("The solution was found with the desired tolerance !\n");

}
   /* End of main program */

