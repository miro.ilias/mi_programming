#include <stdio.h>
//#include <conio.h>
#include <stdlib.h>
#include <iostream.h>

int fn[10];
int fdashn[10];
int n,i;
double x0;
double fnval,fdashnval;

double power(double a,int b)
{
	int i;
	double c=1;
	for (i=0;i<b;i++) c=c*a;
	return c;
}

void getfunction()
{
	int i;
	printf("Enter the degree of the function: ");
	cin >> n;
	printf("\nStart entering the coefficients starting from the constant term\n\n");
	for (i=0;i<n+1;i++)
	{
		printf("Enter coefiicient of degree %d: ",i);
		cin >> fn[i];
	}
}

void displayfunction()
{
	int i;
	printf("\nCurrent function is:\n ");
	for (i=n;i>-1;i--)
	{
		if (i==0) printf("%d",fn[i]);
		else if (fn[i]==0) printf("");
		else printf("%dx^%d + ",fn[i],i);
	}
}

int getiterations()
{
	int no;
	printf("\nEnter The nember of iteratins: ");
	cin >> no;
	return no;
}

double fx(double x)
{
	int i;
	double ans=0;
	for (i=n;i>-1;i--)
	{
		ans=ans+fn[i]*power(x,i);
	}
	return ans;
}

void getderivative()
{
	int i;

	for (i=1;i<n+1;i++)
	{
		fdashn[i-1]=fn[i]*i;
	}
}

void displayderivative()
{
	int i;
	printf("\nCurrent function derivative is:\n ");
	for (i=n-1;i>-1;i--)
	{
		if (i==0) printf("%d",fdashn[i]);
		else if (fdashn[i]==0) printf("");
		else printf("%dx^%d + ",fdashn[i],i);
	}
}

double fdashx(double x)
{
	int i;
	double ans=0;
	for (i=n-1;i>-1;i--)
	{
		ans=ans+fdashn[i]*power(x,i);
	}
	return ans;
}

void getinitialval()
{
	printf("\n\nEnter the initial value: ");
	cin >> x0;
}

int main()
{
	int i=0,itr=0;
	//clrscr();
	getfunction();
	itr=getiterations();
	displayfunction();
	getderivative();
	displayderivative();
	getinitialval();

	do
	{
 fnval=fx(x0);
fdashnval=fdashx(x0);
x0=x0-fnval/fdashnval;
i++;

	} while (i<itr);

	cout << "\n\nThe root of the given equation is " << x0;

	return 0;
}

