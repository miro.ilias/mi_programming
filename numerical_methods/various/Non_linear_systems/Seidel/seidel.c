/*
---------------------------------------------------------------------------
 Algo2-9.c   C program for implementing Algorithm 2.9
 Algorithm translated to  C  by: Dr. Norman Fahrer
 IBM and Macintosh verification by: Daniel Mathews
 
 NUMERICAL METHODS: C Programs, (c) John H. Mathews 1995
 To accompany the text:
 NUMERICAL METHODS for Mathematics, Science and Engineering, 2nd Ed, 1992
 Prentice Hall, Englewood Cliffs, New Jersey, 07632, U.S.A.
 Prentice Hall, Inc.; USA, Canada, Mexico ISBN 0-13-624990-6
 Prentice Hall, International Editions:   ISBN 0-13-625047-5
 This free software is compliments of the author.
 E-mail address:       in%"mathews@fullerton.edu"
 
 Algorithm 2.9* (Fixed Point Iteration in higher dimensions).
 Section   2.6, Iteration for Nonlinear Systems, Page 108
---------------------------------------------------------------------------
*/

/*
---------------------------------------------------------------------------

 Algorithm 2.9 (Nonlinear Seidel Iteration). To solve the
 nonlinear fixed-point system

              x = g_1 (x,y,z)
              y = g_2 (x,y,z)
              z = g_3 (x,y,z)

 given one initial approximation  P_0 = (p_0,q_0,r_0), and
 generating a sequence  {P_k} = {(p_k, q_k, r_k)}  that
 converges to the solution  P = (p,q,r) 
 [ i.e.; p = g_1(p,q,r), q = g_2(p,q,r) and r = g_3(p,q,r) ].

---------------------------------------------------------------------------
*/

/* User has to supply THREE functions named :
   g1function, g2function and g3function

   An example is included in this program     */

#include<stdio.h>
#include<stdio.h>
#include<math.h>

#define Max     199     //  Maximum Number of Iterations


/*  define prototype for USER-SUPPLIED functions  */

    double g1function(double x, double y, double z);
    double g2function(double x, double y, double z);
    double g3function(double x, double y, double z);

/*  EXAMPLE for "g1function" : from Sec 2.6, p. 109 No. 1 */

    double g1function(double x, double y, double z)

    {
         return ( 0.125 * ( 8.0*x - 4*x*x + y*y + 1.0 ) );
    }

    double g2function(double x, double y, double z)

    {
         return ( 0.25 * (2.0*x - x*x + 4.0*y - y*y +3.0) );
    }

    double g3function(double x, double y, double z)

    {
         return ( 0 );
    }


/* -------------------------------------------------------- */

/*  Main program for algorithm 2.9  */

    int main(void)

{
    double Tol = 1E-6;            /* Tolerance                    */
    double Sep = 1;               /* Initialize                   */
    double Inf = 1E+100;          /* Maximum for Sep              */
    int K = 0;                    /* Loop-counter                 */

    double P[Max], Q[Max], R[Max];

    printf("-----------------------------------------------------\n");
    printf("----------Nonlinear Seidel Iteration-----------------\n");
    printf("----------Problem No. 1 from page 109----------------\n");
    printf("-----------------------------------------------------\n");
    printf("Please enter initial approximation : P0 = (p0,q0,r0) :\n");
    printf("For this EXAMPLE type : 1.1  2.0  0.0 !\n");
    scanf("%lf %lf %lf",&P[0], &Q[0], &R[0]);
    printf("-----------------------------------------------------\n");
    printf("The initial approximation are : %lf\n",P[0]);
    printf("                              : %lf\n",Q[0]);
    printf("                              : %lf\n",R[0]);


/****    Zaciatok iteracii *****/

    while ( (K < Max) && (Sep > Tol) ) {

        K++;    /* Perform Seidel iteration */

  /* Spocitaj nove hodnoty, pricom hned vyuzi predtym vypocitane */
        P[K] = g1function( P[K-1], Q[K-1], R[K-1] );
        Q[K] = g2function( P[K], Q[K-1], R[K-1] );
        R[K] = g3function( P[K], Q[K], R[K-1] );

  Sep = fabs(P[K] - P[K-1]) + fabs(Q[K] - Q[K-1]) + fabs(R[K] - R[K-1]) ;

 /*   Troska uhladeny vypis z iteracie  */

   printf("Iter=%d  P=%lf  Q=%lf  R=%lf   > Sep=%lf \n",K,P[K],Q[K],R[K],Sep);

        if(Sep > Inf) break;


    }   /* End of while-loop */

    printf("----------------------------------------------\n");

    if (Sep < Tol) {
        printf("After %d iterations the Seidel method\n",K);
        printf("was successful and found the solution.\n");
    }
    else {
        printf("Seidel iteration did not converge.\n");
        printf("After %d iterations the status is:\n",K);
    }
    printf("----------------------------------------------\n");
    printf("P[%d] = %lf\n",K,P[K]);
    printf("Q[%d] = %lf\n",K,Q[K]);
    printf("R[%d] = %lf\n",K,R[K]);
    printf("----------------------------------------------\n");

}   /* End of main program */

