//
//  This is an example of usage of Langrange's Method 
//  for interpolation with unequal intervals. 
//
#include <stdio.h>
//#include <conio.h>

float value[10][10],x;
int n;

void getvalue()
{
	int i,j;
	printf("Enter the number of terms:");
	scanf("%d",&n);
	printf("Enter the x and y values simultaneously :");
	for (i=0;i<n;i++)
		for (j=0;j<=1;j++)
			scanf("%f",&value[i][j]);
	printf("Enter the value of x to interpolate :");
	scanf("%f",&x);
}

void getans()
{
	int i,k;
	float prod1,prod2,sum=0;

	for (k=0;k<=n;k++)
	{
		prod1=1;
		prod2=1;
		for(i=0;i<=n;i++)
		{
			if (i==k) continue;
			prod1*=(x-value[i][0]);
			prod2*=(value[k][0]-value[i][0]);
		}
		sum+=prod1/prod2*value[k][1];
	}
	printf("The coressponding value of y is :");
	printf("%.4f\n",sum);
}

 int main()
{
	//clrscr();
	getvalue();
	getans();
	//getch();
}


