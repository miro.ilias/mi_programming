/*
 from http://www.geocities.com/kirentanna/progs/c/gaussele.txt
*/
#include <stdio.h>
#include <stdlib.h>

#define max 20

int n;

float mat[max][max];
float ans[max];

/* Vymen riadky r1 a r2   */
void swaprows(int r1,int r2)
{
int i;
float temprow[max]; // prechodny riadok
for(i=0;i<=n;i++) temprow[i]=mat[r1][i];
for(i=0;i<=n;i++) mat[r1][i]=mat[r2][i];
for(i=0;i<=n;i++) mat[r2][i]=temprow[i];
}


/* Skontroluj konzistenciu systemu linearnych rovnic */
void checkmat(int c)
{
	int i;
	if(mat[c][c]==0)
	{
	if(c==n-1)
		{
    printf("\nSustava nema riesenie !\n");
    exit(1);
		}
		for(i=c+1;i<=n-1;i++)
		{
		if(mat[i][c]!=0)
		{
			swaprows(c,i);
			break;
			}
		}
		checkmat(c);
	}
}

/*   Vstup matice   */
void getmat()
{
     int i,j;
     printf("\nVstup poctu premennych: ");
     scanf("%d",&n);
     printf("\nVstup koeficienov:\n\n");
     for(i=0;i<n;i++)
	   {
              printf("Pre rovnicu %d : ",i+1);
              for(j=0;j<n+1;j++)
		{
                  scanf("%f",&mat[i][j]);
		}
	}
}

void showmat()
{
int i,j;
printf("\nCelkova matica je:\n");
for(i=0;i<n;i++)
{
      for(j=0;j<n+1;j++)
    {
           printf("%.2f\t",mat[i][j]);
       }
     printf("\n");
       }
   }



/*  */
void makezero(int r,int c)
{
int i;
float pivot=mat[c][c];
float factor=mat[r][c]/pivot;
for(i=0;i<=n;i++)
{
mat[r][i]-=factor*mat[c][i];
}
}

void getans()
{
int i,j;
float term=0;
for(i=n-1;i>=0;i--)
{
for(j=i+1;j<n;j++)
{
	term+=ans[j]*mat[i][j];
}
        ans[i]=(mat[i][n]-term)/mat[i][i];
	term=0;
}
}

void showans()
{
int i;
printf("\n\nRiesenie systemu linearnych rovnic je:\n\n");
for(i=0;i<n;i++) printf("x%d=\t%f\n",i+1,ans[i]);
}

int main()
{
	int i,j;
	getmat();
	showmat();
	for(j=0;j<=n-1;j++)
	{
		checkmat(j);
		for(i=j+1;i<n;i++)
		{
			if(i==j) continue;
			makezero(i,j);
			showmat();
		}
	}
	showmat();

	getans();
	showans();

	return 0;
}
