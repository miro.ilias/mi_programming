/**************************************************************************/
/*       Calculation of PI by James Gregory (ca. 1671) approximation       */
/*     PI = 4 { 1 - 1/3 + 1/5 - 1/7 + 1/9 ... +[(-1)**n]/[2*k+1] ... }     */
/*                                                                        */
/*                                                                        */
/*                              Mendel Cooper                             */
/*                             3138 Foster Ave.                           */
/*                           Baltimore, MD 21224                          */
/*                                                                        */
/*                                   06/91                                */
/*                   Source code placed in the public domain              */                 
/**************************************************************************/



/* may need #include <math.h> */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MAX 5000

main()
{
double intermediate_result = 0,numerator,Pi;
register int k;


for (k = 0; k <= MAX; k++)
{  
      if(k%2)
         numerator = -1.0;
         else numerator = 1.0;
      
      intermediate_result += numerator / (2*(double)k +1);
      Pi = 4.0 * intermediate_result;

      printf("Term #%5d ------>  � � %f  \n",k,Pi);  }

}




