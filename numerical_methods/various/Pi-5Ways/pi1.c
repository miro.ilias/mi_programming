/**************************************************************************/
/*               Calculate PI  by John Wallis approximation               */
/*             Pi = 2( 2/1 * 2/3 * 4/3 * 4/5 * 6/5 * 6/7 .....)            */
/*                                                                        */
/*                              Mendel Cooper                             */
/*                             3138 Foster Ave.                           */
/*                           Baltimore, MD 21224                          */
/*                                                                        */
/*                                   06/91                                */
/*                   Source code placed in the public domain              */                 
/**************************************************************************/

#include <stdio.h>
#include <math.h>
#define MAX 5000

main()
{
int numerator[MAX+1], denominator[MAX+1];
register int k;
double Pi = 2.0; /*really!*/

for (k = 0; k <= MAX; k+=2)      /*initialize arrays*/
{   numerator[k] = k + 2;
    denominator[k] = k + 1;   }

for (k = 1; k <= MAX; k+=2)
{   numerator[k] = k + 1;
    denominator[k] = k + 2;   }


for (k = 0; k <= MAX; k++)
   {  Pi *= (double)numerator[k]/(double)denominator[k];
      printf("Term #%5d ---------->  � � %f  \n",k,Pi);  }

}


