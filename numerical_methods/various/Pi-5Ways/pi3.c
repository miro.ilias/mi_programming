/**************************************************************************/
/*              Calculate � by Leonhard Euler approximation               */
/*   � � � { 6 ( 1/1 + 1/4 + 1/9 + 1/16 + 1/25 ... + 1/(n+1)**2 ...) }    */
/*                                                                        */
/*                                                                        */
/*                              Mendel Cooper                             */
/*                             3138 Foster Ave.                           */
/*                           Baltimore, MD 21224                          */
/*                                                                        */
/*                                   06/91                                */
/*                   Source code placed in the public domain              */                 
/**************************************************************************/


/*may need #include <math.h> */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MAX 5000

main()
{
double sqrt(),intermediate_result = 0,Pi;
register int k;

for (k = 0; k <= MAX; k++)
{  
      
      intermediate_result += 1.0 / ( (k + 1.0) * (k + 1.0) );
      Pi = sqrt( 6.0 * intermediate_result);

      printf("Term #%5d  ------->  � � %f  \n",k,Pi);  }

}


