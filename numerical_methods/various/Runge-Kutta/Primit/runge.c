/*
       This is an example of usage of 
       Runge Kutta Method for solving 
       of first order differential equations. 
*/

#include <stdio.h>
#include <math.h>

int n,choice, i;
float x0, y_0, z0, h, y_1, x1, sum, temp, k1,k2,k3,k4;
float l1,l2,l3,l4;

// deklaracia funkcii
float f ( float, float, float);
float g(float p, float q, float r);

int main()
{

printf("Enter starting value of x and corr. val of y and z.\n");
scanf("%f%f%f", &x0, &y_0, &z0);

printf("Enter value of x for which y is found \n");
scanf("%f", &x1);

h = x1-x0;

k1 = h * f ( x0, y_0, z0);

l1 = h * g ( x0, y_0, z0);
k2 = h * f(x0 + h/2, y_0 + k1/2, z0+ l1/2);

l2 = h * g(x0 + h/2, y_0 + k1/2, z0+ l1/2);
k3 = h * f(x0 + h/2, y_0 + k2/2, z0+l2/2);

l3 = h * g(x0 + h/2, y_0 + k2/2, z0+l2/2);
k4 = h * f (x0+h , y_0+k3, z0+ l3);
l4 = h * g (x0+h , y_0+k3, z0+l3);

y_1 = y_0 + (k1+2*k2+2*k3+k4)/6;


printf("y = %f", y_1);

	//getch();

         return 0;
}

float f(float p, float q, float r)
{
	return(r);
}

float g(float p, float q, float r)
{
	return(p*r*r - q*q);
}
