#!/bin/sh
DoExitAsm ()
{ echo "An error occurred while assembling $1"; exit 1; }
DoExitLink ()
{ echo "An error occurred while linking $1"; exit 1; }
echo Linking p
/usr/bin/ld    -s -L. -o p link.res
if [ $? != 0 ]; then DoExitLink p; fi
