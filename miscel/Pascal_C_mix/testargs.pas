program testaocc;
 
 {$mode objfpc}
 
 Const
   P : Pchar
     = 'example';
   F : Pchar
     = 'This %s uses printf to print numbers (%d) and strings.'#10;
 
 procedure printf(fm: pchar;args: array of const);cdecl;external 'c';
 
 begin
  printf(F,[P,123]);
 end.

