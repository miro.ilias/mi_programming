program printlength;
 
 {$linklib c} { Case sensitive }
 
 { Declaration for the standard C function strlen }
 Function strlen (P : pchar) : longint; cdecl;external;
 
 begin
   Writeln (strlen('Programming is easy !'));
 end.

