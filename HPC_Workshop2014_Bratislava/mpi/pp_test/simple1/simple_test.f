!
!     mpxlf simple_test.f
!
        program simpletest
c
        implicit none
        integer ierr
        include 'mpif.h'
        integer status(MPI_STATUS_SIZE)
c
        integer my_rank,size
c
        call mpi_init(ierr)
        call mpi_comm_rank(MPI_COMM_WORLD,my_rank,ierr)
        call mpi_comm_size(MPI_COMM_WORLD,size,ierr)
c
        write (6,*) "sample msg from: ", my_rank

        call mpi_finalize(ierr)
        end
