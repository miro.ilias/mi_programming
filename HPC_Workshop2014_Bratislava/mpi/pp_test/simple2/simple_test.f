c
        program simpletest
c
        implicit none
        integer ierr
        include 'mpif.h'
        integer status(MPI_STATUS_SIZE)
c
        integer my_rank,nprocs,tag
c
        call mpi_init(ierr)
        call mpi_comm_rank(MPI_COMM_WORLD,my_rank,ierr)
        call mpi_comm_size(MPI_COMM_WORLD,nprocs,ierr)
c
        ! clenenie podla cisla vlaken ...
        if (my_rank.eq.0) then
          write (6,*) "msg from master, rank: ", my_rank,
     &                " of nprocs=",nprocs
        else
          write (6,*) "msg from slave, rank: ", my_rank,
     &                 " of nprocs=",nprocs
        endif

        call mpi_finalize(ierr)
        end

