#@ job_type = parallel
#@ job_name = testik
#@ class = testing
#@ network.MPI = sn_all,not_shared,US
#@ error = job.err
#@ output = job.out
##@ wall_clock_limit = 96:00:00
#@ node = 1
#@ total_tasks = 4
#@ environment = RT_GRQ=ON;OMP_NUM_THREADS=1
#@ rset = RSET_MCM_AFFINITY
#@ mcm_affinity_options = mcm_mem_req mcm_distribute mcm_sni_none
#@ task_affinity = core(1)
#@ queue

Home=`pwd`

./a.out > $Home/log
