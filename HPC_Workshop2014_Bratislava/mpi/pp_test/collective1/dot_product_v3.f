c
c sync send-recieve
c
        program dotproduct
c send-recieve verzia
        implicit none
        integer ierr
        include 'mpif.h'
        integer status(MPI_STATUS_SIZE)
c
        integer my_rank,size
        integer i,j,k,n
        integer size_of, size_so_far
        parameter (n=10)
        real*8 a(1:n), b(1:n), c(2:n), dotp, local_dot

        call mpi_init(ierr)
        call mpi_comm_rank(MPI_COMM_WORLD,my_rank,ierr)
        call mpi_comm_size(MPI_COMM_WORLD,size,ierr)
c
        dotp = 0.0d0
c
           do i=1,n
              a(i) = 1.0d0
              b(i) = 2.0d0
           end do
c
           size_of = n/size
           size_so_far = 0

           local_dot = 0.0d0
           do j=0,size-1

              if (j.eq.(size-1)) then
                 size_of = n - size_so_far
              end if

              if (my_rank.eq.j) then
                write (6,*) "my_rank: ",my_rank,"size_so_far: ",
     & size_so_far
                do k=0,size_of-1
                   local_dot = local_dot + 
     & a(1+size_so_far+k)*b(1+size_so_far+k)
                end do
              end if

              size_so_far = size_so_far + size_of

           end do

           write (6,*) "my rank: ",my_rank,
     & "loc dot: " ,local_dot

!       .... stiahne vsetky lokalne dotproduky do vysledneho dotp
        call mpi_reduce (local_dot,dotp,1,
     & MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)
c
        if (my_rank.eq.0) write (6,*) "Master: Final dot p: ",dotp
c
        
        call mpi_finalize(ierr)
        end
