c
c sync send-recieve
c
        program dotproduct
c send-recieve verzia
        implicit none
        integer ierr
        include 'mpif.h'
        integer status(MPI_STATUS_SIZE)
c
        integer my_rank,size,tag
        integer i,j,n
        integer size_of, size_so_far
        parameter (n=10)
        real*8 a(1:n), b(1:n), c(2:n), dotp

        call mpi_init(ierr)
        call mpi_comm_rank(MPI_COMM_WORLD,my_rank,ierr)
        call mpi_comm_size(MPI_COMM_WORLD,size,ierr)
        tag=1
c
c on master
c
        if (my_rank.eq.0) then

c          generate a,b
           do i=1,n
              a(i) = 1.0d0
              b(i) = 2.0d0
           end do

c          send proportional a,b segments to slaves

           size_of = n/size
           size_so_far = 0

           do j=0,size-1

              if (j.eq.(size-1)) then
                 size_of = n - size_so_far
              end if

              if (j.gt.0) then
                 call mpi_send(a(1+size_so_far), size_of, MPI_REAL8,
     & j, tag, MPI_COMM_WORLD, ierr)
                 call mpi_send(b(1+size_so_far), size_of, MPI_REAL8,
     & j, tag, MPI_COMM_WORLD, ierr)
              end if
              size_so_far = size_so_far + size_of

           end do
c
           do j=1,size-1
              call mpi_recv(c(j+1), 1, MPI_REAL8,
     & j, tag, MPI_COMM_WORLD, status, ierr)
           end do

           dotp=0.0d0
           do i=0,n/size
              dotp=dotp+a(i)*b(i)
           end do

           do j=2,size
              dotp=dotp+c(j)
           end do

           write (6,*) 'Vysledok: ',dotp
              
        else
c
c on slaves
c
           size_so_far = 0
           size_of = n/size
           do j=0,my_rank
              if (j.eq.(size-1)) then
                 size_of = n - size_so_far
              end if
              size_so_far = size_so_far + size_of
           end do

c           write (6,*) 'Slave: ',my_rank,size_of

           call mpi_recv(a(1), size_of, MPI_REAL8,
     & 0, tag, MPI_COMM_WORLD, status, ierr)
           call mpi_recv(b(1), size_of, MPI_REAL8,
     & 0, tag, MPI_COMM_WORLD, status, ierr)

           dotp=0.0d0
           do i=1,size_of
              dotp=dotp+a(i)*b(i)
           end do

           call mpi_send(dotp, 1, MPI_REAL8,
     & 0, tag, MPI_COMM_WORLD, ierr)

        end if

        call mpi_finalize(ierr)
        end
