#@ job_type = parallel
#@ job_name = threads
#@ class = testing
#@ error = job.err
#@ output = job.out
#@ network.MPI = sn_all,not_shared,US
#@ node = 1
#@ tasks_per_node = 2
#@ wall_clock_limit = 00:30:00
#@ queue

poe ./a.out
