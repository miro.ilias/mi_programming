      PROGRAM HELLO
      INTEGER NTHREADS, TID, OMP_GET_NUM_THREADS,
     +        OMP_GET_THREAD_NUM
!$OMP PARALLEL PRIVATE(TID)
      TID = OMP_GET_THREAD_NUM()
!$OMP SINGLE
      NTHREADS = OMP_GET_NUM_THREADS()
      PRINT *, 'Thread',TID,':Number of threads = ', NTHREADS
!$OMP END SINGLE
      PRINT *, 'Hello World from thread = ', TID
!$OMP END PARALLEL
      PRINT *, 'Number of threads = ', NTHREADS
      NTHREADS = OMP_GET_NUM_THREADS()
      PRINT *, 'Number of threads = ', NTHREADS
      END
