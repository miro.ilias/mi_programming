!
! Vypocet cisla Pi numerickou integraciou
!
      program numerical_integration
      integer num_steps
      parameter (num_steps=1000000)
      real*8 x,dx
      real*8 sum
      integer i
c
      dx=1.0d0/num_steps
c
      sum=0.0d0
!!$OMP PARALLEL DO REDUCTION (+:sum)

!    ...  lepsia, kompaktnejsia forma prikazu  ...

!$OMP PARALLEL DO PRIVATE(i,x) SHARED(dx)
!$OMP&            REDUCTION (+:sum)
      do i=1,num_steps
         x=(i-0.5d0)*dx
         sum=sum+4.0d0/(1.0d0+x*x)
      enddo
      write(*,*) dx*sum
c
      end
