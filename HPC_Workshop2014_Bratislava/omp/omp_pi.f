      program numerical_integration
      parameter (num_steps=1000000)
      parameter (nthreads=2)
      real*8 pi,x,dx
      real*8 sum(1:nthreads)
      integer i
      integer TID,OMP_GET_NUM_THREADS,OMP_GET_THREAD_NUM
      integer NT
c
      dx=1.0d0/num_steps
c
!$OMP PARALLEL SHARED(CHUNK,dx,sum) PRIVATE(TID,i,x,pi)
      TID = OMP_GET_THREAD_NUM()
      NT = OMP_GET_NUM_THREADS()
      TID=TID+1
      sum(TID)=0.0d0
      do i=TID,num_steps,NT
         x=(i-0.5d0)*dx
         sum(TID)=sum(TID)+4.0d0/(1.0d0+x*x)
      enddo
!$OMP END PARALLEL
      pi=0.0d0
      do i=1,NT
        pi=pi+sum(i)
      enddo
      pi=dx*pi
      write(*,*) pi
c
c
      end
