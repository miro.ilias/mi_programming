      program mm
      integer dim1, dim2, dim3
      parameter (dim1 = 1500, dim2 = 1500, dim3 = 1500)
      double precision A(dim1, dim2), B(dim2, dim3), C(dim1, dim3)
      double precision trace
      call srand(86456)
!$OMP PARALLEL DO PRIVATE(i,j)
      do i = 1, dim1
        do j = 1, dim2
          !A(i, j) = rand()
          A(i, j) = 3.1
        enddo
      enddo
!$OMP PARALLEL DO PRIVATE(i,j)
      do i = 1, dim2
        do j = 1, dim3
          !B(i, j) = rand()
          B(i, j) = 6.45
        enddo
      enddo
!$OMP PARALLEL DO PRIVATE(i,j,k) SCHEDULE(STATIC,100)
! i,j,k idexy musia byt privatne, inak by sa prepisovali !
      do i = 1, dim1
        do j = 1, dim3
          C(i, j) = 0.
          do k = 1, dim2
            C(i, j) = C(i, j) + A(i, k)*B(k, j)
          enddo
        enddo
      enddo

      trace=0.
       do i=1,dim1
        trace=trace+c(i,i)
       end do
      print *, trace
      end
