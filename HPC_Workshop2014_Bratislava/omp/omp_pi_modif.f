      program numerical_integration
      parameter (num_steps=1000000)
      real*8 pi,x,dx
      real*8 sum
      integer i
      integer TID,OMP_GET_NUM_THREADS,OMP_GET_THREAD_NUM
      integer NTHREADS
c
      dx=1.0d0/num_steps
c
!$OMP PARALLEL SHARED(dx,sum) PRIVATE(TID,i,x,pi)
      TID = OMP_GET_THREAD_NUM()
      NTHREADS = OMP_GET_NUM_THREADS()
      sum=0.0d0
      do i=TID+1,num_steps,NTHREADS
         x=(i-0.5d0)*dx
!$OMP ATOMIC
         sum=sum+4.0d0/(1.0d0+x*x)
      enddo
!$OMP END PARALLEL
      pi=dx*sum
      write(*,*) pi
c
c
      end
