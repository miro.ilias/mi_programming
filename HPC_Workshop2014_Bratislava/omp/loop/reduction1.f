!
!
!
      PROGRAM REDUCTION
      INTEGER I, N
      REAL*8 A(100), SUM, PRODUCT
!
      N = 20
      DO I = 1, N
        A(I) = I * 1.0d0
      ENDDO
      SUM = 0.0d0
      PRODUCT = 1.0d0
!$OMP PARALLEL DO REDUCTION(*:PRODUCT) REDUCTION(+:SUM)
      DO I = 1, N
        SUM = SUM + A(I)
        PRODUCT = PRODUCT * A(I)
      ENDDO

      PRINT *, '   Sum     = ', SUM
      PRINT *, '   Product = ', PRODUCT
      END
