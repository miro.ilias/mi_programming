        program soft
c
c        V(a,b,c)_ijk <- T(a,d,i,j) . (db|ck)
c
        implicit none
        integer no,nv,nc
        parameter (no=16)
C       parameter (no=32)
        parameter (nv=64)
C       parameter (nv=128)
        parameter (nc=128)
C       parameter (nc=128)
c
        real*8 T2(1:nv*nv*no*no)
        real*8 L21(1:nv*nv*nc)
        real*8 L22(1:nv*no*nc)
        real*8 Eo(1:no),Ev(1:nv)
c
c       help var
c
        real*8 V1(nv*nc)
        real*8 V2(nv*nv)
        real*8 V3(nv*nv*nv)
        real*8 W(nv*nv*nv)
c
        real*8 E3,Rijk
c
        integer i,j,ij,k,ad,possT
        integer chunk
c
c       Garbfill section (T2,L21,L22,Eo,Ev)
        call grbFil (T2,nv*nv*no*no,1.0d-2)
        call grbFil (L21,nv*nv*nc,1.0d-2)
        call grbFil (L22,nv*no*nc,1.0d-2)
        call grbFil (Eo,no,5.0d1)
        do i=1,no
          Eo(i)=-Eo(i)
        end do
        call grbFil (Ev,nv,5.0d1)
c
        call set0 (V1,nv*nc)
        call set0 (V2,nv*nv)
c       call set0 (V3,nv*nv*nv)
c       call set0 (W,nv*nv*nv)
c
        E3=0.0d0
        chunk=1
CCC!$OMP& REDUCTION(+:E3)
c
!$OMP  PARALLEL DO
!$OMP& DEFAULT(SHARED) PRIVATE(k,ij,i,j,Rijk,W,possT,ad)
!$OMP& FIRSTPRIVATE(V2,V3,E3)
!$OMP& SCHEDULE(STATIC,1)
        do k=1,no
c         ext V1(c,_k,m) <- L22(c,k,m)
          call extL (V1(1),L22(1),k,no,nv,nc)
c         calc W(d,b,c) <- L21(d,b,m) . V1(T)(c,m)
          call set0 (W,nv*nv*nv)
C         call dgemm ('N','T',nv*nv,nv,nc,1.0d0,L21(1),nv*nv,V1(1),nv,
C    c              1.0d0,W(1),nv*nv)

          ij=0
          do j=1,no
          do i=1,no
          ij=ij+1
c
c           ext V2(a,d) <- T2(a,d,i,j)
            possT=nv*nv*(ij-1)
            do ad=1,nv*nv
              V2(ad)=T2(possT+ad)
            end do
c
c           calc V3(a,b,c) <- V2(a,d) . W(d,b,c)
            call set0 (V3,nv*nv*nv)
C           call dgemm ('N','N',nv,nv*nv,nv,1.0d0,V2(1),nv,W(1),nv,
C    c              1.0d0,V3(1),nv)
c           
c           E3 = V3(a,b,c).V3(a,b,c)/R(ijkabc)
            Rijk=Eo(i)+Eo(j)+Eo(k)
            call modV3 (V3,Ev,Rijk,E3,nv)
c
c
          end do
          end do
        end do
!$OMP  END PARALLEL DO
c
        write(*,*) 'Allreduce=',E3
c
c
        stop
        end
c
c       --------------------------
c
        subroutine extL (V1,L2,k,no,nv,nc)
c
c       V1(c,m)_k <- L2(c,k,m)
c
        implicit none
        integer k,no,nv,nc
        real*8 V1(1:nv,1:nc)
        real*8 L2(1:nv,1:no,1:nc)
c
c       help variables
        integer c,m
c
        do m=1,nc
          do c=1,nv
            V1(c,m)=L2(c,k,m)
          end do
        end do
c
        return
        end
c
c       --------------------------
c
        subroutine modV3 (V3,Ev,Rijk,E3,nv)
c
c       V3(a,b,c) = V3(a,b,c)/R(ijkabc)
c
        implicit none
        integer nv
        real*8 V3(1:nv,1:nv,1:nv)
        real*8 Ev(1:nv)
        real*8 Rijk,E3
c
c       help variables
        integer a,b,c
        real*8 Rcijk,Rbcijk,Rabcijk
c
c        E3=E3+1.0d0
c        return
c
        do c=1,nv
        Rcijk=Rijk-Ev(c)
          do b=1,nv
          Rbcijk=Rcijk-Ev(b)
            do a=1,nv
              Rabcijk=Rbcijk-Ev(a)
              E3=E3+V3(a,b,c)*V3(a,b,c)/Rabcijk
            end do
          end do
        end do
        return
        end
c
c       --------------------------
c
        subroutine set0 (A,n)
        integer n
        real*8 A(1:n)
        integer i
        do i=1,n
        A(i)=0.0d0
        end do
        return
        end
c
c       --------------------------
c
        subroutine grbFil (A,n,sch)
        integer n
        real*8 A(1:n)
        real*8 sch,schp,add
        integer i
        schp=sch/n
        add=0.0d0
        do i=1,n
        add=add+schp
        A(i)=add
        end do

        return
        end
