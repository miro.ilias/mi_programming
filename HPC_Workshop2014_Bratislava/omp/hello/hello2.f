!
!  export OMP_NUM_THREADS=12
!  xlf_r -qsmp hello2.f
!
!
      PROGRAM HELLO
      INTEGER NTHREADS, TID, OMP_GET_NUM_THREADS,
     +        OMP_GET_THREAD_NUM
!$OMP PARALLEL PRIVATE(TID,NTHREADS)
      TID = OMP_GET_THREAD_NUM()
      PRINT *, 'Hello World from thread = ', TID
! ... tato cast kodu sa vykonava iba na jednom vlakne - SINGLE
!$OMP SINGLE
      NTHREADS = OMP_GET_NUM_THREADS()
!   .. iba 1x sa vypise pocet vlakien
      PRINT *, 'Single Thread',TID,':Number of threads = ', NTHREADS
!$OMP END SINGLE
! iba na master threade

!$OMP MASTER
      NTHREADS = OMP_GET_NUM_THREADS()
!   .. iba 1x sa vypise pocet vlakien a to z MASTER threadu
      PRINT *, 'Master Thread',TID,':Number of threads = ', NTHREADS
!$OMP END MASTER
!$OMP END PARALLEL
      END
