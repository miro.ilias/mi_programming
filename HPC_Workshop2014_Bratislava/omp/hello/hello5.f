!
!
!
      PROGRAM HELLO
      INTEGER NTHREADS, TID, OMP_GET_NUM_THREADS,
     +        OMP_GET_THREAD_NUM
!$OMP PARALLEL
!$OMP SINGLE
      print *,'part 1:'
      NTHREADS = OMP_GET_NUM_THREADS()
      PRINT *, 'Number of threads = ', NTHREADS
!$OMP END SINGLE
!$OMP END PARALLEL

! zmena poctu vlaken v programe - akceptuje iba v seriovej casti kodu !
      CALL OMP_SET_NUM_THREADS(2)

!$OMP PARALLEL
!$OMP SINGLE
      print *,'part 2:'
      NTHREADS = OMP_GET_NUM_THREADS()
      PRINT *, 'Number of threads = ', NTHREADS
!$OMP END SINGLE
!$OMP END PARALLEL
      END
