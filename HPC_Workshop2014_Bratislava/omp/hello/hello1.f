!============================================================================
!
!  xlf_r -qsmp hello1.f
!
! Pocet fyzickych procesorov (system.nastavenie):
! lsdev | grep -i proc | wc -l
!
!   Urcujem pocet threadov:
!
! export OMP_NUM_THREADS=4
!
!============================================================================

      PROGRAM HELLO_OMP
      INTEGER NTHREADS, TID, OMP_GET_NUM_THREADS,
     +        OMP_GET_THREAD_NUM
! ... tu zacne paralelizacia ...
!$OMP PARALLEL PRIVATE(TID,NTHREADS)
      TID = OMP_GET_THREAD_NUM()  !vrati hodnotu aktualneho vlakna
      NTHREADS = OMP_GET_NUM_THREADS()
      WRITE (*,*) 'Thread',TID,'; Number of threads =', NTHREADS
      WRITE (*,*) 'Hello World from thread =', TID
!$OMP END PARALLEL

! tu pokracuje seriovy beh
       print *,'tu pokracuje seriovy beh'
      END
