!
! podprogramy v paralel. sekcii
!
      PROGRAM HELLO
      COMMON /test/ NTHREADS
      INTEGER NTHREADS, TID, OMP_GET_NUM_THREADS,
     +        OMP_GET_THREAD_NUM
!$OMP PARALLEL PRIVATE(TID)
      TID = OMP_GET_THREAD_NUM()
      PRINT *, 'Hello World from thread = ', TID
!$OMP SINGLE
      CALL NTHREADSIS()
!$OMP END SINGLE
!$OMP END PARALLEL
      PRINT *, 'Right after after parallel = ', NTHREADS
      NTHREADS = OMP_GET_NUM_THREADS()
      PRINT *, 'A new query = ', NTHREADS
      END

      SUBROUTINE NTHREADSIS()
! premenna v paralelnej casti je definovana ako private
      COMMON /test/ NTHREADS
      INTEGER NTHREADS,OMP_GET_NUM_THREADS
      NTHREADS = OMP_GET_NUM_THREADS()
      PRINT *, 'NTHREADSIS: Number of threads is:', NTHREADS
      RETURN
      END
