!
! Vypocet pi cisla, seriovo
!
      program numerical_integration
      integer num_steps
      parameter (num_steps=1000000)
      real*8 x,dx
      real*8 sum
      integer i
c
      dx=1.0d0/num_steps
c
      sum=0.0
      do i=1,num_steps
         x=(i-0.5)*dx
         sum=sum+4.0/(1.0+x*x)
      enddo
      write(*,*) sum*dx
c
      end
