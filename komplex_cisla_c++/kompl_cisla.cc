  extern "C" {
  #include <stdio.h>
  #include <math.h>
}  

#include <iostream.h>

// definicia triedy...
class complex {
 public: double real,imag;

   void set(double r, double i); // nastav komplexnu hodnotu

   void print(); // zobrazenie

  // complex operator = (complex x);

  // absolutna hodn. komplex.cisla...
   double abs_hod ();

  // amplituda komplex.cisla...
   double amplituda ();
};

// deklaracia hlav. funkcie...
int main(void);

// nastav hodnotu komplex. cisla...
void complex::set(double r, double i)
{
  real = r;
  imag = i;
}

// Absolutna hodnota komplex. cisla 
double complex::abs_hod()
{
  return sqrt(real*real + imag*imag);
}

// Amplituda komplex. cisla 
double complex::amplituda()
{
  return asin(imag/(sqrt(imag*imag+real*real)));
}

// vypis hodnotu komplexneho cisla...
void complex::print()
{
 printf("(%g, %g)\n",real,imag); 
}

complex operator + (complex a, complex x)
{
  complex temp;
  temp.real = a.real + x.real;
  temp.imag = a.imag + x.imag;
  return temp;
}

complex operator + (complex a, double x)
{
  complex temp;
  temp.real = a.real + x;
  temp.imag = a.imag ;
  return temp;
}


complex operator - (complex a, complex x)
{
  complex temp;
  temp.real = a.real - x.real;
  temp.imag = a.imag - x.imag;
  return temp;
}

complex operator * (complex a, complex x)
{
  complex temp;
  temp.real = a.real*(x.real)-a.imag*x.imag;
  temp.imag = a.real*x.imag + x.real*a.imag;
  return temp;
}

complex operator / (complex a, complex x)
{
  complex temp;
  double menov;

  menov  = x.real*x.real + x.imag*x.imag;

  temp.real = (a.real*(x.real)+a.imag*x.imag)/menov;
  temp.imag = (a.imag*x.real + a.real*x.imag)/menov;
  return temp;
}

// Toto mi nefunguje...
/* complex complex :: operator = (complex a)
{
  complex temp;
  temp.real = a.real;
  temp.imag = a.imag;
  return temp;
}  */


int main(void)
{
 double a1,a2,b1,b2;
 complex a,b,sucet,rozdiel,sucin,podiel;

 printf("Operacie s komplex. cislami - objektovy pristup:\n");

 printf("\n Vstup 2 komplex.cisel a,b:\n");
 printf("a.real="); scanf("%lf",&a1);
 printf("a.imag="); scanf("%lf",&a2);
 printf("b.real="); scanf("%lf",&b1);
 printf("b.imag="); scanf("%lf",&b2);

 a.set(a1,a2);
 b.set(b1,b2);

 sucet = a + b + b + 12.0;
 sucin = a*b;
 rozdiel = a - b;
 podiel = a / b;

 printf("absol. hodnota a amplituda a:%lf %lf\n",\
           a.abs_hod(),a.amplituda() );
 printf("absol. hodnota a amplituda b:%lf %lf\n", \
           b.abs_hod(),b.amplituda() );

 printf("sucet   a+b+b+12.0=");sucet.print();
 printf("rozdiel   a-b=");rozdiel.print();
 printf("sucin     a*b=");sucin.print();
 printf("podiel    a/b=");podiel.print();

}

