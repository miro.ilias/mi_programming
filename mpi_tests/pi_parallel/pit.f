      program Pocitaj_Pi_paralelne

      include "mpif.h"
      double precision  PI25DT
      parameter        (PI25DT = 3.141592653589793238462643d0)

      double precision  mypi, pi, h, sum, x, f, a
      double precision starttime, endtime

      integer lu, n, myid, numprocs, i, ierr
      data lu / 12 /

c...function to integrate
      f(a) = 4.d0 / (1.d0 + a*a)

      call MPI_INIT(ierr)
      write(*,*) "MPI_INIT done, ierr=",ierr

      call MPI_COMM_RANK(MPI_COMM_WORLD, myid, ierr)
      write(*,*) "MPI_COMM_RANK done, myid,ierr=",myid,ierr

      call MPI_COMM_SIZE(MPI_COMM_WORLD, numprocs, ierr)
      write(*,*) "MPI_COM_SIZE done, numprocs,ierr=",
     &            numprocs,ierr

C   ... read the input file...
      Open (lu,file="pi_input",status="old",form="formatted")
      read(lu,*) n
      close(lu,status="keep")

C 10   if ( myid .eq. 0 ) then
C        print *, 'Enter the number of intervals: (0 quits) '
C        read(*,*) n
C     endif
c                                 broadcast n
      starttime = MPI_WTIME()
      write(*,*) "startime=",starttime

      call MPI_BCAST(n,1,MPI_INTEGER,0,MPI_COMM_WORLD,ierr)
c                                 check for quit signal
c                                 calculate the interval size
      write(*,*) "MPI_BCAST done, n,ierr=",n,ierr
      
      h = 1.0d0/n
      sum  = 0.0d0
      do 20 i = myid+1, n, numprocs
         x = h * (dble(i) - 0.5d0)
         sum = sum + f(x)
 20   continue
      mypi = h * sum

c                collect all the partial sums
      call MPI_REDUCE(mypi,pi,1,MPI_DOUBLE_PRECISION,MPI_SUM,0, 
     &                  MPI_COMM_WORLD,ierr)
      write(*,*) "MPI_REDUCE done, mypi,pi,ierr=",mypi,pi,ierr


      endtime = MPI_WTIME()
      write(*,*) "endtime=",endtime

      Open (lu,file="pi_output",status="unknown",form="formatted")

      if (myid .eq. 0) then
        write(lu,*)
     & '***************************************************'
        write(lu,*) 'for n=',n,' the calculated pi is ', pi,
     &             ' Error is', abs(pi - PI25DT)
        write(*,*)  'for n=',n,' the calculated pi is ', pi,
     &             ' Error is', abs(pi - PI25DT)
        write(lu,*) 'spent time is ', endtime-starttime, ' seconds'
        write(*,*)  'spent time is ', endtime-starttime, ' seconds'
        write(lu,*)
     & '***************************************************'
      endif

      close(lu,status="keep")

      call MPI_FINALIZE(ierr)
      write(*,*) "MPI_FINALIZE done, ierr=",ierr

      stop "Hotovo..." 
      end
