! Sample MPI "hello world" application in Fortran 90
program main
    use mpi
    implicit none
    integer :: ierr, rank, rank_size

    call MPI_INIT(ierr)
    if (ierr .ne. MPI_SUCCESS) then
     stop "...no MPI_INIT success !!!"
     !CALL MPI_ABORT()
    endif

    call MPI_COMM_RANK(MPI_COMM_WORLD, rank, ierr)
    call MPI_COMM_SIZE(MPI_COMM_WORLD, rank_size, ierr)

    if (rank .eq. 0) then
      write(*,'(a,i2,a,i2,a)') "Hello, world, I am process ", rank, " of ", rank_size, " ... MASTER "
    else
      write(*,'(a,i2,a,i2,a)') "Hello, world, I am process ", rank, " of ", rank_size, " ... slave "
    endif

    call MPI_FINALIZE(ierr)
end
