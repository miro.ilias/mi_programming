MPI Hello World
===============

http://mpitutorial.com/tutorials/mpi-hello-world/

milias@lxir127.gsi.de:~/Work/programming/miro_ilias_programming/mpi_tests/hello_world/C/.mpicc mpi_hello_world.c 
milias@lxir127.gsi.de:~/Work/programming/miro_ilias_programming/mpi_tests/hello_world/C/.a.out 
Hello world from processor lxir127, rank 0 out of 1 processors
milias@lxir127.gsi.de:~/Work/programming/miro_ilias_programming/mpi_tests/hello_world/C/.mpirun -np 4 a.out 
Hello world from processor lxir127, rank 1 out of 4 processors
Hello world from processor lxir127, rank 2 out of 4 processors
Hello world from processor lxir127, rank 3 out of 4 processors
Hello world from processor lxir127, rank 0 out of 4 processors
milias@lxir127.gsi.de:~/Work/programming/miro_ilias_programming/mpi_tests/hello_world/C/.




