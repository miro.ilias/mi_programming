#@ job_type = parallel
#@ job_name = mpijob
#@ class = testing
#@ error = job.err
#@ output = job.out
#@ node = 1
#@ tasks_per_node = 8
#@ queue

mpiexec ./exc5_groups_and_comm.x
