! ****************************************************************************
!  FILE: mpi_pi_reduce.f 
!  DESCRIPTION:  
!    MPI pi Calculation Example - Fortran Version
!    Collective Communications examples
!    This program calculates pi using a "dartboard" algorithm.  See
!    Fox et al.(1988) Solving Problems on Concurrent Processors, vol.1
!    page 207.  All processes contribute to the calculation, with the
!    master averaging the values for pi. SPMD Version:  Conditional 
!    statements check if the process is the master or a worker.
!    This version uses mp_reduce to collect results
!    Note: Requires Fortran90 compiler due to random_number() function
!  AUTHOR: Blaise Barney. Adapted from Ros Leibensperger, Cornell Theory
!    Center. Converted to MPI: George L. Gusciora, MHPCC (1/95)  
!  LAST REVISED: 06/13/13 Blaise Barney
! ****************************************************************************
! Explanation of constants and variables used in this program:
!   DARTS          = number of throws at dartboard 
!   ROUNDS         = number of times "DARTS" is iterated 
!   MASTER         = task ID of master task
!   taskid         = task ID of current task 
!   numtasks       = number of tasks
!   homepi         = value of pi calculated by current task
!   pisum          = sum of tasks' pi values 
!   pi 	           = average of pi for this iteration
!   avepi          = average pi value for all iterations 
!   seednum        = seed number - based on taskid

      program pi_reduce
      use mpi

      integer DARTS, ROUNDS, MASTER
      parameter(DARTS = 5000) 
      parameter(ROUNDS = 100)
      parameter(MASTER = 0)

      integer :: taskid, numtasks, i, status(MPI_STATUS_SIZE)
      real*4 :: seednum
      real*8 :: homepi, pi, avepi, pisum, dboard
!      external :: d_vadd

!     Obtain number of tasks and task ID
      call MPI_INIT( ierr )
      call MPI_COMM_RANK( MPI_COMM_WORLD, taskid, ierr )
      call MPI_COMM_SIZE( MPI_COMM_WORLD, numtasks, ierr )
      write(*,*) 'cislo procesu, task ID = ', taskid

      avepi = 0

      do 40 i = 1, ROUNDS
!     Calculate pi using dartboard algorithm 
        homepi = dboard(DARTS)

!     Use mpi_reduce to sum values of homepi across all tasks
!     Master will store the accumulated value in pisum
!     - homepi is the send buffer
!     - pisum is the receive buffer (used by the receiving task only)
!     - MASTER is the task that will receive the result of the reduction
!       operation
!     - d_vadd is a pre-defined reduction function (double-precision
!       floating-point vector addition)
!     - allgrp is the group of tasks that will participate
        call MPI_REDUCE( homepi, pisum, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MASTER, MPI_COMM_WORLD, ierr )
!     Master computes average for this iteration and all iterations 
        if (taskid .eq. MASTER) then
          pi = pisum/numtasks
          avepi = ((avepi*(i-1)) + pi) / i
          write(*,32) DARTS*i, avepi
 32       format('Master:  After',i6,' throws, average value of pi = ',f10.8) 
        endif
 40   continue
!
      if (taskid .eq. MASTER) then
        print *, ' '
        print *,'Master: Real value of PI: 3.1415926535897'
        print *, ' '
      endif
      call MPI_FINALIZE(ierr)
      end

! *****************************************************************************
! function dboard.f
! DESCRIPTION:
!   Used in pi calculation example codes.
!   See mpi_pi_send.f and mpi_pi_reduce.f
!   Throw darts at board.  Done by generating random numbers
!   between 0 and 1 and converting them to values for x and y
!   coordinates and then testing to see if they "land" in
!   the circle."  If so, score is incremented.  After throwing the
!   specified number of darts, pi is calculated.  The computed value
!   of pi is returned as the value of this function, dboard.
!   Note: Requires Fortran90 compiler due to random_number() function
! 
! Explanation of constants and variables used in this function:
!   darts    	= number of throws at dartboard
!   score	= number of darts that hit circle
!   n		= index variable
!   r  		= random number between 0 and 1 
!   x_coord	= x coordinate, between -1 and 1  
!   x_sqr	= square of x coordinate
!   y_coord	= y coordinate, between -1 and 1  
!   y_sqr	= square of y coordinate
!   pi		= computed value of pi
! **************************************************************************/

      real*8    function dboard(darts)
      integer   darts, score, n
      real*4	r
      real*8	x_coord, x_sqr, y_coord, y_sqr, pi

      score = 0

!     "throw darts at the board"
      do 10 n = 1, darts
!     generate random numbers for x and y coordinates
        call random_number(r)
        x_coord = (2.0 * r) - 1.0
	x_sqr = x_coord * x_coord

        call random_number(r)
        y_coord = (2.0 * r) - 1.0
	y_sqr = y_coord * y_coord

!       if dart lands in circle, increment score
	if ((x_sqr + y_sqr) .le. 1.0) then
	  score = score + 1
        endif

 10   continue

!     calculate pi
      pi = 4.0 * score / darts
      dboard = pi
      end
