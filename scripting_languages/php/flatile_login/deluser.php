<?php

/*********************
 *    deluser.php    *
 *********************/

ob_start();

echo '
<link rel="stylesheet" href="stylesheet.css" type="text/css">
<table style="border-collapse: collapse" bordercolor="#111111" cellpadding="3" cellspacing="0" border="1" width="600"><tr class="titlebar" align="center"><td>
<b><font color="#808080">.</font><font color="#666666">:</font> Delete a User <font color="#666666">:</font><font color="#808080">.</font></b>
</td></tr></table>

<table border="0"><tr><td height="1"></td></tr></table>
<table style="border-collapse: collapse" bordercolor="#111111" cellpadding="10" cellspacing="0" border="1" width="600"><tr class="paragraph" align="left"><td>
';

require_once('validate.php');

if(!isset($_COOKIE['username'],$_COOKIE['password']))
	echo 'You must first <a href="login.php">login</a>.';

elseif(!checkLogin($_COOKIE['username'],$_COOKIE['password']))
	echo 'Invalid login.';

elseif(!isAdmin($_COOKIE['username']))
	echo 'You must be an admin to access this page.';

elseif(isset($_POST['user'],$_POST['doit']))
{
	if($_POST['doit'])
	{
		$users=file($file) or die("Could not open file <b>$file</b>");
		$handle=fopen($file,'w') or die("Could not open file <b>$file</b> for writing.");
		foreach($users as $userInfo)
		{
			if(trim(substr($userInfo,33)) != $_POST['user'])
				fwrite($handle,$userInfo);
		}
		echo 'User <b>'.$_POST['user'].'</b> has been removed.';
	}
	else echo 'Are you sure you want to remove <b>'.$_POST['user']."</b>?<p><form method='post' action='$self'>".'<input type="hidden" name="doit" value="1"><input type="hidden" name="user" value="'.$_POST['user'].'"><input class="button" type="submit" value="Yes, do it."></form>';
}
else
{
	require_once('config.php');

	$users=file($file) or die("Could not open file <b>$file</b>");

	echo "<form method='post' action='$self'>Select user to delete: <select name='user'>";
	foreach($users as $userInfo)
		echo '<option>'.substr($userInfo,33).'</option>';
	echo '</select><input type="hidden" name="doit" value="0"><p><input class="button" type="submit" value="Delete"></form><p>';
}

echo '</td></tr></table><p>';

require_once('control.php');

?>