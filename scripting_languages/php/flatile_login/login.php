<?php

/*******************
 *    login.php    *
 *******************/

ob_start();

echo '
<link rel="stylesheet" href="stylesheet.css" type="text/css">
<table style="border-collapse: collapse" bordercolor="#111111" cellpadding="3" cellspacing="0" border="1" width="600"><tr class="titlebar" align="center"><td>
<b><font color="#808080">.</font><font color="#666666">:</font> Login <font color="#666666">:</font><font color="#808080">.</font></b>
</td></tr></table>

<table border="0"><tr><td height="1"></td></tr></table>
<table style="border-collapse: collapse" bordercolor="#111111" cellpadding="10" cellspacing="0" border="1" width="600"><tr class="paragraph" align="left"><td>
';

if(isset($_COOKIE['username'],$_COOKIE['password']))
	die('You are already logged in.<p>Would you like to <a href="logout.php">logout</a>?');

require_once('validate.php');

if(isset($_POST['username'],$_POST['password']))
{
	if(checkLogin($_POST['username'],md5($_POST['password'])))
	{
		if(isset($_POST['remember']))
		{
			setcookie('username', $_POST['username'], time() + 31536000) or die('Could not set login cookie');
			setcookie('password', md5($_POST['password']), time() + 31536000) or die('Could not set login cookie');
		}
		else
		{
			setcookie('username', $_POST['username']) or die('Could not set login cookie');
			setcookie('password', md5($_POST['password'])) or die('Could not set login cookie');
		}
		echo 'You have been logged in.<p><form action="control.php"><input class="button" type="submit" value="Continue"></form>';
	}
	else die('Incorrect login.<p><form action="login.php"><input class="button" type="submit" value="Retry"></form>');
}

else
{
	echo "

	<form method='post' action='$self'>
	Username: <input type='text' name='username'>
	<br>Password: <input type='password' name='password'>
	<br><input type='checkbox' name='remember'>Remember me on this computer
	<p><input class='button' type='submit' value='Login'>
	</form>

	";
}

?>

</td></tr></table>