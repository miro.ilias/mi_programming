<?php

/************************
 *    changepass.php    *
 ************************/

ob_start();

echo '
<link rel="stylesheet" href="stylesheet.css" type="text/css">
<table style="border-collapse: collapse" bordercolor="#111111" cellpadding="3" cellspacing="0" border="1" width="600"><tr class="titlebar" align="center"><td>
<b><font color="#808080">.</font><font color="#666666">:</font> Change your Password <font color="#666666">:</font><font color="#808080">.</font></b>
</td></tr></table>

<table border="0"><tr><td height="1"></td></tr></table>
<table style="border-collapse: collapse" bordercolor="#111111" cellpadding="10" cellspacing="0" border="1" width="600"><tr class="paragraph" align="left"><td>
';

require_once('validate.php');
require_once('config.php');

if(!isset($_COOKIE['username'],$_COOKIE['password']))
	die('You must first <a href="login.php">login</a>.');

if(!checkLogin($_COOKIE['username'],$_COOKIE['password']))
	die('Invalid login.');

if(isset($_POST['oldpassword'],$_POST['password1'],$_POST['password2']))
{
	if($_POST['password1']=="" || $_POST['password1'] != $_POST['password2'])
		echo 'New Passwords do not match or they are blank.';
	elseif(checkLogin($_COOKIE['username'], md5($_POST['oldpassword'])))
	{
		$users=file($file) or die("Could not open file <b>$file</b>");
		$handle=fopen($file,'w') or die("Could not open file <b>$file</b> for writing.");
		foreach($users as $userInfo)
		{
			if(trim(substr($userInfo,33)) == $_COOKIE['username'])
				fwrite($handle, md5($_POST['password1']).substr($userInfo,32));
			else fwrite($handle, $userInfo);
		}
		echo 'Password for <b>'.$_COOKIE['username'].'</b> has been changed.<p>You must now <a href="logout.php">logout</a>.';
	}
	else echo 'Incorrect old password.';
}
else
{
	echo "
	<form method='post' action='$self'>
	Old Password: <input type='password' name='oldpassword'>
	<br>New Password: <input type='password' name='password1'>
	<br>New Password: <input type='password' name='password2'>
	<p><input class='button' type='submit' value='Change Password'>
	</form>
	";
}

echo '</td></tr></table><p>';

require_once('control.php');

?>
