<?php

/**********************
 *    edituser.php    *
 **********************/

ob_start();

echo '
<link rel="stylesheet" href="stylesheet.css" type="text/css">
<table style="border-collapse: collapse" bordercolor="#111111" cellpadding="3" cellspacing="0" border="1" width="600"><tr class="titlebar" align="center"><td>
<b><font color="#808080">.</font><font color="#666666">:</font> Edit a User\'s Password <font color="#666666">:</font><font color="#808080">.</font></b>
</td></tr></table>

<table border="0"><tr><td height="1"></td></tr></table>
<table style="border-collapse: collapse" bordercolor="#111111" cellpadding="10" cellspacing="0" border="1" width="600"><tr class="paragraph" align="left"><td>
';

require_once('validate.php');
require_once('config.php');

if(!isset($_COOKIE['username'],$_COOKIE['password']))
	echo 'You must first <a href="login.php">login</a>.';

elseif(!checkLogin($_COOKIE['username'],$_COOKIE['password']))
	echo 'Invalid login.';

elseif(!isAdmin($_COOKIE['username']))
	echo 'You must be an admin to access this page.';

elseif(isset($_POST['user']))
{
	if(isset($_POST['password1'],$_POST['password2']))
	{
		if($_POST['password1']=="" || $_POST['password1'] != $_POST['password2'])
			echo 'New Passwords do not match or they are blank.';
		else
		{
			$users=file($file) or die("Could not open file <b>$file</b>");
			$handle=fopen($file,'w') or die("Could not open file <b>$file</b> for writing.");
			foreach($users as $userInfo)
			{
				if(trim(substr($userInfo,33)) == $_POST['user'])
					fwrite($handle, md5($_POST['password1']).substr($userInfo,32));
				else fwrite($handle, $userInfo);
			}
			echo 'Password for <b>'.$_POST['user'].'</b> has been changed.';
			if($_POST['user'] == $_COOKIE['username']) echo '<p>You must now <a href="logout.php">logout</a>.';
		}
	}
	else
	{
		echo 'Editing <b> '.$_POST['user']."</b>'s Password.";
		echo "
		<form method='post' action='$self'>
		<input type='hidden' name='user' value='".$_POST['user']."'>
		New Password: <input type='password' name='password1'>
		<br>New Password: <input type='password' name='password2'>
		<p><input class='button' type='submit' value='Change Password'>
		</form>
		";
	}
}
else
{
	$users=file($file) or die("Could not open file <b>$file</b>");

	echo "<form method='post' action='$self'>Select a user to edit: <select name='user'>";
	foreach($users as $userInfo)
		echo '<option>'.substr($userInfo,33).'</option>';
	echo '</select><p><input class="button" type="submit" value="Edit"></form><p>';
}

echo '</td></tr></table><p>';

require_once('control.php');

?>