<?php

/*********************
 *    control.php    *
 *********************/

ob_start();

echo '
<link rel="stylesheet" href="stylesheet.css" type="text/css">
<table style="border-collapse: collapse" bordercolor="#111111" cellpadding="3" cellspacing="0" border="1" width="600"><tr class="titlebar" align="center"><td>
<b><font color="#808080">.</font><font color="#666666">:</font> User Control Panel <font color="#666666">:</font><font color="#808080">.</font></b>
</td></tr></table>

<table border="0"><tr><td height="1"></td></tr></table>
<table style="border-collapse: collapse" bordercolor="#111111" cellpadding="10" cellspacing="0" border="1" width="600"><tr class="paragraph" align="left"><td>
';

require_once('validate.php');
require_once('config.php');

$buttons=array('Login','Join','Logout','Add a User','Delete a User','Change Password','Change Your Password');
$actions=array('login.php','join.php','logout.php','adduser.php','deluser.php','edituser.php','changepass.php');
$permissions=array('0','0','12','2','2','2','1'); //0 is not logged in, 1 is logged in user, 2 is logged in admin

if(isAdmin($_COOKIE['username'])) $your_p='2';
elseif(isset($_COOKIE['username'],$_COOKIE['password'])) $your_p='1';
else $your_p='0';

for($i=0; $i<sizeof($permissions); $i++)
{
	if(($permissions[$i] == $your_p || strstr($permissions[$i],$your_p)) && !stristr($self,$actions[$i]))
		echo '<p><form action="'.$actions[$i].'"><input class="button" type="submit" value="'.$buttons[$i].'"></form>';
}

?>

</td></tr></table>