<?php

/**********************
 *    validate.php    *
 **********************/

require_once('config.php');

function checkLogin($user,$pass)
{
	global $file;	
	$users=file($file) or die("Could not open file <b>$file</b>");
	foreach($users as $userInfo)
	{
		if ( trim(substr($userInfo,33))==$user && substr($userInfo,0,32)==$pass )
			return 1;
	}
	return 0;
}

function isAdmin($user)
{
	global $file;
	$users=file($file) or die("Could not open file <b>$file</b>");
	foreach($users as $userInfo)
	{
		if ( trim(substr($userInfo,33))==$user )
			return substr($userInfo,32,1);
	}
	return 0;
}

function isUsername($user)
{
	global $file;
	$users=file($file) or die("Could not open file <b>$file</b>");
	foreach($users as $userInfo)
	{
		if ( trim(substr($userInfo,33))==$user )
			return 1;
	}
	return 0;
}

?>
