<?php

/*********************
 *    adduser.php    *
 *********************/

ob_start();

echo '
<link rel="stylesheet" href="stylesheet.css" type="text/css">
<table style="border-collapse: collapse" bordercolor="#111111" border="1" cellpadding="3" cellspacing="0" width="600"><tr class="titlebar" align="center"><td>
<b><font color="#808080">.</font><font color="#666666">:</font> Add User <font color="#666666">:</font><font color="#808080">.</font></b>
</td></tr></table>

<table border="0"><tr><td height="1"></td></tr></table>
<table style="border-collapse: collapse" bordercolor="#111111" cellpadding="10" cellspacing="0" border="1" width="600"><tr class="paragraph" align="left"><td>
';

require_once('validate.php');
require_once('config.php');

if(!isset($_COOKIE['username'],$_COOKIE['password']))
	die('You must first <a href="login.php">login</a>.');

if(!checkLogin($_COOKIE['username'],$_COOKIE['password']))
	die('Invalid login.');

if(!isAdmin($_COOKIE['username']))
	die('You must be an admin to access this page.');

if(isset($_POST['username'],$_POST['password1'],$_POST['password2'],$_POST['admin']))
{
	if($_POST['username'] == "") echo 'You must enter a username.';
	elseif(isUsername($_POST['username'])) echo 'That username already exists.';
	elseif($_POST['password1']!="" && $_POST['password1'] == $_POST['password2'])
	{
		$handle=fopen($file,'a') or die("Could not open file: $file");
		fwrite($handle,md5($_POST['password1']).$_POST['admin'].$_POST['username']."\n");
		echo 'User added.<p>';
	}
	else echo 'Passwords do not match or they are blank.<p>';
}

echo "

<form method='post' action='$self'>
Username: <input type='text' name='username'>
<br>Password: <input type='password' name='password1'>
<br>Password: <input type='password' name='password2'>
<br>Is Admin?: <select name='admin'><option selected value='0'>No</option><option value='1'>Yes</option></select>
<p><input class='button' type='submit' value='Add User'>
</form>

</tr></td></table><p>

";

require_once('control.php');

?>