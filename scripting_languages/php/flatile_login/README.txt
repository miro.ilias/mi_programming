/********************
 *    readme.txt    *
 ********************/


by Justin Hagstrom
April 06, 2003


These files make up a basic system for managing user accounts
through a flat-file system (where data is kept in a file, rather
than a database).


The default accounts in userlist.txt are (username/password) :
admin/admin
test/test


In the config.php file, you must edit the $freeJoin and $file
variables:

$file is the path to the file where the userlist is kept.
The file must be chmod'ed to 777 if it is on a Unix machine.
I would reccomend putting it in a folder that is not accessable
through the websever; this will make it more secure.

$freeJoin determines whether people can create accounts themselves.
If it is set to 0, the join.php page will be disables, and an
Admin must create accouts using adduser.php.
