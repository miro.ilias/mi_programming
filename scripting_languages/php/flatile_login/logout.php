<?php

/********************
 *    logout.php    *
 ********************/

ob_start();

echo '
<link rel="stylesheet" href="stylesheet.css" type="text/css">
<table style="border-collapse: collapse" bordercolor="#111111" border="1" cellpadding="3" cellspacing="0" width="600"><tr class="titlebar" align="center"><td>
<b><font color="#808080">.</font><font color="#666666">:</font> Logout <font color="#666666">:</font><font color="#808080">.</font></b>
</td></tr></table>

<table border="0"><tr><td height="1"></td></tr></table>
<table style="border-collapse: collapse" bordercolor="#111111" cellpadding="10" cellspacing="0" border="1" width="600"><tr class="paragraph" align="left"><td>
';

if(!isset($_COOKIE['username'],$_COOKIE['password']))
	die('You are already logged out.<p>Do you want to <a href="login.php">login</a>?');

setcookie('username', $_COOKIE['username'], time() - 3600);
setcookie('password', $_COOKIE['password'], time() - 3600);

echo 'You have been logged out.<p>Do you want to <a href="login.php">login</a>?</td></tr></table>';

?>