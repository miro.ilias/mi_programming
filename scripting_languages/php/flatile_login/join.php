<?php

/******************
 *    join.php    *
 ******************/

ob_start();

echo '
<link rel="stylesheet" href="stylesheet.css" type="text/css">
<table style="border-collapse: collapse" bordercolor="#111111" border="1" cellpadding="3" cellspacing="0" width="600"><tr class="titlebar" align="center"><td>
<b><font color="#808080">.</font><font color="#666666">:</font> Join <font color="#666666">:</font><font color="#808080">.</font></b>
</td></tr></table>

<table border="0"><tr><td height="1"></td></tr></table>
<table style="border-collapse: collapse" bordercolor="#111111" cellpadding="10" cellspacing="0" border="1" width="600"><tr class="paragraph" align="left"><td>
';

require_once('config.php');
require_once('validate.php');

if($freeJoin)
{

if(isset($_POST['username'],$_POST['password1'],$_POST['password2']))
{
	if($_POST['username'] == "") echo 'You must enter a username.';
	elseif(isUsername($_POST['username'])) echo 'That username already exists.';
	elseif($_POST['password1']!="" && $_POST['password1'] == $_POST['password2'])
	{
		$handle=fopen($file,'a') or die("Could not open file: $file");
		fwrite($handle,md5($_POST['password1']).'0'.$_POST['username']."\n");
		echo 'User added.<p>';
	}
	else echo 'Passwords do not match or they are blank.<p>';
}

echo "

<form method='post' action='$self'>
Username: <input type='text' name='username'>
<br>Password: <input type='password' name='password1'>
<br>Password: <input type='password' name='password2'>
<p><input class='button' type='submit' value='Join'>
</form>

";

}
else echo 'You must contact an Admin before you can join.';

echo '</td></tr></table><p>';

require_once('control.php');

?>