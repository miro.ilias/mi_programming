<?php
function barber($type)
{
  echo "Hello ! I'm a barber. You wanted a $type haircut, no problem. <BR>";
}

call_user_func('barber', "<strong>mushroom</strong>");

call_user_func('barber', "<strong>"."shave"."</strong>");

echo "<HR>";
// ===================================
class myclass {
   function say_hello()
   {
       echo "Ahoj!"."  "." Som v clenskej metode triedy !";
   }
}
$classname = "myclass";
call_user_func(array($classname, 'say_hello'));


echo "<HR>";
// ===================================
function increment(&$var)
{
   $var++;
}

$a = 0;
call_user_func('increment', $a);
echo $a; // 0

call_user_func_array('increment', array(&$a)); // You can use this instead
echo $a; // 1

?> 
