<?php

echo 'this is a simple string<BR>';

// Toto nefunguje !
echo "Toto je string ukonceny linefeed\n\r";


echo"<br><HR>";

echo 'You can also have embedded newlines in 
strings this way as it is
okay to do?<BR>';

// Outputs: "I'll be back"
echo 'Arnold once said: "I\'ll be back\n"<BR>';

// Outputs: You deleted C:\*.*?
echo 'You deleted C:\\*.*?<BR>';

// Outputs: You deleted C:\*.*?
echo 'You deleted C:\*.*?<BR>';

// Outputs: This will not expand: \n a newline
echo 'This will not expand: \n a newline<BR>';

// Outputs: Variables do not $expand $either
echo 'Variables do not $expand $either<BR>';

?>

