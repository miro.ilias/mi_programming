<?php

echo "<H2> Test PHP </H2>";


    echo "This is a test<BR>"; // This is a one-line c++ style comment
    /* This is a multi line comment
       yet another line of comment */
    echo "This is yet another test<BR><BR><BR>";
    echo "One Final Test<BR>"; # This is shell-style style comment


if ($expression) { 
    ?>
    <strong>This is true.</strong>
    <?php 
} else { 
    ?>
    <strong><BR><BR>This is false.</strong>
    <?php 
}

?>

<h1>This is an <?php # echo "simple";?> example.</h1>
<p>The header above will say 'This is an example'.


