<?

//
//  The image functions require the GD library to be installed. !!!
//
//
// create an image with width 100px, height 20px
$image = imagecreate(100, 20);

// create a red colour for the background
// as it is the first call to imagecolorallocate(), this fills the background automatically
$red_background = imagecolorallocate($image, 255, 0, 0);
// create a black colour for writing on the image
$black = imagecolorallocate($image, 0, 0, 0);

// write the string "vdhri.net" on the image
// the top left of the text is at the position (10, 2)
imagestring($image, 4, 10, 2, 'vdhri.net', $black);

// output the image
// tell the browser what we're sending it
Header('Content-type: image/png');
// output the image as a png
imagepng($image);

// tidy up
imagedestroy($image);
?>
