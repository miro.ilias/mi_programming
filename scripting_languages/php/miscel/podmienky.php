<?php

$bool = TRUE;   // a boolean
$str  = "foo";  // a string
$int  = 12;     // an integer

echo gettype($bool),"<BR>"; // prints out "boolean"
echo gettype($str),"<BR>";  // prints out "string"

// If this is an integer, increment it by four
if (is_int($int)) {
    echo "<BR> integer =: $int";
    $int += 4;
    echo "<BR>Integer incerased by 4, =: $int";
}

// If $bool is a string, print it out
// (does not print out anything)
if (is_string($bool)) {
    echo "String: $bool";
}

// == is an operator which test
// equality and returns a boolean
if ($action == "show_version") {
    echo "The version is 1.23";
}

// this is not necessary...
$show_separators = TRUE;

if ($show_separators == TRUE) {
    echo "<hr>\n";
}

if ($show_separators) {
    echo "<hr>\n";
}


?>

