<?php

if (strstr($_SERVER["HTTP_USER_AGENT"], "MSIE")) {
?>
<h3>strstr must have returned true</h3>
<center><b>You are using Internet Explorer</b></center>
<?php
}
// ....
 else {
// ... nasleduje cast http jazyka...
?>
<h3>strstr must have returned false!</h3>
<center><b>You are not using Internet Explorer</b></center>
<?php
}
// volaco na testovanie....
$jmeno = "Marek";
if($jmeno == "Marek") echo "V premennej jmeno je ulozena hodnota Marek<BR>";

$jmeno = "Marek";
if($jmeno == "Marek"):
   echo "Podmínka splněna";
   echo "V proměnné jmeno je uložena hodnota Marek";
else:
   echo "Podmínka nesplněna";
   echo "V proměnné jmeno není uložena hodnota Marek";
endif;

?>

