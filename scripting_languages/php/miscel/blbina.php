<?php

  $blbina = 1;                               // puvodni hodnota
    echo ("<p>");
    echo ("Typ blbiny: ");
    echo (gettype($blbina));                 // typ blbiny ;)
    echo ("<br />");
    echo ("Blbina: $blbina");                // hodnota blbiny :)
    echo ("</p>");

  $blbina = (double) $blbina;                // pretypovani na double
    echo ("<p>");
    echo ("Typ blbiny po pretypovani: ");
    echo (gettype($blbina));
    echo ("<br />");
    echo ("Blbina: $blbina");
    echo ("</p>");

  $blbina = (int) $blbina;                   // pretypovani zpet na integer
    echo ("<p>");
    echo ("Typ blbiny po dalsom pretypovani: ");
    echo (gettype($blbina));
    echo ("<br />");
    echo ("Blbina: $blbina");
    echo ("</p>");

  $blbina = (string) $blbina;                // pretypovani na string
    echo ("<p>");
    echo ("Typ blbiny po pretypovani na string: ");
    echo (gettype($blbina));
    echo ("<br />");
    echo ("Blbina: $blbina");
    echo ("</p>");

  $kravina = (int) $blbina;                  // kravina ziska blbinu v integeru
    echo ("<p>");
    echo ("Typ kraviny: ");
    echo (gettype($kravina));                // typ kraviny
    echo ("<br />");
    echo ("Kravina: $kravina");              // hodnota kraviny
    echo ("</p>");
?>

