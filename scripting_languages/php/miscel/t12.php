<?php 

// simple callback example
function foobar() {
    echo "hello world!";
}
   // NEFUNGUJE ANI JEDNO Z TOHO!
//call_user_function('foobar'); 
//call_user_function("foobar"); 
//call_user_function(foobar); 

// method callback examples
class foo {
  function bar() {
    echo "hello world!";
  }
}

$foo = new foo;

call_user_function(array($foo, "bar")); // object method call

call_user_function(array("foo", "bar")); // static class method call

?>

