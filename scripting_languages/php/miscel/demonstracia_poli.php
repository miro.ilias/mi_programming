<?php

// Demonstracia array

$arr = array(
       "foo" => "bar",
        12 => true,
         d => 1.2345);

echo $arr["foo"]; // bar 

echo "<BR>";

echo $arr[12];    // 1

echo "<BR>";

echo $arr[d];    // cislo

echo "<H1> Zase ina ukazka pola </H1><BR>";

$arrx = array("somearray" => array(6 => 5, 13 => 9, "a" => 42));

echo $arrx["somearray"][6];    // 5
echo $arrx["somearray"][13];   // 9
echo $arrx["somearray"]["a"];  // 42


?>

