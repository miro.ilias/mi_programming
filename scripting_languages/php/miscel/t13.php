<?php
$foo = "0";  // $foo is string (ASCII 48)
<!-- bad example, no real operator (must be used with variable, modifies it too)
$foo++;      // $foo is the string "1" (ASCII 49)
-->
$foo += 2;   // $foo is now an integer (2)
$foo = $foo + 1.3;  // $foo is now a float (3.3)
$foo = 5 + "10 Little Piggies"; // $foo is integer (15)
$foo = 5 + "10 Small Pigs";     // $foo is integer (15)
<!--

TODO: explain ++/- - behaviour with strings

examples:

++'001' = '002'
++'abc' = 'abd'
++'xyz' = 'xza'
++'9.9' = '9.0'
++'-3'  = '-4'
- -'9'   = 8 (integer!)
- -'5.5' = '5.5'
- -'-9'  = -10 (integer)
- -'09'  = 8 (integer)
- -'abc' = 'abc'

-->
?>

