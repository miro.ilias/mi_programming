A simple login script that I made. Modify this to suite your need. It use session to store data in session file submited by the page, 
In this example the username and password will be save in the session file created by php.

I introduce the following files.

index.php                         -script file as name of your protected site.
login.html                        -script file as your login page. 
user                              -folder
  test.txt                        -account file under folder(user)
test|password                         -the content of the test.txt, username is test password is password

What you are going to do?

1. In your php.ini, enable session.use_cookies and specify session.save_path

2. Create a folder name "user" under it create an account file   example: "test.txt"

3. The content of your test.txt will be like this
test|password

*note
account filename is the same as username.

5.copy the script and place it to your web directory.

4.edit the index.php and change the $root=to the actual full path where folder(user) is located.

=============================================================================
5.and insert the code of your protected website as shown in the script.
<!//your script starts here when account is valid--->
<! insert your code here.....---->
Password Protected Website<br>
YOU ARE LOGIN AS <? echo $username ?>  <br>

<a href=<?php echo"$SCRIPT_NAME";?>?logout=1>Logout</a>


<!//your script ends here--->

6:  login and use test as username and password as password.

