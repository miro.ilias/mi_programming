// +---------------------------------------------+
// |     IP2COUNTRY                              |
// |     http://www.SysTurn.com                  |
// +---------------------------------------------+
//
// DESCRIPTION:
//   'IP2COUNTRY' provides functions to retrive real user IP address,
//   and to know the country of any IP.
//
// ADVANTAGES:
//   - Does its job very fast because of using indexes in the search.
//   - Doesn't need any database.
//
// ABOUT THE AUTHOR:
//   ip2country function was written by http://www.php.net developers in the first place,
//   But there was a bug in their function, I solved it and sent the batch for them
//   http://bugs.php.net/bug.php?id=30313
//   If you need any help or faced any troubles in PHP, ASP.NET, C# OR even Designing
//   feel free to contact me by email bakr_fathy@yahoo.com
//   OR vist my website http://SysTurn.com (opening April 2005)
//
// USAGE & EXAMPLES:
//   See the included file 'example.php'
//
// Thanks,
//   Bakr Alsharif
//   bakr_fathy@yahoo.com
//   http://SysTurn.com  (opening April 2005)
//   +20127809973
//
