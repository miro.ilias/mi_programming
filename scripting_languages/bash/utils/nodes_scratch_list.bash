#!/bin/bash
for i in `pbsnodes | grep "^comp"`
do

 # echo "Removing data from scratch on $i"
 # ssh $i rm -rf /mnt/local/milias/*

 echo -e "\nScratch on $i :"
 ssh $i "df -h | grep /mnt/local"
 ssh $i "du -sh /mnt/local/milias"
  echo
done
