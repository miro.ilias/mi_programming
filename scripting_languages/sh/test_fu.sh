#!/bin/sh


# declaration of functions ....
#get_answer ("$default_answer") {
#get_answer ('$default_answer') {
get_answer () {
#----------------------------------------------------------------
#
#  usefull procedure to get (typed) the answer - yes or no
#
#  uses the default variable "default_answer"
#
#----------------------------------------------------------------
answer=
while [ "$answer" != "y" -a "$answer" != "n" ]
do

     read answer

# if answer empty and default_answer set
     if [ -z "$answer" -a "$default_answer" = "y" ]; then
            answer="y"
         #   return $answer
     fi
     if [ -z "$answer" -a "$default_answer" = "n" ]; then
               answer="n"
          #   return $answer
     fi

         # if "answer" not found, complain of it ... 
     if [ "$answer" != "y" -a "$answer" != "n" ]; then
           $echo '     please type the proper answer - yes(y) or no(n) \c'
     fi
done
}

# own program ...

echo 'type answer'
get_answer
#echo $x

#if [ get_answer="y" ]; then
# echo "I got Y answer !"
#fi





