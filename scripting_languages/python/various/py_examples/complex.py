#!/usr/bin/python

print "Working with complex numbers!"
a = complex(-10,20.5)   
b=complex(7,-3.45)
print "a=",a,"b=",b
#print "abs(a)=",abs(a),sqr(a.real*a.real+b.imag*b.imag)
print "a.real=",a.real,"b.imag=",b.imag
print "a+b=",a+b," a/b=",a/b," a*b=",a*b

#########################################################################

r1 = int(raw_input("Vloz realnu  zlozku  1. komplex. cisla: "))
i1 = int(raw_input("Vloz imaginarnu cast 1. komplex. cisla: "))
print '-'*40
r2 = int(raw_input("Vloz realnu  zlozku  2. komplex. cisla: "))
i2 = int(raw_input("Vloz imaginarnu cast 2. komplex. cisla: "))
print '-'*40
x1 = complex(r1,i1)
x2 = complex(r2,i2)

print "sucet obidvoch kompl.cisel: ",x1+x2
print "rozdiel obidvoch kompl.cisel: ",x1-x2
print "sucin obidvoch kompl.cisel: ",x1*x2
print "podiel obidvoch kompl.cisel: ",x1/x2
print "absolutna hodnota prveho kompl.cisla: ",abs(x1)
print "absolutna hodnota druheho kompl.cisla: ",abs(x2)
