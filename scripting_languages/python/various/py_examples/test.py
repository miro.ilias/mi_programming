## This program runs a test of knowledge

true = 1
false = 0

# First get the test questions
# Later this will be modified to use file io.
def get_questions():
    # notice how the data is stored as a list of lists
    return [["What color is the daytime sky on a clear day?","blue"],\
            ["What is the answer to life, the universe and everything?","42"],\
            ["What is a three letter word for mouse trap?","cat"]]

# This will test a single question
# it takes a single question in
# it returns true if the user typed the correct answer, otherwise false

def check_question(question_and_answer):
    #extract the question and the answer from the list
    question = question_and_answer[0]
    answer = question_and_answer[1]
    # give the question to the user
    given_answer = raw_input(question)
    # compare the user's answer to the testers answer
    if answer == given_answer:
        print "Correct"
        return true
    else:
        print "Incorrect, correct was:",answer
        return false

# This will run through all the questions
def run_test(questions):
    if len(questions) == 0:
        print "No questions were given."
        # the return exits the function
        return
    index = 0
    right = 0
    while index < len(questions):
        #Check the question
        if check_question(questions[index]):
            right = right + 1
        #go to the next question
        index = index + 1
    #notice the order of the computation, first multiply, then divide
    print "You got ",right*100/len(questions),"% right out of",len(questions)

#now lets run the questions
run_test(get_questions())


