menu_item = 0

list = []

while menu_item != 9:
        print "--------------------"
        print "1. Print the list"
        print "2. Add a name to the list"
        print "3. Remove a name from the list"
        print "4. Change an item in the list"
        print "9. Quit"
        menu_item = input("Pick an item from the menu: ")

        if menu_item == 1:
                current = 0
                if len(list) > 0:
                        while current < len(list):
                                print current,". ",list[current]
                                current = current + 1
                else:
                        print "List is empty"
        elif menu_item == 2:
                name = raw_input("Type in a name to add: ")
                list.append(name)
        elif menu_item == 3:
                del_name = raw_input("What name would you like to remove: ")
                if del_name in list:
                        item_number = list.index(del_name)
                        del list[item_number]
                        #The code above only removes the first occurance of
                        # the name.  The code below from Gerald removes all.
                        #while del_name in list:
                        #       item_number = list.index(del_name)
                        #       del list[item_number]
                else:
                        print del_name," was not found"
        elif menu_item == 4:
                old_name = raw_input("What name would you like to change: ")
                if old_name in list:
                        item_number = list.index(old_name)
                        new_name = raw_input("What is the new name: ")
                        list[item_number] = new_name
                else:
                        print old_name," was not found"
print "Goodbye"


