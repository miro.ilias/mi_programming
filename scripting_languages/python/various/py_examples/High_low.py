#Plays the guessing game higher or lower 
# (originally written by Josh Cogliati, improved by Quique)

#This should actually be something that is semi random like the
# last digits of the time or something else, but that will have to
# wait till a later chapter.  (Extra Credit, modify it to be random
# after the Modules chapter)
number = 78
guess = 0

while guess != number : 
    guess = input ("Guess a number: ")

    if guess > number :
        print "Too high"

    elif guess < number :
            print "Too low"

print "Just right"


