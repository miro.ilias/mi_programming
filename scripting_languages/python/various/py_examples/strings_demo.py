#!/usr/bin/python

import string

print "Hello, working with strings !"

print  'it doesn\'t...'

print  '"Isn\'t," she said.'

hello = "This is a rather long string containing\n\
several lines of text just as you would do in C.\n\
    Note that whitespace at the beginning of the line is\
 significant."

print hello

print """
Usage: thingy [OPTIONS] 
     -h                        Display this usage message
     -H hostname               Hostname to connect to
"""

###############################
word = 'Help' + 'A'
print word, word[0:2], word[:3],word[3:]
print '<' + word*5 + '>'
print word[:2] + word[2:], word[:3] + word[3:]
print 'len("',word,'")=',len(word)
#print 'capitalize("',word,'")=',capitalize(word)


