#
# converts temperature to fahrenheit or celsius
#

def print_options():
    print ("Options:")
    print (" 'p' print options")
    print (" 'c' convert from celsius")
    print (" 'f' convert from fahrenheit")
    print (" 'q' quit the program")


def celsius_to_fahrenheit(c_temp):
    return float(9.0/5.0)*float(c_temp)+float(32)

def fahrenheit_to_celsius(f_temp):
    return (float(f_temp) - float(32.0))*float(5.0/9.0)

choice = "p"
while choice != "q":
    if choice == "c":        
        temp = input("Celsius temperature:")
        print ("Fahrenheit:",celsius_to_fahrenheit(temp))       
    elif choice == "f":
        temp = input("Fahrenheit temperature:")
        print ("Celsius:",fahrenheit_to_celsius(temp))
    elif choice != "q":
        print_options()
    choice = input("option:")


