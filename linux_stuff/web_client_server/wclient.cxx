/* A simple HTTPS client
   It connects to the server, makes an HTTP
   request and waits for the response
*/
#include "common.h"
#include "client.h"
#include <iostream.h>

static char *REQUEST_TEMPLATE=
   "GET / HTTP/1.0\r\nUser-Agent:"
   "EKRClient\r\nHost: %s:%d\r\n\r\n";

static char *host=HOST;
static int port=PORT;
static int require_server_auth=1;

 static int http_request(SSL *ssl)
  {
    char *request=0;
    char buf[BUFSIZZ];
    int r;
    int request_len;

// cout <<"Som v rutine http_request...REQUEST_TEMPLATE=" << REQUEST_TEMPLATE;
    
    /* Now construct our HTTP request */
    request_len=strlen(REQUEST_TEMPLATE)+strlen(host)+6;

    if(!(request=(char *)malloc(request_len)))
      err_exit("Couldn't allocate request");

 //   printf("Pre snprintf...request=%s  request_len=%d \n",
	//    request,request_len);

    snprintf(request,request_len,REQUEST_TEMPLATE,
      host,port);

    /* Find the exact request_len */
    request_len=strlen(request);

    r=SSL_write(ssl,request,request_len);
    switch(SSL_get_error(ssl,r)){      
      case SSL_ERROR_NONE:
        if(request_len!=r)
          err_exit("Incomplete write!");
        break;
        default:
          berr_exit("SSL write problem");
    }
    
    /* Now read the server's response, assuming
       that it's terminated by a close */
      int len;
    while(1){
      r=SSL_read(ssl,buf,BUFSIZZ);

      switch(SSL_get_error(ssl,r)){
        case SSL_ERROR_NONE:
             len=r;
          break;
        case SSL_ERROR_ZERO_RETURN:
          goto shutdown;
        case SSL_ERROR_SYSCALL:
          fprintf(stderr,
            "SSL Error: Premature close\n");
          goto done;
        default:
          berr_exit("SSL read problem");
      }
  //  printf("  buf=%s\n",buf);
     if (len>0) { fwrite(buf,1,len,stdout);  }     
  }
    
  shutdown:  // shut down a TLS/SSL connection
    r=SSL_shutdown(ssl);
    switch(r){
      case 1:
     //   cout << "SSL_shutdown sa uspesne vykonalo...\n";
        break; /* Success */
      case 0:
      case -1:
      default:
        berr_exit("Shutdown failed");
    }
    
  done:
    SSL_free(ssl);
    free(request);
    return(0);
  }
    
 int main(int argc,char **argv)
 {
    SSL_CTX *ctx;
    SSL *ssl;
    BIO *sbio;
    int sock;
    extern char *optarg;
    int c;

 // printf("main: argc=%d \n",argc);

    while((c=getopt(argc,argv,"h:p:i"))!=-1){
      switch(c){
        case 'h':
    //      printf("Bol switch h!\n");
          if(!(host=strdup(optarg)))
            err_exit("Out of memory");
          break;
        case 'p':
      //    printf("Bol switch p!\n");
          if(!(port=atoi(optarg)))
            err_exit("Bogus port specified");
          break;
        case 'i':
       //   printf("Bol switch i!\n");
          require_server_auth=0;
          break;
      }
    }

    /* Build our SSL context*/
 //   printf("Idem volat initialize_ctx...\n");
  //  printf("KEYFILE=%s PASSWORD=%s\n",KEYFILE,PASSWORD);

    ctx=initialize_ctx(KEYFILE,PASSWORD);

    /* Connect the TCP socket*/
    sock=tcp_connect(host,port);
  //  printf("Po volani tcp_connect....host=%s port=%d\n",host,port);

    /* Connect the SSL socket - vvytvor novu SSL styrukturu pre spojenie */
    ssl=SSL_new(ctx);
    if (ssl==NULL) {cout << "Spadlo volanie SSL_new!\n"; exit(-1);}


    sbio=BIO_new_socket(sock,BIO_NOCLOSE);
    SSL_set_bio(ssl,sbio,sbio);

    if(SSL_connect(ssl)<=0)
      berr_exit("SSL connect error");
    if(require_server_auth)
      check_cert(ssl,host);
 
    /* Now make our HTTP request */
    http_request(ssl);

    /* Shutdown the socket */
    destroy_ctx(ctx);
    close(sock);

    exit(0);
  }

