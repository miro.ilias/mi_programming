#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>     
#include <netinet/in.h> // struktury a finkcie suvisiace s Netom
#include <netdb.h>      // pristup k DNS lookapom
#include <arpa/inet.h>  // inet_ntop func 
#include <sys/socket.h> // socket func

struct hostent *hostlist;   /* List of hosts returned
			       by gethostbyname. */
char dotted_ip[15];         /* Buffer for converting
			       the resolved address to
			       a readable format. */
int port;                   /* Port number. */
int sock;                   /* Our connection socket. */
struct sockaddr_in sa;      /* Connection address. */


/* This function gets called whenever the user presses Control-C.
   See the signal(2) manpage for more information. */
void signal_handler(int signum)
{
    switch (signum) {
    case SIGINT:
	printf("\nReceived interrupt signal. Exiting.\n");
	close(sock);
	exit(0);
			
    default:
	printf("\nUnknown signal received. Ignoring.\n");
    }
}


// hlavny program....
int main(int argc, char *argv[])
{
    if (argc < 3) {
	printf("Easy TCP/IP llient.\n");
	printf("Usage: %s <hostname || IP> <port>\n", argv[0]);
	return 1;
    }

    // resolvni hostname na IP (je jedno ci name alebo IP)
    printf("Rezolvujem %s...\n", argv[1]);
    hostlist = gethostbyname(argv[1]);
    if (hostlist == NULL) {
	printf("Chyba: nepodarilo sa resolvnut hostname: %s.\n", argv[1]);
	return 1;
    }

    // ok mame adresu - odfiltruj vsetko co nie je IPv4 (napr. IPv6)
    if (hostlist->h_addrtype != AF_INET) {
	printf("%s nevyzera to ako platna IPv4 adresa.\n",
	       argv[1]);
	return 1;
    }

    // skonvertuj 32-bitovu IP adresu na retazec
    // v pripade ze meno je resolvovane na viac ip nam staci prva
    inet_ntop(AF_INET, hostlist->h_addr_list[0], dotted_ip, 15);
    printf("%s resolvnute na %s.\n", argv[1], dotted_ip);

    // vytvor socket pre spojenie
    sock = socket(PF_INET, SOCK_STREAM, IPPROTO_IP);
    if (sock < 0) {
      printf("Chyba: nepodarilo sa vytvorit socket: %s\n",strerror(errno));
      return 1;
    }

    memset(&sa, 0, sizeof(struct sockaddr_in));

    port = atoi(argv[2]);
    sa.sin_port = htons(port);
	
    // nakopci do sockaddr_in ip adresu (char*)
    memcpy(&sa.sin_addr, hostlist->h_addr_list[0], hostlist->h_length);
	
    sa.sin_family = AF_INET;

    // konektni sa
    printf("Skusam %s na port %i...\n", dotted_ip, port);
    if (connect(sock, (struct sockaddr *)&sa, sizeof(sa)) < 0) {
	printf("Chyba: nepodarilo sa konektnut: %s\n", strerror(errno));
	return 1;
    }

    printf("Oki - Konektnuty! Citam data - stlacte Control-C na ukoncenie.\n");
	
    signal(SIGINT, signal_handler);

    // citaj data dokym nedojde ku chybe
    for (;;) {
	char ch;
	int amt;

	// citaj po jednom byte = neefektivne
	amt = read(sock, &ch, 1);

	// doslo ku chybe ?
	if (amt < 0) {
	    printf("\nChyba pri nacitavani: %s\n", strerror(errno));
	    break;
	} else if (amt == 0) { // => spojenie bolo ukoncene
	    printf("\nSpojenie bolo ukoncene vzdialenym systemom.\n");
	    break;
	}

	// vypis co si nacital na stdout
	putchar(ch);
	fflush(stdout);
    }

    printf("Zatvaram socket.\n");
    close(sock);

    return 0;
}
