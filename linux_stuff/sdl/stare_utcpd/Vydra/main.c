//#include <SDL/SDL.h>
#include <SDL.h>
//#include <SDL/SDL_image.h>
#include <SDL_image.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


#define FRAMES 100

SDL_Surface  *aMainScr;
SDL_Surface  *aImgBg, *aImgPl;

int hwsurface=0, doublebuf=0, fullscreen=0, aPreLoad=0;


SDL_Surface *InitializeVideoSubSys(void) 
{
    SDL_Surface 	*screen;

    // Inicializacia SDL video subsytemu + kontrola chyb
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
	printf("Chyba pri inicializacii SDL: %s\n", SDL_GetError());
	exit(1);
    };
 
    // To Be Sure: program vzdy pred ukoncenim zavola SDL_Quit
    atexit(SDL_Quit);
    
    // test if possible to set
    if ( SDL_VideoModeOK(1024, 768, 24, 
		    (hwsurface ? SDL_HWSURFACE : SDL_SWSURFACE) |                                                                     
		    (doublebuf ? SDL_DOUBLEBUF : 0) |                                                                                 
		    (fullscreen ? SDL_FULLSCREEN : 0)) == 0) {
	printf("Video rezim neni podporovany %s\n", SDL_GetError());
	exit(-1);
    };

    // Nastavenie videomodu (1024x768 (32-bit) true color
//    if ( (screen = SDL_SetVideoMode(800, 600, 24, 
    if ( (screen = SDL_SetVideoMode(1024, 768, 24, 
		    (hwsurface ? SDL_HWSURFACE : SDL_SWSURFACE) |                                                                     
		    (doublebuf ? SDL_DOUBLEBUF : 0) |                                                                                 
		    (fullscreen ? SDL_FULLSCREEN : 0)) ) == NULL) {                                                                     
	printf("Chyba pri nastaveni videomodu: %s\n", SDL_GetError());
	exit(1);
    };
    
    SDL_VideoInfo *aVidInf;
    aVidInf = SDL_GetVideoInfo();

    if ( aVidInf  == NULL) {
	printf("Chyba pri GetVideoInfo: %s\n", SDL_GetError());
	exit(1);
    }

    printf("Video Mode Infos: \n");
    printf("	Is it possible to create hardware surfaces? %i\n", aVidInf->hw_available);
    printf("	Is there a window manager available %i\n", aVidInf->wm_available); 	
    printf("	Are hardware to hardware blits accelerated? %i\n", aVidInf->blit_hw); 	
    printf("	Are hardware to hardware colorkey blits accelerated? %i\n", aVidInf->blit_hw_CC); 	
    printf("	Are hardware to hardware alpha blits accelerated? %i\n", aVidInf->blit_hw_A);
    printf("	Are software to hardware blits accelerated? %i\n", aVidInf->blit_sw);
    printf("	Are software to hardware colorkey blits accelerated? %i\n", aVidInf->blit_sw_CC);
    printf("	Are software to hardware alpha blits accelerated? %i\n", aVidInf->blit_sw_A);
    printf("	Are color fills accelerated? %i\n", aVidInf->blit_fill);
    printf("	Total amount of video memory in Kilobytes %i\n", aVidInf->video_mem);

    return screen;
}

SDL_Surface *LoadPicture(char *paPathName, short int paPreload)
{
    SDL_Surface 	*aTmp,*aImage;

    // Nacitanie bitmapy
    if ( (aTmp = IMG_Load(paPathName)) == NULL) {
	printf("Chyba pri nacitani obrazku: %s\n", SDL_GetError());
	exit(1);
    };

    if (paPreload == 1) {
	aImage = SDL_DisplayFormat(aTmp);
	SDL_FreeSurface(aTmp);
    }
    else
	aImage = aTmp;

    return (aImage);

}

void CopyPicToScreen(SDL_Surface *paScreen, SDL_Surface *paImage,int paX, int paY)
{
    SDL_Rect 		src, dest;

    src.x = 0;
    src.y = 0;
    src.w = paImage->w; // kopiruj cely obrazok
    src.h = paImage->h; // tj. vyska,sirka oblasti ktora sa ide kopirovat jezhodna s obrazkom

    dest.x = paX;
    dest.y = paY;
    dest.w = paImage->w;
    dest.h = paImage->h;

    // vykresli obrazok na Obrazovku
    SDL_BlitSurface(paImage, &src, paScreen, &dest);

}

void MainGameLoop(void)
{
    int aStartTime,aEndTime;
    int i;
    
    aStartTime=time(NULL);

//    CopyPicToScreen(aMainScr, aImgBg, 0, 0);

    for (i=0; i<FRAMES; i++) {
	CopyPicToScreen(aMainScr, aImgBg, 0, 0);
	CopyPicToScreen(aMainScr, aImgPl, i, i);


	// update obrazovky
	if (doublebuf == 1) {
	    SDL_Flip(aMainScr);
	}
	else {
	    SDL_UpdateRect(aMainScr, 0, 0, 0, 0);
	}

	// chvylu pockaj
	// SDL_Delay(10);

    }
    aEndTime=time(NULL);

    printf ("Zvladol %i framov za %i sekund. => %.2f fps\n",FRAMES,aEndTime-aStartTime,(float)FRAMES/(float)(aEndTime-aStartTime));

}

int main(int argc, char *argv[])
{
    int i;
    
    // check args
    if (argc < 2) {
        printf("The Game - help:\n");
	printf("Syntax: %s [rezim] \n",argv[0]);
	printf(" rezim: \n");
	printf("	-fullscreen\n");
	printf("	-hwsurface\n");
	printf("	-preload\n");
	printf("	-doublebuff\n");
	printf("	-normal\n");
        return 1;
    }

    for (i = 0; i < argc; i++) {
	if (!strcmp(argv[i], "-fullscreen")) {
	    printf("Using fullscreen\n");	    
	    fullscreen=1;
	}
	else
	if (!strcmp(argv[i], "-doublebuff")) {
	    printf("Using doublebuff\n");
	    doublebuf=1;
	}
	else
	if (!strcmp(argv[i], "-hwsurface")) {
	    printf("Using hwsurface\n");
	    hwsurface=1;	
	}
	if (!strcmp(argv[i], "-normal")) {
	    printf("Using total lazy normal mode\n");
	    hwsurface=1;	
	}
	if (!strcmp(argv[i], "-preload")) {
	    printf("Using preload\n");
	    aPreLoad=1;	
	}


    }


    aMainScr = InitializeVideoSubSys();

    aImgBg   = LoadPicture("pozadie.jpeg",aPreLoad);
    aImgPl   = LoadPicture("manik.gif",aPreLoad);
    

    MainGameLoop();    
    
    // uvolni pamet alokovanu pre obrazok
    SDL_FreeSurface(aImgBg);
    SDL_FreeSurface(aImgPl);

    return 0;
}
