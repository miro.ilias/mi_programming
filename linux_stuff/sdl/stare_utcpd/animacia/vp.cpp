/* 
 *
 *
 *  */
#include "hlav.h"

void obrazovka:: vykresli_plochu(char *file_name)
{

    screen = SDL_SetVideoMode(640, 480, 8, SDL_SWSURFACE);
    image  = SDL_SetVideoMode(640, 480, 8, SDL_SWSURFACE);
    
    if ( screen == NULL || image == NULL ) {
       fprintf(stderr, "Couldn't set 640x480x8 video mode: %s\n",
          SDL_GetError());
          exit(1);
    }

    atexit(SDL_Quit);

    printf("Set 640x480 at %d bits-per-pixel mode\n",
     screen->format->BitsPerPixel);

   /* Nacitaj BMP obrazok do pamat. miesta */
    image = SDL_LoadBMP(file_name);
   if (image == NULL) {
    fprintf(stderr, "Nemohol nacitat obrazok\
         zo suboru %s: Chyba je: %s\n", file_name, SDL_GetError());
     exit(-1);
   }
    printf ("obrazovka:: vykresli_plochu >>> Rozmery nacitaneho obrazka: %d %d\n",image->w, image->h);

/*
 * 
 *  Palettized screen modes will have a default palette (a standard
 * * 8*8*4 colour cube), but if the image is palettized as well we can
 *    * use that palette for a nicer colour matching
 *  */

 if (image->format->palette && screen->format->palette) {
  SDL_SetColors(screen, image->format->palette->colors, 0,
   image->format->palette->ncolors);
  }

    /* Blit onto the screen surface */
   if(SDL_BlitSurface(image, NULL, screen, NULL) < 0)
     fprintf(stderr, "BlitSurface error: %s\n", SDL_GetError() );

    // Iniciuj obrazok
     SDL_UpdateRect(screen, 0, 0, image->w, image->h);
    

    // Uvolni pamat na obrazok - je v pamati pre obrazovku !
    SDL_FreeSurface(image);
}

