Proper handling of Integer*8 variables & functions
==================================================

Possible problem with g95 (no longer used):

ilias@194.160.135.47:~/programming/fortran/g95_tests/size_problem/.g95  -i8
-Wno=155 test_size.F90 
ilias@194.160.135.47:~/programming/fortran/g95_tests/size_problem/.a.out 
 size= 140733193388037
 size= 10
 size= 15
 size= 20


Fix is through replacing "size(...)" with "int(size(...))". 

 call size_procedure(int(size(a)))

The same holds for
the "shape" function, which is to be replaced by "int(shape(...))".

To generalize this example, for each strict compiler like the g95, one has to
provide proper INTEGER typecasting of all integer functions and variables. To
see places of inconsistent INTEGER/INTEGER types with g95 one first has to
avoid the "-Wno=155" flag suppressing the warning message.

DIRAC code example :

On three different x86_64 PC Linux architectures I (Miro) found (g95 with -i8
flag, 0.93! and 0.92! versions) compilation errors of type:
.
.
.
/usr/bin/g95 -g -Wno=155 -fsloppy-char -i8 -fno-second-underscore -Iinclude
-fmod=modules -cpp -DINT_STAR8 -DUSE_DIRAC_BLAS -DUSE_DIRAC_DLAMCH
-DUSE_DIRAC_DLAPACK -DUSE_DIRAC_ZLAPACK -DVAR_G95 -DPRG_DIRAC -DSYS_LINUX
-DINSTALL_WRKMEM=90000000 -DVAR_MFDS -DMOD_XML -DMOD_OPENRSP -DMOD_OOF
-DMOD_ESR -DMOD_KRCC -DMOD_SRDFT -DMOD_MAGNETIZ -DMOD_MCSCF_spinfree
-DMOD_UNRELEASED -DMOD_CAP -DMOD_ERI -DMOD_DNF  -c -o visual/visual_in_point.o
visual/visual_in_point.F90
In file openrsp/prop_contribs.F90:364

      call oneave(mol, S0, size(p), pp, ccomp, ddime(:size(p)), ffreq, nd, D,
E
                           1
Error: Type mismatch in parameter 'np' at (1).  Passing INTEGER(4) to
INTEGER(8)


Counterexample
Radovan is using g95 0.93! (Aug 18 2010) and gets no warning and no problem
with the "uncorrected" program (!?):

bast@stallo.uit.no:/home/bast/trash> ~/bin/g95/bin/g95 -i8 test.F90 
bast@stallo.uit.no:/home/bast/trash> ./a.out                       
 size= 5
 size= 10
 size= 15
 size= 20
bast@stallo.uit.no:/home/bast/trash> uname -a
Linux stallo-1.local 2.6.18-194.11.3.el5 #1 SMP Fri Sep 17 09:50:20 PDT 2010
x86_64 x86_64 x86_64 GNU/Linux
