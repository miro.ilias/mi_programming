!---------------------------------------------------------------------------------
! short program for testing size() function mainly with the g95 compiler
! Written by Miro Ilias, Matej Bel University, Banska Bystrica, Slovakia
!---------------------------------------------------------------------------------
 program test_size
  real*8 :: a(5)
  character*3 :: b(10)
  integer :: c(15)
  integer, allocatable :: d(:)
  allocate(d(20))
#if defined USE_INT_SIZE
 print *,"using int type casted function  int(size(...)): "
  call size_procedure(int(size(a)))
  call size_procedure(int(size(b)))
  call size_procedure(int(size(c)))
  call size_procedure(int(size(d)))
#else
  call size_procedure(size(a))
  call size_procedure(size(b))
  call size_procedure(size(c))
  call size_procedure(size(d))
#endif
 end program 

 subroutine size_procedure(i_size)
   integer, intent(in) :: i_size
   print *,'size=',i_size
 end subroutine size_procedure
