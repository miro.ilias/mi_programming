module atom

  type element
      integer :: i
      type(element), pointer :: next 
  end type element

end module atom


 program test_dynall
 
   use atom
   type(element), pointer :: head 
 !  type(element), pointer :: head => null()
   integer :: i

   print *,'head 1, is associated(head):',associated(head)
   head => null()
   print *,'head 2, is associated(head):',associated(head)

   do i = 1 , 5
     !call insert_elem(head,i)
     call insert_elem(i,head)
     call print_sll(head)
   enddo

 end program test_dynall

 !subroutine insert_elem(head,i)
 subroutine insert_elem(i,head)
  use atom
  type(element), pointer :: head, temp, temp1, temp_i
  integer, intent(in) :: i
  integer :: j

  print *,'insert_elem: entering i=',i
  print *,'entering head,  ? associated(head)=',associated(head)

  ! .. needed the last term 
  if (associated(head)) then
   temp => head
   temp1 => temp
  else
    temp =>  null()
    temp1 =>  null()
  endif
  do while (associated(temp))
    temp1 => temp
    temp => temp%next
  enddo

  allocate(temp_i)
  temp_i%i = i
  temp_i%next=> null()
  
  if (.not.associated(head)) then
    head => temp_i
    print *,'inserted head%i=',head%i
  else
    !temp%next => temp_i
    temp1%next => temp_i
    print *,'inserted previous temp1%i=',temp1%i,' last temp_i%i=',temp_i%i
  endif

 end subroutine insert_elem

 subroutine print_sll(head)
  use atom
  type(element), pointer :: head, temp

  temp => head
   
  do while (associated(temp))
     print *,'print_sll: printing element i=',temp%i
    temp => temp%next 
  enddo

 end subroutine print_sll

