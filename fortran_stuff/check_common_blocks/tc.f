       Program TEST_COMMON_BLOCKS

       implicit real*8 (a-h,o-z)
       common /common_block/ a,i
 
       a=1.25
       i=-4
       write(*,*) "main prog:", i,a

       call subr

       stop  0
       end

       subroutine subr
       implicit real*8 (a-h,o-z)
C      common /common_block/ a,i  ! does compile perfectly
C      common /common_block2/ i,a   ! does compile with warning...
       common /common_block/ i,a   ! g77 does NOT COMPILE - error with padding

       a=-2.585
       i=8
       write(*,*) "subroutine :", i,a

       return
       end
