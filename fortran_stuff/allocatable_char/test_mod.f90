  module allocatable_character
    
    character, allocatable :: s1(:)

     type, public :: fString
            integer :: size = 0
          character, allocatable :: s(:)
     end type fString

  contains

    function make_string(text,size) result(str)
     integer, intent(in) :: size
     character(*), intent(in) :: text
     type(fString) :: str

      allocate(str%s(isize))

    end function make_string


  end module allocatable_character
