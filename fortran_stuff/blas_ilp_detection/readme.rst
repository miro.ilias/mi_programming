BLAS ILP-MODE DETECTION
========================

ifort test.F90  -L${MKLROOT}/lib/intel64 -lmkl_intel_ilp64 -lmkl_core -lmkl_sequential -lpthread -lm  ... fails, needs -i8 !

ifort test.F90 -lblas ... works

.ifort -i8      -g  my_int_test.F90  -L${MKLROOT}/lib/intel64 -lmkl_intel_ilp64 -lmkl_core -lmkl_sequential -lpthread -lm

GNU BLAS does NOT distinguish between idamax with various parameters !
milias@lxir073.gsi.de:~/Work/programming/miro_ilias_programming/fortran_stuff/blas_ilp_detection/.ifort  -g  idamax_test.F90   -lblas
milias@lxir073.gsi.de:~/Work/programming/miro_ilias_programming/fortran_stuff/blas_ilp_detection/.a.out
 integer4 - I32LP64 mode
 (idamax (1, DX, 1))=           1
 (idamax (N, DX, INCX))=           1
 (idamax (N4, DX, INCX4))=           1
 (idamax (N8, DX, INCX8))=           1


GNU BLAS does NOT distinguish between idamax i4/i8 based on various parameters !
milias@lxir073.gsi.de:~/Work/programming/miro_ilias_programming/fortran_stuff/blas_ilp_detection/.ifort -i8 idamax2_test.F90 -lblas
milias@lxir073.gsi.de:~/Work/programming/miro_ilias_programming/fortran_stuff/blas_ilp_detection/.a.out
 integer8 - ILP64 mode
 (idamax (2, DX, 1))=                     2
 (idamax (N, DX, INCX))=                     2
 sizeof(N)=                     8
 (idamax (N4, DX, INCX4))=                     2
 (idamax (N8, DX, INCX8))=                     2

