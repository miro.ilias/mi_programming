      program i8_test
      integer*8 N_8
      integer*4 N_4(2)
      equivalence (N_8, N_4)
      real*8    a(20)
      integer*8 j8, idamax

      a(:) = 0.0d0
      a(5) = 1.0d0

      N_4(1) = 20
      N_4(2) = 20

      print *, 'N_4, N_8',N_4, N_8

      j8 = idamax(N_8,a,1)

      print *, 'j8 = 5 ?',j8

      end
