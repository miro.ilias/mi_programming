      program i8_test
      real*8    a(3)
      integer*8 j_m,j_p,idamax
      integer*8 n,one_m,one_p

      n = 3
      a(1) =  0.0d0
      a(2) =  1.0d0
      a(3) = -1.0d0

      one_m = -1 ! returns 3 on IBM / 0 on linux (int4/8) 
      one_p = +1 ! returns 2 on IBM / 2 on linux (int4/2)

      print *, 'a:', a

      j_m = idamax(n,a,one_m)
      j_p = idamax(n,a,one_p)

      print *, 'idamax returns (for incr. -1,+1) ',j_m,j_p

      end
