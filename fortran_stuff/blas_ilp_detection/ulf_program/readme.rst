Ulf's program
=============


Linux x86_64
------------

ifort -i8 i8_test.f  idamax_i4.o 
probably i4 blas


ifort -i8 i8_test.f  idamax_i8.o 
probably i8 blas


ifort -i8 i8_test.f  -L${MKLROOT}/lib/intel64 -lmkl_intel_lp64 -lmkl_core -lmkl_sequential -lpthread -lm
 N_4, N_8           1          -1           -4294967295
 j8?                     1
 probably i4 blas


ifort -i8 i8_test.f   -L${MKLROOT}/lib/intel64 -lmkl_intel_ilp64 -lmkl_core -lmkl_sequential -lpthread -lm
 N_4, N_8           1          -1           -4294967295
 j8?                     0
 probably i8 blas


ifort  i8_test.f   -L${MKLROOT}/lib/intel64 -lmkl_intel_ilp64 -lmkl_core -lmkl_sequential -lpthread -lm
 N_4, N_8           1          -1           -4294967295
 j8?                     0
 probably i8 blas


gfortran   i8_test.f   -L${MKLROOT}/lib/intel64 -lmkl_intel_ilp64 -lmkl_core -lmkl_sequential -lpthread -lm
 N_4, N_8           1          -1          -4294967295
 j8?                    0
 probably i8 blas


IBM AIX
-------

xlf90 i8_test.f -lessl
** i8_test   === End of Compilation 1 ===
 N_4, N_8 1 -1 8589934591
 j8? 5099364800
 probably i4 blas


xlf90 -q64 i8_test.f  idamax_i8.o
 N_4, N_8 1 -1 8589934591
 j8? 0
 probably i8 blas


xlf90  i8_test.f  idamax_i4.o
 N_4, N_8 1 -1 8589934591
 j8? 804397504
 probably i4 blas


xlf90 -q64 i8_test.f -lessl6464
 N_4, N_8 1 -1 8589934591
Segmentation fault (core dumped)


xlf90  -q64  i8_test.f -lesslsmp6464
 N_4, N_8 1 -1 8589934591
Segmentation fault (core dumped)


gfortran  -g  -maix64  i8_test.f -lesslsmp6464
 N_4, N_8           1          -1           8589934591
Segmentation fault (core dumped)

