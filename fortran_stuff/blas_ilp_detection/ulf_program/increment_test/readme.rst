gfortran     test_increment.f   -L${MKLROOT}/lib/intel64 -lmkl_intel_lp64 -lmkl_core -lmkl_sequential -lpthread
 idamax returns (for incr. -1,+1)                     0                    2

ifort test_increment.f -lblas
idamax returns (for incr. -1,+1)                      2                     2

ifort -i8  test_increment.f -lblas
idamax returns (for incr. -1,+1)                      2                     2

xlf90 test_increment.f -lblas
idamax returns (for incr. -1,+1)  0 0

xlf90 -q64 test_increment.f -lessl6464
idamax returns (for incr. -1,+1)  3 2

xlf90  test_increment.f -lessl
idamax returns (for incr. -1,+1)  804397448 804397448


Conclusions: increment-method not stable for all libs / servers !

