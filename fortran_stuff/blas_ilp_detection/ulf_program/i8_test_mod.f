      program i8_test

      integer*8 N_8, ONE_8, n
      integer*4 N_4(2)
      equivalence (N_8, N_4)
      real*8    a(5)
      integer*8 j8, idamax

      a(:) = 0.0d0
      a(1) = 1.0d0
      a(2) = 2.0d0
      a(3) = 3.0d0
      a(4) = 4.0d0
      a(5) = -4.0d0

      N_4(1) = 1
      !N_4(1) = -1
      !N_4(1) = -1

      N_4(2) = 0
      !N_4(2) = 1
      !N_4(2) = 0

      !N_4(2) =  2147483647 ! maximum integer*4
      !N_4(2) =  -2147483648 ! minimum integer*4

      ONE_8 = -1

      print *, 'N_4 ',N_4
      print *, 'N_8', N_8

      !N8 = -N8
      !print *, 'N_8', N_8

      N_8 = 5
      !N_8 = 8589934591
      !N_8 = 0
      !print *, 'N_8 reset to ...', N_8

      j8 = idamax(N_8,a,ONE_8)

      print *, 'j8?',j8

      if (j8.eq.0) then
        print *, 'probably i8 blas'
      else
        print *, 'probably i4 blas'
      endif

      end
