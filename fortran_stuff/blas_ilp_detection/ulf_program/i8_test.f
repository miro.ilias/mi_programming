      program i8_test

      integer*8 N_8, ONE_8
      integer*4 N_4(2)
      equivalence (N_8, N_4)
      real*8    a(1)
      integer*8 j8, idamax

      a(1) = 0.0d0
      N_4(1) = 1
      N_4(2) = -1

      ONE_8 = 1

      print *, 'N_4, N_8',N_4, N_8

      j8 = idamax(N_8,a,ONE_8)

      print *, 'j8?',j8

      if (j8.eq.0) then
        print *, 'probably i8 blas'
      else
        print *, 'probably i4 blas'
      endif

      end
