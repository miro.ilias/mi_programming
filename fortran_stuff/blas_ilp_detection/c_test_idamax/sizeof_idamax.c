#include <stdio.h>

void main()
{

//extern int* idamax_(int * , double[] , int *);
//extern long* idamax_(long * , double[] , long *);
//extern * idamax_( long * , double[] , long *);

extern * idamax_(); // external Fortran BLAS function IDAMAX, default as int

//extern  int * idamax_(); // external Fortran BLAS function IDAMAX
//extern  long * idamax_(); // external Fortran BLAS function IDAMAX

//int n=2; int incx=1;
long  n=2; long incx=1;

double dx[2];
dx[0]=1.0; dx[1]=2.0; 

printf("sizeof(n)=%i\n",sizeof(n));
printf("sizeof(*idamax_(&n, dx, &incx))=%i\n",sizeof(*idamax_(&n, dx, &incx)) ); // still returns four !!!
//printf("sizeof(idamax_(&n, dx, &incx))=%i\n",sizeof(idamax_(&n, dx, &incx)) );

// sometimes crashes with wrong integer sizes - on MKL, not with GNU libblas.a !
idamax_(&n, dx, &incx);

}
