#include <stdio.h>

void main()
{

//extern int* idamax_(int * , double[] , int *);
//extern long* idamax_(long * , double[] , long *);
//extern * idamax_( long * , double[] , long *);
extern * idamax_(); // external Fortran BLAS function IDAMAX

//int n=2; int incx=1;
long  n=2; long incx=1;

int rc;

int *rcp;
//long  *rcp;

double dx[2];
dx[0]=1.0; dx[1]=2.0; 

printf("sizeof(n)=%i\n",sizeof(n));

printf("sizeof(*idamax)=%i\n",sizeof(*idamax_));
printf("sizeof((long)*idamax)=%i\n",sizeof((long)(*idamax_)));
printf("sizeof((int)*idamax)=%i\n",sizeof((int)(*idamax_)));
printf("sizeof(idamax)=%i\n",sizeof(idamax_));
//printf("other method=%i\n", (char*)(idamax_+4) - (char*)(idamax_));

//rcp=idamax_(&n, dx, &incx);
rcp=idamax_(&n, dx, &incx);
//printf("rcp=%i\n",*rcp);
printf("sizeof(rc)=%i\n",sizeof(rcp));
printf("sizeof(*rc)=%i\n",sizeof(*rcp));

//idamax_(&n, dx, &incx);

printf("idamax_(&n, dx, &incx)=%i\n",idamax_(&n, dx, &incx) );
printf("sizeof(idamax_(&n, dx, &incx))=%i\n",sizeof(*idamax_(&n, dx, &incx)) );

}
