

icc -g main.c -Wl,--start-group /cvmfs/it.gsi.de/compiler/intel/15.0/composer_xe_2015.2.164/mkl/lib/intel64/libmkl_lapack95_ilp64.a -lmkl_intel_ilp64 -openmp -Wl,--end-group -Wl,--start-group -lmkl_intel_ilp64 -lmkl_intel_thread -lmkl_core -lpthread /usr/lib/x86_64-linux-gnu/libm.so -openmp -Wl,--end-group 

icc sizeof_idamax.c   -Wl,--start-group /cvmfs/it.gsi.de/
compiler/intel/15.0/composer_xe_2015.2.164/mkl/lib/intel64/libmkl_lapack95_lp64.a -lmkl_intel_lp64 -openmp -Wl,--end-group -Wl,--start-group -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -lpthread /usr/lib/x86_64-linux-gnu/libm.so -openmp -Wl,--end-group


gcc main.c -lblas

depends on :

 int n=2; int incx=1;
or  //long  n=2; long incx=1;

-------------------

::

 milias@lxir073.gsi.de:~/Work/programming/miro_ilias_programming/fortran_stuff/blas_ilp_detection/c_test_idamax/. icc -g sizeof_idamax.c -lblas
 milias@lxir073.gsi.de:~/Work/programming/miro_ilias_programming/fortran_stuff/blas_ilp_detection/c_test_idamax/.a.out 
 sizeof(n)=8
  sizeof(*idamax_(&n, dx, &incx))=4
 sizeof(idamax_(&n, dx, &incx))=8

SIZE OF sizeof(*idamax_(&n, dx, &incx)) IS ALWAYS 4 !!!
