program main
implicit none
integer(4)::n,il,iu,m,isuppz(6),iwork(30),lwork,liwork,info
real(8) :: a(3,3),z(3,3),w(3),work(80),vl,vu,abstol

n = 3
il = 1
iu = 3
abstol = 0.0_8
lwork = 80
liwork = 30

a=reshape([0.694444444444445_8,0.208135487069874_8,1.069274406890994E-002_8,0.208135487069874_8,-0.211332634659698_8,&
0.133515358818456_8,1.069274406890994E-002_8,0.133515358818456_8,-0.417584606439560_8],[3,3])

call dsyevr('V','I','U',n,a,n,vl,vu,il,iu,abstol,&
m, w, z, n, isuppz, work, lwork,iwork,liwork,info)
print *, "eval"
print *, w
end program main

