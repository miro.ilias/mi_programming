!
! Areas of shapes of different classes, using different
! function names in each class
!
module class_Rectangle ! define the first object class
type Rectangle; real :: base, height ; end type Rectangle

contains 

! Computation of area for rectangles.
function rectangle_area ( r ) result ( area )
type ( Rectangle ), intent(in) :: r
real :: area
area = r % base * r % height ; ! spocitaj plochu
end function rectangle_area

! Constructor for a Rectangle type
function make_Rectangle (bottom, side) result (name)
! Constructor for a Rectangle type
real, optional, intent(in) :: bottom, side
type (Rectangle) :: name
name = Rectangle (1.,1.) ! default to unit square
if ( present(bottom) ) then ! default to square
name = Rectangle (bottom, bottom) ; end if
if ( present(side) ) name = Rectangle (bottom, side) ! intrinsic
end function make_Rectangle

end module class_Rectangle


module class_Circle ! define the second object class
real :: pi = 3.1415926535897931d0 ! a circle constant

type Circle ! novy datovy typ
real :: radius ; end type Circle

contains ! Computation of area for circles.

function circle_area ( c ) result ( area )
type ( Circle ), intent(in) :: c
real :: area
area = pi * c % radius**2 ;
end function circle_area

end module class_Circle

program geometry ! for both types in a single function

use class_Circle
use class_Rectangle

! ====  Interface to generic routine to compute ====
interface compute_area
   module procedure rectangle_area, circle_area
end interface

 ! Declare a set geometric objects.
! type ( Rectangle ) :: four_sides
type ( Rectangle ) :: four_sides, square, unit_sq

 type ( Circle ) :: two_sides ! inside, outside
 real :: area = 0.0 ! the result

 ! Initialize a rectangle and compute its area.

 four_sides = Rectangle ( 2.1, 4.3 ) ! implicit constructor

 area = compute_area ( four_sides ) ! generic function
 write ( 6,10 ) four_sides, area ! implicit components list
 10 format ("Area of ",f3.1," by ",f3.1," rectangle is ",f5.2)

! Initialize a circle and compute its area.
 two_sides = Circle ( 5.4 ) ! implicit constructor
 area = compute_area ( two_sides ) ! generic function

  write ( 6,20 ) two_sides, area
 20 format ("Area of circle with ",f3.1," radius is ",f9.5 )

! Running gives:
! Area of 2.1 by 4.3 rectangle is 9.03
! Area of circle with 5.4 radius is 91.60885

!====================================================
! Test manual constructors
!====================================================
four_sides = make_Rectangle (2.1,4.3) ! manual constructor, 1
area = compute_area ( four_sides) ! generic function
write ( 6,10 ) four_sides, area


!            Make a square
square = make_Rectangle (2.1) ! manual constructor, 2
area = compute_area ( square) ! generic function

write ( 6,10 ) square, area
! "Default constructor", here a unit square
unit_sq = make_Rectangle () ! manual constructor, 3
area = compute_area (unit_sq) ! generic function
write ( 6,10 ) unit_sq, area 

! Running gives:
! Area of 2.1 by 4.3 rectangle is 9.03
! Area of 2.1 by 2.1 rectangle is 4.41
! Area of 1.0 by 1.0 rectangle is 1.00

end program geometry

