!
!
!
 module nieco

 type balik
  integer :: i=-1
  real*8 :: a=12.56
 end type balik

 contains

 subroutine xx(b)
  type(balik),intent(in) :: b
  print *,'i=',b%i,' a=',b%a
 end subroutine xx

 end module

 program testuj_inicializaciu
 use nieco

 type(balik) :: b
 call xx(b) 
 b = balik(1,2.34)
 call xx(b) 
 
 end
 
