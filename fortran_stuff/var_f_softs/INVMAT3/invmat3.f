***********************************************************************
***  Tato procedura sluzi k najdeniu inverznej matice 3 na 3 k      ***
*** vstupujucej matici A(3 na 3) , ak ona existuje.                 ***
***   Vstup: A....matica 3 na 3                                     ***
***   Vystup: X....inverzna matica k matici A, ak IS=0              ***
***           IS...parameter o tom, ci inverz.matica existuje(0/1)  ***
***********************************************************************

	SUBROUTINE InvMat3(A,X,IS)

	REAL*8 A(3,3) , X(3,3) , D0 , B(2,2) , z

	INTEGER*1 IS , Riadok , Stlpec

	Call DetMat3(A,D0)

	    IS = 0

	write(*,*) "=========DetMat3=",D0

	IF (D0 .eq. 0) THEN
	    IS = 1
	    RETURN
	ENDIF

	DO Riadok = 1 , 3
	   DO Stlpec = 1 , 3
	      Call VypPrvInvMat3(Riadok,Stlpec,D0,A,z)
	      X(Stlpec,Riadok) = z
	   ENDDO
	ENDDO

C -------------------------------------------------------------
	RETURN
	END

C =====================================================================

	Subroutine Detmat2(B,det2)

	Real*8 B(2,2) , det2

	det2 = (B(1,1)*B(2,2))-(B(1,2)*B(2,1))

	Return
	End

C =====================================================================

	Subroutine VypPrvInvMat3(I,J,D0,A,z)

	REAL*8 A(3,3) , z , D0 , det2 , B(2,2)
	INTEGER*1 I , J , Ria , Stl  

        Ria = 0

	Do I1 = 1 , 3
	   If (I1 .ne. I) then
	      Ria = Ria +1
	      Stl = 0
	      Do J1 = 1 , 3
		 If (J1 .ne. J) then
		    Stl = Stl + 1
		    B(Ria,Stl)=A(I1,J1)
		    write(*,*) "Ria= ",Ria," Stl= ",Stl,": I1= ",I1," J1= ",J1
	         EndIf
	      EndDo
	    EndIf
	EndDo

	Call DetMat2(B,det2)
	z = det2 / D0

	Return
	End

C =====================================================================
C == Napisal Miro Ilias dna 4.8.1997 na oslavu Pana a Spasitela J.K. ==
C =====================================================================



	           

	


