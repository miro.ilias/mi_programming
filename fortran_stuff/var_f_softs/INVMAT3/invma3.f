***********************************************************************
***  Tato procedura sluzi k najdeniu inverznej matice 3 na 3 k      ***
*** vstupujucej matici A(3 na 3) , ak ona existuje.                 ***
***   Vstup: A....matica 3 na 3                                     ***
***   Vystup: X....inverzna matica k matici A, ak IS=0              ***
***           IS...parameter o tom, ci inverz.matica existuje(0/1)  ***
***********************************************************************

	SUBROUTINE InvMat3(A,X,IS)

	REAL*8 A(3,3) , X(3,3) , D0 , b(3) , e(3)

	INTEGER*1 IS , I , J 

	Call DetMat3(A,D0) 

	IF (D0. eq. 0) THEN
	   IS = 1
	   RETURN
	ENDIF

	Do I = 1 , 3

c       --- prichysta sa vektor e ---
	   Do J = 1 , 3
	      If (I. ne. J)  then
		 e(J) = 0
	      else
		 e(J) = 1
	      endif
	   EndDo

c        --- Riesi sa sustava linear.rovnic ---
	
	   Call Cramer3(A,e,b,IS)

	   IF (IS .eq. 0) THEN
	      Do J = 1 ,3
		 X(J,I) = b(J)
	      EndDo
	   EndIF

	EndDo

	RETURN
	END

C =====================================================================
C == Napisal Miro Ilias dna 4.8.1997 na oslavu Pana a Spasitela J.K. ==
C =====================================================================



	           

	


