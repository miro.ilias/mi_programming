*******************************************************************
*** Procedura na vycislenie determinantu 3.stupna matice A(3,3) ***
***  Vstupuju: A.....matica 3 na 3                              ***
***  Vystup  : det3mat....determinant tejto matice              ***
*******************************************************************  

	SUBROUTINE DetMat3(A,detmat)

	REAL*8 A(3,3) , detmat

	write(*,*) "Matica A v DetMat3:"
	write(*,*) ((a(i,j),j=1,3),i=1,3)
c	write(*,*) "----",A(1,1)*A(1,2)

	detmat = A(1,1)*((A(2,2)*A(3,3))-(A(3,2)*A(2,3))) -
     &           A(2,1)*((A(1,2)*A(3,3))-(A(3,2)*A(1,3))) +
     &           A(3,1)*((A(1,2)*A(2,3))-(A(1,3)*A(2,2)))
	
	write(*,*) "DetMat3 v procedure = ",detmat
	RETURN

	END

C ******************************************************************
C *** Napisal M.Ilias na oslavu nasho Pana a Spasitela J.K.      ***
C ******************************************************************
