*********************************************************************
***   Tato procedura sluzi k rieseniu sustavy 3 linearnych rovnic ***
*** s troma neznamimi. Korene sa vyrataju Cramerovym pravudlom.   ***
***                          A.x=b 				  ***
*** Vstup: A....matica 3.3 , b....3-rozm.vektor koef.pravej strany***

*** Vystup: x....3-rozmer.vektor korenov ; 
***         IS..parameter o rieseniach tejto sustavy              ***
***    
***  IS=0........sustava ma prave jedno riesenie
***  IS=1........sustava ma nekonecne vela rieseni
***  IS=2........sustava nema riesenie
***      Vstupna matica A a vektor b ostanu nezmenene.            ***
*********************************************************************

	SUBROUTINE Cramer3(A,b,x,IS)

	REAL*8 A(3,3) , b(3) , x(3) , D0 , D(3)
	REAL*8  PomStl(3) 

	INTEGER*1 IS , Stl

	CALL DetMat3(A,D0)

	DO Stl = 1  ,  3
	  
	   CALL VymStlp3(A,b,PomStl,Stl)

	   CALL DetMat3(A,D(Stl))

	   CALL OpacVymStlp3(A,b,PomStl,Stl)

	ENDDO

C   ----------------------------------

	IS = 0

	IF ((D0.eq.0).and.((D(1).eq.0).or.(D(2).eq.0).or.(D(3).eq.0)))
     &    IS = 1

	IF ((D0.eq.0).and.((D(1).ne.0).or.(D(2).ne.0).or.(D(3).ne.0)))
     &    IS = 2

C   ----------------------------------
	IF ( IS .eq. 0 ) THEN

       	   Do I = 1 , 3
	  
	      x( I ) = D(I) / D0

	   EndDo

	ENDIF

	RETURN
	END

C ---------------------------------------------------------------------

	Subroutine VymStlp3(A,b,PomStl,Stl)

	Real*8 A(3,3) , b(3) , PomStl(3)
	Integer*1 Stl

	Do I = 1 , 3

	   PomStl( I )   =   A( I , Stl )
	   A( I , Stl )  =   b(I)

	EndDo
	   
	Return
	End

C --------------------------------------------------------------------- 

	Subroutine OpacVymStlp3(A,b,PomStl,Stl)

	Real*8 A(3,3) , b(3) , PomStl(3)
	Integer*1 Stl

	Do  I  =  1  ,  3

	    A( I , Stl ) = PomStl(I)

	EndDo

	Return
	End

C ====================================================================    
C ===  Napisal Miro Ilias dna 4.8.1997 na oslavu nasho Pana J.K.   ===
C ====================================================================


