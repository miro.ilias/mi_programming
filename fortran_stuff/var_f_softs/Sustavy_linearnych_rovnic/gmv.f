C *********************************************************************
C ** Program na vytvorenie suboru "mv", ktory obsahuje N, maticu    ***
C ** N.N a N-rozmerny vektor pre riesenie sustavy linearnych rovnic ***
C **								    ***
C **               A(N,N) . x(N) = b(N)				    ***
C **								    ***
C *********************************************************************

	PROGRAM Generuj_maticu_A_a_vekror_b

	REAL*4 NAH,A,B,D,DIFF,NMIN,NMAX,PRVOK,NAH1
	INTEGER N
	CHARACTER*1 VOLBA

  5	WRITE(*,*) "ZELATE SI GENEROVANIE CISEL - G/g "
	WRITE(*,*) "ALEBO VLASTNY VSTUP - V/v ? "

	READ(*,"(A1)") VOLBA

	IF (.NOT.((VOLBA.EQ."V").OR.(VOLBA.EQ."G").OR.(VOLBA.EQ."v")
     &   .OR.(VOLBA.EQ."g"))) GOTO 5
	
	open(3,file="mv",access="sequential",status="unknown")

	WRITE(*,*) "VSTUP N - ROZMERU MATICE, MAX.100"
	READ (*,*) N
	write(3,*) N

C -------------------------------------------------------------------
	IF ((VOLBA.EQ."G").or.(VOLBA.eq."g")) THEN
	 write(*,*) "---------------------------------------------"
	 write(*,*) " Genereju sa cisla v intervale <Nmin;Nmax> s "
	 write(*,*) "minimalnou diferenciou DIFF.                 "
	 write(*,*) "---------------------------------------------"

	WRITE(*,*) "Vstup hranic intervalu - Nmin,Nmax: "
	READ(*,*) NMIN,NMAX
	WRITE(*,*) "Vstup minimalnej diferencie medzi dvoma generovanymi
     &cislami - DIFF :"

	READ(*,*) DIFF
	D=-LOG10(DIFF)
	A=NMAX-NMIN+DIFF
	B=NMIN

	ENDIF
C -------------------------------------------------------------------
	DO I=1,N
	   DO J=1,N

	      IF ((VOLBA.EQ."G").or.(VOLBA.EQ."g")) THEN
	         NAH=ranD(NAH)
c		 write(*,*) "NAH=",NAh
	         NAH1=(ANINT((A*NAH)*(10**D))/(10**D))+B
c	         WRITE(*,*) I,J,NAH1
		 write(3,*) I,J,NAH1
              ENDIF

	      IF ((VOLBA.EQ."V").or.(VOLBA.EQ."v")) THEN
	         WRITE(*,"(5x,a,2I4,a)") "VSTUP PRVKU ",I,J," MATICE  A:"
		 READ(*,*) PRVOK
		 WRITE(3,*) I,J,PRVOK
	      ENDIF	              

	   ENDDO
	ENDDO

C ------------------------------------------------------------------
C	Teraz vstupuje vektor
C----------------------------------
	DO I=1,N

	      IF ((VOLBA.EQ."G").or.(VOLBA.EQ."g")) THEN
	         NAH=ranD(NAH)
	         NAH1=(ANINT((A*NAH)*(10**D))/(10**D))+B
		 write(3,*) I,NAH1
              ENDIF

	      IF ((VOLBA.EQ."V").or.(VOLBA.EQ."v")) THEN
	         WRITE(*,"(a,I3,a)") "Vstup prvku ",I," vektora b:  "
		 READ(*,*) PRVOK
		 WRITE(3,*) I,PRVOK
	      ENDIF	              
	   ENDDO
*********************************************************************
	write(*,*) "Hotovo, subor `mv` je hotovy."
	stop 1000

	END
C ===============================================================		
