	program Test_riesenia_systemu_linearnych rovnic


	parameter(ws=100)

	real*8 a(ws,ws) , b(ws) , x(ws) , rv(ws)
	real*8 prv

	integer n

	open(3,file="mv", access="sequential",status="unknown")
	open(4,file="sustava.output",access="sequential",status="unknown")

*** Nacitaj maticu A a vektor b ***   

	read(3,*) n

	do i=1,n
	   do j=1,n
	      read(3,*) ii,jj,a(ii,jj)
           enddo
  	enddo

	do i=1,n
	    read(3,*) b(i)
	enddo

	rewind 3
	
	write(*,*) "Sustava je nacitana !"

**** Riesi sustavu rovnic *****

	call Gauss(n,a,x,b)

	write(*,*) "Sustava je vyriesena procedurou 'gauss' !"

*** Nanovo nacitam maticu a + vektor b *** 

	read(3,*) n

	do i=1,n
	   do j=1,n
	      read(3,*) ii,jj,a(ii,jj)
           enddo
  	enddo

	do i=1,n
	    read(3,*) b(i)
	enddo

	rewind 3

********************************************
*** Najdem rezidualny vektor rv = A.x-b  ***
********************************************

	prv = 0

	do i=1 , n

	   ps=0

	   do j=1 , n
	     ps = ps + (a(i,j)*x(j))
	   enddo

	   rv(i) = ps - b(i)
	   prv   = prv + dabs(rv(i))

	enddo

	prv = prv/n


***************************************************************
**** Nasleduje vypis do suboru ****
***********************************

	write(4,"(10x,a,i2,a,i2,a)") "Nacitana matica A(",n,";",
     &n,") je:" 

	do i=1,n
	   do j=1,n
	      write(4,"(13x,a,i2,a,i2,a,d24.16)") "A(",i,";",j,")=",a(i,j)
	   enddo
	enddo

	write(4,"(10x,a,i2,a)") "Nacitany vektor b(",n,") je:"

	do i=1,n
	   write(4,"(13x,a,i2,a,d24.a6)") "b(",i,")=",b(i)
	enddo

	write(4,"(10x,a)") "*************************************"	
	write(4,"(10x,a)") "    *==== Procedura 'Gauss' ====*"

	write(4,"(13x,a,i2,a)") "Vektor korenov  x(",n,") je:"

	do i=1,n
	   write(4,"(13x,a,i2,a,d20.12)") "x(",i,")=",x(i)
	enddo

	write(4,"(10x,a)") "**********************************************"
	write(4,"(5x,a,d10.5)") "Priemer absolutnych hodnot prvkov rezi
     &dualneho vektora je : ",prv



	stop 150 
	end
	


