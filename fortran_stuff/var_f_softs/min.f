C **************************************************
	program MINPOTKRIV

	double precision x1,x2,x3,y1,y2,y3,a,b,c,d
	double precision citatel,menovatel

	write(*,*) "*****************************************************************"
	write(*,*) " ---   Tento program sluzi k najdeniu minima potencialovej    ---"
	write(*,*) " ---  krivky, pozostavajucej z troch dvojic jej bodov :       ---"
	write(*,*) " ---        [d1,E(d1)] , [d2,E(d2)] , [d3,E(d3)].             ---"
	write(*,*) " ---   Vystupom je mezijadrova vzdialenost d, zodpovedajuca   ---"
	write(*,*) " ---  minimu paraboly, prelozenej tymito troma vstupujucimi   ---"
	write(*,*) " ---  bodmi.				               	  ---"
	write(*,*) " ****************************************************************"
	write(*,*) "vstup d1,E(d1)"
	read(*,*) x1,y1
	write(*,*) "vstup d2,E(d2)"
	read(*,*) x2,y2
	write(*,*) "vstup d3,E(d3)"
	read(*,*) x3,y3

	citatel=((x1-x2)*(x1+x2)*(y2-y3)-(y1-y2)*((x2*x2)-(x3*x3)))
	menovatel=((x1-x2)*((x1+x2)*(x2-x3)+(x3*x3-x2*x2)))

	b=citatel/menovatel

	a=((y1-y2)/((x1*x1)-(x2*x2)))-b*((x1-x2)/(x1*x1-x2*x2))

	d=-b/(2*a)

	write(*,*) " ---------- Nova medzijadrova vzdialenost je: ---------",d

	end

