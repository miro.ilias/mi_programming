
	program READ_INPDAT

c**********************************************************************
c=====	This common contains most common arrays of whole program ======
c===== Program vypise parametre systemu zo suboru INPDAT         ======
c**********************************************************************

c======================================================================
c2	characteristics from MOLCAS
c====================================================================== 

	parameter(maxorb=255)
	integer*2 i

c2.1	Number of active electrons
	integer*4 nactel , krok , tnorb
c
c2.2	spin state of the system
	integer*4 ispin
c
c2.3	number of irreps in the system
	integer*4 nsym
c
c2.4	symmetry state of the system
	integer*4 lsym
c
c2.5	matrix multiplication table
	integer*4 mmul(1:8,1:8)
c
c2.6	vectors containing size of the system
	integer*4 noa(1:8),nob(1:8),nva(1:8),nvb(1:8),norb(1:8)
c
c2.7	orbital energies
	double precision eps(1:maxorb)

c**********************************************************************
c1====================	  read INPDAT       ===========================
c**********************************************************************

	open (unit=1,file='INPDAT',form='unformatted')
	read (1) nactel,ispin,nsym,lsym,mmul,noa,nob,nva,nvb,norb,eps
	close (1)

c=====================================================================

	tnorb=0

	do i=1,nsym
	   tnorb = tnorb + norb(i)
	enddo

	write(*,"(5x,a,I2)") "nactel=",nactel
	write(*,"(5x,a,I2)") "ispin=",ispin
	write(*,"(5x,a,I2)") "nsym=",nsym
	write(*,"(5x,a,I2)") "lsym=",lsym
	write(*,*) "------------------------------------------------"
	write(*,*) "matrix multiplicatiom table:"    
	write(*,*) ((mmul(i,j),i=1,8),j=1,8)
	write(*,*) "------------------------------------------------"

	write(*,*) "noa...."
	write(*,*) (noa(i),i=1,8)

	write(*,*) "nva...."
	write(*,*) (nva(i),i=1,8)

	write(*,*) "nob....."
	write(*,*) (nob(i),i=1,8)

	write(*,*) "nvb....."
	write(*,*) (nvb(i),i=1,8)

	write(*,*) "norb...."
	write(*,*) (norb(i),i=1,8)
	

	write(*,*) "....Orbitalne energie...."

	write(*,*) (eps(j),j=1,tnorb+1)

c********************************************************************** 

	end
