C ****************************************************************
C ** PROCEDURA NA VYTVORENIE SYMETRICKEJ MATICE, KTORA SA ULOZI **
C ** DO SUBORU "SYMMAT"						**
C ****************************************************************

	PROGRAM GENSYMA

	REAL*4 NAH,A,B,D,DIFF,NMIN,NMAX,PRVOK,NAH1
	INTEGER N
	CHARACTER*1 VOLBA

  5	WRITE(*,*) "ZELATE SI GENEROVANIE CISEL SYMETRIC.MATICE - G "
	WRITE(*,*) "ALEBO VLASTNY VSTUP - V ? "

	READ(*,"(A1)") VOLBA

	IF (.NOT.((VOLBA.EQ."V").OR.(VOLBA.EQ."G"))) GOTO 5
	
	open(3,file="symmat",access="sequential",status="unknown")

	WRITE(*,*) "VSTUP N - ROZMERU MATICE, MAX.100"
	READ (*,*) N
	write(3,*) N

C -------------------------------------------------------------------
	IF (VOLBA.EQ."G") THEN

	WRITE(*,*) "VSTUP HRANIC INTERVALU - Nmin,Nmax: "
	READ(*,*) NMIN,NMAX
	WRITE(*,*) "VSTUP MINIMAL. DIFERENCIE MEDZI DVOMA GENER.CIS.:"
	READ(*,*) DIFF
	D=-LOG10(DIFF)
	A=NMAX-NMIN+DIFF
	B=NMIN
c        write(*,*) "A=",A," B=",B," D=",D 
	ENDIF
C -------------------------------------------------------------------
	DO I=1,N
	   DO J=I,N

	      IF (VOLBA.EQ."G") THEN
	         NAH=ranD(NAH)
c		 write(*,*) "NAH=",NAh
	         NAH1=(ANINT((A*NAH)*(10**D))/(10**D))+B
c	         WRITE(*,*) I,J,NAH1
		 write(3,*) I,J,NAH1
              ENDIF

	      IF (VOLBA.EQ."V") THEN
	         WRITE(*,*) "VSTUP PRVKU ",I,J," SYMETR.MATICE:"
		 READ(*,*) PRVOK
		 WRITE(3,*) I,J,PRVOK
	      ENDIF	              
	   ENDDO
	ENDDO
 	stop 20 
	END
C ===============================================================		
