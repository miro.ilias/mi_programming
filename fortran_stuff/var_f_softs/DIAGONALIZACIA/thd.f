C *******************************************************************
C *  Program na testovanie diagonalizacnej procedury pre symetricke *
C * matice. 							    *
C *******************************************************************

	PROGRAM TESTDIAGONALIZACIE

	PARAMETER(SK=50)

	REAL*8	A(SK,SK),VV(SK,SK),ZZ,AP(SK,SK),RSAH,S1
	real*8 ssah,sah,priem,s,NN

	INTEGER N,II,JJ

	OPEN(2,FILE="symmat",ACCESS="SEQUENTIAL",STATUS="UNKNOWN")
	OPEN(3,FILE="HDiag.out",ACCESS="SEQUENTIAL",STATUS="UNKNOWN")

C ======================================================
C === NACiTANIE SYMETRIC.MATICE A ZO SuBORU "SYMMAT" ===
C ======================================================

	READ(2,*) N
	DO I=1,N
	   DO J=I,N
	      READ(2,*) II,JJ,ZZ
	      write(*,*) II,JJ,ZZ
	      A(I,J)=ZZ
c	      A(J,I)=ZZ
	      AP(I,J)=ZZ
	      AP(J,I)=ZZ
	   ENDDO
	ENDDO
	write(*,*) "matica je nacitana !!!"
	close(2,status="keep")

C -------------------- Uvod vystupu do suboru "symmat" --------------------------

	write(3,"(9x,a)") "-------------------------------------------"
	write(3,"(10x,a)") "**********   Nacitana matica   **********"
	write(3,"(9x,a)") "-------------------------------------------"
	write(3,"(21x,a,i2)") "Rad matice: N=",N
	write(3,"(16x,a)") "Prvky symetrickej matice A:"
	write(3,*)

	do i=1,N
	   do j=i,N
	      write(3,"(16x,a2,i2,a1,i2,a2,D20.14)") "A(",i,";",j,")=",A(I,J)
           enddo
	   write(3,*)
	enddo
c	write(3,"(9x,a)") "-------------------------------------------------------"

C ========== MOJA DIAGONALIZ5CIA ==============


	CALL HDIAG(A,VV,N,50,0)

C	write(3,*)
	write(3,"(5x,a)") "****************************************************************"
	write(3,"(5x,a)") "*** Pre diagonalizaciu matice A sa pouzije procedura 'HDIAG'. ***"
	write(3,"(5x,a)") "****************************************************************"
	write(3,*)

C +++++++ Nasleduje vypis vlast.cisel,vlast.vektorov a rezidualnych prvkov do vystupu ++++++++

	sah=0
	ssah=0

	DO I = 1 , N

	   write(3,"(12x,a)") " =============================================== "
	   WRITE(3,"(12x,a,i2,a,D17.10,a)") " ==== ",I,". vlastne cislo: ",A(I,I),"  ==== "
	   write(3,"(12x,a)") " =============================================== "
	   WRITE(3,"(23x,a,)") "*+*+* Vlastny vektor: *+*+* "
	   WRITE(3,*) (VV(J,I),J=1,N)

c           --- vypis vektora rezidui ---

c	   write(3,"(23x,a)") "*=*=* Vektor rezidui: *=*=*"

	   do j=1,N

	      S1=0
              do k=1,N   
	         S1=S1+(AP(j,k)*VV(k,I))
	      enddo

	      S2=S1-(A(I,I)*VV(j,I))
c	      write(3,"(D23.15,0x,$)") S2

	      sah=sah+abs(S2)
	      ssah=ssah+(S2*S2)

	   enddo     
c	   write(3,"(/,0x,a,$)") "Priemer absolutnych hodnot prvkov rezidualneho vektora: "
c	   write(3,"(D17.10)") (pah/N) 
	   write(3,*)
	ENDDo

c	write(3,*) "sah=",sah
c	write(3,*) "ssah=",ssah

	priem=(sah/(N*N))
	NN=dfloat(N)
	s=dsqrt((1/((NN*NN)-1))*(ssah-(2*priem*sah)+(NN*NN*priem*priem)))

	write(3,"(0x,a)") "*****************************************************************************"
	write(3,"(0x,a,$)") "*** Priemer absol. hodnot prvkov vset. rezidual. vektorov: "
	write(3,"(d14.7,A)") priem," ***"
	write(3,"(0x,a)") "***                                                                       ***"
	write(3,"(a,d14.6,a)") "*** Odhad chyby absol.hodnot prvkov vset.rezidual.vektorov:",s," ***"
	Write(3,"(0x,a)") "*****************************************************************************"

c	write(3,"(a,d17.10)") "Odhad chyby:",s

	STOP 10 

	end
