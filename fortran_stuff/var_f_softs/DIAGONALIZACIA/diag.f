C *************************************************************************
C *   Procedura pre Jacobiho diagonalizaciu symetrickej matice A(N,N).    *
C * Vstupuju:  A...symetricka matica N*N,ktora sa zdiagonalizuje	  * 
C * --------   IG=1 alebo IG=0....rozhodovaci parameter	pre vlast.vektory *
C *            N...rad matice (1-SKALA)                                   *
C * Vystup:    A...zdiagonalizovana matica - vlastne cisla ma na diagonale*
C * -------    VV(N,N). . matica vlastnych vektorov, ktore su ulozene     *
C *		          v jej stlpcoch, ak IG=1           		  *
C *************************************************************************

        SUBROUTINE DIAG(A,N,VV,IG)
 	PARAMETER(SK=50)
        DOUBLE PRECISION A(SK,SK),P(SK,SK),APT(SK,SK)
	DOUBLE PRECISION UH,NP,CT,MN,CS,SN,PT(SK,SK),PI
	DOUBLE PRECISION DP(SK)
	DOUBLE PRECISION VV(SK,SK),PM(SK,SK)
	INTEGER N,R,S,I,J,Q,IG
C *********************************
	IF (N.EQ.1) RETURN
C *********************************
	PI=2*DACOS(0.D0)
C -------------------------------------------------------------
	DO I=1,N
	   DO J=I,N
	      IF (I.EQ.J) THEN
		 DP(I)=0.D0
        	 VV(I,J)=1.D0
	      ELSE
	 	 VV(I,J)=0.D0
		 VV(J,I)=0.D0
	      ENDIF
	   ENDDO
	ENDDO	
c -------------------------------------------------------------
C --- Hladanie indexov maximalneho nediagonalneho prvku R,S ---    
C -------------------------------------------------------------
 1	NP=DABS(A(1,2))
	R=1
	S=2
	DO I=1,N-1
	   DO J=I+1,N
	      IF (NP.LT.DABS(A(I,J))) THEN
		 NP=DABS(A(I,J))
		 R=I
		 S=J
	      ENDIF
	   ENDDO
	ENDDO
C ------------------------------------------
	CT=2*A(R,S)
	MN=A(S,S)-A(R,R)
	IF (MN.EQ.0) THEN
	   IF (CT.GT.0) UH=PI*(0.25)
	   IF (CT.LT.0) UH=-PI*(0.25)
        ELSE 
	   UH=(0.5)*DATAN(CT/MN)
	ENDIF
C ----------------------------------------------------------------
C --- Vytvorenie oboch nasobiacich diagonalizacnych matic P,PT ---	
C ----------------------------------------------------------------
	DO I=1,N
	   DO J=I,N
	      IF (I.EQ.J) THEN 
		 P(I,J)=1
 		 PT(I,J)=1
	      ELSE
		 P(I,J)=0
		 P(J,I)=0
		 PT(I,J)=0
	         PT(J,I)=0
	      ENDIF
	   ENDDO
	ENDDO
	CS=DCOS(UH)
	SN=DSIN(UH)
	P(R,R)=CS
	P(R,S)=-SN
	P(S,S)=CS
	P(S,R)=SN
	PT(R,R)=CS
	PT(S,S)=CS
	PT(R,S)=SN
	PT(S,R)=-SN
C ************************************************************************
C --- Nasobenim A=P.A.PT sa eliminuju nediagonalne prvky A(R,S)=A(S,R) --- 
C         - Ukladanie diagonalizacnych matic PT do matice VV -
C ************************************************************************
	IF (IG.EQ.1) THEN
       	   CALL NASMAT(N,VV,PT,PM)
	   DO I=1,N
	      DO J=1,N
	         VV(I,J)=PM(I,J)
	      ENDDO
	   ENDDO
	ENDIF
	CALL NASMAT(N,A,PT,APT)
	CALL NASMAT(N,P,APT,A)

c ------- t#to poistku radgej zrug!m.... -------------
c	A(R,S)=0
c	A(S,R)=0

C ------------------------------------------
C --- Zistenie ukoncenia diagonalizacie  ---
C ------------------------------------------
	Q=0
	DO I=1,N
	      IF (A(I,I).EQ.DP(I))  Q=Q+1
              DP(I)=A(I,I)
	ENDDO
C ***********************************************************
C - Opakuj diagonalizovanie, ak po predchadzajucom nasobeni -
C - sa zmenili diagonalne prvky. 			    -
C -----------------------------------------------------------
	IF (Q.LT.N) GOTO 1
C *******************************************************************
C ***   Matica A je teraz zdiagonalizovana, jej vlastne hodnoty   ***
C ***   sa nachadzaju na jej hlavnej diagonale.                   ***
C *******************************************************************
        RETURN 
	END
C ------------------------------------------------------------
C  Procedura pre nasobenie matic N*N
C  VSTUPUJU MATICE A,B a ich rad N ; VYSTUPOM JE MATICA C=A.B
C ------------------------------------------------------------
	SUBROUTINE NASMAT(N,A,B,C)
	PARAMETER (SK=50)
        INTEGER N
	DOUBLE PRECISION A(SK,SK),B(SK,SK),C(SK,SK),P
	DO I=1,N
	   DO J=1,N
	      P=0
	      DO K=1,N
		 P=P+A(I,K)*B(K,J)
	      ENDDO
	      C(I,J)=P
	  ENDDO
	ENDDO
	RETURN
	END				   		

C ===============================================================
C = Napisal Miroslav Ilias dna 3.6 '95 v Prievidzi na svojom PC =
C ===============================================================		
