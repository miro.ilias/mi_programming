C *******************************************************************
	SUBROUTINE HDIAG(H,U,N,NBMX,IEGEN)
C -------------------------------------------------------------------
C - H...MATICA N*N, KTORA SA ZDIAGONALIZUJE		            -
C - U...MATICA N*N - OBSAHUJE V STLPCOCH VLASTNE VEKTORY,AK IEGEN=0 -
C - N...RAD MATIC H,U						    -
C - NBMX...MAXIMALNY RAD MATIC H,U, POUZITYCH V PROGRAME	    -
C -------------------------------------------------------------------		



C *******************************************************************
C     FORTRAN IV DIAGONALIZATION OF A REAL SYMMETRIC MATRIX BY THE
C     JACOBI METHOD.
C     MAY 19,1959,REVISED TO FORTRAN IV AUG 17,1966
C     CALLING SEQUENCE FOR DIAGONALIZATION
C            CALL HDIAG(H,U,X,IQ,N,NBMX,IEGEN,NR)
C            WHERE H IS THE ARRAY TO BE DIAGONALIZED.
C     N IS THE ORDER OF THE MATRIX, H.
C     IEGEN MUST BE SET UNEQUAL TO ZERO IF ONLY EIGENVALUES ARE
C            TO BE COMPUTED.
C     IEGEN MUST BE SET EQUAL TO ZERO IF EIGENVALUES AND EIGENVECTORS
C            ARE TO BE COMPUTED.
C     U IS THE UNITARY MATRIX USED FOR FORMATION OF THE EIGENVECTORS.
C     NR IS THE NUMBER OF ROTATIONS.
C     NBMX IS THE MAXIMUM ORDER OF THE MATRIX H  TO BE DIAGONALISED
C     X,IQ ARE ADDITIONAL ARRAYS BOTH ARE OF LEGNTH AT LEAST NBMX
C     THE SUBROUTINE OPERATES ONLY ON THE ELEMENTS OF H THAT ARE TO THE
C            RIGHT OF THE MAIN DIAGONAL.  THUS, ONLY A TRIANGULAR
C            SECTION NEED BE STORED IN THE ARRAY H.
C *********************************************************************
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION H(NBMX,NBMX),U(NBMX,NBMX),X(1),IQ(1)
C =====================================================================
C     SET INDICATOR FOR SHUT OFF RAP  IS APPROX 2**-46.               =
C     SPECIFICALLY FOR THE CDC6600 48 BIT MANTISSA                    =
C =====================================================================
      DATA RAPS/2.0D-13/,HDTES/1.0D38/
      XMAX=0.D0
      IF(IEGEN.NE.0) GO TO 15
C ---------------------------------------------------------------------
C ---               UTVORENIE JEDNOTKOVEJ MATICE U                  ---
C ---------------------------------------------------------------------      
      DO 10 I=1,N
         DO 10 J=1,N
            U(I,J)= 0.D0
            IF(I.EQ.J) U(I,J)=1.D0
 10    CONTINUE
C ---------------------------------------------------------------------
 15   NR=0
      IF(N.LE.1) RETURN
C =====================================================================
C     SCAN FOR LARGEST OFF DIAGONAL ELEMENT IN EACH ROW               =
C     X(I) CONTAINS LARGEST ELEMENT IN ITH ROW                        =
C     IQ(I) HOLDS SECOND SUBSCRIPT DEFINING POSITION OF ELEMENT       =
C =====================================================================
      NMI1=N-1
      DO 30 I=1,NMI1
         X(I)=0.D0
         IPL1=I+1
         DO 30 J=IPL1,N
            IF(X(I).GT.DABS(H(I,J))) GO TO 30
            X(I)=DABS(H(I,J))
            IQ(I)=J
 30   CONTINUE
C -----------------------------------------------------
C     SET UP ZERO CUT OFF (RAP) , INITIALIZE HDTEST.  -
C -----------------------------------------------------
      RAP=7.45058060D-9
      HDTEST=1.0D38
C ---------------------------------------------------------------------
C     FIND MAXIMUM OF X(I) S FOR PIVOT ELEMENT AND TEST FOR END OF PROB
C ---------------------------------------------------------------------
 40   DO  70  I=1,NMI1
          IF(I.LE.1) GO TO 60
          IF(XMAX.GE.X(I)) GO TO 70
 60       XMAX=X(I)
          IPIV=I
          JPIV=IQ(I)
 70   CONTINUE
C ---------------------------------------------------------------------
C     IS MAX. X(I) EQUAL TO ZERO, IF LESS THAN HDTEST, REVISE HDTEST
C ---------------------------------------------------------------------
      IF(XMAX.LE.0.0D0) RETURN
      IF(HDTEST.LE.0.0D0) GO TO 90
      IF(XMAX.GT.HDTEST) GO TO 148
 90   HDIMIN = DABS ( H(1,1) )
      DO 110  I= 2,N
         IF(HDIMIN.GT.DABS(H(I,I))) HDIMIN=DABS(H(I,I))
 110  CONTINUE
      HDTEST=HDIMIN*RAP
C ---------------------------------------------------------------------
C     RETURN IF MAX.H(I,J)LESS THAN RAP*DABS(MIN(H(K,K)))
C ---------------------------------------------------------------------
      IF(HDTEST.GE.XMAX) RETURN
 148  NR = NR+1
C     COMPUTE TANGENT, SINE AND COSINE,H(I,I),H(J,J)
 150  HII=H(IPIV,IPIV)
      HJJ=H(JPIV,JPIV)
      TANG=DSIGN(2.0D0,(HII-HJJ))*H(IPIV,JPIV)/(DABS (
     1HII         -HJJ         )+DSQRT((HII-HJJ)*(HII-HJJ)
     2+4.0D0*H(IPIV,JPIV)*H(IPIV,JPIV)))
      COSINE=1.D0/DSQRT(1.D0+TANG*TANG)
      SINE=TANG*COSINE
      H(IPIV,IPIV)=COSINE*COSINE*(HII+TANG*(2.D0*H(IPIV,JPIV)+TANG*HJJ
     1))
      H(JPIV,JPIV)=COSINE*COSINE*(HJJ-TANG*(2.D0*H(IPIV,JPIV)-TANG*HII
     1))
      H(IPIV,JPIV)=0.D0
C ---------------------------------------------------------------------
C      PSEUDO RANK THE EIGENVALUES
C      ADJUST SINE AND COS FOR COMPUTATION OF H(IK) AND U(IK)
C ---------------------------------------------------------------------
      IF(H(IPIV,IPIV).GE.H(JPIV,JPIV)) GO TO 153
      HTEMP=H(IPIV,IPIV)
      H(IPIV,IPIV)=H(JPIV,JPIV)
      H(JPIV,JPIV)=HTEMP
C ---------------------------------------------------------------------
C       RECOMPUTE SINE AND COS
C ---------------------------------------------------------------------
      HTEMP=DSIGN(1.0D0,-SINE)*COSINE
      COSINE=DABS(SINE)
      SINE=HTEMP
 153  CONTINUE
C ---------------------------------------------------------------------
C     THE I OR J ROW.
C ---------------------------------------------------------------------
      DO 350 I=1,NMI1
         IF(I.EQ.IPIV.OR.I.EQ.JPIV) GO TO 350
         IF(IQ(I).NE.IPIV.AND.IQ(I).NE.JPIV) GO TO 350
         K=IQ(I)
         HTEMP=H(I,K)
         H(I,K)=0.D0
         IPL1=I+1
         X(I)=0.D0
C     --------------------------------------
C     SEARCH IN DEPLETED ROW FOR NEW MAXIMUM
C     --------------------------------------
         DO 320 J=IPL1,N
            IF (X(I).GT.DABS(H(I,J))) GO TO 320
            X(I)=DABS(H(I,J))
            IQ(I)=J
 320     CONTINUE
         H(I,K)=HTEMP
 350  CONTINUE
      X(IPIV)=0.D0
      X(JPIV)=0.D0
C ---------------------------------------
C     CHANGE THE OTHER ELEMENTS OF H
C ---------------------------------------
      DO 530 I=1,N
         IF(I-IPIV)370,530,420
 370     HTEMP = H(I,IPIV)
         H(I,IPIV) = COSINE*HTEMP + SINE*H(I,JPIV)
         IF(X(I).GE.DABS(H(I,IPIV))) GO TO 390
         X(I)=DABS(H(I,IPIV))
         IQ(I)=IPIV
 390     H(I,JPIV) = -SINE*HTEMP + COSINE*H(I,JPIV)
         IF(X(I).GE.DABS(H(I,JPIV))) GO TO 530
         X(I)=DABS(H(I,JPIV))
         IQ(I)=JPIV
         GO TO 530
 420     IF(I-JPIV)430,530,480
 430     HTEMP = H(IPIV,I)
         H(IPIV,I) = COSINE*HTEMP + SINE*H(I,JPIV)
         IF(X(IPIV).GE.DABS(H(IPIV,I))) GO TO 450
         X(IPIV)=DABS(H(IPIV,I))
         IQ(IPIV)=I
 450     H(I,JPIV) = -SINE*HTEMP + COSINE*H(I,JPIV)
         IF(X(I).GE.DABS(H(I,JPIV))) GO TO 530
         X(I)=DABS(H(I,JPIV))
         IQ(I)=JPIV
         GO TO 530
 480     HTEMP = H(IPIV,I)
         H(IPIV,I) = COSINE*HTEMP + SINE*H(JPIV,I)
         IF(X(IPIV).GE.DABS(H(IPIV,I))) GO TO 500
         X(IPIV)=DABS(H(IPIV,I))
         IQ(IPIV)=I
 500     H(JPIV,I) = -SINE*HTEMP + COSINE*H(JPIV,I)
         IF(X(JPIV).GT.DABS(H(JPIV,I))) GO TO 530
         X(JPIV)=DABS(H(JPIV,I))
         IQ(JPIV)=I
 530  CONTINUE
C ---------------------------------------------------------------------
C     TEST FOR COMPUTATION OF EIGENVECTORS
C ---------------------------------------------------------------------
      IF(IEGEN.NE.0)  GO TO 40
      DO 550 I=1,N
         HTEMP=U(I,IPIV)
         U(I,IPIV)=COSINE*HTEMP+SINE*U(I,JPIV)
 550     U(I,JPIV)=-SINE*HTEMP+COSINE*U(I,JPIV)
      GO TO 40
      END
C *********************************************************************
C *********************************************************************
C *********************************************************************                                            
