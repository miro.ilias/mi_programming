C *********************************************************************
C *  Tento program sl��i k n�jdeniu minima potenci�lovej
C * krivky. Dan�mi bodmi ...
C ****************************************************************


	PROGRAM MINPOTKR
	PARAMETER(SK=100)
        DOUBLE PRECISION ENE(SK),DAB(SK),D0,D1,EMIN,A(SK,SK),RK(SK)
        DOUBLE PRECISION PP
        INTEGER IMIN,I,N
	write(*,*) "vstup poctu bodov potenc.krivky N "
	read(*,*) N
C ------------------------------------------------------------------
C -- Na��tanie bodov potenci�lovej krivky  ---
C -----------------------------------------------------------------
	DO I=1,N
           WRITE(*,*) "VSTUP DVOJICE BODOV d,E(D) - ",I
	   READ(*,*) DAB(I),ENE(I)
	ENDDO	

C ----------------------------------------------------------------------
C ----------     Nasleduje n�jdenie minima zo vstupuj�cich bodov -------
C ----------------------------------------------------------------------
        EMIN=ENE(1)
        DO I=1,N
           IF (ENE(I).LE.EMIN) THEN
              EMIN=ENE(I)
              D0=DAB(I)
           ENDIF
        ENDDO
C ----------------------------------------------------------------------
C ---       Prelo�enie polynomu N-1 stup�a dan�mi bodmi ---
C --      -       rie�enie s�stavy line�rnych rovn�c ---
C ------------------------------------------------------------
C ** Vytvorenie matice A(N,N) **
        DO I=1,N
           DO J=1,N
              A(I,J)=(DAB(I))**(J-1)
           ENDDO
        ENDDO

        CALL GEM(N,A,RK,ENE)

C -----------------------------------------------------------------
C --- Nasleduje kontrolovan� iterovanie = h�adanie optim�lnej
C --- medzijadrovej vzdialenosti                               ----
C ------------------------------------------------------------------

	D=D0
2	CONTINUE
C ===	OPAKUJ ===
C       = AK =
        PP=ENE2D(D,RK,N)
        IF (PP.NE.0) THEN
C         = TAK =
	   D=D1
C	   D=D-(ENE1D(N,D,RK)/ENE2D(D,RK,N))
C	  = INAK =
	ELSE   	    
	     IF (((RK(N).NE.0).AND.(E3IT(D,RK,N).LT.0).AND.(INT((N-2)/2)
     &	       .NE.((N-2)/2))).OR.((RK(N).NE.0).AND.(E3IT(D,RK,N).GT.0). 
     &	        AND.(INT((N-2)/2).EQ.((N-2)/2)))) THEN
		D=D1
		D=(E3IT(D,RK,N)/(RK(N)*(N-1)))**(N-2)
	     ENDIF
	ENDIF
C ===	POKIAL
	IF (DABS(D1-D).NE.0) THEN
	   GOTO 2
	ENDIF


C ---------------------------------------------------------------------
C -------- Iter�cia je ukon�en�, nasleduje kontrola v�sledku ----------
C ---------------------------------------------------------------------
	IF (ENERG(D,RK,N).GT.EMIN) THEN
	   WRITE(*,*) "ZLE JE !!!"
	   STOP
	ENDIF
C ---------------------------------------------------------------------
C -------- V�pis optimalizovanej medzijadrovej vzdialenosti -----------
C ---------------------------------------------------------------------
	WRITE(*,*) "OPTIMALIZOV.VZDIALENOST JE:",D
	WRITE(*,*) "ENERGIA JE ",ENERG(D,RK,N)
	STOP
	END
C *********************************************************************

C *********** FUNKCIE A PPRG *******************
	
C *********************************************************************

	FUNCTION ENERG(D,RK,N)
	PARAMETER(SK=100)
	REAL*8 RK(SK),D,S
	INTEGER N,I
	S=0
	DO I=1,N
	   S=S+(RK(I)*(D**(I-1)))
	ENDDO
	ENERG=S
	RETURN
	END
c ---------------------------------------------------------------------	
	FUNCTION ENE1D(N,D,RK)
	PARAMETER(SK=100)
	REAL*8 RK(SK),D,S
	INTEGER N,I
	S=0
	DO I=2,N
           S=S+(RK(I)*DFLOAT(I-1)*(D**(I-2)))
	ENDDO
	ENE1D=S
	RETURN
	END
c ---------------------------------------------------------------------
	FUNCTION ENE2D(D,RK,N)
	PARAMETER(SK=100)
	INTEGER N,I
	REAL*8 RK(SK),D,S
	S=0
	DO I=3,N
	   S=S+(RK(I)*DFLOAT((I-2)*(I-1))*(D**(I-3)))
	ENDDO
	ENE2D=S
	RETURN
	END
c ---------------------------------------------------------------------
	FUNCTION E3IT(D,RK,N)
	PARAMETER(SK=100)
	REAL*8 RK(SK),D,S
	INTEGER N,I
	S=0
	DO I=2,N-1
	   S=S+(RK(I)*DFLOAT(I-1)*(D**(I-2)))
	ENDDO
	E3IT=S
	RETURN
	END
	
C ****************************************************************
C *                                                              *
C *  Procedura pre vypocet korenov sustavy N linearnych rovnic   *
C * s N neznamymi Gaussovou eliminacnou metodou. Vstupuje rad N, *
C * matica A(N,N) a vektor b(N), vystupom je vektor korenov x(N).*
C *                A(N,N).x(N)=b(N)                              *
C *                                                              *
C ****************************************************************
      SUBROUTINE GEM(N,A,X,B)
      PARAMETER(SK=100)
      DOUBLE PRECISION A(SK,SK+1),B(SK),X(SK),S,P
      INTEGER N,R,I
C ----------- presun vektora b do n+1 stlpca matice A ----------------
      DO I=1,N
         A(I,N+1)=B(I)
      ENDDO
C ---------------------------------------------------------------------
      R=1
C ================== OPAKUJ ======================
 1      CONTINUE
C ================== POKIAL ======================
      IF (.NOT.(R.LE.N-1)) GOTO 2
C ================== ROB =========================
      IF (A(R,R).EQ.0) THEN
         I=R
 3             I=I+1
             IF ((I.LT.N).AND.(A(I,R).EQ.0)) GOTO 3
         IF (A(I,R).EQ.0) THEN
            R=R+1
            GOTO 1
         ENDIF
C          --- VYMENA RIADKOV, ABY A(R,R)<>0 ---
         DO J=R,N+1
            S=A(I,J)
            A(I,J)=A(R,J)
            A(R,J)=S
         ENDDO
      ENDIF
C --- UPRAVA A(R,R)=1 ---
      P=1/A(R,R)
      DO J=R,N+1
         A(R,J)=A(R,J)*P
      ENDDO
C --- UPRAVA PRVKOV A(I,R)<>0 NA A(I,R)=-1 A PRIPOCITANIE R-TEHO RIADKU ---
      DO I=R+1,N
         IF (A(I,R).NE.0) THEN
            P=-1/A(I,R)
            DO J=R,N+1
               A(I,J)=A(I,J)*P
                 A(I,J)=A(I,J)+A(R,J)
            ENDDO
         ENDIF
      ENDDO
C ----------------------------------------------------------
      R=R+1
      GOTO 1
C ================== *OPAKUJ =====================
 2       CONTINUE
C ----------------------------------------------------------
C ---    MATICA A(N,N+1) JE V TROJUHOLNIKOVOM TVARE      ---
C ----------------------------------------------------------
      S=A(N,N+1)
      I=0
      CALL RO(S,A,N,I,X)
      DO I=1,N-1
         P=0
         DO J=N,N-I+1,-1
            P=P+(A(N-I,J)*X(J))
         ENDDO
         S=A(N-I,N+1)-P
         CALL RO(S,A,N,I,X)
      ENDDO
      RETURN
      END
C ==============================================================
      SUBROUTINE RO(S,A,N,I,X)
      PARAMETER(SK=100)
      DOUBLE PRECISION A(SK,SK+1),X(SK),S
      INTEGER N,I
      IF ((S.EQ.0).AND.(A(N-I,N-I).EQ.0)) THEN
         WRITE(*,*) "EXISTUJE NEKONECNE VELA KORENOV !"
         STOP
      ENDIF
      IF ((S.NE.0).AND.(A(N-I,N-I).EQ.0)) THEN
         WRITE(*,*) "SUSTAVA NEMA RIESENIE !"
      ENDIF
      X(N-I)=S/A(N-I,N-I)
      S=0
      RETURN
      END


C *********************************************************************
C *** Napisal Miroslav Ilias dna 21.7.1995 v Prievidzi na svojom PC ***
C *********************************************************************
