C *********************************************************************
C *** Tento program sluzi na prepocet medzijadrovych vzdialenosti ***
C *** z Angstromov na Bohry. Vstupny subor, ktoreho prve tri riadky ***
C *** obsahuju hlavicku, 4.riadok pocet bodov potenc.krivky, a dalsie *
C *** riadky obsahuju R[Angstr.], E[a.u.], sa nazyva ANG.INP, a vy-   **
C *** stupny subor s N, R[Bohr], E[a.u.], sa vola BOHR.OUT          ***
C *********************************************************************
	program prepis
	parameter(ang=1.88972664)
	real*8 r,ene
	integer n
	character*72 r1,r2,r3
	open(3,file="ang.inp",access="sequential",status="unknown")
	open(4,file="bohr.out",access="sequential",status="unknown")
	read(3,"(A72,/,A72,/,A72)") r1,r2,r3
c	stop
	read(3,*) n
	write(4,"(A72,/,A72,/,A72)") r1,r2,r3
	write(4,*) "---  Vzdialenosti su v Bohroch..."
	write(4,"(I2)") n
	do i=1,n
	   read(3,*) r,ene
	   write(4,"(F8.6,4X,F21.15)") r*ang,-ene
	enddo
	stop
	end

	
