C ***********************************************************
C *** Procedura, ktora danymi bodmi prelozi exponencialu  ***
C *** v tvare f(x)=p1*exp(p2*x)+p3, kde [ x(i),y(i) ] su  ***
C *** vstupujuce body a p1,p2,p3 su zasa parametre, ktore **
C *** sa dopocitavaju metodou najmensich stvorcov.        ***
C ***
C *** Ak sa neda prelozit, potom bude IES = 1             ***
C ***********************************************************

	SUBROUTINE EXPREG(N,X,Y,P,IES)

	PARAMETER(RM=25)
	PARAMETER(Z=5)

	REAL*8 X(RM),Y(RM),P(3) , c(3)
	REAL*8 A(3,3) , InvA(3,3)
	REAL*8 RSS(Z) , MVP(Z,3)

	INTEGER*1 IES , ISINV3 , ISCRAM3 
	INTEGER N , ITKROK

C  --- Inicializacia premennych P(1),P(2),P(3) a prem. ITKROK ---

	P(1) = 1
	P(2) = 1
	P(3) = 1

	ITKROK = 0

			
C =====================================================================
C ==== Zacina sa iteracny cyklus hladania premennych P(1),P(2),P(3)====
C =====================================================================

C             ****************************************
C             ****           Opakuj               ****
C             ****************************************

 2	CONTINUE

	ITKROK  =  ITKROK  +  1

C             **************************************** 

C   -- Nachysta sa matica A pomocou bodov a parametrov exp.regresie -- 

	CALL NMA(A,P,X,Y,N)

C   -- Nachysta sa inverzna matica InvA k matici A --

	CALL InvMat3(A,InvA,ISINV3)

C   --  Nachysta sa vektor pre riesene sustavy lin.rovnic v ramci --
C   -- Newton-Rapsonovej metody                                   --

	c(1) = (A(1,1)*P(1)) + (A(1,2)*P(2)) + (A(1,3)*P(3))

	c(2) = (A(2,1)*P(1)) + (A(2,2)*P(2)) + (A(2,3)*P(3))

	c(3) = (A(3,1)*P(1)) + (A(3,2)*P(2)) + (A(3,3)*P(3))

	CALL Cramer3(A,c,x,ISCRAM3)	

C ---------------------------------------------------------------------





	RETURN
	END

C -------------------------------------------------------
C --- Teraz definujem funkcie F1 , F2 a F3 a EXPREGF  ---
C -------------------------------------------------------

	REAL*8 FUNCTION F1(N,X,Y,P)

	    INTEGER N

	    REAL*8 X(N), Y(N), P(3)
	    REAL*8 S
	    
	    S = 0

	    DO I = 1 , N

	       S = S + ( 2*P(1)*DEXP(2*P(2)*X(I)) ) +
     &                 ( 2*P(3)*DEXP(P(2)*X(I)) )   -
     &                 ( 2*Y(I)*DEXP(P(2)*X(I)) )

            ENDDO

	    F1 = S

	RETURN
        END

C ---------------------------------------------------

	REAL*8 FUNCTION F2(N,X,Y,P)
	
	    INTEGER N

	    REAL*8 X(N), Y(N), P(3)
	    REAL*8 S

	    S = 0

	    DO I = 1 , N

	       S = S + ( 2*P(1)*P(1)*X(I)*DEXP(2*P(2)*X(I)) ) +
     &                 ( 2*P(1)*P(3)*X(I)*DEXP(P(2)*X(I))   ) -
     &                 ( 2*P(1)*X(I)*Y(I)*DEXP(P(2)*X(I))  )

            ENDDO

	    F2 = S

        RETURN
        END

C --------------------------------------------------------------------

	REAL*8 FUNCTION F3(N,X,Y,P)
	
	    INTEGER N

	    REAL*8 X(N), Y(N), P(3)
	    REAL*8 S

	    S = 0

	    DO I = 1 , N

	       S = S + ( 2*P(1)*DEXP(P(2)*X(I)) ) +
     &                 ( 2*P(3) )               -
     &                 ( 2*Y(I) )

            ENDDO

	    F3 = S

        RETURN
        END

C ---------------------------------------------------------------------

	REAL*8 FUNCTION EXPREGF(P,XI)

	REAL*8 P(3) , XI 

	EXPREGF = (P(1)*DEXP(P(2)*XI)) + P(3)

	RETURN
	END

C**********************************************************************
C*  V tejto procedure sa vytvoria prvky matice A pomocou danych bodov *
C* a aktualnych paremetrov P(1) , P(2) , P(3)                         *
C**********************************************************************

	SUBROUTINE NMA(A,P,X,Y,N)

	INTEGER N

	REAL*8 A(3,3) , P(3) , X(N) , Y(N)

C-------------------------------------
C--- Teraz vytvorim prvky matice A ---
C-------------------------------------

	DO I=1,3
           DO J=1,3
              A(I,J)=0
	   ENDDO
	ENDDO

	DO I = 1 , N

	   A(1,1) = A(1,1) + ( 2 * DEXP(2*P(2)*X(I)) )
	 
           A(1,2) = A(1,2) + ( 4 * P(1) * X(I) * DEXP(2*P(2)*X(I)) ) + 
     &                       ( 2 * P(3) * X(I) * DEXP(P(2)*X(I)) )    -
     &                       ( 2 * Y(I)*X(I) * DEXP(P(2)*X(I)) )

	   A(1,3) = A(1,3) + ( 2 * DEXP(P(2)*X(I)) )

	   A(2,2) = A(2,2) +( 4*P(1)*P(1)*X(I)*X(I)*DEXP(2*P(2)*X(I)) )+
     &                      ( 2*P(1)*P(3)*X(I)*X(I)*DEXP(P(2)*X(I)) )  -
     &                      ( 2*P(1)*X(I)*X(I)*Y(I)*DEXP(P(2)*X(I)) )

	   A(2,3) = A(2,3) + ( 2*P(1)*X(I)*DEXP(P(2)*X(I)) )

	   A(3,3) = A(3,3) + 2

	ENDDO

	A(2,1) = A(1,2)
	A(3,1) = A(1,3)
	A(3,2) = A(2,3)
	
	RETURN
	END

***********************************************************************
***  Tato procedura sluzi k najdeniu inverznej matice 3 na 3 k      ***
*** vstupujucej matici A(3 na 3) , ak ona existuje.                 ***
***   Vstup: A....matica 3 na 3                                     ***
***   Vystup: X....inverzna matica k matici A, ak IS=0              ***
***           IS...parameter o tom, ci inverz.matica existuje(0/1)  ***
***********************************************************************

	SUBROUTINE InvMat3(A,X,IS)

	REAL*8 A(3,3) , X(3,3) , D0 

	INTEGER*1 IS 

	Call DetMat3(A,D0)

	    IS = 0

c	write(*,*) "=========DetMat3=",D0

	IF (D0 .eq. 0) THEN
	    IS = 1
	    RETURN
	ENDIF
C ------------------------------------------------------

	X(1,1)=((A(2,2)*A(3,3))-(A(3,2)*A(2,3)))/D0

	X(2,1)=-((A(2,1)*A(3,3))-(A(3,1)*A(2,3)))/D0

	X(3,1)=((A(2,1)*A(3,2))-(A(3,1)*A(2,2)))/D0

C -------------------------------------------------------

	X(1,2)=-((A(1,2)*A(3,3))-(A(3,2)*A(1,3)))/D0

	X(2,2)=((A(1,1)*A(3,3))-(A(3,1)*A(1,3)))/D0

	X(3,2)=-((A(1,1)*A(3,2))-(A(3,1)*A(1,2)))/D0

C --------------------------------------------------------

	X(1,3)=((A(1,2)*A(2,3))-(A(2,2)*A(1,3)))/D0

	X(2,3)=-((A(1,1)*A(2,3))-(A(2,1)*A(1,3)))/D0

	X(3,3)=((A(1,1)*A(2,2))-(A(2,1)*A(1,2)))/D0

C --------------------------------------------------------
	Return
	End

C =====================================================================
C == Napisal Miro Ilias dna 5.8.1997 na oslavu Pana a Spasitela J.K. ==
C =====================================================================

*******************************************************************
*** Procedura na vycislenie determinantu 3.stupna matice A(3,3) ***
***  Vstupuju: A.....matica 3 na 3                              ***
***  Vystup  : det3mat....determinant tejto matice              ***
******************************************************************* 

	SUBROUTINE DetMat3(A,detmat)

	REAL*8 A(3,3) , detmat

c	write(*,*) "Matica A v DetMat3:"
c	write(*,*) ((a(i,j),j=1,3),i=1,3)
c	write(*,*) "----",A(1,1)*A(1,2)

	detmat = A(1,1)*((A(2,2)*A(3,3))-(A(3,2)*A(2,3))) -
     &           A(2,1)*((A(1,2)*A(3,3))-(A(3,2)*A(1,3))) +
     &           A(3,1)*((A(1,2)*A(2,3))-(A(1,3)*A(2,2)))
	
c	write(*,*) "DetMat3 v procedure = ",detmat
	RETURN

	END

C ******************************************************************
C *** Napisal M.Ilias na oslavu nasho Pana a Spasitela J.K.      ***
C ******************************************************************
          

*********************************************************************
***   Tato procedura sluzi k rieseniu sustavy 3 linearnych rovnic ***
*** s troma neznamimi. Korene sa vyrataju Cramerovym pravudlom.   ***
***								  ***
***                          A.x=b 				  ***
***								  ***
*** Vstup: A....matica 3.3 , b....3-rozm.vektor koef.pravej strany***
***								  ***
*** Vystup: x....3-rozmer.vektor korenov ; 			  ***
***         IS..parameter o rieseniach tejto sustavy              ***
***    								  ***
***  IS=0........sustava ma prave jedno riesenie		  ***
***  IS=1........sustava ma nekonecne vela rieseni		  ***
***  IS=2........sustava nema riesenie				  ***
***								  ***
***      Vstupna matica A a vektor b ostanu nezmenene.            ***
***								  ***
*********************************************************************

	SUBROUTINE Cramer3(A,b,x,IS)

	REAL*8 A(3,3) , b(3) , x(3) , D0 , D(3)
	REAL*8  PomStl(3) 

	INTEGER*1 IS , Stl

	CALL DetMat3(A,D0)

	DO Stl = 1  ,  3
	  
	   CALL VymStlp3(A,b,PomStl,Stl)

	   CALL DetMat3(A,D(Stl))

	   CALL OpacVymStlp3(A,b,PomStl,Stl)

	ENDDO

C   ----------------------------------

	IS = 0

	IF ((D0.eq.0).and.((D(1).eq.0).or.(D(2).eq.0).or.(D(3).eq.0)))
     &    IS = 1

	IF ((D0.eq.0).and.((D(1).ne.0).or.(D(2).ne.0).or.(D(3).ne.0)))
     &    IS = 2

C   ----------------------------------
	IF ( IS .eq. 0 ) THEN

       	   Do I = 1 , 3
	  
	      x( I ) = D(I) / D0

	   EndDo

	ENDIF

	RETURN
	END

C ---------------------------------------------------------------------

	Subroutine VymStlp3(A,b,PomStl,Stl)

	Real*8 A(3,3) , b(3) , PomStl(3)
	Integer*1 Stl

	Do I = 1 , 3

	   PomStl( I )   =   A( I , Stl )
	   A( I , Stl )  =   b(I)

	EndDo
	   
	Return
	End

C --------------------------------------------------------------------- 

	Subroutine OpacVymStlp3(A,b,PomStl,Stl)

	Real*8 A(3,3) , b(3) , PomStl(3)
	Integer*1 Stl

	Do  I  =  1  ,  3

	    A( I , Stl ) = PomStl(I)

	EndDo

	Return
	End

C ====================================================================    
C ===  Napisal Miro Ilias dna 4.8.1997 na oslavu nasho Pana J.K.   ===
C ====================================================================

C===================================================================
C==== Procedura na vypocet rezidualneho suctu stvorcov regresie ====
C==== a na ulozenie tohto cisla do zasobnika RSS(Z)             ====
C===================================================================

	SUBROUTINE URSS(RSS,X,Y,P,N,Z)

	INTEGER N , Z
	REAL*8 X(N) , Y(N) , P(3) , RSS(Z) , S

	S = 0

	DO I = 1 , N

	   S = S + (Y(I) - EXPREGF(P,X(I)))**2

	ENDDO

	DO I=1,Z-1
	   RSS(I)=RSS(I+1)
	ENDDO
	RSS(Z)=S

	RETURN
	END

C=======================================================================
C== Procedura, ktora naplni maticu vektorov parametrov P(1),P(2),P(3) ==
C=======================================================================

	SUBROUTINE NMVP(MVP,Z,P)

	INTEGER Z

	REAL*8 MVP(Z,3) , P(3)

	DO I = 1 , Z-1
	   DO J=1,3
	      MVP(I,J) = MVP(I+1,J)
	   ENDDO
	ENDDO

	DO J=1,3
	   MVP(Z,J) = P(J)
	ENDDO

	RETURN
	END





