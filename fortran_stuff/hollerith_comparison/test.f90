 program test_hollerith

 integer :: i,j

! i=4habcd
 i=transfer("abcd",i)
!j=4hefgh
 j=transfer("efgh",j)

 if (i.eq.j) then
   print *,'i=j!'
 else
   print *,'i<>j!'
 endif

 end program test_hollerith
