      program test_hollerith

      integer i,j,k,i1,i2
      integer ilet(6)
      data ilet /1hc,1hc,1hh,1hg,1hp,1hv/

!      i=4habcd
       i=transfer("abcd",i)
!      j=4hefgh
       j=transfer("efgh",j)
!      k=4habcd
       k=transfer("abcd",k)

       if (i.eq.j) then
          print *,'i=j!'
       else
          print *,'i<>j!'
       endif

       if (i.eq.k) then
         print *,'i=k!'
         write(*,*) 'i,k:',i,k
       else
         print *,'i<>k!'
       endif


!      i1=4hs
       i1=transfer("s",i1)
!      i2=4hs
       i2=transfer("s",i2)
!      if (i.eq.4habcd) then
       if (i1.eq.i2) then
        ! print *,'i.eq.4habcd'
         print *,'i1.eq.i2'
         write(*,'(a,a1)') '4hs=',i1
         write(*,*) 'size(4hs)=',sizeof(i1)
       endif

       stop 0
       end 
