
 program check_some_sys_routines

  real :: dtim, dtime, tarray(2)
  integer :: fseek, ierrno, UOUT=6

!  external :: dtime
!  external :: flush
!  external :: abort

   print *,'hello, going to check abort, flush, fseek, dtime... '
  call flush(6)

  print *,' try fseek stuff'

  ierrno = fseek(UOUT,0,2)

  call abort

 end program check_some_sys_routines
