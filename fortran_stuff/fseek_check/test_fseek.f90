 program test_fseek

 integer :: UOUT=8, ierrno,i

 open (UOUT,file="vypis.txt",access="sequential",status="unknown")

 do i=1,10
   write(UOUT,*) i,'....',i+10
 enddo

 call fseek(UOUT,0,2,ierrno)

 ! i=fseek(UOUT,0,2,ierrno)

 write(UOUT,*) 'na konci suboru'

 close(UOUT,status="keep")

 print *,"..subor vypis.txt s pripisanym textom na koncije hotovy"

 end program test_fseek

