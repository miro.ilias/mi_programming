 program test_alloc


 integer, allocatable :: z(:)
 integer i

  print *,'...'

 allocate(z(3))
 
 do i=1, 3
   z(i) = i;   print *," z(",i,")=",z(i)
 enddo

 deallocate(z)

 end program test_alloc
