
 module codata_1998
 ! real*8,parameter :: x=1.020d0
  real*8,parameter :: x_1998=1.020d0
 end module codata_1998

 module codata_2007
 !private
 ! real*8,parameter :: x=1.020d0
  real*8,parameter :: x_2007=1.020d0
 end module codata_2007

 module codata
  use codata_1998
  use codata_2007
  real*8, parameter :: x1=x_1998
  real*8, parameter :: x2
  !real*8, parameter :: x1=x_2007

  real*8, private :: x

 ! real*8, save :: x

 contains

  subroutine set_codata
  use codata_1998
    x=x_1998
    x2=x_1998
  end subroutine set_codata

  function get_x() result(xx)
     real*8 :: xx
      xx=x
   end function get_x

 subroutine print_codata
 end subroutine print_codata

 end module codata

 program codata_test
  use codata
    
    call set_codata
    call print_codata


 end program codata_test


