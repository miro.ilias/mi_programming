
/**
 * Trieda modeluje primitivny automat na predaj
 * cestovnych listkov MHD.
 * Model predpoklada, ze kupujuci vlozi presnu
 * ciastku podla ceny listka.
 * Cena listka je urcena parametrom konstruktora.
 */
public class AutomatMHD
{
    private int aCenaListka;
    // suma vlozenych minci pred tlacou listka
    private int aVlozenaCiastka;
    // celkova suma penazi za vsetky listky
    private int aTrzba;

    /**
     * Konstruktor vytvori automat, ktory bude
     * tlacit cestovne listky pevnej ceny.
     * Cena je urcena parametrom paCenaListka.
     * Pozor - cena listka musi byt kladne cele
     * cislo a tato podmienka sa nekontroluje.
     */
    public AutomatMHD(int paCenaListka)
    {
        aCenaListka = paCenaListka;
        aVlozenaCiastka = 0;
        aTrzba = 0;
    }

    /**
     * Vrati hodnotu ceny listka
     */
    public int dajCenuListka()
    {
        return aCenaListka;
    }
    
    /**
     * Vrati doteraz vlozenu ciastku
     */
    public int dajVlozenuCiastku()
    {
        return aVlozenaCiastka;
    }
    
    /**
     * Prijme mincu danej hodnoty od kupujuceho
     */
    public void vlozMincu(int paHodnotaMince)
    {
        aVlozenaCiastka = aVlozenaCiastka + paHodnotaMince;
    }
    
    /**
     * Vytlaci cestovny listok,
     * pripocita vlozenu ciastku k trzbe a
     * vynuluje vlozenu ciastku
     */
    public void tlacListok()
    {
        // tlac listka do okna konzoly
        System.out.println("*************************");
        System.out.println("* Skolska linka FRI");
        System.out.println("* Cestovny listok");
        System.out.print("* cena ");
        System.out.print(aCenaListka);
        System.out.println(" centov");
        System.out.println("*************************");
        System.out.println();
        
        // pripocitaj vlozenu ciastku k trzbe
        aTrzba = aTrzba + aVlozenaCiastka;
        // nuluj vlozenu ciastku
        aVlozenaCiastka = 0;
    }
}
