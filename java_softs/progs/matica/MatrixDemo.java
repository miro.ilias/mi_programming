/*  
  Demonstracny program pre pracu s maticami  
   
 */ 

public class MatrixDemo
{

 // funkcia na vypis matice na obrazovku 
   public static void printMatrix( int [ ][ ] m )
    {
       for( int i = 0; i < m.length; i++ )
        {
           if( m[ i ] == null ) // osetri zvlast nulu
             System.out.println( "(prazdne)" );
           else // nenulovy prvok
            {
             for( int j = 0; j < m[i].length; j++ )
                System.out.print( m[ i ][ j ] + " " );
                //if ( m[ i ] > 0 ) 
                //   System.out.print(" kladne cislo");
                System.out.println( );
            }
        }
    }

   public static void printVector( int [ ] m )
    {
       for( int i = 0; i < m.length; i++ )
        {
         System.out.print( m[ i ] + " " );
        }
    }
   
   
  //   Hlavny program 
    public static void main( String [ ] args )
    {
        int [ ][ ] a = { { 1, 2 }, { 3, 4 }, { 5, 6 } };
        int [ ][ ] b = { { 1, 2 }, null, { 5, 6 } };
        int [ ][ ] c = { { 1, 2 }, { 3, 4, 5 }, { 6 } };
        int [ ][ ] d = { { 2,3,4 }, { 1,3,4 }, { 6,12,34 } };
        int []e={1,2,3,4};
        System.out.println( "Matica a: " ); printMatrix(a);
        System.out.println( "Matica b: " ); printMatrix(b);
       // System.out.println( "c: " ); printMatrix( c );
       // System.out.println( "d: " ); printMatrix( d );
        System.out.println( "Vektor e: " ); printVector(e); System.out.println( );
    }
}
