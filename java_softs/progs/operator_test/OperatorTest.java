/*

*/

public class OperatorTest
{
    // Program to illustrate basic operators
    // The output is as follows:
    // 12 8 6
    // 6 8 6
    // 6 8 14
    // 22 8 14
    // 24 10 33
    public static void main( String [ ] args )
    {
        System.out.println("\n==== Test operatorov ===== \n");
        
        int a = 12, b = 8, c = 6;

        System.out.println( "a=" + a + " b=" + b + " c=" +  c );
        a = c;
        System.out.println(" -- Po prikaze a=c :"); 
        System.out.println( "a=" +a + " b=" + b + " c=" +  c );
        c += b;
        System.out.println(" -- Po prikaze c += b :"); 
        System.out.println( "a=" + a + " b=" + b + " c=" +  c );
        a = b + c;
        System.out.println(" -- Po prikaze a = b + c :"); 
        System.out.println( " a="+ a + " b=" + b + " c=" +  c );
        a++;
        ++b;
        c = a++ + ++b;
        System.out.println(" -- Po prikazoch a++; ++b; c = a++ + ++b :"); 
        System.out.println(">>>> a=" + a + " b=" + b + " c=" +  c );
    }
}
