#include "HelloWorld.h"
#include <iostream>

using namespace std;

//void Java_HelloWorld_displayMessage(JNIEnv *env, jobject obj);

JNIEXPORT void JNICALL
Java_HelloWorld_displayMessage(JNIEnv *env, jobject obj)
{
cout << "------------" << endl;
cout << "hello world!" << endl;
cout << "------------" << endl;
}

