class HelloWorld
{
public native void displayMessage();
static
 {
  // nacitaj kniznicu
  System.loadLibrary("HelloWorldImp");
  }
 public static void main(String[] args)
  {
  // deklaruj objekt triedy
  HelloWorld hello = new HelloWorld();

  //  volaj metodu z C++ zdroja ...
  hello.displayMessage();

  }

}
