
import javax.swing.JApplet;
import java.awt.Graphics;

public class HelloWorld extends JApplet {
   public static final long serialVersionUID = 1L; // version of class

    public void paint(Graphics g) {
    	g.drawRect(0, 0, 
		   getSize().width - 1,
   		   getSize().height - 1);
        g.drawString("Hello world!", 5, 15);
    }
 }

