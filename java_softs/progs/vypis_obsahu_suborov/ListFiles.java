import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

// ......             
public class ListFiles
{
  public static void main( String [ ] args )
    {
     if( args.length == 0 )
        System.out.println( "Ziadne (textove) subory nespecifikovane !" );

   // vola funkciu na vypis obsahu suborov....
      for( int i = 0; i < args.length; i++ )
            listFile(i,  args[ i ] );
    }

    public static void listFile(int i, String fileName )
    {
        FileReader theFile;
        BufferedReader fileIn = null;
        String oneLine;

        System.out.println( "\n VYPISUJEM OBSAH " + i +
                             ". SUBORU: " + fileName );
        System.out.println("===========================================");
        try
        {
            theFile = new FileReader( fileName );
            fileIn  = new BufferedReader( theFile );
       // vypisuje riadok po riadku az pokial nie je koniec suboru...
            while( ( oneLine = fileIn.readLine( ) ) != null )
                System.out.println( oneLine );
        }
        catch( IOException e )
          {  System.out.println( e ); }

        finally
        {
            // Close the stream
            try
            {
                if(fileIn != null )
                    fileIn.close( );
            }
            catch( IOException e )
              { }
        }
    }
}
