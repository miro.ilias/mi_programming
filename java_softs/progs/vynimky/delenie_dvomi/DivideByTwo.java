
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class DivideByTwo
{
    public static void main( String [ ] args )
    {
        BufferedReader in = new BufferedReader( new
                             InputStreamReader( System.in ) );
        int x;
        String oneLine;

        System.out.println( " Vstup integer cisla " );
        try
        {
            oneLine = in.readLine( );
            x = Integer.parseInt( oneLine );
            System.out.println( "Polovica (integer) z  x je: " + ( x / 2 ) );
        }
        catch( IOException e )
          { System.out.println( e );
            System.out.println(" Vyskytla sa vynimka !");  }
        catch( NumberFormatException e )
          { System.out.println( e );
            System.out.println(" Vyskytla sa vynimka !");
           }
    }
}
