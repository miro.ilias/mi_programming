#include <stdio.h>
#include <stdlib.h>
/*#include <math.h>*/
#include <time.h>
#include <sys/times.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/unistd.h>
#include <mpi.h>
#include <omp.h>


/*
 * This example presents the implementation of the graph partitioning
 * known as the combinatorial optimization problem 'Max-Cut', which is
 * 'NP-complete'. The target is to partition the set of vertices of
 * an edge-weigted undirected graph into two disjoint sets S1 and S2
 * so, that the sum of the weights of all edges connecting S1 and S2
 * is maximal.
 * The edge-weigted undirected graph is represented by an symetric
 * adjacency matrix which represents an input data.
 * The result of the partitioning is formed as an 0/1 value vector,
 * where value 0 denotes a vertex belonging to the set S1 and
 * value 1 denotes a vertex belonging to the set S2.
 */
/*
 * Algorithm:   analytical - generates recursively all possible cut
 *              vectors and calculates its cut. The vector having the
 *              maximal cut value is chosen as the solution. 
 *              There can exist more than one solution with the same 
 *              cut value.
 * Input:       adjmatrix.dat
 * Output:      maxcut-mix.output
 */

int calculate_cut(int nt, int msize, int *adjmatrix, char *fnamout);
void convert2bin(unsigned long n, int pos, int *cutset);
void converttime(char *timestring, long int elapsed);
int prog_exit(int ecode, char *message);

#define BLEN 256
char buffer[BLEN];

int main(int argc, char **argv)
{
    int  msize, *adjmatrix;
    int  nt, nprocs, me, root = 0;
    char *fnamin;
    char *fnamout = "maxcut-mix.output";
    FILE *fpin = NULL;
    int  i, j, k;
    int  error = 0;
    time_t startprog, endprog;
    char *envval;
    //MPI_Comm comm;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &me);
    //comm = MPI_COMM_WORLD;

    buffer[BLEN-1] = '\0';
    gethostname(buffer, BLEN-1);
    fprintf(stdout, "%d] MAIN-MIX START hostname: %s, nprocs= %d\n", 
            me, buffer, nprocs);

    /* adjmatrix17.dat: msize=17  nn=65535 */
    /* adjmatrix32.dat: msize= 32  nn= 2147483647 */

    if (argc < 3) {
      sprintf(buffer, "Error: Input parameters missing!\nUsage: %s <number_of_threads> <input_file> [<output_file>]", argv[0]);
      prog_exit(1, buffer);
    }
    // Set number of threads
    //nt = atoi(argv[1]);

    // Check the EnvVar OMP_NUM_THREADS
    /*
    envval = getenv("OMP_NUM_THREADS");
    if (envval != NULL)
      fprintf(stdout, "%d] OMP_NUM_THREADS=%s\n", me, envval);
    else
      fprintf(stdout, "%d] OMP_NUM_THREADS= \n", me);
    */
    // Number of threads set by OMP_NUM_THREADS
#pragma omp parallel
{   nt = omp_get_num_threads(); }
    fprintf(stdout, "%d] Number of threads= %d\n", me, nt);

    fnamin = argv[2];
    if (argc > 3)
      fnamout = argv[3];

    time(&startprog);

    if (me == root) {
      fpin = fopen(fnamin, "r");
      if (fpin == NULL) {
	sprintf(buffer, "Error: file %s can not be opened.", fnamin);
        prog_exit(1, buffer);
      }
      /* Read input size */
      fscanf(fpin, "%d", &msize);
    }
    MPI_Bcast(&msize, 1, MPI_INT, root, MPI_COMM_WORLD);
    adjmatrix = (int *) malloc(sizeof(int) * msize*msize);
    if (adjmatrix == NULL) {
      fclose(fpin);
      prog_exit(1, "Error: Memory allocation (main).");
    }
    /* Read input data */
    if (me == root) {
      for (i=0; i<msize; i++) {
        k = i*msize;
        for (j=0; j<msize; j++) {
          fscanf(fpin, "%d", &adjmatrix[k+j]);
        }
      }
      fclose(fpin);
    }
    MPI_Bcast(adjmatrix, msize*msize, MPI_INT, root, MPI_COMM_WORLD);

    if (me == root) {
      fprintf(stdout, "Calculation MAXCUT begin ...\n");
      fprintf(stdout, "  Data matrix: %s\n", fnamin);
      fprintf(stdout, "  msize= %d,  nprocs= %d,  nthreads= %d\n", msize, nprocs, nt);
    }

    error = calculate_cut(nt, msize, adjmatrix, fnamout);
    free(adjmatrix);

    time(&endprog);

    if (me == root) {
      fprintf(stdout, "Calculation MAXCUT finished.\n");

      fprintf(stdout, "Program start: %s", ctime(&startprog));
      fprintf(stdout, "Program end:   %s", ctime(&endprog));
      converttime(buffer, (long int)(endprog-startprog));
      fprintf(stdout, "Program elapsed time: %s\n", buffer);
    }
    fprintf(stdout, "%d] MAIN-MIX END (nprocs=%d)\n", me, nprocs);
    fflush(stdout);

    MPI_Finalize();
    return error;
}

int calculate_cut(int nt, int msize, int *adjmatrix, char *fnamout)
{
    FILE *fpout;
    int nprocs, me, tme, root = 0;
    int *cutset;
    int cut, maxcut, *mcuta, *tmcuta;
    unsigned long n, nn, mn, *mna, *tmna;
    unsigned long bl, len, lb, ub;
    unsigned long tbl, *tbla, tlen;
    unsigned long *lower, *upper, *tlower, *tupper;
    int i, j, k, m, pos, fin;
    time_t startl, endl;
    double startt, endt;
    long int *elapst, *work;
    char hostname[BLEN];
    int error = 0;

    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &me);

    //omp_set_dynamic(0);
    //omp_set_num_threads(nt);

    mcuta =  (int *) malloc(sizeof(int) * 2*nprocs);
    mna   =  (unsigned long *) malloc(sizeof(unsigned long) * 2*nprocs);
    lower =  (unsigned long *) malloc(sizeof(unsigned long) * nprocs);
    upper =  (unsigned long *) malloc(sizeof(unsigned long) * nprocs);
    elapst = (long int *) malloc(sizeof(long int) * nprocs*(nt+1));
    work = (long int *) malloc(sizeof(long int) * nprocs*(nt+1));
    if ((mcuta == NULL) || (mna == NULL) ||
        (lower == NULL) || (upper == NULL) || (elapst == NULL) || 
        (work == NULL)) {
      prog_exit(1,"Error: Memory allocation (calculate_cut).");
    }
    tmcuta =  (int *) malloc(sizeof(int) * nt);
    tmna   =  (unsigned long *) malloc(sizeof(unsigned long) * nt);
    tlower =  (unsigned long *) malloc(sizeof(unsigned long) * nprocs*nt);
    tupper =  (unsigned long *) malloc(sizeof(unsigned long) * nprocs*nt);
    tbla =  (unsigned long *) malloc(sizeof(unsigned long) * nprocs);
    if ((tmcuta == NULL) || (tmna == NULL) || 
        (tlower == NULL) || (tupper == NULL) || (tbla == NULL)) {
      prog_exit(1,"Error: Memory allocation (calculate_cut).");
    }

    /* nn = 2^(msize-1) - 1 */
    nn = 1;
    for (i = 1; i < msize; i++) {
        nn = nn * 2;
    }
    nn = nn - 1;

    /* Work distribution between MPI processes */
    len = nn+1;
    // bl = ceiling(dble(len) / dble(nprocs))
    bl = (len / nprocs) + ((len % nprocs) ? 1 : 0);
    /*
    lb = me * bl;
    ub = (me+1)*bl-1;
    if (ub > nn) ub = nn;
    */
    for (i=0; i<nprocs; i++) {
      lower[i] = i*bl;
      upper[i] = (i+1)*bl-1;
    }
    if (upper[nprocs-1] > nn) upper[nprocs-1] = nn;
    //fprintf(stdout, "  P%d] lb= %lu  ub= %lu\n", me, lower[me], upper[me]);

    /* Work distribution between OMP threads */
    /*
      lb = lower[me];
      ub = upper[me];
      tlen = ub-lb+1;
      tbl = (tlen / nt) + ((tlen % nt) ? 1 : 0);
      for (j=0; j<nt; j++) {
        tlower[me*nt+j] = j*tbl + lb;
        tupper[me*nt+j] = (j+1)*tbl-1 + lb;
      }
      if (tupper[me*nt+(nt-1)] > ub) tupper[me*nt+(nt-1)] = ub;
    */
    for (i=0; i<nprocs; i++) {
      lb = lower[i];
      ub = upper[i];
      tlen = ub-lb+1;
      tbl = (tlen / nt) + ((tlen % nt) ? 1 : 0);
      tbla[i] = tbl;
      for (j=0; j<nt; j++) {
        tlower[i*nt+j] = j*tbl + lb;
        tupper[i*nt+j] = (j+1)*tbl-1 + lb;
      }
      if (tupper[i*nt+(nt-1)] > ub) tupper[i*nt+(nt-1)] = ub;
    }

    if (me == root) {
      fprintf(stdout, "  Calculating local cut ...(nn= %lu)\n", nn);
      fprintf(stdout, "  Work distribution:\n");
      fprintf(stdout, "    MPI block size= %lu\n", bl);
      for (i=0; i<nprocs; i++) {
        fprintf(stdout, "    lower[%d]= %lu  upper[%d]= %lu\n", i, lower[i], i, upper[i]);
        fprintf(stdout, "      OMP block size= %lu\n", tbla[i]);
        for (j=0; j<nt; j++) {
          fprintf(stdout, "      tlower[%d]= %lu  tupper[%d]= %lu\n", 
                  j, tlower[i*nt+j], j, tupper[i*nt+j]);
        }
      }
    }
    fflush(stdout);

    for (i=0; i<2*nprocs; i++) {
        mna[i] = 0;
        mcuta[i] = 0;
    }
    for (i=0; i<nprocs*(nt+1); i++) {
        elapst[i] = 0l;
        work[i] = 0l;
    }
    for (i=0; i<nt; i++) {
        tmna[i] = 0;
        tmcuta[i] = 0;
    }
    pos = msize-1;

    time(&startl);

//Start of parallel region
#pragma omp parallel default(shared),firstprivate(pos,msize,me,nt),private(cutset,tme,lb,ub,n,i,j,k,m,cut,maxcut,mn,startt,endt,hostname)
{
    startt = omp_get_wtime();
    tme = omp_get_thread_num();
    m =  omp_get_num_threads();
    hostname[BLEN-1] = '\0';
    gethostname(hostname, BLEN-1);
    maxcut = 0;
    mn = 0;
    lb = tlower[me*nt+tme];
    ub = tupper[me*nt+tme];
    fprintf(stdout, "  P%d] T%d] hostname: %s, nthreads=%d,  lb=%lu, ub=%lu\n", me, tme, hostname, m, lb, ub);
    cutset = (int *) malloc(sizeof(int) * msize);
    if (cutset == NULL) {
      error++;
    }
    else {
      for (n=lb; n<=ub; n++) {
        for (i=0; i<msize; i++)
           cutset[i] = 0;
        convert2bin(n, pos, cutset);
        cut = 0;
        for (i=0; i<msize; i++) {
          k = i*msize;
          for (j=i+1; j<msize; j++) {
            m = k+j;
            if ((adjmatrix[m] != 0) && (cutset[i] != cutset[j])) {
              cut += adjmatrix[m];
            }
          }
        }
        if (maxcut < cut) {
          maxcut = cut;
          mn = n;
        }
      }
      tmcuta[tme] = maxcut;
      tmna[tme] = mn;
      free(cutset);
    }
    endt = omp_get_wtime();
    elapst[me*(nt+1)+tme] = (long int)(endt-startt);
}
// End of parallel region 
    if (error != 0)
      prog_exit(1,"Error: Memory allocation (calculate_cut).");

    // Max-Reduction on OMP-local cuts
    mn = tmna[0];
    maxcut = tmcuta[0];
    for (i=0; i<nt; i++) {
      if (maxcut < tmcuta[i]) {
        maxcut = tmcuta[i];
        mn = tmna[i];
      }
    }
    mcuta[me] = maxcut;
    mna[me] = mn;

    if (me == root) {
      fprintf(stdout, "  Max-Reduction on MPI-local cuts\n");
    }
    (void) MPI_Allreduce(mcuta, &mcuta[nprocs], nprocs, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
    (void) MPI_Allreduce(mna, &mna[nprocs], nprocs, MPI_UNSIGNED_LONG, MPI_SUM, MPI_COMM_WORLD);
    time(&endl);

    mn = mna[nprocs];
    maxcut = mcuta[nprocs];
    for (i=nprocs; i<2*nprocs; i++) {
      if (maxcut < mcuta[i]) {
        maxcut = mcuta[i];
        mn = mna[i];
      }
    }

    cutset = (int *) malloc(sizeof(int) * msize);
    for (i=0; i<msize; i++)
      cutset[i] = 0;
    convert2bin(mn, pos, cutset);

    if (me == root) {
      fpout = fopen(fnamout, "w");
      if (fpout == NULL) {
        sprintf(buffer, "Error: file %s can not be opened.\n", fnamout);
        prog_exit(1, buffer);
      }
      fprintf(fpout, "Calculation MAXCUT: msize= %d  nn= %lu\n", msize, nn);
      fprintf(fpout, "MaxCut Value= %d\n", maxcut);
      fprintf(fpout, "MaxCut Set:  ");
      for (i=0; i<msize-1; i++)
        fprintf(fpout, "%d, ", cutset[i]);
      fprintf(fpout, "%d\n", cutset[msize-1]);
      fflush(fpout);
      fin = fclose(fpout);
      if (fin != 0) {
        fprintf(stdout, "Error: file %s can not be closed.\n", fnamout);
        error++;
      }

      fprintf(stdout, "  MaxCut Value= %d\n", maxcut);
      fprintf(stdout, "  MaxCut Set:  ");
      for (i=0; i<msize-1; i++)
        fprintf(stdout, "%d, ", cutset[i]);
      fprintf(stdout, "%d\n", cutset[msize-1]);
    }
    MPI_Bcast(&error, 1, MPI_INT, root, MPI_COMM_WORLD);
/*
    converttime(buffer, (long int)(endl-startl));
    fprintf(stdout, "P%d] Loop elapsed time: %s\n", me, buffer);
*/
    elapst[me*(nt+1)+nt] = (long int)(endl-startl);
    k = nprocs*(nt+1);
    (void) MPI_Allreduce(elapst, work, nprocs*(nt+1), MPI_LONG, MPI_SUM, MPI_COMM_WORLD);
    if (me == root) {
      for (i=0; i<nprocs; i++) {
        converttime(buffer, work[i*(nt+1)+nt]);
        fprintf(stdout, "  Loop[%d] elapsed time: %s\n", i, buffer);
        for (j=0; j<nt; j++) {
          converttime(buffer, work[i*(nt+1)+j]);
          fprintf(stdout, "    Thread[%d] elapsed time: %s\n", j, buffer);
          
        }
      }
    }
    fflush(stdout);

    MPI_Barrier(MPI_COMM_WORLD);
    free(cutset);
    free(mcuta);
    free(mna);
    free(lower);
    free(upper);
    free(elapst);
    free(work);
    free(tmcuta);
    free(tmna);
    free(tlower);
    free(tupper);
    free(tbla);

    return error;
}

void convert2bin(unsigned long n, int pos, int *cutset)
{   
    if (n == 0)
        return;
    if (n == 1) {
        cutset[pos] = 1;
        return;
    }
    cutset[pos] = n % 2;
    convert2bin(n/2, pos-1, cutset);
}

void converttime(char *timestring, long int elapsed)
{
    int sec=0, min=0, hour=0;
    if (elapsed >= 60) {
      min = (int) (elapsed/60);
      sec = elapsed-(min*60);
      if (min >= 60) {
        hour = (int) (min/60);
        min = min-(hour*60);
      }
    }
    else {
      sec = (int) elapsed;
    }
    sprintf(timestring, "%0.2d:%0.2d:%0.2d", hour,min,sec);
}

// Function terminates/aborts all processes
int prog_exit(int ecode, char *message)
{
    int me;
    MPI_Comm_rank(MPI_COMM_WORLD, &me);
    if (message != NULL)
      fprintf(stdout, "P%d] %s\n", me, message);
    MPI_Abort(MPI_COMM_WORLD, ecode);
    exit(ecode);
}

