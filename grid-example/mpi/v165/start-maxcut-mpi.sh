#!/bin/sh
#
# Return values:
# 0       - Success
# n != 0  - Failure


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Print an info message
function infomsg () {
  echo "MaxCut [INFO]: "$@
}
#------------------------------------------------------------------
# Print an error message and exit
function errmsg () {
  echo "MaxCut [ERROR]: "$@
  exit 1
}
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

export JOBDIR=`pwd`
command=`basename $0`

echo "MaxCut [INFO]: Command $command Start ("`date`")"

numarg=$#
if [ $numarg -lt 3 ]; then
  echo "MaxCut [ERROR]: Incorrect number of arguments!"
  echo "Usage:  $command <exec_file> <input_file> <num_MPIprocs> [<num_MPIprocs_pernode>]"
  exit 1
fi

# Executable 
EXEC=$1
# Input
INPUT=$2
# Output
OUTPUT=${EXEC}.output
# StdOutput
STDOUT=${EXEC}.stdout
# Number of MPI processes
NPROCS=$3
# Number of MPI processes per node
NPN=""
[ $numarg -gt 3 ] && NPN=$4

# Checking
[ -e "$EXEC" ] || errmsg "File $EXEC not available!"
chmod +x ${EXEC}
#[ -e "${EXEC}.c" ] || errmsg "File ${EXEC}.c not available!"
[ -e "$INPUT" ] || errmsg "File $INPUT not available!"

# Run hook file
# Create the pre-run and post-run hooks
HOOKS="hooks.sh"
cat > ${HOOKS} << EOF
#!/bin/sh
#
# Return values:
# 0       - Success
# n != 0  - Failure

# This function will be called before the execution of MPI application
pre_run_hook () {
  echo "----- Pre-run hook Start -----"
  echo "Host: "\`hostname\`
  echo "Current Dir: \$PWD"
  ls -lF
  echo "----- Pre-run hook End -----"
  return 0
}

# This function will be called after the execution of MPI application
post_run_hook () {
  echo "----- Post-run hook Start -----"
  echo "Host: "\`hostname\`
  echo "Job Dir: $JOBDIR"
  echo "Parent Dir (OLDPWD): \$OLDPWD"
  echo "Current Dir: \$PWD"
  if [ "$JOBDIR" != "\$PWD" ]; then
    echo "Copy output files to $JOBDIR"
    [ -e "${OUTPUT}" ] && cp -f -p ${OUTPUT} $JOBDIR
    [ -e "${STDOUT}" ] && cp -f -p ${STDOUT} $JOBDIR
    # Cleaning
    rm -f ${EXEC} ${INPUT}
  fi
  echo "----- Post-run hook End -----"
  return 0
}
EOF

# Checking
[ -e "${HOOKS}" ] || errmsg "File ${HOOKS} not available"
chmod +x ${HOOKS}
PRE_HOOK=${HOOKS}
POST_HOOK=${HOOKS}

infomsg "Arguments:"
echo "  EXEC=      $EXEC"
echo "  INPUT=     $INPUT"
echo "  OUTPUT=    $OUTPUT"
echo "  STDOUT=    $STDOUT"
echo "  PRE_HOOK=  $PRE_HOOK"
echo "  POST_HOOK= $POST_HOOK"
echo "  NPROCS=    $NPROCS"
[ -n "$NPN" ] && echo "  NPN=       $NPN"

# Debug info
if [ -z "$PBS_NODEFILE" -o ! -f "$PBS_NODEFILE" ]; then
  infomsg "PBS_NODEFILE not available!"
else
  infomsg "PBS_NODEFILE:"
  cat $PBS_NODEFILE
fi

infomsg "Host: " `hostname`
infomsg "Current Dir: ${JOBDIR}"
ls -lF

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# CMD options / Environment Variables for mpi_start / I2G_MPI_START
#------------------------------------------------------------------
#-h		show help message and exit 
#-V		show mpi-start version 
#-t mpi_type	use mpi_type as MPI implementation (I2G_MPI_TYPE=mpi_type)
#-v		be verbose (I2G_MPI_START_VERBOSE=1)
#-vv		include debug information 
#		(I2G_MPI_START_VERBOSE=1, I2G_MPI_START_DEBUG=1)
#-vvv		include full trace 
#		(I2G_MPI_START_VERBOSE=1, I2G_MPI_START_DEBUG=1,
#		I2G_MPI_START_TRACE=1)
#-pre hook	use hook-script as pre-hook file (I2G_MPI_PRE_RUN_HOOK=hook)
#-post hook	use hook-script as post-hook file (I2G_MPI_POST_RUN_HOOK=hook)
#-pcmd cmd	use cmd as pre-command (I2G_MPI_PRECOMMAND=cmd)
#-npnode n	start n processes per node (I2G_MPI_PER_NODE=n)
#-pnode		start only 1 process per node (I2G_MPI_SINGLE_PROCESS=1)
#-np n		start exactly n processes (I2G_MPI_NP=n)
#-i file	use file as standard input file (I2G_MPI_APPLICATION_STDIN=file)
#-o file	use file as standard output file (I2G_MPI_APPLICATION_STDOUT=file)
#-e file	use file as standard error file (I2G_MPI_APPLICATION_STDERR=file)
#-x VAR[=VALUE]	export the environment variable VAR, optionally define value
#               (will not be seen by mpi-start!)
#-d VAR=VALUE	define mpi-start variable VAR with specified VALUE
#--		optional separator for application and arguments, after this, any 
#		arguments will be considered the application to run and its arguments
#------------------------------------------------------------------
#MPI_FLAVOUR=OPENMPI
#MPI_FLAVOUR_LOWER=`echo $MPI_FLAVOUR | tr '[:upper:]' '[:lower:]'`
#export I2G_MPI_TYPE=$MPI_FLAVOUR_LOWER
#export I2G_MPI_APPLICATION=
#export I2G_MPI_APPLICATION_ARGS=
#export I2G_MPI_TYPE=
#export I2G_MPI_APPLICATION_STDIN=
#export I2G_MPI_APPLICATION_STDOUT=
#export I2G_MPI_APPLICATION_STDERR=
#export I2G_MPI_PRE_RUN_HOOK=
#export I2G_MPI_POST_RUN_HOOK=
#export I2G_MPI_SINGLE_PROCESS=
#export I2G_MPI_PER_NODE=
#export I2G_MPI_NP=
#export I2G_MPI_START_VERBOSE=
#export I2G_MPI_START_DEBUG=
#export I2G_MPI_START_TRACE=
#export I2G_MPI_VERSION=
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# Debug info
if [ "x$MPI_OPENMPI_PATH" == "x" ]; then
  errmsg "MPI_OPENMPI_PATH not set!"
fi
infomsg "MPI_OPENMPI_PATH= $MPI_OPENMPI_PATH"
#
module purge
module load openmpi/1.6.5
# Default MPI: MPI_OPENMPI_PATH=/opt/openmpi/1.6.5

#if [ "x${I2G_MPI_START}" == "x" ]; then
#  errmsg "I2G_MPI_START not set!"
#fi
#infomsg "I2G_MPI_START: $I2G_MPI_START"
#
MPI_EXEC=`which mpi-start 2>/dev/null`
if [ "x${MPI_EXEC}" == "x" ]; then
  errmsg "mpi-start not found!"
fi
infomsg "MPI_EXEC: $MPI_EXEC"

echo ""
infomsg "MPI MaxCut start: "`date`
infomsg "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
excode=0

#export I2G_MPI_TYPE="openmpi"
#export I2G_MPI_APPLICATION="${EXEC}"
#export I2G_MPI_APPLICATION_ARGS="${INPUT} ${OUTPUT}"
#export I2G_MPI_APPLICATION_STDOUT="${STDOUT}"
#export I2G_MPI_APPLICATION_STDERR="${STDOUT}"
#export I2G_MPI_PRE_RUN_HOOK=${PRE_HOOK}
#export I2G_MPI_POST_RUN_HOOK=${POST_HOOK}
#export I2G_MPI_NP=$NPROCS
#[ "x$NPN" != "x" ] && export I2G_MPI_PER_NODE=$NPN
#export I2G_MPI_START_VERBOSE=1
#export I2G_MPI_START_DEBUG=1
##
## Invoke mpi-start
#$I2G_MPI_START
#excode=$?

ALLOC="-np $NPROCS"
[ "x$NPN" != "x" ] && ALLOC="-np $NPROCS -npnode $NPN"
OPTIONS="-t openmpi -vv -pre ${PRE_HOOK} -post ${POST_HOOK} ${ALLOC} -o ${STDOUT} -e ${STDOUT} --"
#
# Invoke mpi-start
infomsg "Execute:  ${MPI_EXEC} ${OPTIONS} ${EXEC} ${INPUT} ${OUTPUT}"
${MPI_EXEC} ${OPTIONS} ${EXEC} ${INPUT} ${OUTPUT}
excode=$?

infomsg "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
infomsg "MPI MaxCut end: "`date`
infomsg "ExitCode: $excode"
ls -lF

echo ""
[ $excode -ne 0 ] && errmsg "${EXEC} failed!"
echo "MaxCut [INFO]: Command $command Finish ("`date`")"

exit 0

