#!/bin/sh
#
# Return values:
# 0       - Success
# n != 0  - Failure


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Print an info message
function infomsg () {
  echo "MaxCut [INFO]: "$@
}
#------------------------------------------------------------------
# Print an error message and exit
function errmsg () {
  echo "MaxCut [ERROR]: "$@
  exit 1
}
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

export JOBDIR=`pwd`
command=`basename $0`

echo "MaxCut [INFO]: Command $command Start ("`date`")"

numarg=$#
if [ $numarg -lt 3 ]; then
  echo "MaxCut [ERROR]: Incorrect number of arguments!"
  echo "Usage:  $command <exec_file> <input_file> <num_MPIprocs> [<num_MPIprocs_pernode>]"
  exit 1
fi

# Executable 
EXEC=$1
# Input
INPUT=$2
# Output
OUTPUT=${EXEC}.output
# StdOutput
STDOUT=${EXEC}.stdout
# Number of MPI processes
NPROCS=$3
# Number of MPI processes per node
NPN=""
[ $numarg -gt 3 ] && NPN=$4

# Checking
[ -e "$EXEC" ] || errmsg "File $EXEC not available!"
chmod +x ${EXEC}
[ -e "$INPUT" ] || errmsg "File $INPUT not available!"

infomsg "Arguments:"
echo "  EXEC=      $EXEC"
echo "  INPUT=     $INPUT"
echo "  OUTPUT=    $OUTPUT"
echo "  STDOUT=    $STDOUT"
echo "  NPROCS=    $NPROCS"
[ -n "$NPN" ] && echo "  NPN=       $NPN"

# Debug info
if [ -z "$PBS_NODEFILE" -o ! -f "$PBS_NODEFILE" ]; then
  infomsg "PBS_NODEFILE not available!"
else
  infomsg "PBS_NODEFILE:"
  cat $PBS_NODEFILE
fi

infomsg "Host: " `hostname`
infomsg "Current Dir: ${JOBDIR}"
ls -lF

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# Debug info
if [ "x$MPI_OPENMPI_PATH" == "x" ]; then
  infomsg "MPI_OPENMPI_PATH not set!"
fi
infomsg "MPI_OPENMPI_PATH= $MPI_OPENMPI_PATH"
#
module purge
module load openmpi-gcc/1.10.0
#
# Running on 1 node without the MPI-Start
export MPI_HOME="/shared/software/openmpi/1.10.0-gcc"
infomsg "MPI_HOME= $MPI_HOME"
MPI_EXEC="${MPI_HOME}/bin/mpiexec"
if [ ! -e "${MPI_EXEC}" ]; then
  errmsg "${MPI_EXEC} not available!"
fi
infomsg "MPI_EXEC: $MPI_EXEC"
PREFIX="--prefix $MPI_HOME"

echo ""
infomsg "MPI MaxCut start: "`date`
infomsg "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
excode=0

# Direct invocation of mpiexec - only for running on 1 node
ALLOC="-np $NPROCS"
[ "x$NPN" != "x" ] && ALLOC="-np $NPROCS -npernode $NPN"
OPTIONS="--wdir ${JOBDIR} --output-filename ${STDOUT}"
infomsg "Execute:  ${MPI_EXEC} ${PREFIX} ${ALLOC} ${OPTIONS} ${EXEC} ${NTHRS} ${INPUT} ${OUTPUT}"
${MPI_EXEC} ${PREFIX} ${ALLOC} ${OPTIONS} ${EXEC} ${NTHRS} ${INPUT} ${OUTPUT}
excode=$?
cat ${STDOUT}.* > ${STDOUT}
rm ${STDOUT}.*

infomsg "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
infomsg "MPI MaxCut end: "`date`
infomsg "ExitCode: $excode"
ls -lF

echo ""
[ $excode -ne 0 ] && errmsg "${EXEC} failed!"
echo "MaxCut [INFO]: Command $command Finish ("`date`")"

exit 0

