/**

MIC MKL usage

*/

#include <iostream>
#include "omp.h"
#include <mkl.h>

int main()
{
  int i;
  int SIZE=1024;
  double *A = new double[SIZE];
  double *B = new double[SIZE];
  double *C = new double[SIZE];

  /* initialize with some data */
  for (i=0; i<SIZE; i++) {
    A[i] = i;
    B[i] = i*i;
  }

  /* Lab assigment : use MKL daxpy( n, a, x, incx, y, incy) to sum two vectors
   * on MIC A=A+B
   * 
   * y:=ax+y
   *
   * n      length of the vectors
   * a      scalar value
   * x      vector
   * incx   increment of elements of x (=1)
   * y      vector
   * incy   increment of elements of y (=1)
   *
   */

  
  /* insert offload pragma, */
  #pragma offload target(mic) ...
  {
      double a = 1.0;
      int incx = 1;
      int incy = 1;
      /* insert call to daxpy function
  }
}
