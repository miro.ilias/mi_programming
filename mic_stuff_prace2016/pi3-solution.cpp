#include <iostream>
#include "omp.h"

int main(int argc, char* argv[])
{
    const int niter = 10000000;
    double result=0 , result1 = 0,  result2 = 0;


    #pragma omp parallel 
    {
     #pragma omp single nowait
     {
      #pragma offload target(mic) inout (result1)
      {
       #pragma omp parallel for reduction(+:result1)
        for (int i = 0; i < niter; i=i+2) {
            const double t = (i + 0.5) / niter;
            result1 += 4.0 / (t * t + 1.0);
        }
      }
     }

      std::cout << "Tida " << omp_get_thread_num()  << '\n';


      #pragma omp for schedule(dynamic) reduction(+:result2)
        for (int i = 1; i < niter; i=i+2) {
            const double t = (i + 0.5) / niter;
            result2 += 4.0 / (t * t + 1.0);
        }
       std::cout << "Tid " << omp_get_thread_num()  << '\n';
    }


    result1 /= niter;
    result2 /= niter;

    std::cout << "Pi 1 ~ " << result1 << '\n';
    std::cout << "Pi 2 ~ " << result2 << '\n';
    std::cout << "Pi S ~ " << result1+result2 << '\n';

}

