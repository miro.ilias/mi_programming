#!/bin/bash

# Turn on offload info: 
export OFFLOAD_REPORT=2
# Compile: 
module load intel/2016.01
icpc pi1-solution.cpp -openmp -o pi1-solution
# Run: 
./pi1-solution
