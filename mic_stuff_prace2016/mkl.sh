#!/bin/bash

# Turn on offload info: 
export OFFLOAD_REPORT=2
# Compile: 
module load intel/2016.01
icpc mkl.cpp -openmp -mkl -o mkl
# Run: 
./mkl
