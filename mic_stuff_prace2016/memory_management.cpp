/**

MIC Memory management 

*/

#include <iostream>
#include "omp.h"

/*
__attribute__((target(mic))) add_mic(T *a, T *b, T *c, int size) {
  int i = 0;
  #pragma omp parallel for
    for (i = 0; i < size; i++)
      c[i] = a[i] + b[i]; }
*/

int main()
{
  int i;
  int SIZE=1024;
  double *A = new double[SIZE];
  double *B = new double[SIZE];
  double *C = new double[SIZE];

  /* initialize with some data */
  for (i=0; i<SIZE; i++) {
    A[i] = i;
    B[i] = i*i;
  }

  /* Lab assigment : write code that performs the following operations on MIC
     with given vectors A,B,C :

     1. C = A + B, transfer A, B to MIC and keep A,B on MIC
     2. C = A - B, reuse A, B and keep on MIC
     3. C = A * B, reuse A, B and return C to host */

  /* insert offload pragma, */
  #pragma offload target(mic) in(in1,in2)  inout(res)
  { // 1. Parallel loop from main function
    #pragma omp parallel for
    for (i=0; i<SIZE; i++)
      res[i] = in1[i] + in2[i];

    // 2. ... or parallel loop is called inside the function
    //add_mic(in1, in2, res, SIZE);
  }

  //Check the results with CPU implementation
  T res_cpu[SIZE];
  add_cpu(in1, in2, res_cpu, SIZE);
  compare(res, res_cpu, SIZE);
}
