/*

Calculate PI

Lab assigment : offload the calculation to coprocessor

*/

#include <iostream>

int main(int argc, char* argv[])
{
    const int niter = 100000;
    double result = 0;

    /* Lab assigment : insert offload pragma */
    #pragma offload target(mic:1)
    #pragma omp parallel for reduction(+:result)

    for (int i = 0; i < niter; ++i) {
        const double t = (i + 0.5) / niter;
        result += 4.0 / (t * t + 1.0);
    }
    result /= niter;
    std::cout << "Pi ~ " << result << '\n';
}

