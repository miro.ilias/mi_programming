#!/bin/bash

# Turn on offload info: 
export OFFLOAD_REPORT=2
export OFFLOAD_INIT=on_start
export MIC_ENV_PREFIX=MIC
export MIC_OMP_NUM_THREADS=120
# Compile: 
module load intel/2016.01
icpc pi2.cpp -openmp -o pi2
# Run: 
./pi2
