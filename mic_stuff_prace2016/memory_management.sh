#!/bin/bash

# Turn on offload info: 
export OFFLOAD_REPORT=2
# Compile: 
module load intel/2016.01
icpc memory_management_solution.cpp -openmp -o memory-management
# Run: 
./memory-management
