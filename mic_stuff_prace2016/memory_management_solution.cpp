/**

  MIC Memory management 

  Lab assigment : write code that performs the following operations on MIC
  with given vectors A,B,C :

  1. C = A + B, transfer A, B to MIC and keep A,B on MIC
  2. C = A - B, reuse A, B and keep on MIC
  3. C = A * B, reuse A, B and return C to host (C[i] = A[i]*B[i])

  You can use either offloaded inline code or offloaded functions

*/


#include <stdio.h>
#include <stdlib.h>

typedef int T;

#define SIZE 1000

T C_cpu[SIZE];

// MIC function to add two vectors
__attribute__((target(mic))) void add_mic(T *a, T *b, T *c, int size) {
    int i;
    /* Lab assigment : insert OMP parallel loop */
#pragma omp parallel for
    for (i = 0; i < size; i++)
        c[i] = a[i] + b[i];
}

// MIC function to subtract two vectors
__attribute__((target(mic))) void sub_mic(T *a, T *b, T *c, int size) {
    int i;
    /* Lab assigment : complete code */
#pragma omp parallel for
    for (i = 0; i < size; i++)
        c[i] = a[i] - b[i];
}

// MIC function to multiply two vectors
__attribute__((target(mic))) void  mult_mic(T *a, T *b, T *c, int size) {
    int i;
#pragma omp parallel for
    for (i = 0; i < size; i++)
        c[i] = a[i] * b[i];
}

// CPU function to add two vectors
void add_cpu (T *a, T *b, T *c, int size) {
    int i;
    for (i = 0; i < size; i++)
        c[i] = a[i] + b[i];
}

// CPU function to subctract two vectors
void sub_cpu (T *a, T *b, T *c, int size) {
    int i;
    for (i = 0; i < size; i++)
        c[i] = a[i] - b[i];
}

// CPU function to multiply two vectors
void mult_cpu (T *a, T *b, T *c, int size) {
    int i;
    for (i = 0; i < size; i++)
        c[i] = a[i] * b[i];
}

// CPU function to generate a vector of random numbers
void random_T (T *a, int size) {
    int i;
    for (i = 0; i < size; i++) {
        a[i] = rand() % 10000; // random number between 0 and 9999
    }
}

// CPU function to compare two vectors
int compare(T *a, T *b, T size ){
    int pass = 0;
    int i;
    for (i = 0; i < size; i++){
        if (a[i] != b[i]) {
            printf("Value mismatch at location %d, values %d and %d\n",i, a[i], b[i]);
            pass = 1;
        }
    }
    if (pass == 0) printf ("Test passed\n"); else printf ("Test Failed\n");
    return pass;
}


int main()
{
    int i;

    int A[SIZE];
    int B[SIZE];
    int C[SIZE];

    random_T(A, SIZE);
    random_T(B, SIZE);

    /* Lab assigment : add offload pragma, with proper in/inout attributes */
#pragma offload target(mic) in(A : alloc_if(1) free_if(0)) \ 
    in(B : alloc_if(1) free_if(0)) \
        inout(C : alloc_if(1) free_if(0))
        {

            /* Lab assigment : add OMP parallel loop or use function below */
#pragma omp parallel for
            for (i=0; i<SIZE; i++)
                C[i] = A[i] + B[i];

            // or parallel loop is called inside the function
            add_mic(A, B, C, SIZE);

        }


    //Check the results with CPU implementation

    add_cpu(A, B, C_cpu, SIZE);
    compare(C, C_cpu, SIZE);

    /* Lab assigment : add code for steps 2 and 3 */
#pragma offload target(mic) nocopy(A : alloc_if(0) free_if(0)) \ 
    nocopy(B : alloc_if(0) free_if(0))  out(C : alloc_if(0) free_if(0)) 
    {

        /* Lab assigment : add OMP parallel loop or use function below */
#pragma omp parallel for
        for (i=0; i<SIZE; i++)
            C[i] = A[i] - B[i];

        // or parallel loop is called inside the function
        sub_mic(A, B, C, SIZE); 

    }


    //Check the results with CPU implementation

    sub_cpu(A, B, C_cpu, SIZE);
    compare(C, C_cpu, SIZE);

#pragma offload target(mic) nocopy(A : alloc_if(0) free_if(1)) \ 
    nocopy(B: alloc_if(0) free_if(1)) \ 
        out(C : alloc_if(0) free_if(1))
        {

            /* Lab assigment : add OMP parallel loop or use function below */
#pragma omp parallel for
            for (i=0; i<SIZE; i++)
                C[i] = A[i] * B[i];

            // or parallel loop is called inside the function
            mult_mic(A, B, C, SIZE);

        }


    //Check the results with CPU implementation

    mult_cpu(A, B, C_cpu, SIZE);
    compare(C, C_cpu, SIZE);

}
