/*

Lab assigment : split the calculation between CPU and MIC and execute them concurrently

*/

#include <iostream>
#include "omp.h"

int main(int argc, char* argv[])
{
    const int niter = 10000000;
    double result=0 , result1 = 0,  result2 = 0;


    /* lab assigment : start a OMP parallel region */
    {
        /* lab assigment : start a single OMP thread, offload to MIC, do OMP parallel loop with reduction on MIC */

        for (int i = 0; i < niter; i=i+2) {
            const double t = (i + 0.5) / niter;
            result1 += 4.0 / (t * t + 1.0);
        }
        sleep(5); 


      std::cout << "Tida " << omp_get_thread_num()  << '\n';


      /* lab assigment : do OMP parallel loop  with reduction on CPU. With dynamic scheduling you can reuse the previous thread */
        for (int i = 1; i < niter; i=i+2) {
            const double t = (i + 0.5) / niter;
            result2 += 4.0 / (t * t + 1.0);
        }
       sleep(5);
       std::cout << "Tid " << omp_get_thread_num()  << '\n';
    }


    result1 /= niter;
    result2 /= niter;

    std::cout << "Pi 1 ~ " << result1 << '\n';
    std::cout << "Pi 2 ~ " << result2 << '\n';
    std::cout << "Pi S ~ " << result1+result2 << '\n';

}

