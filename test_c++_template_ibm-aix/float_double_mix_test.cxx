#include <cstdlib>
 #include <cmath>
 #include <iostream>

 // This assumes that type U can be casted to type U
 // normally not a brilliant idea...
 template <typename T, typename U> 
 T sum(T rhs, U lhs) 
 {
      return (rhs + lhs);
 }

 int main()
 {
       float a = 2.0;
       double b = -3.0;
       std::cout << "a = " << a << "; b = " << b << std::endl;
       std::cout << "a^2 = " << std::pow(a, 2) << std::endl;
       std::cout << "abs(a) = " << std::abs(a) << std::endl;
       std::cout << "abs(b) = " << std::abs(b) << std::endl;
       std::cout << "sum(a, b) = " << sum(a, b) << std::endl;
       std::cout << "sum(abs(a), abs(b)) = " << sum(std::abs(a), std::abs(b)) << std::endl;

       return EXIT_SUCCESS;
 }
