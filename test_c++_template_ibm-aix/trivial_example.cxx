#include <cstdlib>
#include <cmath>
#include <iostream>

template <typename T>
T sum(T rhs, T lhs)
{
    return (rhs + lhs);
}

int main()
{
    double a = 2.0;
    double b = -3.0;
    std::cout << "a = " << a << "; b = " << b << std::endl;
    std::cout << "a^2 = " << std::pow(a, 2) << std::endl;
    std::cout << "abs(b) = " << std::abs(b) << std::endl;
    std::cout << "sum(a, b) = " << sum(a, b) << std::endl;

    return EXIT_SUCCESS;
}
