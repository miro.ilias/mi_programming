===================================
Testing the lapack on IBM AIX Aurel
===================================

Get sources :
-------------
http://diracprogram.org/doc/master/programmers/diag.html
http://diracprogram.org/doc/master/programmers/diag.html#independent-demonstration-program

COMPILATION on Aurel:
---------------------

works:
~~~~~~
ilias@147.213.80.175:~/work/programming/miro_ilias_programming/lapack_DSYEVR_test/.xlf dsyerv_check.F90 eispack.F /gpfs/home/ilias/bin/lapack/lapack-3.5.0/build_xl_qextname/lib/liblapack.a  /usr/lib/libesslsmp.a

ilias@147.213.80.175:~/work/programming/miro_ilias_programming/lapack_DSYEVR_test/.xlf dsyerv_check.F90 eispack.F /usr/lib/libesslsmp.a /gpfs/home/ilias/bin/lapack/lapack-3.5.0/build_xl_qextname/lib/liblapack.a  

does not work:
~~~~~~~~~~~~~~
xlf  -qextname dsyerv_check.F90 eispack.F  /usr/lib/libesslsmp.a  /gpfs/home/ilias/bin/lapack/lapack-3.5.0/build_xl_qextname/lib/liblapack.a
ld: 0711-317 ERROR: Undefined symbol: .dsyevr_
- the problem is missing "-qextname" in liblapack.a symbols

ilias@147.213.80.175:~/work/programming/miro_ilias_programming/lapack_DSYEVR_test/.xlf dsyerv_check.F90 eispack.F /usr/lib/libesslsmp.a
ld: 0711-317 ERROR: Undefined symbol: .dsyevr
 the problem is missing routine in Aurel's essl library
