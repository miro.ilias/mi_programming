===================================
Testing the DSYERV lapack's routine
===================================
See

http://diracprogram.org/doc/master/programmers/diag.html

download newest lapack from http://www.netlib.org/lapack/lapack-3.5.0.tgz

or better, do git clone,  https://github.com/live-clones/lapack

LATEST commit: 918d784c6d91320152993a62f2b3189bca8413a8 (Wed Jun 17 14:44:54 2015)

COMPILE:
--------
milias@login.grid.umb.sk:~/Work/programming/miro_ilias_programming/lapack_DSYEVR_test/.gfortran -o dsyerv_gfortran_GNUlibs.x  dsyerv_check.F90  eispack.F  /home/milias/Work/programming/miro_ilias_programming/lapack_DSYEVR_test/lapack/build_1/lib/liblapack.a  /home/milias/Work/programming/miro_ilias_programming/lapack_DSYEVR_test/lapack/build_1/lib/libblas.a   

TEST:
-----

milias@login.grid.umb.sk:~/Work/programming/miro_ilias_programming/lapack_DSYEVR_test/.dsyerv_gfortran_GNUlibs.x
  Welcome to the testing program for DIRAC's diagonalization routines !
 read N=           9  NZ=           1
 matrix for diagonalization was read
 arrays for RS subroutine allocated
 Eispack's RS eigenvalues:
           1  -1.5000000000000062
           2  -1.5000000000000002
           3  -1.4999999999999982
           4  0.49999999999999772
           5  0.49999999999999856
           6  0.49999999999999994
           7  0.50000000000000033
           8  0.50000000000000122
           9   2.4999999999999969
  **** EISPACK RS ****
 U^{+}*A*U - eps ?= 0> norm/diag:0.1912D-15  norm/offdiag:0.1008D-15
     U^{+}*U - I ?= 0> norm/diag:0.2344D-15  norm/offdiag:0.7239D-16
     U*U^{+} - I ?= 0> norm/diag:0.9869D-16  norm/offdiag:0.9320D-16
 LAPACK DSYEVR eigenvalues:
           1  -1.5000000000000058
           2  -1.5000000000000004
           3  -1.4999999999999978
           4  0.49999999999999545
           5  0.49999999999999856
           6  0.49999999999999989
           7  0.49999999999999989
           8  0.50000000000000133
           9   2.4999999999999867
  **** LAPACK DSYEVR ****
 U^{+}*A*U - eps ?= 0> norm/diag:0.1616D-14  norm/offdiag:0.1419D-06
     U^{+}*U - I ?= 0> norm/diag:0.3454D-15  norm/offdiag:0.2838D-06
     U*U^{+} - I ?= 0> norm/diag:0.1047D-05  norm/offdiag:0.7950D-06
milias@login.grid.umb.sk:~/Work/programming/miro_ilias_programming/lapack_DSYEVR_test/.

