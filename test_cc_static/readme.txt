gcc --static test.c

/.ls
a.out*	test.c

/.ldd a.out 
	not a dynamic executable


PROBLEM:
========

Dear experts,

on the scientific linux server (Linux login 2.6.32-279.11.1.el6.x86_64 #1 SMP
Tue Oct 16 11:16:02 CDT 2012 x86_64 x86_64 x86_64 GNU/Linux; Scientific Linux
release 6.3 (Carbon)
I am trying to compile/link statically OpenMPI 1.6.4 with Intel compilers,
getting this error below.

Elementary test program

int main(int argc, char **argv)
{
        return 0;
}
gcc -static -Wl,--export-dynamic test.c

gives the same kind of error

/usr/bin/ld: dynamic STT_GNU_IFUNC symbol `strcmp' with pointer equality in
`/usr/lib/gcc/x86_64-redhat-linux/4.4.6/../../../../lib64/libc.a(strcmp.o)'
can not be used when making an executable; recompile with -fPIE and relink
with -pie
collect2: ld returned 1 exit status

icc -static -Wl,--export-dynamic test.c
ld: dynamic STT_GNU_IFUNC symbol `strcmp' with pointer equality in
`/usr/lib/gcc/x86_64-redhat-linux/4.4.6/../../../../lib64/libc.a(strcmp.o)'
can not be used when making an executable; recompile with -fPIE and relink
with -pie

Upon searching the net I found that this problem is related to package
'binutils', on my Debian/Ubuntu the solution is in installing the
"binutils-gold" package.
Please do you have clue about package name for this scientific linux ?

